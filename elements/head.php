

<!-- Google Analytics Tracking Code



================================================== -->

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-153089549-14"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());

  gtag('config', 'UA-153089549-14');

</script>



<!-- Basic Page Needs

================================================== -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="icon" type="image/png" href="<?php echo SITE_URL; ?>assets/images/favicon.png">

<!-- CSS



================================================== -->

<script src="https://use.fontawesome.com/1e1accfd94.js"></script> 

<link rel="stylesheet" href="<?= SITE_URL ?>assets/css/bootstrap.css">

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="stylesheet" href="<?= SITE_URL ?>assets/css/style.css">

<link rel="stylesheet" href="<?= SITE_URL ?>assets/css/colors/green.css">

<link rel="stylesheet" href="<?= SITE_URL ?>assets/css/croppie.css">

<link rel="stylesheet" href="<?= SITE_URL ?>assets/css/custom.css">

<!--<link rel="stylesheet" href="<?= SITE_URL ?>assets/bootstrap-modal/css/bootstrap.min.css">-->

