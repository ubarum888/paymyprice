<div id="header">

		<div class="container">
			<!-- Left Side Content -->

			<div class="left-side">

				<!-- Logo -->

				<div id="logo">

					<a href="<?= SITE_URL ?>"><img src="<?= SITE_URL ?>assets/images/logo.svg" alt=""></a>
					
				</div>



				<!-- Main Navigation -->
			<?php if(!isset($_SESSION['user']['result']['id']) || empty($_SESSION['user']['result']['id'])) {?>
				<nav id="navigation">
					<ul id="responsive">

						<li><a href="<?= SITE_URL ?>" class="current">Home</a></li>
						<li><a href="<?= SITE_URL ?>how-it-works.php">How It Works</a></li>

						<li><a href="<?= SITE_URL ?>who-we-are.php">Who We Are</a></li>

						<li><a href="<?= SITE_URL ?>contact-us.php">Contact Us</a></li>					 

					</ul>

				</nav>
			<?php } ?>
				<div class="clearfix"></div>

				<!-- Main Navigation / End -->

				

			</div>

			<!-- Left Side Content / End -->



			<!-- Right Side Content / End -->


			<div class="right-side">
				<div class="header-widget">
					<a href="#sign-in-dialog" class="popup-with-zoom-anim-inline log-in-button"><i class="icon-feather-log-in"></i> <span>Log In / Register</span></a>
				</div>

				<!-- Mobile Navigation Button -->
				<span class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</span>
			</div>
	
			<!-- Right Side Content / End -->



		</div>

	</div>

