 <!-- Sign In Popup
================================================== -->

<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">
	<!--Tabs -->
	<div class="sign-in-form">
		<ul class="popup-tabs-nav">
			<li><a href="#login">Log In</a></li>
			<li><a href="#register">Register</a></li>
		</ul>

		<div class="popup-tabs-container">
				<!-- Login -->

			<div class="popup-tab-content" id="login">
				<!-- Welcome Text -->

				<div class="welcome-text">

					<h3>We're glad to see you again!</h3>
					<span>Don't have an account? <a href="#" class="register-tab">Sign Up!</a></span>

				</div>

				<div class="notification error closeable hide" id="validate_msg">
					<p>Please fill in all the fields required</p>
					<a class="close"></a>

				</div>
				<!-- Form -->
				<form id="signin_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('signin_form')?>" data-name="<?= base64_encode('signin_form')?>" method="POST" onsubmit="return validateForm(this.id, event)">
					<div class="input-with-icon-left">

						<i class="icon-material-baseline-mail-outline"></i>

						<input type="text" class="input-text with-border" name="email" id="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'" maxlength="50" placeholder="Email Address" title="Enter Email Address"/>
					</div>
					<div class="input-with-icon-left">  
						<i class="icon-material-outline-lock"></i>
						<input type="password" class="input-text with-border" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'" maxlength="15" name="password" id="password" placeholder="Password" title="Enter Password"/>
					</div>

					<a href="<?= SITE_URL?>forgot-password.php" class="forgot-password">Forgot Password?</a>

				<!-- Button -->

					<button type="submit" name="submit" class="button full-width button-sliding-icon ripple-effect margin-top-10 has-spinner" >Log In <i class="icon-material-outline-arrow-right-alt"></i> <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button> 

				</form> 

			</div>  

			<div class="popup-tab-content" id="register" style="display: none;">

				<!-- Welcome Text -->



				<div class="welcome-text">
					<h3>Let's create your account!</h3>

				</div>
				<!-- Account Type -->



				<div class="account-type">
					<div class="button-section padt_30 text-center">
					<a href="<?= CLIENT_URL?>signup-step.php" class="btn btn-outline-success">Hire a Professional</a>
					<a href="<?= VENDOR_URL?>signup-step.php" class="btn btn-outline-success">Work as a Service Provider</a>

				</div>
				</div>	
			</div>
		</div>
	</div>
</div>



<!-- Sign In Popup / End -->   

<!-- Footer
================================================== -->
	<div class="footer-top-section">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">

					<!-- Footer Rows Container -->
					<div class="footer-rows-container">
						
						<!-- Left Side -->
						<div class="footer-rows-left">
							<div class="footer-row">
								<div class="footer-row-inner footer-logo">
                  <a href="<?= SITE_URL ?>">
                    <img src="<?= SITE_URL ?>assets/images/logo.svg" alt="PayMyPrice logo">
                  </a>
								</div>
							</div>
						</div>
						
						<!-- Right Side -->
						<div class="footer-rows-right">

							<!-- Social Icons -->
							<div class="footer-row">
								<div class="footer-row-inner">
									<ul class="footer-social-links">
										<li>
											<a href="https://www.tiktok.com/@paymyprice" target="_blank" title="TikTok" data-tippy-placement="bottom" data-tippy-theme="light">
                        <img class="tiktok_icon" src="<?php echo SITE_URL . 'assets/images/tik-tok.svg';?>" title="TikTok"/> 
											</a>
										</li>
										<li>
											<a href="https://www.facebook.com/paymyprice" target="_blank" title="Facebook" data-tippy-placement="bottom" data-tippy-theme="light">
												<i class="icon-brand-facebook-f"></i>
											</a>
										</li>
										<li>
											<a href="https://twitter.com/paymyprice1" target="_blank" title="Twitter" data-tippy-placement="bottom" data-tippy-theme="light">
												<i class="icon-brand-twitter"></i>
											</a>
										</li>
										<li>
											<a href="https://www.instagram.com/paymyprice/" target="_blank" title="Instagram" data-tippy-placement="bottom" data-tippy-theme="light">
												<i class="icon-brand-instagram"></i>
											</a>
										</li>
										<li>
											<a href="https://www.linkedin.com/company/paymyprice/" target="_blank" title="LinkedIn" data-tippy-placement="bottom" data-tippy-theme="light">
												<i class="icon-brand-linkedin-in"></i>
											</a>
										</li>
									</ul>
									<div class="clearfix"></div>
								</div>
							</div>
							
							<!-- Language Switcher -->
							 
						</div>

					</div>
					<!-- Footer Rows Container / End -->
				</div>
			</div>
		</div>	
	<!-- Footer Top Section / End -->

	<!-- Footer Middle Section -->
	<div class="footer-middle-section">
		<div class="container">
			<div class="row">

        <div class="col-xl-4 col-lg-4 col-md-12"></div>

				<!-- Links -->
				<div class="col-xl-2 col-lg-2 col-md-3">
					<div class="footer-links">
						<a href="<?php echo SITE_URL . 'employer/signup-step.php';?>"><h3>Hire a Pro</h3></a>
						<!--<ul>
							<li><a href="#"><span>Browse Jobs</span></a></li>
							<li><a href="#"><span>Add Resume</span></a></li>
							<li><a href="#"><span>Job Alerts</span></a></li>
							<li><a href="#"><span>My Bookmarks</span></a></li>
						</ul>-->
					</div>
				</div>

				<!-- Links -->
				<div class="col-xl-2 col-lg-2 col-md-3">
					<div class="footer-links">
          <a href="<?php echo SITE_URL . 'freelancer/signup-step.php';?>"><h3>Find Customers</h3>
						<!--<ul>
							<li><a href="#"><span>Browse Candidates</span></a></li>
							<li><a href="#"><span>Post a Job</span></a></li>
							<li><a href="#"><span>Post a Task</span></a></li>
							<li><a href="#"><span>Plans & Pricing</span></a></li>
						</ul>-->
					</div>
				</div>

				<!-- Links -->
				<div class="col-xl-2 col-lg-2 col-md-3">
					<div class="footer-links">
						<h3>Helpful Links</h3>
						<ul>
							<li><a href="<?php echo SITE_URL . 'how-it-works.php';?>"><span>How It Works</span></a></li>
							<li><a href="<?php echo SITE_URL . 'who-we-are.php';?>"><span>Who We Are</span></a></li>
							<li><a href="<?php echo SITE_URL . 'contact-us.php';?>"><span>Contact Us</span></a></li>
						</ul>
					</div>
				</div>

				<!-- Links -->
				<div class="col-xl-2 col-lg-2 col-md-3">
					<div class="footer-links">
						<h3>Account</h3>
						<ul>
							<li><a href="#sign-in-dialog" class="popup-with-zoom-anim-inline"><span>Log In / Register</span></a></li>
							<li><a class="support_link" style="opacity:0;" type="button" data-toggle="modal" data-target="#supportModal"><span>Contact Support</span></a></li>
						</ul>
					</div>
				</div>

				<!-- Newsletter -->
				<!-- <div class="col-xl-4 col-lg-4 col-md-12">
					<h3><i class="icon-feather-mail"></i> Sign Up For a Newsletter</h3>
					<p>Weekly breaking news, analysis and cutting edge advices on job searching.</p>
					<form action="#" method="get" class="newsletter">
						<input type="text" name="fname" placeholder="Enter your email address">
						<button type="submit"><i class="icon-feather-arrow-right"></i></button>
					</form> -->
				</div>
			</div>
		</div>
	</div>
	<!-- Footer Middle Section / End -->
	
	<!-- Footer Copyrights -->
	<div class="footer-bottom-section">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 text-left">
					© <?php echo date("Y"); ?><strong> PayMyPrice</strong>. All Rights Reserved.
				</div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 text-right">
          <div class="footer-links">
						<ul>
							<li><a href="<?php echo SITE_URL . 'terms-of-service.php';?>"><span>Terms of Service</span></a></li>
						</ul>
					</div>
        </div>
			</div>
		</div>
	</div>
	<!-- Footer Copyrights / End -->
	<!-- Footer Top Section -->
</div>
<!-- Footer / End -->

</div>
<!-- Wrapper / End -->

<!-- Support Modal Popup Form -->
<!-- Modal -->
<div class="modal fade" id="supportModal" tabindex="-1" role="dialog" aria-labelledby="supportModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="supportModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- end:Support Modal Popup Form -->


<!-- Scripts
================================================== -->
<?php include_once('foot-script.php');?>