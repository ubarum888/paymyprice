<?php include('helpers/classes/users.php');

$objUser= new USER();

$jobDetail = $objUser->getJobDetailsById($_GET['job'], '');  


if($jobDetail['result']['min_budget'] !== '' && $jobDetail['result']['max_budget'] !== ''){
    $budget_price = '$'.number_format($jobDetail['result']['min_budget']).' - $'.number_format($jobDetail['result']['max_budget']);
  } else if($jobDetail['result']['min_budget'] !== ''){
    $budget_price = '$'.number_format($jobDetail['result']['min_budget']);
  } else if($jobDetail['result']['max_budget'] !== ''){
    $budget_price = '$'.number_format($jobDetail['result']['max_budget']);
  } 

    if(isset($jobDetail['result']['c_logo']) && !empty($jobDetail['result']['c_logo'])){
		$cLogo = $jobDetail['result']['c_logo'];
	}else{
		$cLogo = 'company-logo.png';
	}

?>

<!doctype html>
<html lang="en">
<head>
<?php include_once('elements/head.php');?>
 </head>  
<body>


<!-- Wrapper -->

<div id="wrapper">
<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth">
	<?php include_once('elements/header.php');?>
</header>
<input type="hidden" id="post_id" value="<?=$_GET['job']?>"/>

<div class="clearfix"></div>

<!-- Header Container / End -->


<!-- Titlebar
================================================== -->
<div class="single-page-header" data-background-image="images/single-task.jpg">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-page-header-inner">
					<div class="left-side">
						<div class="header-image"><a href="#"><img src="<?= SITE_URL.'uploads/profile/'.$cLogo?>" alt="Company Logo"></a></div>
						<div class="header-details">
							<h3><?= $jobDetail['result']['title']?></h3>
							<h5>About the Employer</h5>
							<ul>
								<li><a href="single-company-profile.html"><i class="icon-material-outline-business"></i> <?= $jobDetail['result']['fname'].' '.$jobDetail['result']['lname']?></a></li>
								
								<li><div class="star-rating" data-rating="<?= $jobDetail['result']['rate']?>.0"></div></li>

								<?php if($jobDetail['result']['nationality'] !== ""){?>
									<li><img class="flag" src="https://www.countryflags.io/<?= $jobDetail['result']['country']?>/shiny/64.png" alt=""><?= $jobDetail['result']['country']?> </li>
								<?php } ?>
								<li><div class="verified-badge-with-title">Verified</div></li>
							</ul>
						</div>
					</div>
					<div class="right-side">
						<div class="salary-box">
							<div class="salary-type">Project Budget</div>
							<div class="salary-amount"><?= $budget_price ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Page Content
================================================== -->
<div class="container">
	<div class="row">
		
		<!-- Content -->
		<div class="col-xl-8 col-lg-8 content-right-offset">
			
			<!-- Description -->
			<div class="single-page-section">
				<h3 class="margin-bottom-25">Project Description</h3>
				<p><?= $jobDetail['result']['description']?></p>
			</div>			
		

			<!-- Skills -->
			<?php if(!empty($jobDetail['result']['tags'])){?>
				<div class="single-page-section">
					<h3>Skills Required</h3>
					<div class="task-tags">
						<?php $tags = explode(',', $jobDetail['result']['tags']);
							foreach ($tags as $tag) {?>
								<span><?= $tag?></span>  
							<?php } ?>				
					</div>
				</div>
			<?php } ?>
			<div class="clearfix"></div>

			<?php if(isset($jobDetail['result']['proposal']) && !empty($jobDetail['result']['proposal'])){?>
				<div class="single-page-section padr_20">
					<h3 class="margin-bottom-25">Your porposal to win this job </h3>
					<!-- Listings Container -->					

					<div class="listings-container grid-layout"> 
						<p><?= $jobDetail['result']['proposal'] ?></p>
					</div>
				</div>
			<?php } ?>	
			
			<!-- Freelancers Bidding -->
			<div class="boxed-list margin-bottom-60">
				<div class="boxed-list-headline">
					<h3><i class="icon-material-outline-group"></i> Freelancers Bidding</h3>
				</div>
				<ul class="boxed-list-ul" id="load_data" data-name="freelancer-bid"></ul>
				<div id="load_data_message" class="text-center"></div> 	
			</div>

		</div>	

		

		<div class="col-xl-4 col-lg-4">
			<div class="sidebar-container">

					<a href="#sign-in-dialog" class="apply-now-button popup-with-zoom-anim-inline">Login to bid<i class="icon-material-outline-arrow-right-alt"></i></a>	
			
				<!-- Sidebar Widget -->
				<div class="sidebar-widget">
					<div class="job-overview">
						<div class="job-overview-headline">Job Summary</div>
						<div class="job-overview-inner">
							<ul>
								<li>
									<i class="icon-material-outline-location-on"></i>
									<span>Location</span>
									<h5><?= $jobDetail['result']['state'].', '.$jobDetail['result']['country']?></h5>
								</li>

								<li>
									<i class="icon-material-outline-business-center"></i>
									<span>Job Type</span>
									<h5><?= $jobDetail['result']['job_type']?></h5>
								</li>

								<li>
									<i class="icon-material-outline-access-time"></i>
									<span>Date Posted</span>
									<h5><?= $jobDetail['result']['time_ago']['result']?></h5>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<!-- Spacer -->
<div class="margin-top-15"></div>
<!-- Spacer / End-->


<!-- Footer

================================================== -->

<div id="footer">	 

  <?php include_once('elements/footer.php');?>	 

</div>

<!-- Footer / End -->



</div>

<!-- Wrapper / End -->


<!-- Scripts

================================================== -->

<?php include_once('elements/foot-script.php');?>

<script src="<?= SITE_URL?>scroll-infinite.js"></script>

</body>
</html>