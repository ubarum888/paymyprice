<?php require_once('../helpers/classes/stripe.php');
require_once('../helpers/classes/users.php');
require_once('restricted.php');
$objStripe= new STRIPE();
$objUser= new USER();

$balance = $objStripe->getConnectAccountBalance($_SESSION['user']['result']['id']);  
$connectedAccount = $objStripe->getConnectAccountInfo($_SESSION['user']['result']['id']);  

//echo "<pre>";print_r($balance);die;


?>  
<!doctype html>

<html lang="en">

<head>
	<?php include_once('../elements/head.php');?>
</head>
<body class="gray">
<!-- Wrapper -->

<div id="wrapper">
<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth dashboard-header not-sticky">  
  <?php include_once('dashboard_header.php');?>  
</header>

<div class="clearfix"></div>
<!-- Header Container / End -->

<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Wallet</h2>
			</div>
			<?php if($connectedAccount['result']['capabilities']['card_payments'] == 'inactive' || $connectedAccount['result']['capabilities']['transfers'] == "inactive"){?>		
					<div class="notification warning  closeable">
					<p>This account has provided the required information to fully onboard onto Stripe. They can accept payments and receive payouts.</p>
					<a class="close"></a>							
				</div>				
			<?php } ?>	
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<!-- Container -->
<div class="container">
	<div class="row">
		<div class="col-xl-12 col-lg-12 content-right-offset">

					<!-- Hedline -->
			<h3 class="margin-top-50">Payment Method</h3>

			<!-- Payment Methods Accordion -->
	<!-- 		<div class="payment margin-top-30">
				<div class="payment-tab">
					<div class="payment-tab-trigger">
						<input type="radio" name="cardType" id="creditCart" value="creditCard">
						<label for="creditCart">Credit / Debit Card</label>
						<img class="payment-logo" src="https://i.imgur.com/IHEKLgm.png" alt="">
					</div>

					<div class="payment-tab-content">
						<div class="row payment-form-row">
							<span>Visa</span>
							<span>************5353</span>
							<span>03/2024</span>

						</div>
					</div>
				</div>

			</div> -->
			<!-- Payment Methods Accordion / End -->

			<!-- Billing Cycle Radios  -->
			<div class="billing-cycle margin-top-25">
				<!-- Radio -->
				<div class="radio">					
					<label for="radio-5">						
						Pending Balance
						<span class="billing-cycle-details">
							<span class="regular-price-tag">$<?= $balance['result']->pending[0]->amount/100 ?> </span>
						</span>
					</label>
					<ul class="list-1">
						<li>Pending : meaning the funds are not yet available to pay out</li>
					</ul>
				</div>
			
				<!-- Radio -->
				<div class="radio">					
					<label for="radio-6">
						Available Balance
						<span class="billing-cycle-details">
							<span class="discounted-price-tag">$<?= $balance['result']->available[0]->amount/100 ?></span>
						</span>
					</label>
					<ul class="list-1">
						<li>Available : meaning the funds can be paid out now</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Container / End -->


</div>
<!-- Wrapper / End -->

<!-- Scripts

================================================== -->

<?php include_once('../elements/foot-script.php');?>

</body>

</html>