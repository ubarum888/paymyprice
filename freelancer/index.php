<?php include('../helpers/classes/users.php');
require_once('restricted.php');
$objUser= new USER();
$noOfBids = $objUser->getCountOfAppliedBid($_SESSION['user']['result']['id']);  
$bidWon = $objUser->getCountOfBidWon($_SESSION['user']['result']['id']);  
$noOfReview = $objUser->getCountOfReviewByFreelancer($_SESSION['user']['result']['id']);  
//echo"<pre>";print_r($noOfReview);die;
?>   
<!doctype html>

<html lang="en">

<head>

	<?php include_once('../elements/head.php');?>

</head>

<body class="gray">



<!-- Wrapper -->

<div id="wrapper">



<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth dashboard-header not-sticky">

  <?php include_once('dashboard_header.php');?>

</header>
<input type="hidden" id="user_id" value="<?= base64_encode($_SESSION['user']['result']['id'])?>"/>  
<div class="clearfix"></div>

<!-- Header Container / End -->





<!-- Dashboard Container -->

<div class="dashboard-container">



	<!-- Dashboard Sidebar

	================================================== -->

	<div class="dashboard-sidebar">

		<?php include_once('dashboard_sidebar.php');?>

	</div>

	<!-- Dashboard Sidebar / End -->





	<!-- Dashboard Content

	================================================== -->

	<div class="dashboard-content-container" data-simplebar>

		<div class="dashboard-content-inner" >

			

			<!-- Dashboard Headline -->

			<div class="dashboard-headline">

				<h3>Howdy, <?= $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']?>!</h3>

				<span>We are glad to see you again!</span>



				<!-- Breadcrumbs -->

				 

			</div>

	

			<!-- Fun Facts Container -->

			<div class="fun-facts-container">


				<div class="fun-fact" data-fun-fact-color="#b81b7f">

					<div class="fun-fact-text">

						<span>Jobs Applied</span>

						<h4><?= $noOfBids['result']?></h4>

					</div>

					<div class="fun-fact-icon"><i class="icon-material-outline-business-center"></i></div>

				</div>

				<div class="fun-fact" data-fun-fact-color="#36bd78">

					<div class="fun-fact-text">

						<span>Jobs Completed</span> 

						<h4><?= $bidWon['result']?></h4>

					</div>

					<div class="fun-fact-icon"><i class="icon-material-outline-gavel"></i></div>

				</div>

				<div class="fun-fact" data-fun-fact-color="#efa80f">

					<div class="fun-fact-text">

						<span>Reviews</span>

						<h4><?= $noOfReview['result']?></h4>

					</div>

					<div class="fun-fact-icon"><i class="icon-material-outline-rate-review"></i></div>

				</div>



				<!-- Last one has to be hidden below 1600px, sorry :( -->

				<div class="fun-fact" data-fun-fact-color="#2a41e6">

					<div class="fun-fact-text">

						<span>This Month Views</span>

						<h4>987</h4>

					</div>

					<div class="fun-fact-icon"><i class="icon-feather-trending-up"></i></div>

				</div>

			</div>

			

			<!-- Row -->

			<div class="row">



				<div class="col-xl-12">

					<!-- Dashboard Box -->

					<div class="listings-container compact-list-layout margin-top-35">

						<div class="freelancer-dashboard-headline">
							<h2>My Activities</h2> 
							<div class="sort-by">								
								 <select class="selectpicker hide-tick" onchange="sortBy(this.value)">
                                    <option value="">None</option>
                                    <option value="2">Accepted</option>
                                    <option value="0">Rejected</option>
                                    <option value="1">Applied</option>
                                </select>					
							</div>
						</div>	

						<div id="load_data" data-name="freelancer-activity"></div>
               			<div id="load_data_message" class="text-center"></div> 

					</div>

					<!-- Dashboard Box / End -->

				</div>

			</div>

			<!-- Row / End -->



			<!-- Footer -->

			<div class="dashboard-footer-spacer"></div>

			<div class="small-footer margin-top-15">

				<?php include_once('dashboard_footer.php');?>

			</div>

			<!-- Footer / End -->

		</div>

	</div>

	<!-- Dashboard Content / End -->



</div>

<!-- Dashboard Container / End -->



</div>

<!-- Wrapper / End -->


<?php include_once('../elements/foot-script.php');?>
<script src="<?= SITE_URL?>scroll-infinite.js"></script>

</body>

</html>