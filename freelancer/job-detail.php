<?php include('../helpers/classes/users.php');

require_once('restricted.php');
$objUser= new USER();

$jobDetail = $objUser->getJobDetailsById($_GET['job'], $_SESSION['user']['result']['id']);  
//$jobView = $objUser->jobViewedBy($_GET['job'], $_SESSION['user']['result']['id']);  

if($jobDetail['result']['min_budget'] !== '' && $jobDetail['result']['max_budget'] !== ''){
    $budget_price = '$'.number_format($jobDetail['result']['min_budget']).' - $'.number_format($jobDetail['result']['max_budget']);
  } else if($jobDetail['result']['min_budget'] !== ''){
    $budget_price = '$'.number_format($jobDetail['result']['min_budget']);
  } else if($jobDetail['result']['max_budget'] !== ''){
    $budget_price = '$'.number_format($jobDetail['result']['max_budget']);
  } 

    if(isset($jobDetail['result']['c_logo']) && !empty($jobDetail['result']['c_logo'])){
		$cLogo = $jobDetail['result']['c_logo'];
	}else{
		$cLogo = 'company-logo.png';
	}

?>

<!doctype html>
<html lang="en">
<head>
<?php include_once('../elements/head.php');?>
 </head>  
<body>


<!-- Wrapper -->

<div id="wrapper">
<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth">
	<?php include_once('dashboard_header.php');?>	
</header>
<input type="hidden" id="post_id" value="<?=$_GET['job']?>"/>

<div class="clearfix"></div>

<!-- Header Container / End -->


<!-- Titlebar
================================================== -->
<div class="single-page-header" data-background-image="images/single-task.jpg">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-page-header-inner">
					<div class="left-side">
						<div class="header-image"><a href="#"><img src="<?= SITE_URL.'uploads/profile/'.$cLogo?>" alt="Company Logo"></a></div>
						<div class="header-details">
							<h3><?= $jobDetail['result']['title']?></h3>
							<h5>About the Employer</h5>
							<ul>
                <!-- <li><a href="single-company-profile.html"><i class="icon-material-outline-business"></i> <?= $jobDetail['result']['fname'].' '.$jobDetail['result']['lname']?></a></li> -->
                <li><i class="icon-material-outline-business"></i> <?= $jobDetail['result']['fname'].' '.$jobDetail['result']['lname']?></li>
								
								<li><div class="star-rating" data-rating="<?= $jobDetail['result']['rate']?>.0"></div></li>

								<?php if($jobDetail['result']['nationality'] !== ""){?>
									<li><img class="flag" src="https://www.countryflags.io/<?= $jobDetail['result']['country']?>/shiny/64.png" alt=""><?= $jobDetail['result']['country']?> </li>
								<?php } ?>
								<li><div class="verified-badge-with-title">Verified</div></li>
							</ul>
						</div>
					</div>
					<div class="right-side">
						<div class="salary-box">
							<div class="salary-type">Project Budget</div>
							<div class="salary-amount"><?= $budget_price ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Page Content
================================================== -->
<div class="container">
	<div class="row">
		
		<!-- Content -->
		<div class="col-xl-8 col-lg-8 content-right-offset">
			
			<!-- Description -->
			<div class="single-page-section">
				<h3 class="margin-bottom-25">Project Description</h3>
				<p><?= $jobDetail['result']['description']?></p>
			</div>			
		

			<!-- Skills -->
			<?php if(!empty($jobDetail['result']['tags'])){?>
				<div class="single-page-section">
					<h3>Skills Required</h3>
					<div class="task-tags">
						<?php $tags = explode(',', $jobDetail['result']['tags']);
							foreach ($tags as $tag) {?>
								<span><?= $tag?></span>  
							<?php } ?>				
					</div>
				</div>
			<?php } ?>
			<div class="clearfix"></div>

			<?php if(isset($jobDetail['result']['proposal']) && !empty($jobDetail['result']['proposal'])){?>
				<div class="single-page-section padr_20">
					<h3 class="margin-bottom-25">Your porposal to win this job </h3>
					<!-- Listings Container -->					

					<div class="listings-container grid-layout"> 
						<p><?= $jobDetail['result']['proposal'] ?></p>
					</div>
				</div>
			<?php } ?>	
			
			<!-- Freelancers Bidding -->
			<div class="boxed-list margin-bottom-60">
				<div class="boxed-list-headline">
					<h3><i class="icon-material-outline-group"></i> Freelancers Bidding</h3>
				</div>
				<ul class="boxed-list-ul" id="load_data" data-name="freelancer-bid"></ul>
				<div id="load_data_message" class="text-center"></div> 	
			</div>

		</div>	

		

		<div class="col-xl-4 col-lg-4">
			<div class="sidebar-container">

				<?php if(isset($jobDetail['result']['status']) && ($jobDetail['result']['status'] == '1')){?>
					<span class="dashboard-status-button green">Approval Pending</span>	
					<a href="<?= SITE_URL ?>helpers/magnific-popup.php?popup=edit-proposal&pId=<?= base64_encode($jobDetail['result']['pId'])?>&job=<?= $_GET['job']?>" class="apply-now-button popup-with-zoom-anim-ajax">Edit a Bid <i class="icon-material-outline-arrow-right-alt"></i></a>
				<?php } else if(isset($jobDetail['result']['status']) && ($jobDetail['result']['status'] == '3')){?>
					<span class="dashboard-status-button green">Hired</span>
				<?php } else if(isset($jobDetail['result']['status']) && ($jobDetail['result']['status'] == '4')){?>
					<span class="dashboard-status-button green">Contract Start</span>	
				<?php } else if(isset($jobDetail['result']['status']) && ($jobDetail['result']['status'] == '2')){?>
					<span class="dashboard-status-button green">Bid Accepted</span>	
				<?php } else if(isset($jobDetail['result']['status']) && ($jobDetail['result']['status'] == '5')){?>
					<span class="dashboard-status-button green">End Contract</span>	
				<?php } else {?>
					<a href="#small-dialog" class="apply-now-button popup-with-zoom-anim-bid">Place a Bid <i class="icon-material-outline-arrow-right-alt"></i></a>	
				<?php } ?>	
					<a href="messages.php?eId=<?= base64_encode($jobDetail['result']['user_id'])?>" class="button dark ripple-effect apply-now-button"><i class="icon-feather-message-square"></i> Send Message</a>
				<!-- Sidebar Widget -->
				<div class="sidebar-widget">
					<div class="job-overview">
						<div class="job-overview-headline">Job Summary</div>
						<div class="job-overview-inner">
							<ul>
								<li>
									<i class="icon-material-outline-location-on"></i>
									<span>Location</span>
									<h5><?= $jobDetail['result']['state'].', '.$jobDetail['result']['country']?></h5>
								</li>

								<li>
									<i class="icon-material-outline-business-center"></i>
									<span>Job Type</span>
									<h5><?= $jobDetail['result']['job_type']?></h5>
								</li>

								<li>
									<i class="icon-material-outline-access-time"></i>
									<span>Date Posted</span>
									<h5><?= $jobDetail['result']['time_ago']['result']?></h5>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<!-- Spacer -->
<div class="margin-top-15"></div>
<!-- Spacer / End-->


<!-- Footer

================================================== -->

<div id="footer">	 

  <?php include_once('../elements/footer.php');?>	 

</div>

<!-- Footer / End -->



</div>

<!-- Wrapper / End -->



<!-- Apply for a job popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

	<!--Tabs -->
	<div class="sign-in-form">

		<ul class="popup-tabs-nav">
			<li><a href="#tab">Place a Bid</a></li>
		</ul>

		<div class="popup-tabs-container">

			<!-- Tab -->
			<div class="popup-tab-content" id="tab">
				
				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3 class="margin-bottom-25">Bid on this job! </h3>
				</div>  
					
				<!-- Form -->
				<form id="bid_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('bid_form')?>" data-name="<?= base64_encode('bid_form')?>" method="POST" onsubmit="return validateForm(this.id, event)" autocomplete="off">

					<input type="hidden" name="post_id"  title="reload page" value="<?=$_GET['job']?>"/>					
					<input type="hidden" name="user_id" title="reload page" value="<?= base64_encode($_SESSION['user']['result']['id'])?>"/>  
					<input type="hidden" name="email_id" title="reload page" value="<?= base64_encode($jobDetail['result']['email'])?>"/> 
					<input type="hidden" name="client_id" title="reload page" value="<?= base64_encode($jobDetail['result']['client_id'])?>"/> 
						<div class="bidding-inner">
							<span class="bidding-detail">Set your <strong>minimal rate</strong></span>									
					
							<div class="input-with-icon">
								<input class="with-border" type="text" name="minimal_rate" title="Minimal Rate" onkeypress="return isNumberKey(event)" placeholder="Minimal Rate" >
								<i class="currency">USD</i>
							</div>		
							
							
							<!-- Headline -->
							<span class="bidding-detail margin-top-30">Set your <strong>delivery time</strong></span>
							<!-- Fields -->
							<div class="bidding-fields row">
								<div class="bidding-field col-xl-6">
									<!-- Quantity Buttons -->
									<div class="qtyButtons">
										<div class="qtyDec"></div>
										<input type="text" name="delivery_qty" value="1">
										<div class="qtyInc"></div>
									</div>
								</div>
								<div class="bidding-field col-xl-6">
									<select class="selectpicker default" name="delivery_time">
										<option value="days" selected>Days</option>
										<option value="hours">Hours</option>
									</select>
								</div>
							</div>

							<!-- Headline -->
							<span class="bidding-detail margin-top-30">Write your <strong>porposal</strong> to win this job</span>		

							<div class="form-group margin-top-30">
								<textarea class="form-control" name="proposal" rows="10" cols="95" title="write your porposal to win this job"></textarea>
							</div>	
						</div>		
							
					<!-- Button -->				
					<button type="submit" name="submit" class="button margin-top-35 full-width button-sliding-icon ripple-effect has-spinner">Place a Bid <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>

				</form>
				
				

			</div>

		</div>
	</div>
</div>
<!-- Apply for a job popup / End -->


<!-- Scripts

================================================== -->

<?php include_once('../elements/foot-script.php');?>

<script src="<?= SITE_URL?>scroll-infinite.js"></script>
<script type="text/javascript">
	
	$('.popup-with-zoom-anim-bid').magnificPopup({ 
		 type: 'inline',
		 fixedContentPos: false,
		 fixedBgPos: true,
		 overflowY: 'auto',
		 closeBtnInside: true,
		// preloader: false,
		 closeOnBgClick:false,
		 midClick: true,
		 removalDelay: 300,
		 mainClass: 'my-mfp-zoom-in',
		 preloader: true,

		  callbacks: {
		    open: function() {
		    	var rate = $('#minimal_rate').val();
		    	var delivery = $('#qtyInput').val() +' '+ $('#durationInput').val();
		    	$('.minimal_rate').text(rate);
		    	
		    	$('.duration').text(delivery);		
		    	$('.duration-input').val(delivery);		     
		    	$('.rate-input').val(rate);
		    },
		    close: function() {
		      // Will fire when popup is closed
		    }
		    // e.t.c.
		  }
	});

</script>
</body>
</html>