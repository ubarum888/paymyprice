<div class="dashboard-sidebar-inner" data-simplebar>

			<div class="dashboard-nav-container">



				<!-- Responsive Navigation Trigger -->

				<a href="#" class="dashboard-responsive-nav-trigger">

					<span class="hamburger hamburger--collapse" >

						<span class="hamburger-box">

							<span class="hamburger-inner"></span>

						</span>

					</span>

					<span class="trigger-title">Dashboard Navigation</span>

				</a>

				

				<!-- Navigation -->

				<div class="dashboard-nav">

					<div class="dashboard-nav-inner">



						<ul data-submenu-title="Start">

							<li class="active"><a href="<?= VENDOR_URL?>"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>

							<li><a href="<?= VENDOR_URL?>messages.php"><i class="icon-material-outline-question-answer"></i> Messages <!-- <span class="nav-tag">2</span> --></a></li>

							 

							<li><a href="<?= VENDOR_URL?>reviews.php"><i class="icon-material-outline-rate-review"></i> Reviews</a></li>

						</ul>

						

						<ul data-submenu-title="Organize and Manage">

							<li><a href="#"><i class="icon-material-outline-business-center"></i> Jobs</a>

								<ul>

									<li><a href="<?= VENDOR_URL?>manage-jobs.php">Manage Jobs </a></li>	

								</ul>	
							</li>
						</ul>

						<ul data-submenu-title="Account">
							<li><a href="<?= VENDOR_URL?>settings.php"><i class="icon-material-outline-settings"></i> Settings</a></li>

							<li><a href="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('logout')?>"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>

						</ul>
					</div>

				</div>

				<!-- Navigation / End -->
			</div>
		</div>