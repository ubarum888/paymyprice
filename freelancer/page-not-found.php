<?php include('helpers/classes/admin.php');  
include('helpers/classes/users.php'); 

$objAdmin = new ADMIN();
$objUser= new USER(); 

$category = $objAdmin->getAllMainCategoryById($_GET['c']);  
//echo"<pre>";print_r($category);
?>
<!doctype html>
<html lang="en">
<head>
 	<?php include_once('elements/head.php');?>

</head>
<body>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->
<header id="header-container" class="fullwidth">
	<?php include_once('elements/header.php');?>	 

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->



<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>404 Not Found</h2>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li>404 Not Found</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Page Content
================================================== -->
<!-- Container -->
<div class="container">

	<div class="row">
		<div class="col-xl-12">

			<section id="not-found" class="center margin-top-50 margin-bottom-25">
				<h2>404 <i class="icon-line-awesome-question-circle"></i></h2>
				<p>We're sorry, but the page you were looking for doesn't exist</p>
			</section>

			<div class="row">
				<div class="col-xl-8 offset-xl-2">
						<div class="intro-banner-search-form not-found-search margin-bottom-50">
							<!-- Search Field -->
							<div class="intro-search-field ">
								<input id="intro-keywords" type="text" placeholder="What Are You Looking For?">
							</div>

							<!-- Button -->
							<div class="intro-search-button">
								<button class="button ripple-effect">Search</button>
							</div>
						</div>
				</div>
			</div>

		</div>
	</div>

</div>
<!-- Container / End -->


<!-- Spacer -->
<div class="margin-top-70"></div>
<!-- Spacer / End-->


<!-- Footer
================================================== -->
<div id="footer">	 
 <?php include_once('elements/footer.php');?>	
</div>
<!-- Footer / End -->

</div>
<!-- Wrapper / End -->



</body>
</html>