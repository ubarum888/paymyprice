<div class="small-footer-copyrights">

					© <?= CURRENT_YEAR?> <strong>PayMyPrice</strong>. All Rights Reserved.

				</div>

        <ul class="footer-social-links">
          <li>
            <a href="https://www.tiktok.com/@paymyprice" target="_blank" title="TikTok" data-tippy-placement="bottom" data-tippy-theme="light">
              <img class="tiktok_icon" src="<?php echo SITE_URL . 'assets/images/tik-tok.svg';?>" title="TikTok"/> 
            </a>
          </li>
          <li>
            <a href="https://www.facebook.com/paymyprice" target="_blank" title="Facebook" data-tippy-placement="bottom" data-tippy-theme="light">
              <i class="icon-brand-facebook-f"></i>
            </a>
          </li>
          <li>
            <a href="https://twitter.com/paymyprice1" target="_blank" title="Twitter" data-tippy-placement="bottom" data-tippy-theme="light">
              <i class="icon-brand-twitter"></i>
            </a>
          </li>
          <li>
            <a href="https://www.instagram.com/paymyprice/" target="_blank" title="Instagram" data-tippy-placement="bottom" data-tippy-theme="light">
              <i class="icon-brand-instagram"></i>
            </a>
          </li>
          <li>
            <a href="https://www.linkedin.com/company/paymyprice/" target="_blank" title="LinkedIn" data-tippy-placement="bottom" data-tippy-theme="light">
              <i class="icon-brand-linkedin-in"></i>
            </a>
          </li>
        </ul>

				<div class="clearfix"></div>