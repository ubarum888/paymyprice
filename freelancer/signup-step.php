<?php include('../helpers/classes/users.php');
//require_once('restricted.php');
$objUser= new USER();
$categories = $objUser->getAllCategories();  
?>
<!doctype html>
<html lang="en">
<head>
<!--  Essential META Tags -->

<title>Freelancer Signup - PayMyPrice</title>

<meta property="og:title" content="Freelancer Signup - PayMyPrice">

<meta property="og:description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">

<meta property="og:image" content="https://paymyprice.com/assets/images/logo.svg">

<meta property="og:url" content="https://paymyprice.com">

<meta name="twitter:card" content="summary_large_image">





<!--  Non-Essential, But Recommended -->



<meta property="og:site_name" content="PayMyPrice">

<meta name="twitter:image:alt" content="PayMyPrice logo">
	<?php include_once('../elements/head.php');?>
	<style>
		.steps-progressbar li { width: 33%;}
	</style>
</head>
<body>
<!-- Wrapper -->
<div id="wrapper">
<!-- Header Container
================================================== -->
<header id="header-container" class="fullwidth">
 <?php include_once('../elements/header.php');?>
</header>
<div class="clearfix"></div>
<!-- Header Container / End -->
 
<!-- Page Content   
================================================== -->
<div class="container padt_70">
	<div class="row">
		<div class="col-sm-12"> 
		    <ul class="steps-progressbar">
		          <li class="step1 step">Choose Category</li>
		          <li class="step2">Enter Address</li>
		          <li class="step3">User Signup</li>		          
		  </ul>    
		  </div>	   
		<div class="col-xl-8 center">

			<form id="freelancer_signup_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('freelancer_signup_form')?>" data-name="<?= base64_encode('freelancer_signup_form')?>" method="POST" onsubmit="return validateForm(this.id, event)">
				<section class="wow " data-wow-duration="1.5s" id="step1">
						<div class="login-register-page" >
							<!-- Welcome Text -->
							<div class="welcome-text">
								<h3 style="font-size: 26px;">What services do you provide?</h3>
								<span>Select up to 10 categories.</span>
							</div>
							<div class="notification error closeable hide" id="validate_step1">
								<p>Please Select up to 10 categories</p>
								<a class="close"></a>
							</div>		
									
							<div class="row">						
								<?php foreach($categories['result'] as $key=>$cat){?>
									<div class="checkbox col-sm-4 categories-chk pad_all_12">
										<input type="checkbox" name="category[]" value="<?= $cat['id']?>" id="chekcbox<?= $key?>">
										<label for="chekcbox<?= $key?>"><span class="checkbox-icon"></span><?= $cat['category']?></label>
									</div>
								<?php } ?>											 
							</div>								
							<div class="text-center margin-top-10">
								<button type="button" class="button button-sliding-icon ripple-effect next1">Next <i class="icon-feather-arrow-right"></i></button>	
							</div>				
						</div>
					</section>
					<section class="wow slideInRight" data-wow-duration="1.5s" id="step2" style="display:none">
						<div class="login-register-page" >
						<!-- Welcome Text -->
								<div class="welcome-text">
									<h3 style="font-size: 26px;">Enter Your Address</h3>							
								</div>	
								<div class="notification error closeable hide" id="validate_step2">
									<p>Please fill in all the fields required</p>
									<a class="close"></a>
								</div>	
								<div class="input-with-icon-left no-border">
									<i class="icon-material-outline-account-circle"></i>
									<input type="text" class="input-text" id="autocomplete" title="Please type address" placeholder="Type your address..">
								</div>
								<div class="text-center margin-top-10">
									<button type="button" class="button gray button-sliding-icon ripple-effect back2"><i class="icon-feather-arrow-left"></i> Prev</button>
									<button type="button" class="button button-sliding-icon ripple-effect next2">Next <i class="icon-feather-arrow-right"></i></button>	
								</div>	
						</div>
					</section>	

					<section class=" wow slideInRight" data-wow-duration="1.5s" id="step3" style="display:none">
							<div class="login-register-page">
								<!-- Welcome Text -->
							<div class="welcome-text">
								<h3 style="font-size: 26px;">Let's create your account!</h3>
								<span>Already have an account? <a href="#sign-in-dialog" class="popup-with-zoom-anim-inline log-in-button">Log In!</a></span>
							</div>
							<div class="notification error closeable hide" id="validate_msg">
								<p>Please fill in all the fields required</p>
								<a class="close"></a>
							</div>	
				
								<div class="row">
									<div class="col">
										<div class="input-with-icon-left">
											<input type="text" class="input-text with-border" name="fname" maxlength="20" onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'" id="fname" placeholder="First Name" title="Please Enter First Name"/>
										</div>
									</div>
									<div class="col">
										<div class="input-with-icon-left">
											<input type="text" class="input-text with-border" name="lname" maxlength="20" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'" id="lname" placeholder="Last Name" title="Please Enter Last Name">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col">
										<div class="input-with-icon-left">
											<input type="text" class="input-text with-border" name="email" maxlength="50" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'" placeholder="Email Address" title="Please Enter Eamil Address"/>
										</div>
									</div>
									<div class="col">
										<div class="input-with-icon-left">
											<input type="text" class="input-text with-border" name="phone" id="phone" maxlength="15" onkeypress="return isNumberKey(event)" placeholder="Phone Number" title="Please Enter Phone Number"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col">
										<div class="input-with-icon-left">
											<input type="text" class="input-text with-border" name="address" id="address" placeholder="Address" title="Please Enter Address"/>
										</div>
									</div>	
								</div>
								<div class="row">
									<div class="col">
										<div class="input-with-icon-left">
											<input type="text" class="input-text with-border" name="state" maxlength="20" id="administrative_area_level_1" placeholder="State" title="Please Enter State"/>
										</div>
									</div>
									<div class="col">
										<div class="input-with-icon-left">
											<input type="text" class="input-text with-border" name="country" maxlength="20" id="country" placeholder="Country" title="Please Enter Country"/>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="input-with-icon-left">						 
											<input type="text" class="input-text with-border" name="zip" maxlength="10" onkeypress="return isNumberKey(event)" id="postal_code" placeholder="Zip" title="Please Enter Zip"/>
										</div>
									</div>
								</div>
								<input type="hidden" class="field" id="street_number" name="street" disabled="true" />
								<input type="hidden" class="field" id="route" name="route" disabled="true" />
								<input type="hidden" class="field" id="locality" name="city" disabled="true" />
			   
								<div class="row">
									<div class="col mt-20">
										<div class="input-with-icon-left">
											<div class="checkbox">												
												<input type="checkbox" name="checkbox_agree" id="chekcbox-agree">
												<label class="chkbox-agree" for="chekcbox-agree"><span class="checkbox-icon agree-chkbox"></span>  Yes, I understand and agree to the <a href="<?php echo SITE_URL . 'terms-of-service.php';?>">paymyprice Terms of Service</a>, including the <a href="javascript:void(0)">User Agreement</a> and <a href="javascript:void(0)">Privacy Policy.</a></label>
											</div> 
										</div>
									</div>	
								</div>
							  
									<!-- Button -->
									 
								 
								<div class="text-center margin-top-10">
									<button type="button" class="button gray ripple-effect button-sliding-icon back3"><i class="icon-feather-arrow-left"></i> Back</button>
									
									
									<button type="submit" class="button button-sliding-icon ripple-effect  has-spinner" name="submit">Register <i class="icon-material-outline-arrow-right-alt"></i> <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>	
								</div>
						</div>
				</section>

				<section class="center margin-top-50 margin-bottom-25 wow slideInRight" data-wow-duration="1.5s" id="step4" style="display:none">
					<div class="welcome-text">
						<h3 style="font-size: 26px;">Signup Successfully</h3>
						<p>Your account has been registered. Your account is on hold for approval.</p>							
					</div>	
					<!-- <h3>Dear Freelancer <i class="icon-line-awesome-question-circle"></i></h3> -->
					
				</section>
			</form>
		</div>
	</div>
</div>
<!-- Spacer -->
<div class="margin-top-70"></div>
<!-- Spacer / End-->
<!-- Footer
================================================== -->
<div id="footer">
 <?php include_once('../elements/footer.php');?>	
</div>
<!-- Footer / End -->
</div>
<!-- Wrapper / End -->
<!-- Scripts
================================================== -->
<?php include_once('../elements/foot-script.php');?>  
<?php include_once('../elements/google-provider.php'); ?>
<script type="text/javascript">
		$(document).ready(function(){
			 /******************* SignUp sliding infoo ********************************/
					
		wow = new WOW(
			{
			boxClass:     'wow', 	
			animateClass: 'animated',
			offset:0				
			}
		);
		wow.init();
		
		$('.next1').click(function(){
			$('#validate_step1').addClass('hide');
			if($('.categories-chk input:checkbox:checked').length > 0 && $('.categories-chk input:checkbox:checked').length < 11){
				$("section").hide();
				$(window).scrollTop(0);				
				$("#step2").css({"animation-name": "slideInRight" });
				$("#step2").show();
				$('.step1').addClass('active');
				$('.step2').addClass('step');
			}else{
				$('#validate_step1').removeClass('hide').fadeTo(100,1);
			}   
		});

		$('.next2').click(function(){
			$('#validate_step2').addClass('hide');
			var address = $('#autocomplete').val();
			if(address !== ''){
				$(window).scrollTop(0);		
				$("section").hide();						
				$("#step3").css({"animation-name": "slideInRight" });
				$("#step3").show();	
				$('#address').val(address);
				$('.step2').addClass('active');
				$('.step3').addClass('step');
			}else{
				$('#validate_step2').removeClass('hide').fadeTo(100,1);
			}
		});
		
		$('.back2').click(function(e){ 							
			e.preventDefault();				 
				$("section").hide();	
				$(window).scrollTop(0);		
			$("#step1").css({"animation-name": "slideInLeft" });				
			$("#step1").show();
			$('.step1').removeClass('active');
			$('.step2').removeClass('step');
			//new WOW().init(); 
		});
		$('.back3').click(function(e){ 							
			e.preventDefault();				 
				$("section").hide();	
				$(window).scrollTop(0);		
			$("#step2").css({"animation-name": "slideInLeft" });				
			$("#step2").show();
			$('.step2').removeClass('active');
			$('.step3').removeClass('step');
			//new WOW().init(); 
		});
		
		
	});
</script>
</body>
</html>