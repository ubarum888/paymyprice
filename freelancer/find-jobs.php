<?php include('../helpers/classes/users.php');  
require_once('restricted.php');
$objUser= new USER(); 
$your_categories = array();
//$profileDetail = $objUser->getProfileDetailByUserId($_SESSION['user']['result']['id']); 
$categories = $objUser->getSelectedCategories($_SESSION['user']['result']['id']); 
// echo "<pre>";print_r($categories); 
?>  

<!doctype html>

<html lang="en">

<head>

<?php include_once('../elements/head.php');?>   

<body class="gray">
<!-- Wrapper -->

<div id="wrapper">



<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth">

   <?php include_once('dashboard_header.php');?> 

</header>

<input type="hidden" id="user_id" value="<?= base64_encode($_SESSION['user']['result']['id'])?>"/>  

<div class="clearfix"></div>

<!-- Header Container / End -->



<!-- Spacer -->

<div class="margin-top-90"></div>

<!-- Spacer / End-->



<!-- Page Content

================================================== -->

<div class="container">
	<div class="row">
		<div class="col-xl-3 col-lg-4">
			<div class="sidebar-container set-filter">
			

			<!-- Keywords -->

				<div class="sidebar-widget" id="set_tags">
					<h3>Keywords</h3>
					<div class="keywords-container">
						<div class="keyword-input-container">
							<input type="text" class="keyword-input" placeholder="e.g. job title"/>
							<button type="button" class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button>
						</div>
						<div class="keywords-list"><!-- keywords go here --></div>
						<div class="clearfix"></div>
					</div>
				</div>				

				<!-- Category -->

				<div class="sidebar-widget" id="category_id">

					<h3>Category</h3>

					<select class="selectpicker default" multiple data-selected-text-format="count" data-size="7" title="Your Selected Categories">
						<?php 
						foreach($categories['result'] as $key=>$cat){
							$your_categories[] = $cat['id'];?>	
							<option value="<?= $cat['id']?>"><?= $cat['category']?></option>
						<?php } ?>
					</select>
				</div>

				<input type="hidden" id="categoryId_list" value="<?= implode(',',$your_categories)?>"/>   

				<!-- Job Types -->

				<div class="sidebar-widget" id="job_type">
					<h3>Job Type</h3>
					<div class="switches-list">		
						<div class="switch-container">
							<label class="switch"><input type="checkbox" value="full time" data-name="1"><span class="switch-button"></span> Full Time</label>
						</div>

						<div class="switch-container">
							<label class="switch"><input type="checkbox" value="part time" data-name="2"><span class="switch-button"></span> Part Time</label>
						</div> 	
					</div>
				</div>

				<div class="sidebar-widget" id="payment_type">
					<h3>Payment Type</h3>
					<div class="switches-list">
						<div class="switch-container">
							<label class="switch"><input type="checkbox" value="Fixed" data-name="1"><span class="switch-button"></span> Fixed Price</label>
						</div>

						<div class="switch-container">
							<label class="switch"><input type="checkbox" value="Hourly" data-name="2"><span class="switch-button"></span> Hourly</label>
						</div> 
					</div>
				</div>				

				<div class="sidebar-widget" id="payment_process">
					<h3>Payment Processing</h3>
					<div class="switches-list">
						<div class="switch-container">
							<label class="switch"><input type="checkbox" value="daily" data-name="1"><span class="switch-button"></span> Daily</label>
						</div>

						<div class="switch-container">
							<label class="switch"><input type="checkbox" value="weekly" data-name="2"><span class="switch-button"></span> Weekly</label>
						</div> 

						<div class="switch-container">
							<label class="switch"><input type="checkbox" value="monthly" data-name="3"><span class="switch-button"></span> Monthly</label>
						</div> 
					</div>
				</div> 

				

			
				<div class="clearfix"></div>

			</div>
		</div>
		<div class="col-xl-9 col-lg-8 content-left-offset">

			<h3 class="page-title">Search Results</h3>

			<div class="notify-box margin-top-15">
				<div class="switch-container">
					<label class="switch"><input type="checkbox"><span class="switch-button"></span><span class="switch-text">Turn on email alerts for this search</span></label>
				</div>


				<div class="sort-by">
					<span>Sort by:</span>
					<select class="selectpicker hide-tick" id="sort_by" onchange="sortBy()">
						<option value="">Relevance</option>
						<option value="DESC">Newest</option>
						<option value="ASC">Oldest</option>
						<option value="rand">Random</option>  
					</select>
				</div>
			</div>
			
			<!-- Tasks Container -->
			<div class="tasks-list-container margin-top-35">

				<div id="load_data" data-name="job-listing"></div>
                <div id="load_data_message" class="text-center"></div> 	

				<!-- Pagination -->
				<div class="clearfix"></div>			

			</div>
			<!-- Tasks Container / End -->

		</div>
	</div>
</div>


<!-- Footer

================================================== -->

<div id="footer">

 <?php include_once('../elements/footer.php');?>	 

</div>

<!-- Footer / End -->



</div>

<!-- Wrapper / End -->



<!-- Scripts

================================================== -->

<?php include_once('../elements/foot-script.php');?>



<script src="<?= SITE_URL?>scroll-infinite.js"></script>

<!-- Google Autocomplete -->

<script>

	function initAutocomplete() {

		 var options = {

		  types: ['(cities)'],

		  // componentRestrictions: {country: "us"}

		 };



		 var input = document.getElementById('autocomplete-input');

		 var autocomplete = new google.maps.places.Autocomplete(input, options);

	}

</script>



<!-- Google API & Maps -->

<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<script src="https://maps.googleapis.com/maps/api/js?key=&libraries=places"></script>



</body>

</html>