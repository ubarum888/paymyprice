<?php include('../helpers/classes/users.php');  
require_once('restricted.php');
$objUser= new USER();

if(isset($_GET['view']) && ($_GET['view'] == 'all')){
	$readAll = $objUser->setMessagesToReadAllByUserId($_SESSION['user']['result']['id']);  
}
?>    

<!doctype html>

<html lang="en">

<head>
	<?php include_once('../elements/head.php');?>
</head>
<body class="gray">
<!-- Wrapper -->

<div id="wrapper">
<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth dashboard-header not-sticky">

 
  <?php include_once('dashboard_header.php');?>

</header>

<div class="clearfix"></div>

<!-- Header Container / End -->





<!-- Dashboard Container -->

<div class="dashboard-container">



	<!-- Dashboard Sidebar

	================================================== -->

	<div class="dashboard-sidebar">

		<?php include_once('dashboard_sidebar.php');?>

	</div>

	<!-- Dashboard Sidebar / End -->
	<!-- Dashboard Content

	================================================== -->

	<div class="dashboard-content-container" data-simplebar>

		<div class="dashboard-content-inner" >		

			<!-- Dashboard Headline -->

			<div class="dashboard-headline">

				<h3>Messages</h3>

				<!-- Breadcrumbs -->			 

			</div>

	

				<div class="messages-container margin-top-0">
					<div class="messages-container-inner">
						<!-- Messages -->
						<div class="messages-inbox">

							<div class="messages-headline">

								<div class="input-with-icon">

										<input id="autocomplete-input" type="text" placeholder="Search">

									<i class="icon-material-outline-search"></i>

								</div>

							</div>

							<ul id="user-activate-list">
							<?php foreach ($message_user['result']  as $key => $value) {?>
															
								<li class="user-message-list" id="message-user-<?=  $key?>" data-id="<?=  $key?>"><!---active-message-->
									<a href="javascript:void(0)" >
										<div class="message-avatar"><i class="status-icon user-active-status-<?=  $key?> status-offline"></i><img src="<?= SITE_URL.'uploads/profile/'.$value[1]?>" alt="" /></div>

										<div class="message-by">
											<div class="message-by-headline">  
												<h5 class="user-message-name"><?= $value[0]?></h5>
												<!-- <span>Yesterday</span>  -->
											</div>
											<!-- <span class="pro-title">Website design development</span> -->
											<!-- <p>Hi Tom! Hate to break it to you but I'm actually on vacation</p> -->
										</div>
									</a>
								</li>
							<?php } ?>
							</ul>
						</div>

						<!-- Messages / End -->

							<!-- Message Content -->

						<div class="message-content user-message-box"> 
							<div class="message-box-homepage">
								<div class="message-content-inner">
									<h4> Start Conversation on tap to user</h4>
								</div>
							</div>

							<div class="message-box-activepage hide">
								<div class="messages-headline">
									<h4 id="user-message-name"></h4>
									<a href="#" class="message-action"><i class="icon-feather-trash-2"></i> Delete Conversation</a>
								</div>							
	   
								<!-- Message Content Inner -->

								<div class="message-content-inner">
									<span id='image-loading'></span>  	
										<div class="message-inner"><!--- ALL messages are display Here---></div>
										
										<div class="message-bubble typing hide" >
											<div class="message-bubble-inner">
												<div class="message-avatar"><img src="" alt=""></div>
												<div class="message-text">
													<!-- Typing Indicator -->
													<div class="typing-indicator">  
														<span></span>
														<span></span>
														<span></span>
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>	
										
									</div>
									<!-- Message Content Inner / End -->					

								<!-- Reply Area -->

								<div class="message-reply">
									<form method="post" name="#123" id="send_form">
			                           <textarea cols="1" rows="1" placeholder="Your Message" name="msg" data-autoresize></textarea>
			                            <input type="hidden" name="from" class="from" value="<?= $_SESSION['user']['result']['id'] ?>"/>
			                            <input type="hidden" name="to" id="send_to" class="to" />
			                            <input type="hidden" name="typing"  class="typing" value="false"/>
			                              
			                        </form>								
									<button type="button" class="button ripple-effect" id="send_message">Send</button>
								</div>
							</div>	
						</div>
						<!-- Message Content -->
					</div>
			</div>

			<!-- Messages Container / End -->			

			<!-- Footer -->

			<div class="dashboard-footer-spacer"></div>
			<div class="small-footer margin-top-15">
				 <?php include_once('dashboard_footer.php');?>
			</div>
			<!-- Footer / End -->
		</div>
	</div>

	<!-- Dashboard Content / End -->
</div>
<!-- Dashboard Container / End -->
</div>

<!-- Wrapper / End -->

<!-- Scripts

================================================== -->
<?php include_once('../elements/foot-script.php');?>
<script>
	var user_id =  $('#user_id').data('id');
    var name = $('#user_id').data('name');

 	// trigger click on users & create chat box
    $('.messages-container .user-message-list').on('click',function(){
    	$('.user-message-box .message-box-homepage').addClass('hide'); 
    	$('.user-message-list').removeClass('active-message');   
    	$(this).addClass('active-message');
    	$('#image-loading').show(); 	    	
        
		var id = $(this).data('id');
		$('.user-message-box .message-box-activepage').removeClass('hide');		 
		  
		$('.user-message-box').attr('id', 'message-box-'+id);
		$('.user-message-box #user-message-name').text($('.user-message-name', this).text());	
		var own_img = $('.user-menu .user-avatar img').attr('src');
		var to_img = $('.message-avatar img', this).attr('src');
		$('.message-reply #send_to').val(id);
		$('.user-message-box .typing img').attr('src', to_img);
		  
		$('.message-inner').html(''); // Empty Chat box..
		$.ajax({  
	      url:SITE_URL+"helpers/functions.php?type=Z2V0X21lc3NhZ2Vz",
	      method:"POST",
	      data:{'id':id, 'user_id':user_id},
	      dataType:"json",
	      success:function(response)
	      {    
	       //console.log(response);    
	     
	        if(response.result.length > 0){
		          $.each(response.result, function( index, value ) {
		          		
		          		if(user_id == value.msg_from){ // Msg By Me...
		          			  $('#message-box-'+id+' .message-content-inner .message-inner').append('<div class="message-bubble me">'+
					                '<div class="message-bubble-inner">'+
					                    '<div class="message-avatar"><img src="'+own_img+'" alt="" /></div>'+  
					                    '<div class="message-text"><p>'+value.message+'</p></div>'+
					                '</div>'+
					                '<div class="clearfix"></div>'+
					            '</div>');
		          		}else { // Msg By User...
		          			$('#message-box-'+id+' .message-content-inner .message-inner').append('<div class="message-bubble">'+
				                    '<div class="message-bubble-inner">'+
				                    '<div class="message-avatar"><img src="'+to_img+'" alt="" /></div>'+
				                    '<div class="message-text"><p>'+value.message+'</p></div>'+
				                '</div>'+
				                '<div class="clearfix"></div>'+
				            '</div>');
		          		}
		                     
		          }); 
		        }        
	         $('#image-loading').hide(); 	             
	      }
	  });

	}); 

</script>
<?php if(isset($_GET['eId']) && ($_GET['eId'] !== "")){
$id = base64_decode($_GET['eId']);?>
	<script type="text/javascript">
		$('#message-user-<?= $id?>').click();
	</script>
<?php } ?>

</body>

</html>