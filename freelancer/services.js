/* ----------------- Start Document ----------------- */

var SITE_URL = 'http://paymyprice.aronasoft.com/';  
var CLIENT_URL = 'http://paymyprice.aronasoft.com/employer/'; 
var VENDOR_URL = 'http://paymyprice.aronasoft.com/freelancer/';   

function sendRequest(requestData, requestType, form, formId) {
    try {
        $.ajax({
            url:requestType,
            type:"POST",
            data:requestData,
            processData: false,
            contentType: false,
            success:function(response) {
              var obj = $.parseJSON(response);  
              form['submit'].classList.remove('active'); 
              form['submit'].disabled = false;                        
              if(obj.msg == 'success'){
                if(obj.form_reset == 'on'){form.reset();}                 
                if(obj.redirect == 'on'){window.location.replace(obj.href);}

                if(formId == "add_card_form"){
                  $('#addCard').hide();
                  $('#card_list table tbody').html('');
                  $('#card_list table tbody').prepend('<tr data-id="'+obj.result.id+'"><th scope="row">1</th><td>'+obj.result.account_no+'</td><td>'+obj.result.expire_card+'</td><td><i class="icon-line-awesome-pencil"></i></td></tr>');
                  $('#card_list').fadeIn();
                }
              }
              alertNotificationMsg(obj.notice);
            }
        }); 
    }catch (e) {
        console.log(e);
    }
}

function alertNotificationMsg(msg) {
  try {      
      Snackbar.show({
          text: msg,
          pos: 'bottom-center',
          showAction: false,
          actionText: "Dismiss",
          duration: 5000,
          textColor: '#fff',
          backgroundColor: '#383838'
      }); 
    }catch (e) {
        console.log(e);
    } 
}


function validateForm(id, event) {
  try {    
    event.preventDefault();        
    var onlyletter = /^[A-Za-z]+$/; 
    var onlyNumber = /^[0-9]+$/;        
    var isValid = true;    
    // Access the form element...         
    var form = document.forms[id];    
    form['submit'].classList.add("active");
    form['submit'].disabled = true;
    var formData = new FormData(form);
    var reqType = form.action;  

     // Display the key/value pairs and validate one by one..
    for (var pair of formData.entries()) {
         console.log(pair[0]  +' ='+pair[1]);
      if(pair[1] == '') { 
        //console.log(form[pair[0]].title);
          if(form[pair[0]].title) { 
            if(form[pair[0]].classList.contains("selectpicker")){          
                form[pair[0]].parentElement.getElementsByTagName('button')[0].style.border = '1px solid #ff0000';              
            }else{          
              form[pair[0]].style.border = '1px solid #ff0000';
              form[pair[0]].focus();
              isValid = false;  
            }
          }
       }else{
          if(form[pair[0]].title) { 
            if(form[pair[0]].classList.contains("selectpicker")){
              form[pair[0]].parentElement.getElementsByTagName('button')[0].style.border = '1px solid #e0e0e0';        
            }else{          
                form[pair[0]].style.border = '1px solid #e0e0e0';
            }   
            
          }
       }   
    } 
      //Return error if term and condition checkbox unchecked...
      if(id == 'freelancer_signup_form'){
         if(form['checkbox'].checked === false)  {   
            form.getElementsByClassName('checkbox-icon')[0].style.border = '2px solid #ff0000'; 
            form.getElementsByClassName('chkbox-agree')[0].style.color = '#ff0000';
            isValid = false;
         }else{
            form.getElementsByClassName('checkbox-icon')[0].style.border = '2px solid #b4b4b4';   
            form.getElementsByClassName('chkbox-agree')[0].style.color = '#212529'; 
         }        
      }  

      if(id == 'employer_signup_form'){
         if(form['checkbox'].checked === false)  {   
            form.getElementsByClassName('checkbox-icon')[0].style.border = '2px solid #ff0000'; 
            form.getElementsByClassName('chkbox-agree')[0].style.color = '#ff0000';
            isValid = false;
         }else{
            form.getElementsByClassName('checkbox-icon')[0].style.border = '2px solid #b4b4b4';   
            form.getElementsByClassName('chkbox-agree')[0].style.color = '#212529'; 
         }        
      } 

      if(id == 'job_posting_form'){       
          if($('.payment-type input:checkbox:checked').length == 0){
            form.getElementsByClassName('pt')[0].style.border = '2px solid #ff0000';               
            isValid = false;
          }else{
            form.getElementsByClassName('pt')[0].style.border = '2px solid #b4b4b4'; 
            var value = $('.payment-type input:checkbox:checked').val();
            formData.append ('payment_type', value);
          }
          if($('.payment-process input:checkbox:checked').length == 0){
            form.getElementsByClassName('pp')[0].style.border = '2px solid #ff0000';             
            isValid = false;
          }else{
            form.getElementsByClassName('pp')[0].style.border = '2px solid #b4b4b4';   
             var value = $('.payment-process input:checkbox:checked').val();
             formData.append ('payment_process', value);
          } 
          var tags = [];
          $('.keywords-list .keyword-text').each(function( index ) {
              tags.push($(this).text());
          });
          if(tags.length > 9){            
            isValid = false;
          }
          formData.append('tags', tags);
      }

       if(id == 'add_detail_form'){
          var skills = [];
          $('.keywords-list .keyword-text').each(function( index ) {
              skills.push($(this).text());
          });
          if(skills.length > 9 ){           
            isValid = false;
          }else if(skills.length == 0){
            form.getElementsByClassName('skill-type')[0].style.border = '1px solid #ff0000';             
            isValid = false;
          }else{
            form.getElementsByClassName('skill-type')[0].style.border = '1px solid #b4b4b4'; 
          }
          formData.append('skills', skills);
      }
           
     
      if(isValid == false){
        form['submit'].classList.remove("active");
        form['submit'].disabled = false; 
          throw 'Client end error';
      }       
      sendRequest(formData, reqType, form, id); 
    }catch (err) { 
     console.log(err);          
     // validateErrorMsg(err);
     $('#validate_msg').fadeTo(100,1);
      return false;  
    }  
}  




// When the user clicks on the password field, show the message box
function validateErrorMsg(err) {    
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',  
      showCloseButton: true,
      showConfirmButton: false,    
      customClass: {
        container: 'validation-error-show',        
      },   
    })
    Toast.fire({     
      title: '<div id="validate_msg"><ul>'+err.join("")+'</ul></div>'
    })
}

function visiblePassword() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}

  // only checked one checkbox at a time in question 3....  
  $('.payment-type .one_checked').on('change', function() {
      $('.payment-type .one_checked').not(this).prop('checked', false); 
  });
  
  // only checked one checkbox at a time in question 1....
  $('.payment-process .one_checked').on('change', function() {
      $('.payment-process .one_checked').not(this).prop('checked', false);
  });


function isNumberKey(evt) {
  try {
      var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
    }catch (err) {       
      console.log(err);
        return false;  
    } 
}


$('#category-select .nice-select li').click(function(){
  $('#category').val($(this).data('value'));
});



  function hideExtraContent() {
    var showChar = 100;
    var ellipsestext = "...";
    var moretext = "more";
    var lesstext = "less";
    $('#load_data .more').each(function() {
        var content = $(this).html();

        if(content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar-1, content.length - showChar);

            var html = c + '<span class="moreelipses">'+ellipsestext+'</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

            $(this).html(html);
        }

    });

    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });   
  }

  function sendProposalForJob(){      
     Swal.fire({
          title: 'Write your porposal to win this job',
          html:  '<textarea  class="single-textarea" id="proposal" placeholder="Message"></textarea>',       
          showCancelButton: true,
          confirmButtonText: 'Submit Your Proposal',
          showLoaderOnConfirm: true,
          customClass: 'sweetalert-lg',       
          preConfirm: () => { 
                var proposal = document.getElementById('proposal').value;
                var post_id = document.getElementById('post_id').value; 
                var user_id = document.getElementById('user_id').value; 
                var email_id = document.getElementById('email_id').value; 
                var client_id = document.getElementById('client_id').value;
             
                if((proposal == "") || (post_id == "") || (user_id == "") || (email_id == "") || (client_id == "")){
                  return  Swal.showValidationMessage( 'All fields are required !');
                }
                return new Promise(function (resolve, reject) {
                    $.ajax({
                        url: SITE_URL+'helpers/functions.php?type=c3VibWl0X3Byb3Bvc2Fs',  
                        type:"POST",             
                        data:{'proposal':proposal, 'post_id':post_id, 'user_id':user_id, 'email_id':email_id, 'client_id':client_id},
                         success: function(response) {
                          var obj = $.parseJSON(response); 
                           if(obj.msg == 'success'){                           
                                return resolve(response);
                              }else{
                                 Swal.showValidationMessage(obj.notice);
                                 return resolve("Server not respond");
                              }
                         },
                        error: function(a, b, c){
                             return resolve("Server not respond");
                         }
                    })
                })
          },           
          allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
          if (result.value) { 
            $('#job_apply').addClass('hide'); 
            $('#job_applied').removeClass('hide');           
            successNotificationMsg('Your proposal has been submitted.');
          }
      });
    }


function responseProposal(element, call, request, id, status, title) {  
    Swal.fire({
        title: title,
        html:  '<textarea  class="single-textarea" id="response" placeholder="Message"></textarea>',       
        showCancelButton: true,
        confirmButtonText: 'Submit Response',
        showLoaderOnConfirm: true,
        customClass: 'sweetalert-lg',       
        preConfirm: () => { 
              var response = document.getElementById('response').value; 
              if((response == "")){
                return  Swal.showValidationMessage( 'All fields are required !');
              }
              return new Promise(function (resolve, reject) {
                  $.ajax({
                      url: SITE_URL+'helpers/functions.php?type=cG9ycG9zYWxfcmVzcG9uc2U=',  
                      type:"POST",  
                       data:{'id':id, 'request':request, 'status':status, 'response':response},   
                       success: function(response) {
                        var obj = $.parseJSON(response); 
                         if(obj.msg == 'success'){                           
                              return resolve(response);
                            }else{
                               Swal.showValidationMessage(obj.notice);
                               return resolve("Server not respond");
                            }
                       },
                      error: function(a, b, c){
                           return resolve("Server not respond");
                       }
                  })
              })
        },           
        allowOutsideClick: () => !Swal.isLoading()
      }).then((result) => {
        if (result.value) { 
          $('#acpt_rjt').addClass('hide');
          $('#btn-'+call).removeClass('hide');
          successNotificationMsg('Your response has been submitted.');              
        }
    });
}


function myJobsContent(value){
    /*if(value.status == '2'){
      var status = '<span class="mb-30 green-color">Proposal Accepted</span>';
    }else if(value.status  == '0'){
      var status = '<span class="mb-30 red-color">Proposal Rejected</span>';
    }else if(value.status == '1'){
      var status = '<span class="mb-30 blue-color">Applied</span>';
    } 

    if(value.p_img == null){
      var photo = 'no-image.png';
    }else{
      var photo = value.p_img;
    }    */
	 
    
    $('#load_data').append('<li>'+
                  '<div class="job-listing freelancer">'+                   
                    '<div class="job-listing-details">'+
                      '<div class="job-listing-description">'+
                        '<h3 class="job-listing-title"><a href="#">'+value.title+'</a></h3>'+
                        '<p class="more">'+value.description+'</p>'+
                        '<div class="job-listing-footer padt_30">'+
                          '<ul>'+
                            '<li><i class="icon-material-outline-date-range"></i> Posted on '+value.posted_on+'</li>'+
                            '<li><i class="icon-material-outline-date-range"></i> Applied on '+value.applied_on+'</li>'+
                          '</ul>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                  '<div class="buttons-to-right always-visible">'+
                    '<a href="manage-jobs-details.php?job=='+value.encryptId+'" class="button ripple-effect"><i class="icon-material-outline-supervisor-account"></i> View Proposal </a>'+
                    '<a href="#" class="button gray ripple-effect ico" title="Edit" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>'+
                  '</div>'+
               '</li>');
}

function myJobListingContent(value){
    if(value.status == '2'){
      var status = '<span class="mb-30 green-color">Proposal Accepted</span>';
    }else if(value.status  == '0'){
      var status = '<span class="mb-30 red-color">Proposal Rejected</span>';
    }else if(value.status == '1'){
      var status = '<span class="mb-30 blue-color">Applied</span>';
    } else{
        var status = ' <a href="job-detail.php?job='+value.encryptId+'">Apply</a>';
    } 

    if(value.p_img == null){
      var photo = 'no-image.png';
    }else{
      var photo = value.p_img;
    }
    $('#load_data').append('<a href="'+VENDOR_URL+'job-detail.php?job='+value.encryptId+'" class="job-listing">'+
          '<div class="job-listing-details">'+            
            '<div class="job-listing-company-logo">'+
              '<img src="'+SITE_URL+'assets/images/company-logo-02.png" alt="">'+
            '</div>'+
            '<div class="job-listing-description">'+
              '<h4 class="job-listing-company">'+value.fname+' '+value.lname+' <span class="verified-badge" title="Verified Employer" data-tippy-placement="top"></span></h4>'+
              '<h3 class="job-listing-title">'+value.title+'</h3>'+
              '<p class="job-listing-text more">'+value.description+'</p>'+
            '</div>'+
            '<span class="bookmark-icon"></span>'+
          '</div>'+
          '<div class="job-listing-footer">'+
            '<ul>'+
              '<li><i class="icon-material-outline-location-on"></i>'+value.state+', '+value.country+'</li>'+
              '<li><i class="icon-material-outline-business-center"></i>'+value.job_type+'</li>'+
              '<li><i class="icon-material-outline-account-balance-wallet"></i> $35000-$38000</li>'+
              '<li><i class="icon-material-outline-access-time"></i>'+value.time_ago.result+'</li>'+
            '</ul>'+
          '</div>'+
        '</a>');
}


function myPostsContent(value){
    var count = 0;
    var display = '';
     // if(value['post_status'] == 1) {
     //     var status = '<span class="status-open">Open</span>';
     //  }else if(value['post_status'] == 0) {
     //      var status = 'span class="status-close">Closed</span>';    
     //  }

    if(value.status == '2'){
      var status = '<span class="dashboard-status-button green">Accepted</span>';
    }else if(value.status  == '0'){
      var status = '<span class="dashboard-status-button green">Rejected</span>';
    }else if(value.status == '1'){
      var status = '<span class="dashboard-status-button green">Pending Approval</span>';
    } else if(value.expired_on > value.now){
        var status = '<span class="dashboard-status-button red">Expired</span>';
        display = 'hide';
    } else{
        var status = '<span class="dashboard-status-button yellow">Expiring</span>';
    } 

    if(value.no_of_proposal[value.id]){
       count = value.no_of_proposal[value.id];
    }  

    if(value.p_img == null){
      var photo = 'no-image.png';
    }else{
      var photo = value.p_img;
    }  
    $('#load_data').append('<li>'+
          '<div class="job-listing">'+
            '<div class="job-listing-details">'+
              '<div class="job-listing-description">'+
                '<h3 class="job-listing-title"><a href="#">'+value.title+'</a> '+status+'</h3>'+              
                '<div class="job-listing-footer">'+
                  '<ul>'+
                   '<li class="'+display+'"><i class="icon-material-outline-date-range"></i> Posted on '+value.posted_on+'</li>'+
                    '<li><i class="icon-material-outline-date-range"></i> Expiring on '+value.expire_on+'</li>'+
                  '</ul>'+
               '</div>'+
              '</div>'+
            '</div>'+
          '</div>'+                 
          '<div class="buttons-to-right always-visible">'+
            '<a href="manage-candidates.php" class="button ripple-effect"><i class="icon-material-outline-supervisor-account"></i> Manage Candidates <span class="button-info">'+count+'</span></a>'+
            '<a href="#" class="button gray ripple-effect ico" title="Edit" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>'+
            '<a href="#" class="button gray ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>'+
          '</div>'+
        '</li>');
}


 function freelancerActivityContent(value){
    if(value.status == '2'){
      var status = '<span class="accepted">Accepted</span>';
    }else if(value.status  == '0'){
      var status = '<span class="rejected">Rejected</span>';
    }else if(value.status == '1'){
      var status = '<span class="applied">Applied</span>';
    }

    $('#load_data').append('<a href="job-detail.php?job='+value.encryptId+'" class="job-listing">'+
          '<div class="job-listing-details">'+
            '<div class="job-listing-company-logo">'+
              '<img src="'+SITE_URL+'assets/images/company-logo-01.png" alt="">'+
            '</div>'+
            '<div class="job-listing-description">'+
              '<h3 class="job-listing-title">'+value.title+'</h3>'+
              '<div class="job-listing-footer">'+
                '<ul>'+
                  '<li><i class="icon-material-outline-business"></i> Hexagon <div class="verified-badge" data-tippy-placement="top" data-tippy="" data-original-title="Verified Employer"></div></li>'+
                  '<li><i class="icon-material-outline-location-on"></i> '+value.state+', '+value.country+'</li>'+
                  '<li><i class="icon-material-outline-business-center"></i> '+value.job_type+'</li>'+
                  '<li><i class="icon-material-outline-access-time"></i> '+value.time_ago.result+'</li>'+
                '</ul>'+
              '</div>'+
            '</div>'+
            status
          +'</div>'+
        '</a>');
}


function freelancerListing(value){
    $('#load_data').append('<div class="freelancer listing">'+
          '<div class="freelancer-overview">'+
            '<div class="freelancer-overview-inner">'+
              '<span class="bookmark-icon"></span>'+
             '<div class="freelancer-avatar">'+
                '<div class="verified-badge"></div>'+
                '<a href="freelancer-profile.php?f='+value.encryptId+'"><img src="'+SITE_URL+'uploads/profile/user-avatar-placeholder.png" alt=""></a>'+
              '</div>'+
              '<div class="freelancer-name">'+
                '<h4><a href="freelancer-profile.php?f='+value.encryptId+'">'+value.fname+' '+value.lname+'<img class="flag" src="'+SITE_URL+'assets/images/flags/gb.svg" alt="" title="'+value.nationality+'" data-tippy-placement="top"></a></h4>'+
                '<span>UI/UX Designer</span>'+
             '</div>'+
              '<div class="freelancer-rating">'+
                '<div class="star-rating" data-rating="4.9"></div>'+
              '</div>'+
            '</div>'+
          '</div>'+
          '<div class="freelancer-details">'+
            '<div class="freelancer-details-list">'+
              '<ul>'+
                '<li>Location <strong><i class="icon-material-outline-location-on"></i> London</strong></li>'+
                '<li>Rate <strong>$'+value.hourly_rate+' / hr</strong></li>'+
                '<li>Job Success <strong>95%</strong></li>'+
              '</ul>'+
            '</div>'+
            '<a href="freelancer-profile.php?f='+value.encryptId+'" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>'+
          '</div>'+
        '</div>');
}


$('#post-detail-more').click(function(){    
      $('#post-detail-view').slideToggle('slow'); 
    if ($(this).text() == "View More Detail")
       $(this).text("View Less Detail")
    else
       $(this).text("View More Detail");  
});  



//$(document).ready(function(){ 


 function sortBy(sort) {
    try { 
       var limit = 8;
       var start = 0;
       var action = 'inactive';
       var scroll_to = $("#load_data").data('name');
       var userId = $('#user_id').val();
        $('#load_data').html('');      
        load_infinite_data(limit, start, scroll_to, userId, sort);
    }catch (err) {       
      console.log(err);
        return false;  
    } 
}

function setCategoryFilter(filter_cat, element){
  try {
      var limit = 8;
      var start = 0;
      var action = 'inactive';
      var scroll_to = $("#load_data").data('name');
      var userId = $('#user_id').val();
      if(element.checked){
        var date = $(".filter-date input:checked").val();
       
        $('.filter-category input:checkbox').removeAttr('checked');
          element.checked = true;        
          $('#load_data').html('');      
          load_infinite_data(limit, start, scroll_to, userId, null, filter_cat, date);
      }else{
          $('#load_data').html('');      
          load_infinite_data(limit, start, scroll_to, userId);
      }      
    }catch (err) {       
      console.log(err);
      return false;  
  } 
}

function setPostDateFilter(filter_date, element){
  try {
    //return false;
     var limit = 8;  
     var start = 0;
     var action = 'inactive';
     var scroll_to = $("#load_data").data('name');
     var userId = $('#user_id').val();
     if(element.checked){      
        var category = $(".filter-category input:checked").data('value');       
        $('.filter-date input:checkbox').removeAttr('checked');
          element.checked = true;        
          $('#load_data').html('');      
          load_infinite_data(limit, start, scroll_to, userId, null, category, filter_date);
      } 
    }catch (err) {       
      console.log(err);
      return false;  
  } 
}  


// Function to preview image after validation
      $(function() {
            var reset_img = $('#image_preview img').attr('src');
            //console.log(reset_img);
            var $image_crop;
            $(".change-photo-btn.reset").click(function() {              
                $image_crop.croppie('destroy');
                  //$image_crop = "";
                $('.change-photo-btn').hide();                 
                $('#crop_preview').hide();
                $('#image_preview img').attr('src', reset_img);     
                $('#image_preview').fadeIn();              
               // location.reload();
            });
            $(".avatar-wrapper .file-upload").change(function() {
             // $("#message").empty(); // To remove the previous error message
              $('#image-loading').show();
              var file = this.files[0];
              var imagefile = file.type;
              var match= ["image/jpeg","image/png","image/jpg"];
              if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))) {  
                //$("#message").html(errorMsg('Please Select A valid Image File.Only jpeg, jpg and png Images type allowed')); 
               // $('#image-loading').hide();    
               console.log('errror');          
                return false;
              }else {                            
                $image_crop = $('#crop_preview').croppie({
                    enableExif: true,
                    enableOrientation: true,  
                    viewport: {
                        width: 155,
                        height: 150,
                        type: 'square'  
                    }                  
                });
                setTimeout(function(){ 
                 // console.log($image_crop);
                  var reader = new FileReader();                        
                  reader.onload = function (event) {
                      $image_crop.croppie('bind', {
                        url: event.target.result,
                        zoom : 0,                     
                      }).then(function(){
                        console.log('jQuery bind complete');
                      });        
                  }
                  reader.readAsDataURL(file); 
                  $('#image_preview').hide();                 
                  $('#image-loading').hide();                    
                  $('#crop_preview').fadeIn();
                  $('.avatar-wrapper').css('overflow', 'visible');  
                  $('.change-photo-btn.save').fadeIn();   
                  $('.change-photo-btn.reset').fadeIn();   
                }, 1000);
                 
              }              
               
                $('.change-photo-btn.save').click(function(event){
                   $('#image-loading').show(); 
                      $image_crop.croppie('result', {
                      type: 'blob',
                      size: 'viewport',
                    }).then(function(response){                      
                      var formData = new FormData();
                      formData.append('profile', response);
                      $.ajax({
                          url:SITE_URL+"helpers/functions.php?type=dXNlcl9wcm9maWxlX3VwbG9hZA==",
                          type: "POST",
                          data:formData,
                          processData: false,
                          contentType: false,
                          success:function(response) {
                            var obj = $.parseJSON(response);  
                            $('#image-loading').hide();              
                            if(obj.msg == 'success'){
                                 $image_crop.croppie('destroy');                                  
                                $('.change-photo-btn').hide();                 
                                $('#crop_preview').hide();
                                $('#image_preview img').attr('src', SITE_URL+'uploads/profile/'+obj.result); 
                                   $('#image_preview').fadeIn();    
                            }else{
                              $(".change-photo-btn.reset").click(); 
                            }
                            alertNotificationMsg(obj.notice);  
                          }
                      });
                    })
                });
            }); 
      });

    $(".toggle-password").click(function() {
      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });  

    $('body').on('keyup', '.expire_card', function(e){   
       //console.log($(this).val());
      var val = $(this).val();
      if(!isNaN(val)) {
          if(val > 1 && val < 10 && val.length == 1) {
              temp_val = "0" + val + "/";
              $(this).val(temp_val);
          }
          else if (val >= 1 && val < 10 && val.length == 2 && e.keyCode != 8) {
              temp_val = val + "/";
              $(this).val(temp_val);                
          }
          else if(val > 9 && val.length == 2 && e.keyCode != 8) {
              temp_val = val + "/";
              $(this).val(temp_val);
          }
      }      
    });



    /************ Notification alerts ********************/

 var notify_limit = 8;
 var notify_start = 0;
 var userId = $('#user_id').val();

function load_unseen_notification(view = '')
{  
  $('#load_notify').html('');

  $.ajax({  
      url:SITE_URL+"helpers/functions.php?type=dmlld19ub3RpZmljYXRpb25z",
      method:"POST",
      data:{'view':view},
      dataType:"json",
      success:function(response)
      {        
         //var obj = $.parseJSON(response); 
        //console.log(response.result.length);
        if(response.result.length > 0){
          $.each(response.result, function( index, value ) {
              $('#load_notify').append('<li class="notifications-not-read">'+
                  '<a href="dashboard-manage-candidates.html">'+
                    '<span class="notification-icon"><i class="icon-material-outline-group"></i></span>'+
                    '<span class="notification-text">'+
                      '<strong>'+value.fname+' '+value.lname+'</strong> applied for a job <span class="color">'+value.title+'</span>'+
                    '</span>'+
                  '</a>'+
                '</li>'); 
          }); 
        }
       
         
        if(response.unread > 0){
           $('.notify-alert .notify-unread').text(response.unread).removeClass('hide');
        }        
      }
  });
}
if(userId){
    load_unseen_notification();

    setInterval(function(){
      load_unseen_notification();
    }, 60000); // call function after 60 sec repeatly....
}

$(document).on('click', '.header-notifications-trigger', function(){
    if($('.notify-alert').hasClass('active')){
       $('.notify-alert .notify-unread').text('0').addClass('hide');
      load_unseen_notification('yes');
    }
});






// ------------------ End Document ------------------ //
