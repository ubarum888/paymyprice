<?php include('helpers/classes/admin.php');  
include('helpers/classes/users.php'); 

$objAdmin = new ADMIN();
$objUser= new USER(); 

//$category = $objAdmin->getAllMainCategoryById($_GET['c']);  
//echo"<pre>";print_r($category);
?>
<!doctype html>
<html lang="en">
<head>

  <!--  Essential META Tags -->
  <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1">
  <link rel="canonical" href="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">

  <meta property="og:locale" content="en_US">
  <meta property="og:type" content="website">
  <meta property="og:url" content="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
  <meta property="og:site_name" content="PayMyPrice">

  <title>Contact Us - PayMyPrice</title>
  <meta property="og:title" content="Contact Us - PayMyPrice">
  <meta name="twitter:title" content="Contact Us - PayMyPrice">

  <meta name="description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">
  <meta property="og:description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">
  <meta name="twitter:description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">

  <meta property="og:image" content="https://paymyprice.com/assets/images/logo.svg">
  <meta property="og:image:secure_url" content="https://paymyprice.com/assets/images/logo.svg">
  <meta name="twitter:image" content="https://paymyprice.com/assets/images/logo.svg">
  <meta name="twitter:card" content="summary_large_image">
  <meta property="og:image:width" content="400">
  <meta property="og:image:height" content="50">

 	<?php include_once('elements/head.php');?>

</head>
<body>

<!-- Wrapper -->
<div id="wrapper" class="wrapper_contact_us">

<!-- Header Container
================================================== -->
<header id="header-container" class="fullwidth">
	<?php include_once('elements/header.php');?>	 

</header>
<div class="clearfix"></div>
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Contact</h1>				
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
    <div class="col-xl-12 contact_us_accordion">
      <p>Have a question about our service? Please fill out the form below to be contacted by a representative.</p>
      <h2>FAQ:</h2>
      <br>
      <p><strong>Can I be a service provider and customer at the same time?</strong></p>
      <p class="answer">At this time you will be required to use a different email address for each. Future enhancements include an option to switch between both functionalities.</p>
      <p><strong>Do I need to pay a fee to join your platform?</strong></>
      <p class="answer">Absolutely not! It is a free service for both customers and service providers. The only fees we charge at this time are the percentage processing fee for services fulfilled via our website.</p>
      <p><strong>How many listings am I limited to as a service provider?</strong></p>
      <p class="answer">At this time you can have a maximum of 5 listings per profile. This ensures quality in our community. If you exceed the 5, please contact us for our enhanced business services profiles.</p>
      <p><strong>How long does the process take from submission of my contractor information to approval?</strong></p>
      <p class="answer">We process requests on a first in, first out basis. Typically we get approvals done within 48 hours.</p>
      <p><strong>I don’t have a way to be paid electronically, will you send a check?</strong></p>
      <p class="answer">Unfortunately we only work with electronic payments at this time.</p>
      <br>
      
    </div>
		<div class="col-xl-12">
			<div class="contact-location-info margin-bottom-50">
				<div class="contact-address">
					<ul>
						<li class="contact-address-headline">Our Office</li>
						<li>Chicago, IL 60601</li>
						<li>Phone <a href="tel:855-469-7742">855-4-MY-PRICE</a></li>
						<li>Service Provider Inquiries <a href="mailto:providers@paymyprice.com">providers@paymyprice.com</a></li>
						<li>General Inquiries <a href="mailto:info@paymyprice.com">info@paymyprice.com</a></li>
						<li>Support <a href="mailto:support@paymyprice.com">support@paymyprice.com</a></li>
						<li>
							<div class="freelancer-socials">
								<ul>
                <li>
											<a class="contact_us_tiktok_icon_link" href="https://www.tiktok.com/@paymyprice" target="_blank" title="TikTok" data-tippy-placement="bottom" data-tippy-theme="light">
                        <!--<img class="tiktok_icon contact_us_tiktok_icon" src="<?php echo SITE_URL . 'assets/images/tik-tok-gray.svg';?>" title="TikTok"/>-->
                        <svg class="contact_us_tiktok_icon" id="Capa_1" enable-background="new 0 0 512 512" height="512" viewBox="0 0 512 512" width="512" xmlns="http://www.w3.org/2000/svg"><g><path fill="#b9b9b9" d="m480.32 128.39c-29.22 0-56.18-9.68-77.83-26.01-24.83-18.72-42.67-46.18-48.97-77.83-1.56-7.82-2.4-15.89-2.48-24.16h-83.47v228.08l-.1 124.93c0 33.4-21.75 61.72-51.9 71.68-8.75 2.89-18.2 4.26-28.04 3.72-12.56-.69-24.33-4.48-34.56-10.6-21.77-13.02-36.53-36.64-36.93-63.66-.63-42.23 33.51-76.66 75.71-76.66 8.33 0 16.33 1.36 23.82 3.83v-62.34-22.41c-7.9-1.17-15.94-1.78-24.07-1.78-46.19 0-89.39 19.2-120.27 53.79-23.34 26.14-37.34 59.49-39.5 94.46-2.83 45.94 13.98 89.61 46.58 121.83 4.79 4.73 9.82 9.12 15.08 13.17 27.95 21.51 62.12 33.17 98.11 33.17 8.13 0 16.17-.6 24.07-1.77 33.62-4.98 64.64-20.37 89.12-44.57 30.08-29.73 46.7-69.2 46.88-111.21l-.43-186.56c14.35 11.07 30.04 20.23 46.88 27.34 26.19 11.05 53.96 16.65 82.54 16.64v-60.61-22.49c.02.02-.22.02-.24.02z"/></g></svg>
											</a>
										</li>
										<li>
											<a href="https://www.facebook.com/paymyprice" target="_blank" title="Facebook" data-tippy-placement="bottom" data-tippy-theme="light">
												<i class="icon-brand-facebook-f"></i>
											</a>
										</li>
										<li>
											<a href="https://twitter.com/paymyprice1" target="_blank" title="Twitter" data-tippy-placement="bottom" data-tippy-theme="light">
												<i class="icon-brand-twitter"></i>
											</a>
										</li>
										<li>
											<a href="https://www.instagram.com/paymyprice/" target="_blank" title="Instagram" data-tippy-placement="bottom" data-tippy-theme="light">
												<i class="icon-brand-instagram"></i>
											</a>
										</li>
										<li>
											<a href="https://www.linkedin.com/company/paymyprice/" target="_blank" title="LinkedIn" data-tippy-placement="bottom" data-tippy-theme="light">
												<i class="icon-brand-linkedin-in"></i>
											</a>
										</li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
				<div id="single-job-map-container">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d514923.8805383!2d-87.99905609169225!3d41.88167857018463!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e2ca8ae32a579%3A0x64e6fb9b4c70c480!2sChicago%2C%20IL%2060601%2C%20USA!5e0!3m2!1sen!2smx!4v1601421653161!5m2!1sen!2smx&z=20" width="600" height="450" frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0" style="width:100%;min-height: 457px;height:100%;border:0;"></iframe></div>
					<!--<div id="singleListingMap" data-latitude="41.88" data-longitude="-87.62" data-map-icon="im im-icon-Hamburger" style="position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div class="gm-style" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;"><div tabindex="0" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default; touch-action: pan-x pan-y;"><div style="z-index: 1; position: absolute; left: 50%; top: 50%; width: 100%; transform: translate(0px, 0px);"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; z-index: 985; transform: matrix(1, 0, 0, 1, -160, -46);"><div style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: -256px; top: 0px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: -256px; top: -256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 0px; top: -256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 256px; top: -256px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div><div style="position: absolute; left: 256px; top: 0px; width: 256px; height: 256px;"><div style="width: 256px; height: 256px;"></div></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; z-index: 985; transform: matrix(1, 0, 0, 1, -160, -46);"><div style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; transition: opacity 200ms linear 0s;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i5243!3i12665!4i256!2m3!1e0!2sm!3i524243896!3m17!2sen-US!3sUS!5e18!12m4!1e68!2m2!1sset!2sRoadmap!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjJ8cy5lOmwudC5mfHAuYzojZmY3NDc0NzR8cC5sOjIzLHMudDozN3xzLmU6Zy5mfHAuYzojZmZmMzhlYjAscy50OjM0fHMuZTpnLmZ8cC5jOiNmZmNlZDdkYixzLnQ6MzZ8cy5lOmcuZnxwLmM6I2ZmZmZhNWE4LHMudDo0MHxzLmU6Zy5mfHAuYzojZmZjN2U1Yzgscy50OjM4fHMuZTpnLmZ8cC5jOiNmZmQ2Y2JjNyxzLnQ6MzV8cy5lOmcuZnxwLmM6I2ZmYzRjOWU4LHMudDozOXxzLmU6Zy5mfHAuYzojZmZiMWVhZjEscy50OjN8cy5lOmd8cC5sOjEwMCxzLnQ6M3xzLmU6bHxwLnY6b2ZmfHAubDoxMDAscy50OjQ5fHMuZTpnLmZ8cC5jOiNmZmZmZDRhNSxzLnQ6NTB8cy5lOmcuZnxwLmM6I2ZmZmZlOWQyLHMudDo1MXxwLnY6c2ltcGxpZmllZCxzLnQ6NTF8cy5lOmcuZnxwLnc6My4wMCxzLnQ6NTF8cy5lOmcuc3xwLnc6MC4zMCxzLnQ6NTF8cy5lOmwudHxwLnY6b24scy50OjUxfHMuZTpsLnQuZnxwLmM6I2ZmNzQ3NDc0fHAubDozNixzLnQ6NTF8cy5lOmwudC5zfHAuYzojZmZlOWU1ZGN8cC5sOjMwLHMudDo2NXxzLmU6Z3xwLnY6b258cC5sOjEwMCxzLnQ6NnxwLmM6I2ZmZDJlN2Y3!4e0&amp;key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;token=91850" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -256px; top: 0px; width: 256px; height: 256px; transition: opacity 200ms linear 0s;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i5242!3i12665!4i256!2m3!1e0!2sm!3i524243896!3m17!2sen-US!3sUS!5e18!12m4!1e68!2m2!1sset!2sRoadmap!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjJ8cy5lOmwudC5mfHAuYzojZmY3NDc0NzR8cC5sOjIzLHMudDozN3xzLmU6Zy5mfHAuYzojZmZmMzhlYjAscy50OjM0fHMuZTpnLmZ8cC5jOiNmZmNlZDdkYixzLnQ6MzZ8cy5lOmcuZnxwLmM6I2ZmZmZhNWE4LHMudDo0MHxzLmU6Zy5mfHAuYzojZmZjN2U1Yzgscy50OjM4fHMuZTpnLmZ8cC5jOiNmZmQ2Y2JjNyxzLnQ6MzV8cy5lOmcuZnxwLmM6I2ZmYzRjOWU4LHMudDozOXxzLmU6Zy5mfHAuYzojZmZiMWVhZjEscy50OjN8cy5lOmd8cC5sOjEwMCxzLnQ6M3xzLmU6bHxwLnY6b2ZmfHAubDoxMDAscy50OjQ5fHMuZTpnLmZ8cC5jOiNmZmZmZDRhNSxzLnQ6NTB8cy5lOmcuZnxwLmM6I2ZmZmZlOWQyLHMudDo1MXxwLnY6c2ltcGxpZmllZCxzLnQ6NTF8cy5lOmcuZnxwLnc6My4wMCxzLnQ6NTF8cy5lOmcuc3xwLnc6MC4zMCxzLnQ6NTF8cy5lOmwudHxwLnY6b24scy50OjUxfHMuZTpsLnQuZnxwLmM6I2ZmNzQ3NDc0fHAubDozNixzLnQ6NTF8cy5lOmwudC5zfHAuYzojZmZlOWU1ZGN8cC5sOjMwLHMudDo2NXxzLmU6Z3xwLnY6b258cC5sOjEwMCxzLnQ6NnxwLmM6I2ZmZDJlN2Y3!4e0&amp;key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;token=64607" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -256px; top: -256px; width: 256px; height: 256px; transition: opacity 200ms linear 0s;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i5242!3i12664!4i256!2m3!1e0!2sm!3i524243896!3m17!2sen-US!3sUS!5e18!12m4!1e68!2m2!1sset!2sRoadmap!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjJ8cy5lOmwudC5mfHAuYzojZmY3NDc0NzR8cC5sOjIzLHMudDozN3xzLmU6Zy5mfHAuYzojZmZmMzhlYjAscy50OjM0fHMuZTpnLmZ8cC5jOiNmZmNlZDdkYixzLnQ6MzZ8cy5lOmcuZnxwLmM6I2ZmZmZhNWE4LHMudDo0MHxzLmU6Zy5mfHAuYzojZmZjN2U1Yzgscy50OjM4fHMuZTpnLmZ8cC5jOiNmZmQ2Y2JjNyxzLnQ6MzV8cy5lOmcuZnxwLmM6I2ZmYzRjOWU4LHMudDozOXxzLmU6Zy5mfHAuYzojZmZiMWVhZjEscy50OjN8cy5lOmd8cC5sOjEwMCxzLnQ6M3xzLmU6bHxwLnY6b2ZmfHAubDoxMDAscy50OjQ5fHMuZTpnLmZ8cC5jOiNmZmZmZDRhNSxzLnQ6NTB8cy5lOmcuZnxwLmM6I2ZmZmZlOWQyLHMudDo1MXxwLnY6c2ltcGxpZmllZCxzLnQ6NTF8cy5lOmcuZnxwLnc6My4wMCxzLnQ6NTF8cy5lOmcuc3xwLnc6MC4zMCxzLnQ6NTF8cy5lOmwudHxwLnY6b24scy50OjUxfHMuZTpsLnQuZnxwLmM6I2ZmNzQ3NDc0fHAubDozNixzLnQ6NTF8cy5lOmwudC5zfHAuYzojZmZlOWU1ZGN8cC5sOjMwLHMudDo2NXxzLmU6Z3xwLnY6b258cC5sOjEwMCxzLnQ6NnxwLmM6I2ZmZDJlN2Y3!4e0&amp;key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;token=72292" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 0px; top: -256px; width: 256px; height: 256px; transition: opacity 200ms linear 0s;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i5243!3i12664!4i256!2m3!1e0!2sm!3i524243896!3m17!2sen-US!3sUS!5e18!12m4!1e68!2m2!1sset!2sRoadmap!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjJ8cy5lOmwudC5mfHAuYzojZmY3NDc0NzR8cC5sOjIzLHMudDozN3xzLmU6Zy5mfHAuYzojZmZmMzhlYjAscy50OjM0fHMuZTpnLmZ8cC5jOiNmZmNlZDdkYixzLnQ6MzZ8cy5lOmcuZnxwLmM6I2ZmZmZhNWE4LHMudDo0MHxzLmU6Zy5mfHAuYzojZmZjN2U1Yzgscy50OjM4fHMuZTpnLmZ8cC5jOiNmZmQ2Y2JjNyxzLnQ6MzV8cy5lOmcuZnxwLmM6I2ZmYzRjOWU4LHMudDozOXxzLmU6Zy5mfHAuYzojZmZiMWVhZjEscy50OjN8cy5lOmd8cC5sOjEwMCxzLnQ6M3xzLmU6bHxwLnY6b2ZmfHAubDoxMDAscy50OjQ5fHMuZTpnLmZ8cC5jOiNmZmZmZDRhNSxzLnQ6NTB8cy5lOmcuZnxwLmM6I2ZmZmZlOWQyLHMudDo1MXxwLnY6c2ltcGxpZmllZCxzLnQ6NTF8cy5lOmcuZnxwLnc6My4wMCxzLnQ6NTF8cy5lOmcuc3xwLnc6MC4zMCxzLnQ6NTF8cy5lOmwudHxwLnY6b24scy50OjUxfHMuZTpsLnQuZnxwLmM6I2ZmNzQ3NDc0fHAubDozNixzLnQ6NTF8cy5lOmwudC5zfHAuYzojZmZlOWU1ZGN8cC5sOjMwLHMudDo2NXxzLmU6Z3xwLnY6b258cC5sOjEwMCxzLnQ6NnxwLmM6I2ZmZDJlN2Y3!4e0&amp;key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;token=99535" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 256px; top: -256px; width: 256px; height: 256px; transition: opacity 200ms linear 0s;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i5244!3i12664!4i256!2m3!1e0!2sm!3i524243896!3m17!2sen-US!3sUS!5e18!12m4!1e68!2m2!1sset!2sRoadmap!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjJ8cy5lOmwudC5mfHAuYzojZmY3NDc0NzR8cC5sOjIzLHMudDozN3xzLmU6Zy5mfHAuYzojZmZmMzhlYjAscy50OjM0fHMuZTpnLmZ8cC5jOiNmZmNlZDdkYixzLnQ6MzZ8cy5lOmcuZnxwLmM6I2ZmZmZhNWE4LHMudDo0MHxzLmU6Zy5mfHAuYzojZmZjN2U1Yzgscy50OjM4fHMuZTpnLmZ8cC5jOiNmZmQ2Y2JjNyxzLnQ6MzV8cy5lOmcuZnxwLmM6I2ZmYzRjOWU4LHMudDozOXxzLmU6Zy5mfHAuYzojZmZiMWVhZjEscy50OjN8cy5lOmd8cC5sOjEwMCxzLnQ6M3xzLmU6bHxwLnY6b2ZmfHAubDoxMDAscy50OjQ5fHMuZTpnLmZ8cC5jOiNmZmZmZDRhNSxzLnQ6NTB8cy5lOmcuZnxwLmM6I2ZmZmZlOWQyLHMudDo1MXxwLnY6c2ltcGxpZmllZCxzLnQ6NTF8cy5lOmcuZnxwLnc6My4wMCxzLnQ6NTF8cy5lOmcuc3xwLnc6MC4zMCxzLnQ6NTF8cy5lOmwudHxwLnY6b24scy50OjUxfHMuZTpsLnQuZnxwLmM6I2ZmNzQ3NDc0fHAubDozNixzLnQ6NTF8cy5lOmwudC5zfHAuYzojZmZlOWU1ZGN8cC5sOjMwLHMudDo2NXxzLmU6Z3xwLnY6b258cC5sOjEwMCxzLnQ6NnxwLmM6I2ZmZDJlN2Y3!4e0&amp;key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;token=126778" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 256px; top: 0px; width: 256px; height: 256px; transition: opacity 200ms linear 0s;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i15!2i5244!3i12665!4i256!2m3!1e0!2sm!3i524243896!3m17!2sen-US!3sUS!5e18!12m4!1e68!2m2!1sset!2sRoadmap!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjJ8cy5lOmwudC5mfHAuYzojZmY3NDc0NzR8cC5sOjIzLHMudDozN3xzLmU6Zy5mfHAuYzojZmZmMzhlYjAscy50OjM0fHMuZTpnLmZ8cC5jOiNmZmNlZDdkYixzLnQ6MzZ8cy5lOmcuZnxwLmM6I2ZmZmZhNWE4LHMudDo0MHxzLmU6Zy5mfHAuYzojZmZjN2U1Yzgscy50OjM4fHMuZTpnLmZ8cC5jOiNmZmQ2Y2JjNyxzLnQ6MzV8cy5lOmcuZnxwLmM6I2ZmYzRjOWU4LHMudDozOXxzLmU6Zy5mfHAuYzojZmZiMWVhZjEscy50OjN8cy5lOmd8cC5sOjEwMCxzLnQ6M3xzLmU6bHxwLnY6b2ZmfHAubDoxMDAscy50OjQ5fHMuZTpnLmZ8cC5jOiNmZmZmZDRhNSxzLnQ6NTB8cy5lOmcuZnxwLmM6I2ZmZmZlOWQyLHMudDo1MXxwLnY6c2ltcGxpZmllZCxzLnQ6NTF8cy5lOmcuZnxwLnc6My4wMCxzLnQ6NTF8cy5lOmcuc3xwLnc6MC4zMCxzLnQ6NTF8cy5lOmwudHxwLnY6b24scy50OjUxfHMuZTpsLnQuZnxwLmM6I2ZmNzQ3NDc0fHAubDozNixzLnQ6NTF8cy5lOmwudC5zfHAuYzojZmZlOWU1ZGN8cC5sOjMwLHMudDo2NXxzLmU6Z3xwLnY6b258cC5sOjEwMCxzLnQ6NnxwLmM6I2ZmZDJlN2Y3!4e0&amp;key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;token=119093" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="z-index: 2; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; opacity: 0;"><p class="gm-style-pbt"></p></div><div style="z-index: 3; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; touch-action: pan-x pan-y;"><div style="z-index: 4; position: absolute; left: 50%; top: 50%; width: 100%; transform: translate(0px, 0px);"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"><div class="map-marker-container" data-marker_id="1" style="left: 0.23734px; top: -0.109956px;"><div class="marker-container"><div class="marker-card"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div></div><iframe aria-hidden="true" frameborder="0" tabindex="-1" style="z-index: -1; position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; border: none;"></iframe><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" rel="noopener" href="https://maps.google.com/maps?ll=37.777842,-122.391805&amp;z=15&amp;t=m&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3" title="Open this area in Google Maps (opens a new window)" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img alt="" src="https://maps.gstatic.com/mapfiles/api-3/images/google_white5.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div><div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-sizing: border-box; box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 300px; height: 180px; position: absolute; left: 115px; top: 80px;"><div style="padding: 0px 0px 10px; font-size: 16px; box-sizing: border-box;">Map Data</div><div style="font-size: 13px;">Map data ©2020 Google</div><button draggable="false" title="Close" aria-label="Close" type="button" class="gm-ui-hover-effect" style="background: none; display: block; border: 0px; margin: 0px; padding: 0px; position: absolute; cursor: pointer; user-select: none; top: 0px; right: 0px; width: 37px; height: 37px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2224px%22%20height%3D%2224px%22%20viewBox%3D%220%200%2024%2024%22%20fill%3D%22%23000000%22%3E%0A%20%20%20%20%3Cpath%20d%3D%22M19%206.41L17.59%205%2012%2010.59%206.41%205%205%206.41%2010.59%2012%205%2017.59%206.41%2019%2012%2013.41%2017.59%2019%2019%2017.59%2013.41%2012z%22%2F%3E%0A%20%20%20%20%3Cpath%20d%3D%22M0%200h24v24H0z%22%20fill%3D%22none%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="pointer-events: none; display: block; width: 13px; height: 13px; margin: 12px;"></button></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 188px; bottom: 0px; width: 121px;"><div draggable="false" class="gm-style-cc" style="user-select: none; height: 14px; line-height: 14px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; box-sizing: border-box; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="text-decoration: none; cursor: pointer; display: none;">Map Data</a><span>Map data ©2020 Google</span></div></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2020 Google</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; user-select: none; height: 14px; line-height: 14px; position: absolute; right: 96px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; box-sizing: border-box; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/en-US_US/help/terms_maps.html" target="_blank" rel="noopener" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div></div><button draggable="false" title="Toggle fullscreen view" aria-label="Toggle fullscreen view" type="button" style="background: none rgb(255, 255, 255); display: none; border: 0px; margin: 10px; padding: 0px; position: absolute; cursor: pointer; user-select: none; top: 0px; right: 0px;"></button><div style="padding: 5px; z-index: 0; position: absolute; right: 0px; top: 124px;"><div><div class="custom-zoom-in"></div><div class="custom-zoom-out"></div></div></div><div draggable="false" class="gm-style-cc" style="user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; box-sizing: border-box; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_blank" rel="noopener" title="Report errors in the road map or imagery to Google" href="https://www.google.com/maps/@37.777842,-122.391805,15z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Report a map error</a></div></div><div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="0" controlheight="0" style="margin: 10px; user-select: none; position: absolute; display: none; bottom: 14px; right: 0px;"><div class="gmnoprint" controlwidth="40" controlheight="40" style="display: none; position: absolute;"><div style="width: 40px; height: 40px;"><button draggable="false" title="Rotate map 90 degrees" aria-label="Rotate map 90 degrees" type="button" class="gm-control-active" style="background: none rgb(255, 255, 255); display: none; border: 0px; margin: 0px 0px 32px; padding: 0px; position: relative; cursor: pointer; user-select: none; width: 40px; height: 40px; top: 0px; left: 0px; overflow: hidden; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2224%22%20height%3D%2222%22%20viewBox%3D%220%200%2024%2022%22%3E%0A%20%20%3Cpath%20fill%3D%22%23666%22%20fill-rule%3D%22evenodd%22%20d%3D%22M20%2010c0-5.52-4.48-10-10-10s-10%204.48-10%2010v5h5v-5c0-2.76%202.24-5%205-5s5%202.24%205%205v5h-4l6.5%207%206.5-7h-4v-5z%22%20clip-rule%3D%22evenodd%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 18px; width: 18px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2224%22%20height%3D%2222%22%20viewBox%3D%220%200%2024%2022%22%3E%0A%20%20%3Cpath%20fill%3D%22%23333%22%20fill-rule%3D%22evenodd%22%20d%3D%22M20%2010c0-5.52-4.48-10-10-10s-10%204.48-10%2010v5h5v-5c0-2.76%202.24-5%205-5s5%202.24%205%205v5h-4l6.5%207%206.5-7h-4v-5z%22%20clip-rule%3D%22evenodd%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 18px; width: 18px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2224%22%20height%3D%2222%22%20viewBox%3D%220%200%2024%2022%22%3E%0A%20%20%3Cpath%20fill%3D%22%23111%22%20fill-rule%3D%22evenodd%22%20d%3D%22M20%2010c0-5.52-4.48-10-10-10s-10%204.48-10%2010v5h5v-5c0-2.76%202.24-5%205-5s5%202.24%205%205v5h-4l6.5%207%206.5-7h-4v-5z%22%20clip-rule%3D%22evenodd%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="height: 18px; width: 18px;"></button><button draggable="false" title="Tilt map" aria-label="Tilt map" type="button" class="gm-tilt gm-control-active" style="background: none rgb(255, 255, 255); display: block; border: 0px; margin: 0px; padding: 0px; position: relative; cursor: pointer; user-select: none; width: 40px; height: 40px; top: 0px; left: 0px; overflow: hidden; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218px%22%20height%3D%2216px%22%20viewBox%3D%220%200%2018%2016%22%3E%0A%20%20%3Cpath%20fill%3D%22%23666%22%20d%3D%22M0%2C16h8V9H0V16z%20M10%2C16h8V9h-8V16z%20M0%2C7h8V0H0V7z%20M10%2C0v7h8V0H10z%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="width: 18px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218px%22%20height%3D%2216px%22%20viewBox%3D%220%200%2018%2016%22%3E%0A%20%20%3Cpath%20fill%3D%22%23333%22%20d%3D%22M0%2C16h8V9H0V16z%20M10%2C16h8V9h-8V16z%20M0%2C7h8V0H0V7z%20M10%2C0v7h8V0H10z%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="width: 18px;"><img src="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2218px%22%20height%3D%2216px%22%20viewBox%3D%220%200%2018%2016%22%3E%0A%20%20%3Cpath%20fill%3D%22%23111%22%20d%3D%22M0%2C16h8V9H0V16z%20M10%2C16h8V9h-8V16z%20M0%2C7h8V0H0V7z%20M10%2C0v7h8V0H10z%22%2F%3E%0A%3C%2Fsvg%3E%0A" style="width: 18px;"></button></div></div></div></div></div></div>
					<a href="#" id="streetView">Street View</a>
				</div>-->
			</div>
		</div>

		<!-- <div class="col-xl-8 col-lg-8 offset-xl-2 offset-lg-2"> -->
		<div class="col-xl-12 col-lg-12">

			<section id="contact" class="margin-bottom-60">
				<h3 class="headline margin-top-15 margin-bottom-35">Any questions? Feel free to contact us!</h3>
			
				<form id="contact_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('contact_form')?>" data-name="<?= base64_encode('contact_form')?>" method="POST" onsubmit="return validateForm(this.id, event)">
					<div class="row">
						<div class="col-md-6">
							<div class="input-with-icon-left">
								<input class="with-border" name="name" type="text" id="name" placeholder="Your Name" required="required">
								<i class="icon-material-outline-account-circle"></i>
							</div>
						</div>

						<div class="col-md-6">
							<div class="input-with-icon-left">
								<input class="with-border" name="email" type="email" placeholder="Email Address" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" required="required">
								<i class="icon-material-outline-email"></i>
							</div>
						</div>
					</div>

					<div class="input-with-icon-left">
						<input class="with-border" name="subject" type="text" id="subject" placeholder="Subject" required="required">
						<i class="icon-material-outline-assignment"></i>
					</div>

					<div>
						<textarea class="with-border" name="message" cols="40" rows="5" id="comments" placeholder="Message" spellcheck="true" required="required"></textarea>
					</div>
					<button type="submit" name="submit" class="submit button margin-top-15 has-spinner">Submit Message <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
				</form>
			</section>

		</div>

	</div>
</div> 
 
 
 
<!-- Footer
================================================== -->
<div id="footer">	 
 <?php include_once('elements/footer.php');?>	
</div>
<!-- Footer / End -->

</div>
<!-- Wrapper / End -->



</body>
</html>