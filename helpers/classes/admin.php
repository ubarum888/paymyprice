<?php

require_once("config.php");

class ADMIN extends dbconfig { 

   public static $data;

   function __construct() {

     parent::__construct();

   }
  

    public static function forgotPassword($email) {
	    try{
	    	if($email == "") {
	    		throw new exception("Email field is required");
	    	}
	        $query = "SELECT raw_password FROM `users` WHERE email = '".$email."' AND status = 1 LIMIT 1";
	        $result = dbconfig::run($query);	
	        if(!$result) {
	          throw new exception("Server not respond");
	        }
	        if(mysqli_num_rows($result) == 0){
				throw new exception("Email not found in our records.");
			}
			$row = mysqli_fetch_assoc($result);	
			$check = self::userForgotPasswordEmail($row['raw_password'], $email); 
			$data = array('status'=>'success', 'msg'=>"We sent the password recovery details to your email.", 'result'=>'');
	    }catch (Exception $e) { 
	       $data = array('status'=>'error', 'msg'=>$e->getMessage());
	    }finally {  
	        return $data;
	    }
    }
     

  // login function

	public static function login($formdata){   
		try {
			$query = "SELECT * FROM `users` WHERE email = '".$formdata['email']."' AND password = '".md5($formdata['password'])."' LIMIT 1";
			$result = dbconfig::run($query);  
			if(!$result) {
				throw new exception("Server did not respond. Please try again!");
			}
			if(mysqli_num_rows($result) > 0){
				$resultSet = mysqli_fetch_assoc($result);
				if($resultSet['status'] == 0){
					throw new exception("Please verify your email to login.");
				}
			}else{
				throw new exception("Username/Password Does Not exist or Is Invalid !");
			}	
			$data = array('status'=>'success', 'msg'=>"You Have Logged In Successfully.", 'result'=>$resultSet);	
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data; 
		}
	}	


	public static function sentSmsToUser($phone, $msg) {
	    try{
	    	$data = array("to" => $phone, "from" => SMS_FROM, 'message'=> $msg);                                                                    
			$data_string = json_encode($data);                                                                                   

			$ch = curl_init('https://roor.gynetix.com/standard/api/post/manualTXT/key/5b883eed5ea3e441bb634976d7f36736/response/json');                                                                      
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			    'Content-Type: application/json',                                                                                
			    'Content-Length: ' . strlen($data_string))                                                                       
			);                                                                                                                   

			$result = curl_exec($ch);
			$sms_sent = json_decode($result,true);
			if($sms_sent['status'] == 'failed'){
				throw new exception($sms_sent['message']);
			}
		
			$data = array('status'=>'success', 'msg'=>"Sms sent successfully to provided phone number.", 'result'=>'');
	    }catch (Exception $e) { 
	       $data = array('status'=>'error', 'msg'=>$e->getMessage());
	    }finally {  
	        return $data;
	    } 
    }

	public static function getAllUsersList(){
		try { 	
		   $resultSet = array();
		   $userList = array();
		   $query = "SELECT * FROM `users` WHERE account_type != 3 ORDER BY id DESC";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("Server not responde!");
		   }
		   $count = mysqli_num_rows($result);
		   if($count > 0){
		   		while($row = mysqli_fetch_assoc($result)){
					$resultSet[] =  $row;
					if($row['p_img'] == ""){
						$row['p_img'] = 'user-avatar-placeholder.png';
					}
					$userList[$row['id']] = array($row['fname'].' '.$row['lname'], $row['p_img']);					
				}	
			}   
		   $data = array('status'=>'success', 'msg'=>"All Users Has Been Listed.", 'result'=>$resultSet, 'userList'=>$userList, 'count'=>$count);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    } 

    public static function getUserAlerts($userId){   
     	try {   
     		
     		$notification = 0;       		
     	
		   	$query = "SELECT COUNT(n_id) count FROM `notifications` WHERE notice_to = '".$userId."' AND status = 0 GROUP BY notice_to";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("Server not responde!");
		    }		 
		    if(mysqli_num_rows($result) > 0){   
				$row = mysqli_fetch_assoc($result);	
				$notification = $row['count'];
			}
			
			$data = array('status'=>'success', 'msg'=>"All Notifications fetched.", 'notification'=>$notification);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }  


	public static function deleteUser($id){
		try { 	
		   $resultSet = array();
		   $query = "DELETE FROM `users` WHERE id = '".base64_decode($id)."' ";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("Server not responde!");
		   }		   
		   $data = array('status'=>'success', 'msg'=>"User Account Has Been Removed.");
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    } 

    public static function deleteCategory($id){
		try { 	
		   $resultSet = array();
		   $query = "DELETE FROM `categories` WHERE id = '".base64_decode($id)."' ";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("Server not responde!");
		   }	

		   $query = "DELETE FROM `subcategories` WHERE category_id = '".base64_decode($id)."' ";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("Server not responde!");
		   }

		   $data = array('status'=>'success', 'msg'=>"Category Has Been Removed.");
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    } 
     

    public static function userAccountActivate($formdata){
		try { 
		   $status = 'Deactivate';		   
		   $query = "UPDATE `users` SET status = '".$formdata['status']."' WHERE id = '".base64_decode($formdata['id'])."'";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("Server not responde!");
		   }
		   if($formdata['status'] == 1){
		   		$status = 'Activate';
		   		$query = "SELECT * FROM `users` WHERE id = '".base64_decode($formdata['id'])."' AND status = 1 LIMIT 1";
		        $result = dbconfig::run($query);	
		        if(!$result) {
		          throw new exception("Server not respond");
		        }
		        if(mysqli_num_rows($result) > 0){
					$row = mysqli_fetch_assoc($result);	
			   		$check = self::userAccountActivateEmail($row['id'], $row['email'], $row['raw_password'], $row['fname'].' '.$row['lname']);
			   		$smsSend = self::sentSmsToUser($row['phone'], 'Dear'.ucfirst($row['fname']).' '.ucfirst($row['lname']).' Your PayMyPrice.com account has been approved! Please visit to get started');
			    }	  	
		   }		     
		   $data = array('status'=>'success', 'msg'=>"User Account has been ".$status, 'result'=>$row);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    } 

     public static function addMainCategory($formdata, $banner, $icon){
		try { 
			//echo "<pre>";print_r($formdata);die; 
			unset($formdata['optional_fields']);
			if(in_array("", $formdata, true)){
			  throw new exception("All fields are required!");
			}
			$banner_fetch['result'] = "";


			if(!empty($banner['name']) ){
				$banner_fetch = self::UploadImage($banner, 'banner');	
				if($banner_fetch['status'] == 'error') {
					throw new exception("Banner image not upload");
				}  
			}
			if(!empty($icon['name']) ){
				$icon_fetch = self::UploadImage($icon, 'icon');	
				if($icon_fetch['status'] == 'error') {
					throw new exception("Icon image not upload");
				}  
			}

			$description = mysqli_real_escape_string(self::$con, $formdata['description']);	 

			$query = "INSERT INTO `categories` (category, slug, description, banner_img, icon)VALUES('".$formdata['title']."', '".$formdata['slug']."', '".$description."', '".$banner_fetch['result']."', '".$icon_fetch['result']."')";  
			$lastInsertId = dbconfig::insertrun($query);
			if(!$lastInsertId) {
				throw new exception("Server not responde!");
			}  	

			

			if($formdata['sub_categories'] !== ""){
				$subCat = explode(',', $formdata['sub_categories']);
				foreach ($subCat as $subcat){
	   				 $inserts[] = "('".$lastInsertId."', '".$subcat."')";
	   			}		

				$query = "INSERT INTO `subcategories` (category_id, subcategory)VALUES  ". implode(", ", $inserts); 
				$result = dbconfig::run($query);
				if(!$result) {
					throw new exception("Server not respond");
				}
			}

		   $data = array('status'=>'success', 'msg'=>"Main Category has been Added", 'result'=>'');
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }   
  


     public static function updateMainCategory($formdata, $banner, $icon){
		try { 
//echo "<pre>";print_r($banner_img);die; 
			$banner_img = "";  
			$icon_img = "";
			unset($formdata['optional_fields']);
			if(in_array("", $formdata, true)){
			  throw new exception("All fields are required!");
			}  

			if(!empty($banner['name']) ){
				$banner_fetch = self::UploadImage($banner, 'banner');	
				if($banner_fetch['status'] == 'error') {
					throw new exception("Banner image not upload");
				} 
				$banner_img = ",banner_img = '".$banner_fetch['result']."' ";
			}

			if(!empty($icon['name']) ){
				$icon_fetch = self::UploadImage($icon, 'icon');	
				if($icon_fetch['status'] == 'error') {
					throw new exception("Icon image not upload");
				} 
				$icon_img = ",icon = '".$icon_fetch['result']."' ";
			}
			
			$description = mysqli_real_escape_string(self::$con, $formdata['description']);	 

			$query = "UPDATE `categories` SET category = '".$formdata['title']."', slug = '".$formdata['slug']."' , description = '".$description."' ".$banner_img." ".$icon_img." WHERE id = '".base64_decode($formdata['cId'])."'"; 
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not responde3!");
			}  		

			$query = "DELETE FROM `subcategories` WHERE category_id = '".base64_decode($formdata['cId'])."'"; 
				$result = dbconfig::run($query);
				if(!$result) {
					throw new exception("Server not respond1");
				}


			if($formdata['sub_categories'] !== ""){
				$subCat = explode(',', $formdata['sub_categories']);
				foreach ($subCat as $subcat){
	   				 $inserts[] = "('".base64_decode($formdata['cId'])."', '".$subcat."')";
	   			}		

				$query = "INSERT INTO `subcategories` (category_id, subcategory)VALUES  ". implode(", ", $inserts); 
				$result = dbconfig::run($query);
				if(!$result) {
					throw new exception("Server not respond2");
				}
			}

		   $data = array('status'=>'success', 'msg'=>"Main Category has been Updated", 'result'=>'');
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    } 

    

    
	public static function getAllMainCategories(){
		try { 	
		   $resultSet = array();
		   $query = "SELECT c.*, GROUP_CONCAT(s.subcategory) subcat FROM `categories` c LEFT JOIN `subcategories` s ON(c.id = s.category_id) GROUP BY c.id ORDER BY c.id DESC";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("Server not responde!");
		   }
		   $count = mysqli_num_rows($result);
		   if($count > 0){
		   		while($row = mysqli_fetch_assoc($result)){
					$resultSet[] =  $row;
				}	
			}   
		   $data = array('status'=>'success', 'msg'=>"All main categories Has Been Listed.", 'result'=>$resultSet, 'count'=>$count);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());  
	   } finally {
		   return $data;
	   }
    } 

    public static function autoSuggestionCategory($req){
		try { 	
		  $resultSet = array();
		  $query = "SELECT id, category FROM `categories` WHERE category like '" . $req. "%' ORDER BY category LIMIT 0,8";
		   $result = dbconfig::run($query);
		   if(!$result) {     
			   throw new exception("Server not responde!");
		   }		  		  
		   if(mysqli_num_rows($result) > 0){
		   		while($row = mysqli_fetch_assoc($result)){
					$data['label'] = $row['category']; 
			        $data['value'] = $row['category']; 
			        $data['id'] = $row['id']; 
			        array_push($resultSet, $data); 
				}	
			}   
		   $data = array('status'=>'success', 'msg'=>"Categories Has Been Listed.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
		} 
		
		public static function getMainCategoryBySlug($slug){

			try{

				$resultSet = array();

				$query = ' SELECT id FROM `categories` WHERE slug = "'. filter_var($slug, FILTER_SANITIZE_STRING) . '"';

				$result = dbconfig::run($query);

				if(!$result){
					throw new exception("Server did not respond!");
				}

				$count = mysqli_num_rows($result);

				if($count > 0){
					$row = mysqli_fetch_assoc($result);		

					$resultSet = $row;
				}
				
				$data = array('status'=>'success', 'msg'=>"Main category ID has been fetched.", 'result'=>$resultSet);
			} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
			} finally {
				return $data;
			}
		}

		public static function getSubcategoriesByCategoryId($id){
			try { 	

				$data = array();
				
				$query = "SELECT subcategory FROM `subcategories` WHERE category_id = '".base64_decode($id)."' ORDER BY subcategory DESC";

				$result = dbconfig::run($query);

				if(!$result) {
					throw new exception("Server did not respond!");
				}
				
				foreach($result as $row){
					$data[] = $row['subcategory'];
				}
    
				 $data = array('status'=>'success', 'msg'=>"Subcategories Have Been fetched.", 'result'=>$data);

			 } catch (Exception $e) {
				 $data = array('status'=>'error', 'msg'=>$e->getMessage());
			 } finally {
				 return $data;
			 }
		}

    public static function getAllMainCategoryById($id){
		try { 	
			$resultSet = array();
			$row['subcat'] = "";
			$query = "SELECT c.*, GROUP_CONCAT(s.subcategory) subcat FROM `categories` c LEFT JOIN `subcategories` s ON(c.id = s.category_id) WHERE c.id = '".base64_decode($id)."' GROUP BY c.id ORDER BY c.id DESC";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not responde!");
			}
		   $count = mysqli_num_rows($result);
		   if($count > 0){
		   		$row = mysqli_fetch_assoc($result);		
		   		if($row['subcat'] !== ""){
		   			$row['subcat'] = explode(',', $row['subcat']);
		   		}	
		   		$resultSet = $row;	
			}    
		   $data = array('status'=>'success', 'msg'=>"Main Categories Have Been fetched.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }


      public static function userAccountActivateEmail($id, $email, $password, $fullname){
     	try{
			$bodyContent = '<html><body style="background-color: #eeeeee; margin: 0 !important; padding: 0 !important;">';
			$bodyContent .= '<title></title>';
			$bodyContent .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
			$bodyContent .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
			$bodyContent .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
			$bodyContent .= '<style type="text/css">

			/ FONTS /

			@media screen {

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 400;

				  src: local("Lato Regular"), local("Lato-Regular"), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 700;

				  src: local("Lato Bold"), local("Lato-Bold"), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 400;

				  src: local("Lato Italic"), local("Lato-Italic"), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 700;

				  src: local("Lato Bold Italic"), local("Lato-BoldItalic"), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format("woff");

				}

			}

			

			/ CLIENT-SPECIFIC STYLES /

			body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }

			table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }

			img { -ms-interpolation-mode: bicubic; }			

			/ RESET STYLES /

			img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }

			table { border-collapse: collapse !important; }

			body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }



			/ iOS BLUE LINKS /

			a[x-apple-data-detectors] {

				color: inherit !important;

				text-decoration: none !important;

				font-size: inherit !important;

				font-family: inherit !important;

				font-weight: inherit !important;

				line-height: inherit !important;

			}

			

			/ MOBILE STYLES /

			@media screen and (max-width:600px){

				h1 {

					font-size: 32px !important;

					line-height: 32px !important;

				}

			}



			/ ANDROID CENTER FIX /

			div[style*="margin: 16px 0;"] { margin: 0 !important; }

		</style>';



				$bodyContent .= '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

			We"re thrilled to have you here! Get ready to dive into your new account.

		</div>

		';

				$bodyContent .= '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

			We"re thrilled to have you here! Get ready to dive into your new account.

		</div>



		<table border="0" cellpadding="0" cellspacing="0" width="100%">

			<!-- LOGO -->

			<tbody><tr>

				<td bgcolor="#eee" align="center">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

						<tbody><tr>

							<td align="center" valign="top" style="padding: 50px 30px 50px 30px;">

								  <img src="'.SITE_URL.'assets/images/logo.png"/>

							</td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- HERO -->

			<tr>

				<td bgcolor="#eee" align="center" style="padding: 0px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 0px 30px -15px #808080;">

						<tbody><tr>

							<td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 3px; line-height: 48px;">

							  <h1 style="font-size: 48px; font-weight: 400; margin: 0;">Welcome!</h1>

							</td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- COPY BLOCK -->

			<tr>

				<td bgcolor="#fcfcfc" align="center" style="padding: 0px 10px 0px 10px;">

						<!--[if (gte mso 9)|(IE)]>

						<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

							<td align="center" valign="top" width="600">

							<![endif]-->

						<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 20px 30px -15px #808080;">

						  <!-- COPY -->

						  <tbody>

						<tr>

							<td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px; letter-spacing: .4px">
								  <p style="margin: 0;">Dear '.$fullname.',</p>
							</td>

						</tr>'; 

						$bodyContent .= '<tr><td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px; letter-spacing: .4px">

								  <p style="margin: 0;">Congratulations! Your account has been approved. To get started, please click on the link below.</p>

							</td></tr>'; 

						

						$bodyContent .= '<tr>

						  <td bgcolor="#ffffff" align="center" style="padding: 20px 30px 0px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">		  

						 	<b><a style="color:#f19d39;" href="'.SITE_URL.'helpers/functions.php?type='.base64_encode('email_verify').'&id='.base64_encode($id).'" target="blank">Please click here to get your account </a>
						  </b></td>

					 </tr>';


				$bodyContent .= '<tr>
					 	  <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 0px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;"> 
					 	<b>Username:</b> '.$email.'</td>
					 </tr>';

					$bodyContent .= '<tr>
						  <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 0px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
					 	<b>Password :</b> '.$password.'</td>
					 </tr>';
				  

					  $bodyContent .= '<tr>

						<td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

						  <p style="margin: 0;">If you have any questions, please don’t hesitate to contact us directly.</p>

				</td>

			</tr>

					  <!-- COPY -->

					 <tr>

						<td bgcolor="#ffffff" align="left" style="padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

						 	 <p style="margin: 0;">Thank you,</p>	
						  </td>

					  </tr>

					  <tr>

						<td bgcolor="#ffffff" align="left" style="padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

						  <p style="margin: 0;">The PayMyPrice team</p>							

						</td>

					  </tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- SUPPORT CALLOUT -->

			<tr>

				<td bgcolor="#ffffff" align="center" style="padding: 30px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 0px 30px -15px #808080;">

						<!-- HEADLINE -->  

						<tbody><tr>

						  <td bgcolor="#ffffff" align="center" style="padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

							<h2 style="font-size: 20px; font-weight: 400; color: #1B2A50; margin: 0;">Need more help?</h2>

							<p style="margin: 0;"><a style="color:#f19d39;" href="tel: 855-469-7742">Schedule a call with us</a></p>

						  </td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- FOOTER -->

			<tr>

				<td bgcolor="#ffffff" align="center" style="padding: 0px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

		</tbody></table>

		</body></html>'; 

			$sentEmail = self::SendGridEmail($bodyContent, $email, 'Your account has been approved');
			$data = array('status'=>'success', 'msg'=>"You have been registered successfully login now.", 'result'=>'');

    }catch(Exception $e){

       $data = array('status'=>'error', 'msg'=>$e->getMessage());

    }finally{

        return $data;

    }

   }  



public static function userForgotPasswordEmail($password, $email){
    try{    	

			$bodyContent = '<html><body style="background-color:#eeeeee; margin: 0 !important; padding: 0 !important;">';

			$bodyContent .= '<title></title>';

			$bodyContent .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';

			$bodyContent .= '<meta name="viewport" content="width=device-width, initial-scale=1">';

			$bodyContent .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';

			$bodyContent .= '<style type="text/css">

			/ FONTS /

			@media screen {

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 400;

				  src: local("Lato Regular"), local("Lato-Regular"), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 700;

				  src: local("Lato Bold"), local("Lato-Bold"), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 400;

				  src: local("Lato Italic"), local("Lato-Italic"), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 700;

				  src: local("Lato Bold Italic"), local("Lato-BoldItalic"), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format("woff");

				}

			}

			

			/ CLIENT-SPECIFIC STYLES /

			body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }

			table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }

			img { -ms-interpolation-mode: bicubic; }			

			/ RESET STYLES /

			img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }

			table { border-collapse: collapse !important; }

			body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }



			/ iOS BLUE LINKS /

			a[x-apple-data-detectors] {

				color: inherit !important;

				text-decoration: none !important;

				font-size: inherit !important;

				font-family: inherit !important;

				font-weight: inherit !important;

				line-height: inherit !important;

			}

			

			/ MOBILE STYLES /

			@media screen and (max-width:600px){

				h1 {

					font-size: 32px !important;

					line-height: 32px !important;

				}

			}



			/ ANDROID CENTER FIX /

			div[style*="margin: 16px 0;"] { margin: 0 !important; }

		</style>';



				$bodyContent .= '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

			We"re thrilled to have you here! Get ready to dive into your new account.

		</div>

		';

				$bodyContent .= "<div style='display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;'>

			We are thrilled to have you here! Get ready to dive into your new account.

				</div>

					<table border='0' cellpadding='0' cellspacing='0' width='100%'>

						<!-- LOGO -->

						<tbody><tr>

							<td bgcolor='#eee' align='center'>

								<!--[if (gte mso 9)|(IE)]>

								<table align='center' border='0' cellspacing='0' cellpadding='0' width='600'>

								<tr>

								<td align='center' valign='top' width='600'>

								<![endif]-->

								<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>

									<tbody><tr>

										<td align='center' valign='top' style='padding: 50px 30px 50px 30px;'>

										    <img src='".SITE_URL."'assets/images/logo.png/>

										</td>

									</tr>

								</tbody></table>

								<!--[if (gte mso 9)|(IE)]>

								</td>

								</tr>

								</table>

								<![endif]-->

							</td>

						</tr>

						<!-- HERO -->

						<tr>

							<td bgcolor='#eee' align='center' style='padding: 0px 10px 0px 10px;'>

								<!--[if (gte mso 9)|(IE)]>

								<table align='center' border='0' cellspacing='0' cellpadding='0' width='600'>

								<tr>

								<td align='center' valign='top' width='600'>

								<![endif]-->

								<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>

									<tbody><tr>

										<td bgcolor='#ffffff' align='center' valign='top' style='padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 3px; line-height: 48px;'>

										  <h1 style='font-size: 48px; font-weight: 400; margin: 0;'>Welcome!</h1>

										</td>

									</tr>

								</tbody></table>

								<!--[if (gte mso 9)|(IE)]>

								</td>

								</tr>

								</table>

								<![endif]-->

							</td>

						</tr>

						<!-- COPY BLOCK -->

						<tr>

							<td bgcolor='#f4f4f4' align='center' style='padding: 0px 10px 0px 10px;'>

								<!--[if (gte mso 9)|(IE)]>

								<table align='center' border='0' cellspacing='0' cellpadding='0' width='600'>

								<tr>

								<td align='center' valign='top' width='600'>

								<![endif]-->

								<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>

								  <!-- COPY -->

								  <tbody><tr>

									<td bgcolor='#ffffff' align='center' style='padding: 20px 30px 40px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px; letter-spacing: .4px'>

									  <p style='margin: 0;'>Please find below your credentials to login.</p>

									</td>

								  </tr>

								  <!-- BULLETPROOF BUTTON -->

								  

								  <!-- COPY -->

								  

								  <!-- COPY -->

					<tr>";





							$bodyContent .= "<td bgcolor='#ffffff' align='left' style='padding: 20px 30px 0px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>

					  

						<b>Your new password is :</b> ".$password."

									  </td>

									</tr>

									<tr>

									  <td bgcolor='#ffffff' align='left' style='padding: 20px 30px 0px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>

										<b>Login Link: </b><a style='color:#f19d39;' href='".SITE_URL."login.php' target='blank'>".SITE_URL."</a>

									  </td>

									</tr>

								  <!-- COPY -->

								  <tr>

									<td bgcolor='#ffffff' align='left' style='padding: 20px 30px 20px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>

									  <p style='margin: 0;'>If you have any questions, just reply to this email. We are always happy to help.</p>

									</td>

								  </tr>

								  <!-- COPY --

								  <tr>

									<td bgcolor='#ffffff' align='left' style='padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>

									  <p style='margin: 0;'>Cheers,<br>Paymyprice</p></td>

								  </tr>

								</tbody></table>

								<!--[if (gte mso 9)|(IE)]>

								</td>

								</tr>

								</table>

								<![endif]-->

							</td>

						</tr>

						   <!-- SUPPORT CALLOUT -->

					<tr>

						<td bgcolor='#ffffff' align='center' style='padding: 30px 10px 0px 10px;'>

							<!--[if (gte mso 9)|(IE)]>

							<table align='center' border='0' cellspacing='0' cellpadding='0' width='600'>

							<tr>

							<td align='center' valign='top' width='600'>

							<![endif]-->

							<table border='0' cellpadding=0 cellspacing=0' width='100%' style='max-width: 600px;'>

								<!-- HEADLINE -->

								<tbody><tr>

								  <td bgcolor='#ffffff' align='center' style='padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>

									<h2 style='font-size: 20px; font-weight: 400; color: #1B2A50; margin: 0;'>Need more help?</h2>

									 <p style='margin: 0;'><a style='color:#f19d39;' href='tel: 855-469-7742'>Schedule a call with us</a></p>

								  </td>

								</tr>

							</tbody></table>

							<!--[if (gte mso 9)|(IE)]>

							</td>

							</tr>

							</table>

							<![endif]-->

						</td>

					</tr>

					<!-- FOOTER -->

					

						<tr>

							<td bgcolor='#ffffff' align='center' style='padding: 0px 10px 0px 10px;'>

								<!--[if (gte mso 9)|(IE)]>

								<table align='center' border='0' cellspacing='0' cellpadding='0' width='600'>

								<tr>

								<td align='center' valign='top' width='600'>

								<![endif]-->

								

								<!--[if (gte mso 9)|(IE)]>

								</td>

								</tr>

								</table>

								<![endif]-->

							</td>

						</tr>

					</tbody></table>

					</body></html>"; 
       

		$sentEmail = self::SendGridEmail($bodyContent, $email, 'Password recovery');		
		$data = array('status'=>'success', 'msg'=>"password recovery email sent.", 'result'=>'');

    }catch(Exception $e){

       $data = array('status'=>'error', 'msg'=>$e->getMessage());

    }finally{

        return $data;

    }

}



	/************************ Image Upload  ************************/

   

	public static function UploadImage($file, $type){  
		try{  
			//echo"<pre>";print_r($file);die;

			$target_dir = "../uploads/category/";	
			$target_file = $target_dir . basename($file["name"]);
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			if($file["size"] > 5000000) {  // 5 mb only limit
				throw new exception("Sorry, your file is too large."); 
			}else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"	&& $imageFileType != "gif"){
				throw new exception("Sorry! only JPG, JPEG, PNG & GIF & PDF files are allowed."); 	
			}			

			$upload_name = $type."_".time().".".$imageFileType;                       //store in database...
			$new_target_file = $target_dir.basename($upload_name); 
			if(move_uploaded_file($file["tmp_name"], $new_target_file)){
				$data = array('status'=>'success', 'msg'=>"Image upload Successfully", 'result'=>$upload_name);	
			}else{
				throw new exception("Sorry, there was an error uploading your file."); 
			}				    

		}catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally {
			return $data;
		}  
	}


	public static function uploadIDFileUnlink($filename){

		try {

			unlink("../uploads/ID/".$filename);			 	

			$data = array('status'=>'success', 'msg'=>"Upload ID file unlinked successfully.", 'result'=>'');			

		}catch(Exception $e) {

			$data = array('status'=>'error', 'msg'=>$e->getMessage());

		}finally{

			return $data;

		}

	}


	public static function SendGridEmail($template, $to, $subject) {
       	   require '../sendgrid/autoload.php'; // If you're using Composer (recommended)
			// Comment out the above line if not using Composer
			// require("<PATH TO>/sendgrid-php.php");
			// If not using Composer, uncomment the above line and
			// download sendgrid-php.zip from the latest release here,
			// replacing <PATH TO> with the path to the sendgrid-php.php file,
			// which is included in the download:
			// https://github.com/sendgrid/sendgrid-php/releases

			$email = new \SendGrid\Mail\Mail(); 
			$email->setFrom("info@paymyprice.com", "Paymyprice");
			$email->setSubject($subject);
			$email->addTo($to);
			//$email->addContent("text/plain", "and easy to do anywhere, even with PHP");
			$email->addContent("text/html", $template);
			$sendgrid = new \SendGrid('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');  
	    try{
	    	  $response = $sendgrid->send($email);
			    //echo"<pre>";print_r($response);die;
			$data = array('status'=>'success', 'msg'=>"We sent an email to user.", 'result'=>'');
	    }catch (Exception $e) { 
	       $data = array('status'=>'error', 'msg'=>$e->getMessage());
	    }finally {  
	        return $data;
	    }
    }






}