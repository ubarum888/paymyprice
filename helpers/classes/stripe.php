<?php require_once("config.php");

class STRIPE extends dbconfig { 

   public static $data;

   function __construct() {

     parent::__construct();

   }


    public static function createFreelancerStripeAccount($formdata) { 
    	try {
			// Use Stripe's library to make requests...

			require_once '../stripe/stripe/stripe-php/init.php'; 
			
			$stripe = new \Stripe\StripeClient('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'
			);


			$account =	$stripe->accounts->create([
				'type' => 'custom',
				'country' => 'US',
				'email' => $formdata['email'],
				'default_currency' => 'USD',
				'business_profile' => [
				  			'url' => 'https://paymyprice.com/',
				  			'mcc' => '5734',

				],
			  	'capabilities' => [
				    'card_payments' => ['requested' => true],
				    'transfers' => ['requested' => true],
				],
				'business_type' => 'individual',
				'individual' => [
			  			'address' => [
					  				'city' => $formdata['city'],
					  				'country' => $formdata['country'],
					  				'line1' => $formdata['street'].' '.$formdata['route'],
					  				'postal_code' => $formdata['zip'],
					  				'state' => $formdata['state'],
					  			],
					  	'email' =>  $formdata['email'],					  	
					    'first_name' => $formdata['fname'],
					    'last_name' => $formdata['lname'],  
					    'phone'   => $formdata['phone'],					   
				]				 
			]);

			$data = array('status'=>'success', 'msg'=>"Freelancer Connect Account Has Been Created.", 'result'=>$account->id);
		} catch(\Stripe\Exception\CardException $e) {
		  // Since it's a decline, \Stripe\Exception\CardException will be caught
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} catch (\Stripe\Exception\RateLimitException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Too many requests made to the API too quickly
		} catch (\Stripe\Exception\InvalidRequestException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Exception\AuthenticationException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		} catch (\Stripe\Exception\ApiConnectionException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Network communication with Stripe failed
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Something else happened, completely unrelated to Stripe
		}finally {  
	        return $data;
	    }  
    }


    public static function updateFreelancerConnectedAccount($formdata, $userId, $file) {
    	try {;

			// Use Stripe's library to make requests...    
			require_once '../stripe/stripe/stripe-php/init.php'; 
			
			$stripe = new \Stripe\StripeClient('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'
			);

			$query = "SELECT stripe_account_id FROM `user_details` WHERE user_id = '".$userId."'";
			$result = dbconfig::run($query);  
		   	if(!$result) {
			   throw new exception("Server not responde!");
		   	}
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);					
		    } 		


			$account = $stripe->accounts->update(
			    $row['stripe_account_id'],
			    [
	    		'individual' => [
		           'address' => [
		  				'city' => $formdata['city'],
		  				'country' => $formdata['country'],
		  				'line1' => $formdata['address_line1'].' '.$formdata['address_line2'],
		  				'postal_code' => $formdata['zip'],
		  				'state' => $formdata['state'],
		  			]			       		      
				]    
			]);	      			
		
			$data = array('status'=>'success', 'msg'=>"Freelancer Connect Account Has Been Created.", 'result'=>'');
		}catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}catch(\Stripe\Exception\CardException $e) {
		  // Since it's a decline, \Stripe\Exception\CardException will be caught
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} catch (\Stripe\Exception\RateLimitException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Too many requests made to the API too quickly
		} catch (\Stripe\Exception\InvalidRequestException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Exception\AuthenticationException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		} catch (\Stripe\Exception\ApiConnectionException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Network communication with Stripe failed
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Something else happened, completely unrelated to Stripe
		}finally {  
	        return $data;
	    }  
    }

      public static function verifyFreelancerConnectedAccount($formdata, $userId, $file) {
    	try {
    		//echo "<pre>";print_r($formdata);

		    if(isset($file) && empty($file['name'])){
		    	throw new exception("Required identity document");
		    }
    		$ssn = substr($formdata['ssn'], -4);
//echo "<pre>";print_r($ssn);die;

			// Use Stripe's library to make requests...
    		$dob = explode('-',$formdata['dob']);
			require_once '../stripe/stripe/stripe-php/init.php'; 
			
			$stripe = new \Stripe\StripeClient('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'
			);

			$query = "SELECT stripe_account_id FROM `user_details` WHERE user_id = '".$userId."'";
			$result = dbconfig::run($query);  
		   	if(!$result) {
			   throw new exception("Server not responde!");
		   	}
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);					
		    } 	

	    	$document = $stripe->files->create([
			  'purpose' => 'identity_document',
			  'file' => fopen($file['tmp_name'], 'r'),
			], [
			  'stripe_account' => $row['stripe_account_id'],
			]);	


			$account = $stripe->accounts->update(
			    $row['stripe_account_id'],
			    [
	    		'individual' => [			  
				    'dob' =>[
			            'day' => $dob[2],
			            'month' => $dob[1], 
			            'year' => $dob[0],
			           ],	
			         //'id_number' =>  $formdata['ssn'],	         
			        'ssn_last_4' => $ssn,	
		             'verification' => [
				       		'document' => [
				       			'front' => $document->id
				       		],
			       	]			      
				],	
			    'tos_acceptance' => [
				      'date' => time(),
				      'ip' => $_SERVER['REMOTE_ADDR'], // Assumes you're not using a proxy
				 ],	    
			]);	   

   					   //echo "<pre>";print_r($account);die;
		
			$data = array('status'=>'success', 'msg'=>"Freelancer Connect Account Has Been Created.", 'result'=>'');
		}catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}catch(\Stripe\Exception\CardException $e) {
		  // Since it's a decline, \Stripe\Exception\CardException will be caught
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} catch (\Stripe\Exception\RateLimitException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Too many requests made to the API too quickly
		} catch (\Stripe\Exception\InvalidRequestException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Exception\AuthenticationException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		} catch (\Stripe\Exception\ApiConnectionException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Network communication with Stripe failed
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());

		  // Something else happened, completely unrelated to Stripe
		}finally {  
	        return $data;
	    }  
    }


    public static function addCardToFreelancerAccount($token, $userId) {
    	try {    
			$resultSet = array();

			//echo "<pre>";print_r($token);die;  	
				// Use Stripe's library to make requests...
			require_once '../stripe/stripe/stripe-php/init.php';  // Include Stripe PHP library 
			
			$stripe = new \Stripe\StripeClient('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'
			);

			$query = "SELECT stripe_account_id FROM `user_details` WHERE user_id = '".$userId."'";
			$result = dbconfig::run($query);  
		   	if(!$result) {
			   throw new exception("Stripe Server not responde!");
		   	}
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);					
		    } 
				
			$card = $stripe->accounts->createExternalAccount(
				$row['stripe_account_id'],
			 	['external_account' => $token['id']]
			);

			//echo "<pre>";print_r($card);die; 
			$data = array('status'=>'success', 'msg'=>"Your payment has charged successfull.", 'result'=>$card->id);
		} catch(\Stripe\Exception\CardException $e) {
		  // Since it's a decline, \Stripe\Exception\CardException will be caught
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} catch (\Stripe\Exception\RateLimitException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Too many requests made to the API too quickly
		} catch (\Stripe\Exception\InvalidRequestException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Exception\AuthenticationException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		} catch (\Stripe\Exception\ApiConnectionException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Network communication with Stripe failed
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Something else happened, completely unrelated to Stripe
		}finally {  
	        return $data;

	    }  		 
    }

    public static function deleteCardToFreelancerAccount($id, $userId) {  
    	try {    
			$resultSet = array();

			//echo "<pre>";print_r($token);die;  	
				// Use Stripe's library to make requests...
			require_once '../stripe/stripe/stripe-php/init.php';  // Include Stripe PHP library 
			
			$stripe = new \Stripe\StripeClient('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'
			);	

			$query = "SELECT d.stripe_account_id, c.stripe_card_id FROM `user_details` d INNER JOIN `user_card` c ON(d.user_id = c.user_id AND c.c_id = '".$id."') WHERE d.user_id = '".$userId."'";
			$result = dbconfig::run($query);  
		   	if(!$result) {
			   throw new exception("Stripe server not responde!");
		   	}
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);					
		    } 	
			
		    $card = $stripe->accounts->deleteExternalAccount(
			   $row['stripe_account_id'],
			   $row['stripe_card_id'],
			  []
			);

			//echo "<pre>";print_r($card);die; 
			$data = array('status'=>'success', 'msg'=>"Your payment has charged successfull.", 'result'=>'');
		} catch(\Stripe\Exception\CardException $e) {
		  // Since it's a decline, \Stripe\Exception\CardException will be caught
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} catch (\Stripe\Exception\RateLimitException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Too many requests made to the API too quickly
		} catch (\Stripe\Exception\InvalidRequestException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Exception\AuthenticationException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		} catch (\Stripe\Exception\ApiConnectionException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Network communication with Stripe failed
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Something else happened, completely unrelated to Stripe
		}finally {  
	        return $data;

	    }  		 
    }


     public static function deleteCardToEmployerCustomer($id, $userId) {  
    	try {    
			$resultSet = array();

			//echo "<pre>";print_r($token);die;  	
				// Use Stripe's library to make requests...
			require_once '../stripe/stripe/stripe-php/init.php';  // Include Stripe PHP library 
			
			$stripe = new \Stripe\StripeClient('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'
			);

			$query = "SELECT d.stripe_customer_id, c.stripe_card_id FROM `user_details` d INNER JOIN `user_card` c ON(d.user_id = c.user_id AND c.c_id = '".$id."') WHERE d.user_id = '".$userId."'";
			$result = dbconfig::run($query);  
		   	if(!$result) {
			   throw new exception("Stripe server not responde!");
		   	}
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);					
		    } 			

			$card =  $stripe->customers->deleteSource(
			  $row['stripe_customer_id'],
			   $row['stripe_card_id'],
			  []
			);

			//echo "<pre>";print_r($card);die; 
			$data = array('status'=>'success', 'msg'=>"Your card has removed successfull.", 'result'=>'');
		} catch(\Stripe\Exception\CardException $e) {
		  // Since it's a decline, \Stripe\Exception\CardException will be caught
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} catch (\Stripe\Exception\RateLimitException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Too many requests made to the API too quickly
		} catch (\Stripe\Exception\InvalidRequestException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Exception\AuthenticationException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		} catch (\Stripe\Exception\ApiConnectionException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Network communication with Stripe failed
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Something else happened, completely unrelated to Stripe
		}finally {  
	        return $data;

	    }  		 
    }



    public static function setDefaultCardToAccount($id, $userId) {
    	try {    
			$resultSet = array();
				// Use Stripe's library to make requests...
			require_once '../stripe/stripe/stripe-php/init.php';  // Include Stripe PHP library 
			
			$stripe = new \Stripe\StripeClient('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'
			);
			
			$query = "SELECT d.stripe_account_id, c.stripe_card_id FROM `user_details` d INNER JOIN `user_card` c ON(d.user_id = c.user_id AND c.c_id = '".$id."') WHERE d.user_id = '".$userId."'";
			$result = dbconfig::run($query);  
		   	if(!$result) {
			   throw new exception("Stripe server not responde!");
		   	}
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);					
		    } 		 	

			$card = $stripe->accounts->updateExternalAccount(
			   $row['stripe_account_id'],
			   $row['stripe_card_id'],
			  ['default_for_currency' => true]
			);   
			
			$data = array('status'=>'success', 'msg'=>"Connectec Account Default card set successfully.", 'result'=>'');
		} catch(\Stripe\Exception\CardException $e) {
		  // Since it's a decline, \Stripe\Exception\CardException will be caught
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} catch (\Stripe\Exception\RateLimitException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Too many requests made to the API too quickly
		} catch (\Stripe\Exception\InvalidRequestException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Exception\AuthenticationException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		} catch (\Stripe\Exception\ApiConnectionException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Network communication with Stripe failed
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Something else happened, completely unrelated to Stripe
		}finally {  
	        return $data;

	    }  		 
    }

   public static function makeCardPayment($formdata) {
    	try {    
			$resultSet = array(); 	
				// Use Stripe's library to make requests...
			require_once '../stripe/stripe/stripe-php/init.php';  // Include Stripe PHP library 
			
			$stripe = new \Stripe\StripeClient('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'
			);			
			$amount = base64_decode($formdata['a'])*100;
			$charge = $stripe->charges->create([
			 	'amount'   => $amount,        
				'currency' => 'usd',        
				'customer' => base64_decode($formdata['cId']),
				'transfer_data' => [
					'destination' => base64_decode($formdata['aId']), 

				],     
				'application_fee_amount' => (($amount/ 100) * (STRIPE_PROCESSING_FEES + STRIPE_APPLICATION_FEES)) + STRIPE_TRANSACTION_FEES
			]);

			$resultSet['cId'] = $charge->id;
			$resultSet['amount'] = $amount/100;
			$resultSet['eId'] = base64_decode($formdata['eId']);
			$resultSet['fId'] = base64_decode($formdata['fId']);
			$resultSet['jId'] = base64_decode($formdata['jId']);

			$data = array('status'=>'success', 'msg'=>"Your payment has charged successfull.", 'result'=>$resultSet);
		} catch(\Stripe\Exception\CardException $e) {
		  // Since it's a decline, \Stripe\Exception\CardException will be caught
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} catch (\Stripe\Exception\RateLimitException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Too many requests made to the API too quickly
		} catch (\Stripe\Exception\InvalidRequestException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Exception\AuthenticationException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		} catch (\Stripe\Exception\ApiConnectionException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Network communication with Stripe failed
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Something else happened, completely unrelated to Stripe
		}finally {  
	        return $data;

	    }  		 
    }


    public static function addEmployerAsCustomer($email) {
    	try {
				// Use Stripe's library to make requests...
			require_once '../stripe/stripe/stripe-php/init.php'; 
		
			$stripe = new \Stripe\StripeClient('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'
			);		

			$customer = $stripe->customers->create([
			  'description' => 'Paymyprice employer',
			  'email' => $email,	
			]);

			$data = array('status'=>'success', 'msg'=>"Customer create successfully.", 'result'=>$customer->id);
		} catch(\Stripe\Exception\CardException $e) {
		  // Since it's a decline, \Stripe\Exception\CardException will be caught
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} catch (\Stripe\Exception\RateLimitException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Too many requests made to the API too quickly
		} catch (\Stripe\Exception\InvalidRequestException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Exception\AuthenticationException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		} catch (\Stripe\Exception\ApiConnectionException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Network communication with Stripe failed
		} catch (\Stripe\Exception\ApiErrorException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Something else happened, completely unrelated to Stripe
		}finally {  
	        return $data;
	    } 
    }

    public static function addCardToEmployerCustomer($token, $userId) {
    	try {
				// Use Stripe's library to make requests...
			require_once '../stripe/stripe/stripe-php/init.php'; 
		
			$stripe = new \Stripe\StripeClient('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'
			);

			$query = "SELECT stripe_customer_id FROM `user_details` WHERE user_id = '".$userId."'";
			$result = dbconfig::run($query);  
		   	if(!$result) {
			   throw new exception("Server not responde!");
		   	}
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);					
		    } 
		
			$card = $stripe->customers->createSource(
			  $row['stripe_customer_id'],
			  ['source' => $token['id']]
			);
			
			$data = array('status'=>'success', 'msg'=>"Customer create successfully.", 'result'=>$card->id);
		} catch(\Stripe\Exception\CardException $e) {
		  // Since it's a decline, \Stripe\Exception\CardException will be caught
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} catch (\Stripe\Exception\RateLimitException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Too many requests made to the API too quickly
		} catch (\Stripe\Exception\InvalidRequestException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Exception\AuthenticationException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		} catch (\Stripe\Exception\ApiConnectionException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Network communication with Stripe failed
		} catch (\Stripe\Exception\ApiErrorException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Something else happened, completely unrelated to Stripe
		}finally {  
	        return $data;
	    } 
    }



    public static function setDefaultCardToCustomer($id, $userId) {
    	try {
				// Use Stripe's library to make requests...
			require_once '../stripe/stripe/stripe-php/init.php'; 
		
			$stripe = new \Stripe\StripeClient('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'
			);

			$query = "SELECT d.stripe_customer_id, c.stripe_card_id FROM `user_details` d INNER JOIN `user_card` c ON(d.user_id = c.user_id AND c.c_id = '".$id."') WHERE d.user_id = '".$userId."'";
			$result = dbconfig::run($query);  
		   	if(!$result) {
			   throw new exception("Server not responde!");
		   	}
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);					
		    } 		

		   $customer = $stripe->customers->update(
			 $row['stripe_customer_id'],
			  ['default_source' => $row['stripe_card_id']]
			);		   
			
			$data = array('status'=>'success', 'msg'=>"Customer Default card set successfully.", 'result'=>'');
		} catch(\Stripe\Exception\CardException $e) {
		  // Since it's a decline, \Stripe\Exception\CardException will be caught
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} catch (\Stripe\Exception\RateLimitException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Too many requests made to the API too quickly
		} catch (\Stripe\Exception\InvalidRequestException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Exception\AuthenticationException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		} catch (\Stripe\Exception\ApiConnectionException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Network communication with Stripe failed
		} catch (\Stripe\Exception\ApiErrorException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Something else happened, completely unrelated to Stripe
		}finally {  
	        return $data;
	    } 
    }


    public static function getConnectAccountInfo($userId) {
    	try {
				// Use Stripe's library to make requests...
			require_once '../stripe/stripe/stripe-php/init.php'; 

			$stripe = new \Stripe\StripeClient('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'
			);
			

			$query = "SELECT stripe_account_id FROM `user_details` WHERE user_id = '".$userId."'";
			$result = dbconfig::run($query);  
		   	if(!$result) {
			   throw new exception("Stripe Server not responde!");
		   	}
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);					
		    } 
		
			$account = $stripe->accounts->retrieve(
			  $row['stripe_account_id'],
			  []
			);
		   

			$data = array('status'=>'success', 'msg'=>"Account Fetched successfully.", 'result'=>$account);
		} catch(\Stripe\Exception\CardException $e) {
		  // Since it's a decline, \Stripe\Exception\CardException will be caught
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} catch (\Stripe\Exception\RateLimitException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Too many requests made to the API too quickly
		} catch (\Stripe\Exception\InvalidRequestException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Exception\AuthenticationException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		} catch (\Stripe\Exception\ApiConnectionException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Network communication with Stripe failed
		} catch (\Stripe\Exception\ApiErrorException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Something else happened, completely unrelated to Stripe
		}finally {  
	        return $data;
	    } 
    }


    public static function getConnectAccountBalance($userId) {
    	try {
				// Use Stripe's library to make requests...
			require_once '../stripe/stripe/stripe-php/init.php'; 

			\Stripe\Stripe::setApiKey('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');

			$query = "SELECT stripe_account_id FROM `user_details` WHERE user_id = '".$userId."'";
			$result = dbconfig::run($query);  
		   	if(!$result) {
			   throw new exception("Stripe Server not responde!");
		   	}
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);					
		    } 
				

			$balance = \Stripe\Balance::retrieve([
			  'stripe_account' => $row['stripe_account_id']
			]);	
		

			$data = array('status'=>'success', 'msg'=>"Balance Fetched successfully.", 'result'=>$balance);
		} catch(\Stripe\Exception\CardException $e) {
		  // Since it's a decline, \Stripe\Exception\CardException will be caught
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} catch (\Stripe\Exception\RateLimitException $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Too many requests made to the API too quickly
		} catch (\Stripe\Exception\InvalidRequestException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Invalid parameters were supplied to Stripe's API
		} catch (\Stripe\Exception\AuthenticationException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		} catch (\Stripe\Exception\ApiConnectionException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Network communication with Stripe failed
		} catch (\Stripe\Exception\ApiErrorException $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
		  // Something else happened, completely unrelated to Stripe
		}finally {  
	        return $data;
	    } 
    }


  

 }



			 
