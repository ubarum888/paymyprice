<?php 

require_once("config.php");

class USER extends dbconfig { 

   public static $data;

   function __construct() {

     parent::__construct();

   }
  

	public static function forgotPassword($email) {
	    try{
	    	if($email == "") {
	    		throw new exception("Email field is required");
	    	}
	        $query = "SELECT raw_password FROM `users` WHERE email = '".$email."' AND status = 1 LIMIT 1";
	        $result = dbconfig::run($query);	   

	        if(!$result) {
	          throw new exception("Server not respond");
	        }
	        if(mysqli_num_rows($result) == 0){
				throw new exception("An Email not found in our record");
			}
			$row = mysqli_fetch_assoc($result);	
			$check = self::userForgotPasswordEmail($row['raw_password'], $email); 
			$data = array('status'=>'success', 'msg'=>"We sent a password recovery details to your email.", 'result'=>'');
	    }catch (Exception $e) { 
	       $data = array('status'=>'error', 'msg'=>$e->getMessage());
	    }finally {  
	        return $data;
	    } 
    }

     

  // login function

	public static function login($formdata){  
		try {
			$query = "SELECT * FROM `users` WHERE email = '".$formdata['email']."' AND password = '".md5($formdata['password'])."' LIMIT 1";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not respond. Please try again !");
			}

			if(mysqli_num_rows($result) > 0){
				$resultSet = mysqli_fetch_assoc($result);
				if($resultSet['status'] == 0){
					throw new exception("Dear Customer, Your Account has not approved yet.");
				}

			}else{
				throw new exception("Username/Password Not exist or Invalid !");
			}	
			$data = array('status'=>'success', 'msg'=>"You Have Logged In Successfully.", 'result'=>$resultSet);
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;    
		}
	}	

	public static function sentSmsToUser($phone, $msg) {
	    try{
	    	$data = array("to" => $phone, "from" => SMS_FROM, 'message'=> $msg);                                                                    
			$data_string = json_encode($data);                                                                                   

			$ch = curl_init('https://roor.gynetix.com/standard/api/post/manualTXT/key/5b883eed5ea3e441bb634976d7f36736/response/json');                                                                      
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			    'Content-Type: application/json',                                                                                
			    'Content-Length: ' . strlen($data_string))                                                                       
			);                                                                                                                   

			$result = curl_exec($ch);
			$sms_sent = json_decode($result,true);
			if($sms_sent['status'] == 'failed'){
				throw new exception($sms_sent['message']);
			}
		
			$data = array('status'=>'success', 'msg'=>"Sms sent successfully to provided phone number.", 'result'=>'');
	    }catch (Exception $e) { 
	       $data = array('status'=>'error', 'msg'=>$e->getMessage());
	    }finally {  
	        return $data;
	    } 
    }



	public static function freelancerSignUp($formdata, $accountId){
		try{	
			$line1 = $formdata['street'];	
			$line2 = $formdata['route'];	
			$city = $formdata['city'];
		    unset($formdata['optional_fields']);
		    unset($formdata['city']);
		    unset($formdata['street']);
		    unset($formdata['route']);
		   if(in_array("", $formdata, true)){
			 throw new exception("All fields are required!");
		   }
			$check = self::checkUserExist($formdata['email']); 
			if($check['status'] == 'error') {
	          throw new exception($check['msg']);
			}	
			$password = random_gen(8);	

			$query = "INSERT INTO `users` (fname, lname, password, raw_password, email, phone, address, address_line1, address_line2, city, country, state, zip, account_type)VALUES('".$formdata['fname']."', '".$formdata['lname']."', '".md5($password)."', '".$password."', '".$formdata['email']."', '".$formdata['phone']."', '".$formdata['address']."', '".$line1."', '".$line2."', '".$city."', '".$formdata['country']."', '".$formdata['state']."', '".$formdata['zip']."', 2)";
			$lastInsertId = dbconfig::insertrun($query);
			if(!$lastInsertId) {
				throw new exception("No server response!");
			}
			$query = "INSERT INTO `user_details` (user_id, category_list, stripe_account_id, profile_viewed_by) VALUES ('".$lastInsertId."', '".implode(',', $formdata['category'])."', '".$accountId."', '0')";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not respond. Please try again !");
			}			

			$emailSend = self::userSignupEmail($lastInsertId, $formdata['email'], $formdata['fname'].' '.$formdata['lname'], $password);
			$smsSend = self::sentSmsToUser($formdata['phone'], 'Dear '.ucfirst($formdata['fname']).' '.ucfirst($formdata['lname']).' Your PayMyPrice.com account has been registered and is pending approval.');

			$formdata['user_id'] = $lastInsertId;

			$data = array('status'=>'success', 'msg'=>"Your account has been registered. Your account has on hold for approved.", 'result'=>$formdata);			

		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());

		}finally{
			return $data;
		}
	}



	public static function employerSignUp($formdata, $customerId){

		try{
			//echo "<pre>";print_r($formdata);die; 
			unset($formdata['optional_fields']);
			unset($formdata['city']);
		    unset($formdata['street']);
		    unset($formdata['route']);
			if(in_array("", $formdata, true)){
			  throw new exception("All fields are required!");
			}				

			$check = self::checkUserExist($formdata['email']); 
			if($check['status'] == 'error') {
	          throw new exception($check['msg']);
			}	

			$password = random_gen(8);	
			$query = "INSERT INTO `users` (fname, lname, password, raw_password, email, phone, address, country, state, zip, account_type)VALUES('".$formdata['fname']."', '".$formdata['lname']."', '".md5($password)."', '".$password."', '".$formdata['email']."', '".$formdata['phone']."', '".$formdata['address']."', '".$formdata['country']."', '".$formdata['state']."', '".$formdata['zip']."', 1)";
			$lastInsertId = dbconfig::insertrun($query);	
			if(!$lastInsertId) {
				throw new exception("No server response!");
			}	

			$query = "INSERT INTO `user_details` (user_id, category_list, subcategory_list, price, stripe_customer_id) VALUES ('".$lastInsertId."', '".implode(',', $formdata['category'])."', '".implode(',', $formdata['subcategory'])."', '".$formdata['price']."', '".$customerId."')";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not respond. Please try again !");
			}

			$emailSend = self::userSignupEmail($lastInsertId, $formdata['email'], $formdata['fname'].' '.$formdata['lname'], $password); 
			$smsSend = self::sentSmsToUser($formdata['phone'], 'Dear '.ucfirst($formdata['fname']).' '.ucfirst($formdata['lname']).' Your PayMyPrice.com account has been registered and is pending approval.');
			$data = array('status'=>'success', 'msg'=>"Your account has been registered. Your account has on hold for approved.", 'result'=>'');		
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}


	public static function updateAccount($formdata, $userId, $file){    
		try{			
			$logo_img = '';
			$photo_fetch = '';
			unset($formdata['optional_fields']);
			if(in_array("", $formdata, true)){
			  throw new exception("All fields are required!");
			}	
		
			if($_SESSION['user']['result']['account_type'] == 2){	
				

				$query = "UPDATE `users` SET fname = '".$formdata['fname']."', lname = '".$formdata['lname']."', phone = '".$formdata['phone']."', address = '".$formdata['address']."', address_line1 = '".$formdata['address_line1']."', address_line2 = '".$formdata['address_line2']."', city = '".$formdata['city']."', country = '".$formdata['country']."', state = '".$formdata['state']."', zip = '".$formdata['zip']."'  WHERE id = '".$userId."'";
				$result = dbconfig::run($query);  
				if(!$result) {
					throw new exception("Server not respond. Please try again !");
				}
			}else if($_SESSION['user']['result']['account_type'] == 1){

				if(!empty($file['name']) ){
					$logo_fetch = self::UploadLogo($file);	  
					if($logo_fetch['status'] == 'error') {
						throw new exception("Company Logo not uploaded");
					} 
					$logo_img = ", c_logo = '".$logo_fetch['result']."' "; 
				}
				
				$query = "UPDATE `users` SET fname = '".$formdata['fname']."', lname = '".$formdata['lname']."', phone = '".$formdata['phone']."', address = '".$formdata['address']."', country = '".$formdata['country']."', state = '".$formdata['state']."', zip = '".$formdata['zip']."' ".$logo_img." WHERE id = '".$userId."'";
				$result = dbconfig::run($query);  
				if(!$result) {
					throw new exception("Server not respond. Please try again !");
				}
			}

			$data = array('status'=>'success', 'msg'=>"Your Account Detail Has Been Updated.", 'result'=>'');		
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{  
			return $data;  
		}  
	}


	public static function updateConnectedAccount($formdata, $userId, $file){    
		try{
			$photo_image = '';
			unset($formdata['optional_fields']);
			if(in_array("", $formdata, true)){
			  throw new exception("All fields are required!");
			}				
			
			if(!empty($file['name']) ){  
				$photo_fetch = self::PersonalIdUpload($file);	  

				if($photo_fetch['status'] == 'error') {
					throw new exception("Personal Id not uploaded");  
				} 
				$photo_image = ", personal_id = '".$photo_fetch['result']."' ";
			}
			$ssn = substr($formdata['ssn'], -4);
			$query = "UPDATE `user_details` SET ssn = '".$ssn."', dob = '".$formdata['dob']."' ".$photo_image." WHERE user_id = '".$userId."'";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not respond 1. Please try again !");
			}	

			$data = array('status'=>'success', 'msg'=>"Your Connected Account Information Has Been Updated.", 'result'=>'');		
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{  
			return $data;  
		}  
	}
	    

	public static function addReview($formdata){
		try{
			//echo"<pre>";print_r($formdata);die;
			unset($formdata['optional_fields']);
			if(in_array("", $formdata, true)){
			  throw new exception("All fields are required!");
			}		
			$message = mysqli_real_escape_string(self::$con, $formdata['message']);
			$query = "INSERT INTO `reviews` (post_id, review_from, review_to, dob, dot, message, rating, review_date) VALUES ('".base64_decode($formdata['post_id'])."', '".base64_decode($formdata['r_from'])."', '".base64_decode($formdata['r_to'])."', '".$formdata['dob']."', '".$formdata['dot']."', '".$message."', '".$formdata['rating']."', '".date('Y-m-d H:i:s')."')";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not respond. Please try again !");
			}
			$notify = self::addNotification(base64_decode($formdata['r_from']), base64_decode($formdata['r_to']), base64_decode($formdata['post_id']), 'review', 'review for', $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']);
			$data = array('status'=>'success', 'msg'=>"Your Review Has Been Submitted.", 'result'=>'');	
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}  
	}

	public static function updateReview($formdata){  
		try{
			//echo"<pre>";print_r($formdata);die;
			unset($formdata['optional_fields']);
			if(in_array("", $formdata, true)){
			  throw new exception("All fields are required!");
			}		
			$message = mysqli_real_escape_string(self::$con, $formdata['message']);
			$query = "UPDATE `reviews` SET dob = '".$formdata['dob']."', dot = '".$formdata['dot']."', message = '".$message."', rating = '".$formdata['rating']."', review_date = '".date('Y-m-d H:i:s')."' WHERE post_id = '".base64_decode($formdata['post_id'])."' AND review_from = '".base64_decode($formdata['r_from'])."'";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not respond. Please try again !");
			}
			$notify = self::addNotification(base64_decode($formdata['r_from']), base64_decode($formdata['r_to']), base64_decode($formdata['post_id']), 'review', 'changed review for', $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']);
			$data = array('status'=>'success', 'msg'=>"Your Review Has Been Changed.", 'result'=>'');	
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}


	public static function getUserNoteById($id){
		try{
		   $resultSet = array();
		 
		   $query = "SELECT * FROM `notes` WHERE id = '".base64_decode($id)."' LIMIT 1 ORDER BY id DESC";
		   $result = dbconfig::run($query);  
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		    if(mysqli_num_rows($result) > 0){
				$resultSet = mysqli_fetch_assoc($result);					
		    } 
		   $data = array('status'=>'success', 'msg'=>"User Card Detail Has Been Fetched.", 'result'=>$resultSet);
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}

	public static function addNote($formdata, $userId){
		try{
			//echo"<pre>";print_r($formdata);die;
			unset($formdata['optional_fields']);
			if(in_array("", $formdata, true)){
			  throw new exception("All fields are required!");
			}		
			$note = mysqli_real_escape_string(self::$con, $formdata['note']);
			$query = "INSERT INTO `notes` (user_id, note, priority, created_date) VALUES ('".$userId."', '".$note."', '".$formdata['priority']."', '".date('Y-m-d H:i:s')."')";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not respond. Please try again !");
			}

			$data = array('status'=>'success', 'msg'=>"Your Note Has Been Added.", 'result'=>$formdata);	
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}

	public static function deleteNote($id){
		try { 			  
		   $query = "DELETE FROM `notes` WHERE id = '".base64_decode($id)."' ";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }		   
		   $data = array('status'=>'success', 'msg'=>"Your Note Has Been Removed.");
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    } 

	public static function updateNote($formdata){
		try{
			//echo"<pre>";print_r($formdata);die;
			unset($formdata['optional_fields']);
			if(in_array("", $formdata, true)){
			  throw new exception("All fields are required!");
			}		
			$note = mysqli_real_escape_string(self::$con, $formdata['note']);
			$query = "UPDATE `notes` SET note = '".$note."', priority = '".$formdata['priority']."', created_date = '".date('Y-m-d H:i:s')."' WHERE id = '".base64_decode($formdata['n_id'])."'";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not respond. Please try again !");
			}
			$data = array('status'=>'success', 'msg'=>"Your Note Has Been Added.", 'result'=>$formdata);	
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}


	public static function getAllNotesByUserId($formdata){
		try{
			$resultSet = array();
			$query = "SELECT * FROM `notes` WHERE user_id = '".base64_decode($formdata['scroll_with']['user_id'])."' ORDER BY id DESC LIMIT ".$formdata['scroll_with']["start"].", ".$formdata['scroll_with']["limit"]."";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not respond. Please try again !");
			}
			$count = mysqli_num_rows($result);
		    if($count > 0){
				while($row = mysqli_fetch_assoc($result)){					
					$row['encryptId'] = base64_encode($row['id']);
					$resultSet[] =  $row;
				}
			}	
			
			$data = array('status'=>'success', 'msg'=>"Your Note Has Been Fetched.", 'result'=>$resultSet, 'count'=>$count);	
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}
  
	public static function addCard($formdata, $cardId, $userId){
		try{
				
			$resultSet = array(); 

			$query = "SELECT c_id FROM `user_card` WHERE user_id = '".$userId."'";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not respond. Please try again !");
			}
			
		    if(mysqli_num_rows($result) > 0){
		    	$query = "INSERT INTO `user_card` (user_id, stripe_card_id, account_no, expire_card, brand, created_date) VALUES ('".$userId."', '".$cardId."', '".$formdata['token']['card']['last4']."', '".$formdata['token']['card']['exp_month']."/".$formdata['token']['card']['exp_year']."', '".$formdata['token']['card']['brand']."', '".date('Y-m-d H:i:s')."')";
			}else{
				$query = "INSERT INTO `user_card` (user_id, stripe_card_id, account_no, expire_card, brand, created_date, status) VALUES ('".$userId."', '".$cardId."', '".$formdata['token']['card']['last4']."', '".$formdata['token']['card']['exp_month']."/".$formdata['token']['card']['exp_year']."', '".$formdata['token']['card']['brand']."', '".date('Y-m-d H:i:s')."', 1)";
			}
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not respond 1. Please try again !");
			}
		 
			$data = array('status'=>'success', 'msg'=>"Your Card Has Been Linked.", 'result'=>'');
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}

	public static function deleteCard($id){
		try{
			
			$resultSet = array(); 

			$query = "DELETE FROM `user_card` WHERE c_id = '".base64_decode($id)."'";
			$result = dbconfig::run($query); 
	        if(!$result) {
	          throw new exception("Server not respond");
	        }
			$data = array('status'=>'success', 'msg'=>"Your Card Has Been Unlinked.", 'result'=>$sendToClient);
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}


	public static function updateCard($id, $userId){
		try{		
			$query = "UPDATE `user_card` SET status = 0 WHERE user_id = '".$userId."'";
			$result = dbconfig::run($query); 
	        if(!$result) {
	          throw new exception("Server not respond");
	        }

	        $query = "UPDATE `user_card` SET status = 1 WHERE c_id = '".$id."'";
			$result = dbconfig::run($query); 
	        if(!$result) {
	          throw new exception("Server not respond");
	        }
			
			$data = array('status'=>'success', 'msg'=>"Your Default Card Has Been Set.", 'result'=>'');
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}

	public static function getAllUserCards($userId){
		try{				
			$resultSet = array(); 
			$query = "SELECT * FROM `user_card` WHERE user_id = '".$userId."' ORDER BY c_id DESC";
			$result = dbconfig::run($query); 
	        if(!$result) {
	          throw new exception("Server not respond");
	        }
	        if(mysqli_num_rows($result) > 0){
		   		while($row = mysqli_fetch_assoc($result)){	
		   			$resultSet[] = $row;	
		   		}	
			}	
			
			$data = array('status'=>'success', 'msg'=>"Your Card Has Been Fetched.", 'result'=>$resultSet);
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}
	

	public static function addDetails($formdata, $userId){
		try{
			unset($formdata['optional_fields']);
			if(in_array("", $formdata, true)){  
			  throw new exception("All fields are required!");
			}

			$query = "SELECT d_id FROM `user_details` WHERE user_id = '".$userId."'";
	        $result = dbconfig::run($query); 
	        if(!$result) {
	          throw new exception("Server does not respond");
	        }

	        $tagline = mysqli_real_escape_string(self::$con, $formdata['tagline']);
		   	$yourSelf = mysqli_real_escape_string(self::$con, $formdata['your_self']);

	        if(mysqli_num_rows($result) == 0){
				$query = "INSERT INTO `user_details` (user_id, hourly_rate, tags, tagline, nationality, your_self) VALUES ('".$userId."', '".$formdata['hourly_rate']."', '".$formdata['tags']."', '".$tagline."', '".$formdata['nationality']."', '".$yourSelf."')";
			}else{
				$query = "UPDATE `user_details` SET hourly_rate = '".$formdata['hourly_rate']."', tags = '".$formdata['tags']."', tagline = '".$tagline."',  nationality = '".$formdata['nationality']."', your_self = '".$yourSelf."' WHERE user_id = '".$userId."'";	
			}
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not respond. Please try again !");
			}
			$data = array('status'=>'success', 'msg'=>"Your Profile Has Been Updated.", 'result'=>'');
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}



	



	public static function changePassword($formdata, $userId){
		try{
			unset($formdata['optional_fields']);
			if(in_array("", $formdata, true)){
			  throw new exception("All fields are required!");
			}	

			$query = "SELECT raw_password FROM `users` WHERE id='".$userId."'";
			$result = dbconfig::run($query);	
			if(!$result) {
				throw new exception("No server response!");
			}

			if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);
			}

			if($row['raw_password'] !== $formdata['current_pass'])	{
				throw new exception("Current password not match.");
			}	

			$uppercase = preg_match('@[A-Z]@', $formdata['new_pass']);
			$lowercase = preg_match('@[a-z]@', $formdata['new_pass']);
			$number    = preg_match('@[0-9]@', $formdata['new_pass']);

			if(!$uppercase || !$lowercase || !$number ||  strlen($formdata['new_pass']) < 8) {
				throw new exception("Password should be at least 8 characters in length and should include at least one upper case letter, one number.");
			}  

			if($formdata['new_pass'] !== $formdata['confirm_pass'])	{
				throw new exception("Confirm password not match.");
			}			

			$query = "UPDATE `users` SET password = '".md5($formdata['new_pass'])."', raw_password = '".$formdata['new_pass']."' WHERE id = '".$userId."'";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("No server response!");
			}					

			$data = array('status'=>'success', 'msg'=>"Your Password Has Been Changed.", 'result'=>'');	
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}	



	 // Check if user already exist

   public static function checkUserExist($email){

     try {     

		$query = "SELECT id FROM `users` WHERE email = '".$email."'"; 

		$result = dbconfig::run($query);

		if(!$result) {

         throw new exception("Server not responde11!");

		}

		$count = mysqli_num_rows($result);

			if($count > 0){

				throw new exception("An email already exist in our record. Please register with another email.");

			} 

			$data = array('status'=>'success', 'msg'=>"", 'result'=>'');

		} catch (Exception $e) {

			$data = array('status'=>'error', 'msg'=>$e->getMessage());

		} finally {

			return $data;

		}

   }



    public static function userVerify($id){
     try {   
	     	// $query = "UPDATE `users` SET status = 1 WHERE id='".$id."'";       

	      //   $result = dbconfig::run($query);

	      //   if(!$result) {

	      //     throw new exception("Server not respondeq!");

	      //   }  
			$query = "SELECT * FROM `users` WHERE id ='".$id."' AND status = 1 LIMIT 1";
		   	$result = dbconfig::run($query);
	        if(mysqli_num_rows($result) > 0){
			   $resultSet = mysqli_fetch_assoc($result);
			   $data = array('status'=>'success', 'msg'=>"User detail fetched successfully.", 'result'=>$resultSet);	
			}else{
	          $data = array('status'=>'error', 'msg'=>"username/Password Not exist !", 'result'=>array());
			} 
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }


   public static function jobPost($formdata){   
		try {   

			if($formdata['optional_fields'] !== ""){
				$optional = explode(',', $formdata['optional_fields']);
			}
			foreach ($formdata as $key=>$fields) {
				if(in_array($key, $optional, true)){
					unset($formdata[$key]);
			   }
			}
		    unset($formdata['optional_fields']);
		   if(in_array("", $formdata, true)){
			 throw new exception("All fields are required!");
		   }

		   $title = mysqli_real_escape_string(self::$con, $formdata['title']); 	
		   $description = mysqli_real_escape_string(self::$con, $formdata['description']);			

		   $query = "INSERT INTO `job_post` (user_id, title, description, job_type, category_id, sub_category_id, payment_type, payment_process, min_budget, max_budget, tags, viewed_by, created_date) VALUES ('".base64_decode($formdata['user_id'])."', '".$title."', '".$description."', '".$formdata['job_type']."', '".$formdata['category_id']."', '".$formdata['sub_category_id']."', '".$formdata['payment_type']."', '".$formdata['payment_process']."', '".$formdata['min_budget']."', '".$formdata['max_budget']."', '".$formdata['tags']."', '0', '".date('Y-m-d H:i:s')."')";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }	   

		   $data = array('status'=>'success', 'msg'=>"Your Job Has Been Posted.", 'result'=>'');
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
  }


   public static function updateJobPost($formdata){   
		try {   

			if($formdata['optional_fields'] !== ""){
				$optional = explode(',', $formdata['optional_fields']);
			}
			foreach ($formdata as $key=>$fields) {
				if(in_array($key, $optional, true)){
					unset($formdata[$key]);
			   }
			}
		    unset($formdata['optional_fields']);
		   if(in_array("", $formdata, true)){
			 throw new exception("All fields are required!");
		   }
 
		   $title = mysqli_real_escape_string(self::$con, $formdata['title']); 	
		   $description = mysqli_real_escape_string(self::$con, $formdata['description']);	

		   $query = "UPDATE `job_post` SET title = '".$title."', description = '".$description."', job_type = '".$formdata['job_type']."', category_id = '".$formdata['category_id']."', sub_category_id = '".$formdata['sub_category_id']."', payment_type = '".$formdata['payment_type']."', payment_process = '".$formdata['payment_process']."', min_budget = '".$formdata['min_budget']."', max_budget = '".$formdata['max_budget']."', tags = '".$formdata['tags']."', created_date = '".date('Y-m-d H:i:s')."' WHERE id = '".base64_decode($formdata['post_id'])."'";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }	   

		   $data = array('status'=>'success', 'msg'=>"Your Job Has Been Posted.", 'result'=>'');
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
  }


    public static function getJobListing($sort = NULL){

		try {  

		   $resultSet = array();

		   if(!empty($sort)) {

		   		$sorting = 'ORDER BY FIELD(p.status,'.$sort.') DESC';

		   }else{

		   		$sorting = 'ORDER BY id DESC';

		   }		  

		  $query = "SELECT j.*, p.status FROM  `job_post` j LEFT JOIN `job_proposals` p ON(j.id = p.job_post_id) ".$sorting." ";

		   $result = dbconfig::run($query);

		   if(!$result) {

			   throw new exception("No server response!");

		   }

		   if(mysqli_num_rows($result) > 0){

				while($row = mysqli_fetch_assoc($result)){				

					$row['time_ago'] = self::timeago($row['created_date']);

					$row['encryptId'] = base64_encode($row['id']);

					$resultSet[] =  $row;

				}

			}		   

		   $data = array('status'=>'success', 'msg'=>"Your Jobs Has Been fetched.", 'result'=>$resultSet);

	   } catch (Exception $e) {

		   $data = array('status'=>'error', 'msg'=>$e->getMessage());

	   } finally {

		   return $data;

	   }

    }

   public static function getFeedSetByEmployerId($userId){
		try { 
		   $resultSet = array();
		  $query = "SELECT j.category_id, j.tags t, c.category FROM `job_post` j INNER JOIN `categories` c ON(c.id = j.category_id) WHERE j.user_id = '".$userId."'";
		   //$query = "SELECT c.* FROM `categories` c LEFT JOIN `user_details` d ON find_in_set(c.id, d.category_list) WHERE d.user_id = '".$userId."'";   
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		    $count = mysqli_num_rows($result);  
		    if($count > 0){
				while($row = mysqli_fetch_assoc($result)){					
					$resultSet['category'][$row['category_id']] =  $row['category'];
					$tag[] =  explode(',', $row['t']);
				}
				$resultSet['tags'] = array_unique(call_user_func_array('array_merge', $tag));
				$resultSet['category_set'] = array_keys($resultSet['category']);
			}
		   $data = array('status'=>'success', 'msg'=>"Your Jobs Has Been fetched.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());  
	   } finally {  
		   return $data;
	   }
    }  

    public static function getAllProposalByPost($postId){
		try { 
		   $resultSet = array();
		   $query = "SELECT p.*, u.fname, u.lname, u.p_img, u.country, j.title, d.nationality, d.tagline, d.stripe_account_id, round(IFNULL(AVG(r.rating),0)) rate FROM `job_proposals` p INNER JOIN `users` u ON(u.id = p.user_id) INNER JOIN `user_details` d ON(u.id = d.user_id) INNER JOIN `job_post` j ON(p.job_post_id = j.id) LEFT JOIN `reviews` r ON(u.id = r.review_to) WHERE p.job_post_id = '".base64_decode($postId)."' GROUP BY p.id ORDER BY p.id DESC";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }		 
		    if(mysqli_num_rows($result) > 0){
				while($row = mysqli_fetch_assoc($result)){	
					if($row['id'] !== NULL){
						$resultSet[] =  $row;
					}					
				}
			}
		   $data = array('status'=>'success', 'msg'=>"Your Jobs Has Been fetched.", 'result'=>$resultSet, 'count'=>count($resultSet), 'query'=>$query);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());  
	   } finally {
		   return $data;
	   }
    }  


    public static function getAllProposalById($userId){
		try {  
		   $resultSet = array();
		   $status = array('reject', 'apply', 'accept');
		   $query = "SELECT COUNT(id) count, status FROM `job_proposals` WHERE user_id = '".$userId."' GROUP BY status";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }

		    if(mysqli_num_rows($result) > 0){
				while($row = mysqli_fetch_assoc($result)){	
					$resultSet[$status[$row['status']]] =  $row['count'];
				}
		    }		   

		   $data = array('status'=>'success', 'msg'=>"Fetched All Proposal.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());    
	   } finally {
		   return $data;
	   }
    }  

     
    public static function getUserCardDetailByUserId($userId){
		try {  

		   $resultSet = array();		 
		   $query = "SELECT * FROM `user_card` WHERE user_id = '".base64_decode($userId)."' AND status = 1 LIMIT 1";
		   $result = dbconfig::run($query);  
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		    if(mysqli_num_rows($result) > 0){
				$resultSet = mysqli_fetch_assoc($result);					
		    }	   

		   $data = array('status'=>'success', 'msg'=>"User Card Detail Has Been Fetched.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());  
	   } finally {
		   return $data;
	   }
    }  



    public static function getAllPostById($userId){

		try {  

		   $resultSet = array();		  

		   $status = array('close', 'open');

		   $query = "SELECT COUNT(id) count, post_status FROM `job_post` WHERE user_id = '".$userId."' GROUP BY post_status";

		   $result = dbconfig::run($query);

		   if(!$result) {

			   throw new exception("No server response!");

		   }

		    if(mysqli_num_rows($result) > 0){

				while($row = mysqli_fetch_assoc($result)){	

					$resultSet[$status[$row['post_status']]] =  $row['count'];

				}

		    }		   

		   $data = array('status'=>'success', 'msg'=>"Fetched All Proposal.", 'result'=>$resultSet);

	   } catch (Exception $e) {

		   $data = array('status'=>'error', 'msg'=>$e->getMessage());  

	   } finally {

		   return $data;

	   }

    }  

    public static function getBidDetailById($proposalId){  
		try {   
		   $resultSet = array();
		   $query = "SELECT * FROM `job_proposals` WHERE id = '".base64_decode($proposalId)."' LIMIT 1";
		   $result = dbconfig::run($query);
		   if(!$result) {  
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){  
				$resultSet = mysqli_fetch_assoc($result);
			}		   

		   $data = array('status'=>'success', 'msg'=>"Job Proposal Detail Has Been fetched.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }


    public static function getProposalDetailById($proposalId){
		try { 
		   $resultSet = array();
		   $query = "SELECT j.*, p.proposal, p.applied_date, p.status FROM `job_proposals` p INNER JOIN `job_post` j ON(j.id = p.job_post_id) WHERE p.id  = '".base64_decode($proposalId)."' LIMIT 1";
		   $result = dbconfig::run($query);
		   if(!$result) {  
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);
				$row['posted_on'] = date('d, M Y', strtotime($row['created_date']));
				$row['applied_on'] = date('d, M Y', strtotime($row['applied_date']));
				$resultSet = $row;
			}		   

		   $data = array('status'=>'success', 'msg'=>"Job Proposal Detail Has Been fetched.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }


    public static function getJobDetailsById($id, $userId){  
		try {  
		   $resultSet = array();		   	 

		 	$query = "SELECT j.*, u.fname, u.lname, u.country, u.state, u.email, u.p_img, u.c_logo, u.id client_id, d.nationality, p.status, p.proposal, p.minimal_rate, p.delivery_time, p.id pId, round(IFNULL(AVG(r.rating),0)) rate FROM `job_post` j INNER JOIN `users` u ON(u.id = j.user_id) INNER JOIN `user_details` d ON(u.id = d.user_id) LEFT JOIN `job_proposals` p ON(j.id = p.job_post_id AND p.user_id = '".$userId."' AND p.status != 0) LEFT JOIN `reviews` r ON(j.user_id = r.review_to) WHERE j.id = '".base64_decode($id)."' LIMIT 1"; 
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){
		   		$row = mysqli_fetch_assoc($result);	
		   		//$row['tags'] = '<span class="category-tags">'.$row['category'].'</span>';

		   		$row['time_ago'] = self::timeago($row['created_date']);
				$resultSet = $row;
			}	    

		   $data = array('status'=>'success', 'msg'=>"Your Jobs Has Been fetched.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }    

    public static function getFreelancerJobsDoneCount($id){
		try {  
		   $resultSet = array();		  
		   $query = "SELECT COUNT(id) done FROM `job_proposals` WHERE user_id = '".base64_decode($id)."' AND status = 5 GROUP BY user_id";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }

		   if(mysqli_num_rows($result) > 0){
		   		$row = mysqli_fetch_assoc($result);	
				$resultSet = $row['done'];
			}
		   $data = array('status'=>'success', 'msg'=>"Freelancer Profile Has Been fetched.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }

    } 
 

    public static function getFreelancerDetailsById($id, $userId){
		try {  
		   $resultSet = array();
		   $row['tags'] = "";
		   $query = "SELECT u.*, d.*, o.o_id, p.status bid_status FROM `users` u INNER JOIN `user_details` d ON(u.id = d.user_id AND u.account_type = 2) LEFT JOIN `job_proposals` p ON(u.id = p.user_id AND p.proposal_for = '".$userId."') LEFT JOIN `offers` o ON(u.id = o.offer_to AND o.user_id = '".$userId."') WHERE u.id = '".base64_decode($id)."' LIMIT 1";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }

		   if(mysqli_num_rows($result) > 0){
		   		$row = mysqli_fetch_assoc($result);	
		   		if($row['tags'] !== NULL){
		   			$row['tags'] = explode(',', $row['tags']);
		   		}
		   	
		   		//$row['tags'] = '<span class="category-tags">'.$row['category'].'</span>';

		   		//$row['time_ago'] = self::timeago($row['created_date']);

				$resultSet = $row;
			}
		   $data = array('status'=>'success', 'msg'=>"Freelancer Profile Has Been fetched.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }

    } 


    public static function getOverallRatingOfFreelancer($id){
		try {  
		   $resultSet = array();
		   $query = "SELECT round(IFNULL(AVG(rating),0)) rate FROM `reviews` WHERE review_to = '".base64_decode($id)."'"; 
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }

		   if(mysqli_num_rows($result) > 0){
		   		$resultSet = mysqli_fetch_assoc($result);  	
			}
		   $data = array('status'=>'success', 'msg'=>"Freelancer Profile Has Been fetched.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }

    } 

    public static function getMyProfile($userId){
		try {  
		   $resultSet = array();		  
		   $query = "SELECT u.* , d.*, c.* FROM `users` u LEFT JOIN `user_details` d ON(u.id = d.user_id) LEFT JOIN `user_card` c ON(u.id = c.user_id) WHERE u.id = '".$userId."' LIMIT 1";  
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){
		   		$row = mysqli_fetch_assoc($result);	
		   		if($row['tags']){
		   			$row['tags'] = explode(',', $row['tags']);
		   		}		   		
		   		$resultSet = $row;	
			}	
		   $data = array('status'=>'success', 'msg'=>"All Categories Has Been Listed.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }

   public static function getUserDetail($userId){
		try {  
		   $resultSet = array();
		   $row['tags'] = "";
		   $query = "SELECT stripe_customer_id FROM `user_details` WHERE user_id = '".$userId."' LIMIT 1";  
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){
		   		$row = mysqli_fetch_assoc($result);		   				   		
		   		$resultSet = $row;	
			}
		   $data = array('status'=>'success', 'msg'=>"User Detail Has Been Fetched.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }

    public static function getProfileDetailByUserId($userId){
		try {  
		   $resultSet = array();
		   $row['tags'] = "";
		   $query = "SELECT tags FROM `user_details` WHERE user_id = '".$userId."' LIMIT 1";  
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){
		   		$row = mysqli_fetch_assoc($result);
		   		if($row['tags'] !== ""){
		   			$row['tags'] = explode(',', $row['tags']);  
		   		}		   		
		   		$resultSet = $row;	
			}
		   $data = array('status'=>'success', 'msg'=>"All Categories Has Been Listed.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }



    public static function getAllCategories(){
		try {  
		   $resultSet = array(); 
		   $query = "SELECT * FROM `categories`"; 
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){
		   		while($row = mysqli_fetch_assoc($result)){	
					$resultSet[] =  $row;
				}
			}  
		   $data = array('status'=>'success', 'msg'=>"All Categories Has Been Listed.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }


    public static function getSelectedCategories($userId){  
		try {  
		   $resultSet = array(); 
		   $query = "SELECT c.* FROM `categories` c LEFT JOIN `user_details` d ON find_in_set(c.id, d.category_list) WHERE d.user_id = '".$userId."'"; 
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){
		   		while($row = mysqli_fetch_assoc($result)){	
					$resultSet[] =  $row;
				}
			}  
		   $data = array('status'=>'success', 'msg'=>"All Categories Have Been Listed.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }

    public static function getSelectedSubCategories($userId){  
		try {  
		   $resultSet = array(); 
		   $query = "SELECT c.* FROM `subcategories` c LEFT JOIN `user_details` d ON find_in_set(c.id, d.subcategory_list) WHERE d.user_id = '".$userId."'"; 
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){
		   		while($row = mysqli_fetch_assoc($result)){	
					$resultSet[] =  $row;
				}
			}  
		   $data = array('status'=>'success', 'msg'=>"All Sub-Categories Have Been Listed.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }


    public static function getSubcategoryListByCategoryId($category){
		try { 
		//echo "<pre>";print_r($category);die;   
		   $resultSet = array();
		   $query = "SELECT * FROM `subcategories` WHERE category_id = '".$category."'";  
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }

		   if(mysqli_num_rows($result) > 0){
		   		while($row = mysqli_fetch_assoc($result)){		   			
		   			$resultSet[] =  $row;		   							
				}
			}  
		   $data = array('status'=>'success', 'msg'=>"Subcategories Has Been Listed.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {  
		   return $data;
	   }
    }   


    public static function getAllSubcategoriesByCategoryId($formdata){
		try { 
		//echo "<pre>";print_r($category);die;   
		   $resultSet = array();
		   $query = "SELECT s.*, c.category, c.id cId FROM `subcategories` s INNER JOIN `categories` c ON(s.category_id = c.id) WHERE s.category_id IN (".implode(',', $formdata['category']).")";  
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }

		   if(mysqli_num_rows($result) > 0){
		   		while($row = mysqli_fetch_assoc($result)){
		   			if(isset($formdata['view_only'])){
		   				$resultSet[$row['cId']][] =  $row;
		   			}else{
		   				$resultSet[$row['category']][] =  $row;
		   			}						
				}
			}  
		   $data = array('status'=>'success', 'msg'=>"Subcategories Has Been Listed.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {  
		   return $data;
	   }
    }   

    public static function getPostDetailsById($id){
		try {  
		   $resultSet = array();
		   $query = "SELECT j.*, u.fname, u.lname, u.country, COUNT(p.job_post_id) proposal FROM  `job_post` j INNER JOIN `users` u ON(u.id = j.user_id) LEFT JOIN `job_proposals` p ON(j.id = p.job_post_id) WHERE j.id = '".base64_decode($id)."' GROUP BY p.job_post_id LIMIT 1";
		    $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){
		   		$resultSet = mysqli_fetch_assoc($result);	
			}    

		   $data = array('status'=>'success', 'msg'=>"Your Jobs Has Been fetched.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }

    public static function getPostDetailsForEdit($id){
		try {  
		   $resultSet = array();
		   $row['tags'] = "";
		   $query = "SELECT * FROM `job_post` WHERE id = '".base64_decode($id)."' LIMIT 1";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){
		   		$row = mysqli_fetch_assoc($result);	
		   		if($row['tags'] !== ''){
		   			$row['tags'] = explode(',', $row['tags']);
		   		}
		   		$resultSet = $row;
			}  

		   $data = array('status'=>'success', 'msg'=>"Your Post Has Been fetched.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }


    public static function getProposalByPostId($id){

		try {  

		   $resultSet = array();

		   $query = "SELECT  * FROM  `job_proposals` WHERE job_post_id = '".base64_decode($id)."' LIMIT 1";   		  

		   $result = dbconfig::run($query);

		   if(!$result) {

			   throw new exception("No server response!");

		   }

		   if(mysqli_num_rows($result) > 0){

		   		$resultSet = mysqli_fetch_assoc($result);	

			}		   

		   $data = array('status'=>'success', 'msg'=>"Your Jobs Has Been fetched.", 'result'=>$resultSet);

	   } catch (Exception $e) {

		   $data = array('status'=>'error', 'msg'=>$e->getMessage());

	   } finally {

		   return $data;

	   }

    }  

    public static function getRecentJobs(){
		try {  
		  $resultSet = array();		  		  
		  $query = "SELECT j.*, u.country, u.c_logo FROM  `job_post` j INNER JOIN `users` u ON(j.user_id = u.id) ORDER BY j.id DESC LIMIT 5";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){
				while($row = mysqli_fetch_assoc($result)){								
					$row['time_ago'] = self::timeago($row['created_date']);
					$row['encryptId'] = base64_encode($row['id']);	
					$resultSet[] =  $row;
				}
			}		   
		   $data = array('status'=>'success', 'msg'=>"Recent Jobs Has Been fetched.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }


    public static function getProposalsCountOfPost(){
		try {  
		   $resultSet = array();
		   $query = "SELECT job_post_id, COUNT(job_post_id) count FROM  `job_proposals` GROUP BY job_post_id";  
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }

		   if(mysqli_num_rows($result) > 0){
				while($row = mysqli_fetch_assoc($result)){	
					$resultSet[$row['job_post_id']] =  $row;
				}
			}	  

		   $data = array('status'=>'success', 'msg'=>"Your Jobs Has Been fetched.", 'result'=>$resultSet);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }

    public static function getPostProposals($id){

		try {  

		   $resultSet = array();

		   $query = "SELECT * FROM  `job_post` WHERE id = '".base64_decode($id)."' LIMIT 1";  

		   $result = dbconfig::run($query);

		   if(!$result) {

			   throw new exception("No server response!");

		   }

		   if(mysqli_num_rows($result) > 0){

				$resultSet = mysqli_fetch_assoc($result);	

			}		   

		   $data = array('status'=>'success', 'msg'=>"Your Jobs Has Been fetched.", 'result'=>$resultSet);

	   } catch (Exception $e) {

		   $data = array('status'=>'error', 'msg'=>$e->getMessage());

	   } finally {

		   return $data;

	   }

    }


    public static function hireToFreelancer($formdata){
		try { 
			//echo "<pre>";print_r($formdata);die; 
		   $query = "UPDATE `job_proposals` SET status = 3 WHERE id = '".base64_decode($formdata['pId'])."'";  
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }

		   $notify = self::addNotification(base64_decode($formdata['uId']), base64_decode($formdata['fId']), base64_decode($formdata['jId']), 'bid', 'hired you for', $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']);

		  /* $query = "SELECT fname, lname, email FROM `users` WHERE id = '".base64_decode($formdata['fId'])."' LIMIT 1";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);	
		   }

		   $email = self::emailToFreelancerOnProposalAccept($_SESSION['user']['result']['id'], $row['email'], $row['fname'].' '.$row['lname']); */ 

		   $data = array('status'=>'success', 'msg'=>"You Has Been Hired Freelancer" , 'result'=>'');
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }

    public static function startContract($formdata){
		try { 
			//echo "<pre>";print_r($formdata);die; 
		   $query = "UPDATE `job_proposals` SET status = 4 WHERE id = '".base64_decode($formdata['pId'])."'";  
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }

		   $notify = self::addNotification(base64_decode($formdata['fId']), base64_decode($formdata['eId']), base64_decode($formdata['jId']), 'bid', 'started job for', $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']);

		  /* $query = "SELECT fname, lname, email FROM `users` WHERE id = '".base64_decode($formdata['fId'])."' LIMIT 1";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);	
		   }

		   $email = self::emailToFreelancerOnProposalAccept($_SESSION['user']['result']['id'], $row['email'], $row['fname'].' '.$row['lname']); */ 

		   $data = array('status'=>'success', 'msg'=>"You job Has Been Started" , 'result'=>'');
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }

    public static function endContract($formdata){
		try { 
			//echo "<pre>";print_r($formdata);die; 
		
		   $query = "UPDATE `job_proposals` SET status = 5, reason_to_end = '".$formdata['reason']."', rating_status = 1 WHERE id = '".base64_decode($formdata['pId'])."'";  
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }

		   $notify = self::addNotification(base64_decode($formdata['uId']), base64_decode($formdata['toId']), base64_decode($formdata['jId']), 'bid', 'ended contract for', $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']);


		   	$message = mysqli_real_escape_string(self::$con, $formdata['message']);
			$query = "INSERT INTO `reviews` (post_id, review_from, review_to, message, rating, review_date) VALUES ('".base64_decode($formdata['jId'])."', '".base64_decode($formdata['uId'])."', '".base64_decode($formdata['toId'])."', '".$message."', '".$formdata['rating']."', '".date('Y-m-d H:i:s')."')";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not respond. Please try again !");
			}
			//$notify = self::addNotification(base64_decode($formdata['uId']), base64_decode($formdata['toId']), base64_decode($formdata['jId']), 'review', 'review for', $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']);		

		   $data = array('status'=>'success', 'msg'=>"Your Contract Has Been Ended" , 'result'=>'');
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }

    public static function acceptProposal($formdata){
		try { 
		   $query = "UPDATE `job_proposals` SET status = 2 WHERE id = '".base64_decode($formdata['pId'])."'";  
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }

		   $notify = self::addNotification(base64_decode($formdata['uId']), base64_decode($formdata['fId']), base64_decode($formdata['jId']), 'bid', 'accepted your bid for', $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']);

		   $query = "SELECT fname, lname, email FROM `users` WHERE id = '".base64_decode($formdata['fId'])."' LIMIT 1";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		   if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);	
		   }

		   $email = self::emailToFreelancerOnProposalAccept($_SESSION['user']['result']['id'], $row['email'], $row['fname'].' '.$row['lname']);  

		   $data = array('status'=>'success', 'msg'=>"Proposal Has Been Accepted" , 'result'=>'');
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }

     public static function deleteProposal($formdata){
		try { 
			//echo "<pre>";print_r($formdata);die; 
		   $query = "UPDATE `job_proposals` SET status = 0 WHERE id = '".base64_decode($formdata['del_id'])."'";  
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }		  
		   $notify = self::addNotification(base64_decode($formdata['uId']), base64_decode($formdata['fId']), base64_decode($formdata['jId']), 'bid', 'rejected your bid for', $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']);		   
		   $data = array('status'=>'success', 'msg'=>"Proposal Has Been ".$formdata['request'] , 'result'=>'');
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }    

 
      public static function getMyJobs($formdata){
		try {		  
		   $resultSet = array();
		   $query = "SELECT j.*, p.status, p.applied_date, p.id proposal_id, p.payment_id FROM `job_post` j INNER JOIN `job_proposals` p ON(j.id = p.job_post_id AND p.user_id = '".base64_decode($formdata['scroll_with']['user_id'])."') ORDER BY p.id DESC LIMIT ".$formdata['scroll_with']["start"].", ".$formdata['scroll_with']["limit"]."";	
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		    $count = mysqli_num_rows($result);
		    if($count > 0){
				while($row = mysqli_fetch_assoc($result)){	
					$row['encryptId'] = base64_encode($row['id']);	
					$row['encrypt_pId'] = base64_encode($row['proposal_id']);		
					$row['encrypt_t'] = base64_encode($row['title']);		
					$row['encrypt_eId'] = base64_encode($row['user_id']);	
					$row['encrypt_fId'] = $formdata['scroll_with']['user_id'];	  	
					$row['posted_on'] = date('d, M Y', strtotime($row['created_date']));
					$row['applied_on'] = date('d, M Y', strtotime($row['applied_date']));
					//$row['tags'] = '<span class="category-tags">'.$row['category'].'</span>';
					$resultSet[] =  $row;
				}
			}		   

		   $data = array('status'=>'success', 'msg'=>"Your Jobs Has Been fetched.", 'result'=>$resultSet, 'count'=>$count);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }


   public static function getActiveJobs($userId){
		try {		  
		   $resultSet = array();
		   $query = "SELECT j.* FROM `job_post` j INNER JOIN `job_proposals` p ON(j.id = p.job_post_id AND j.user_id = '".$userId."' AND p.status = 2 AND j.post_status = 1) ORDER BY p.id DESC";	
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		    $count = mysqli_num_rows($result);
		    if($count > 0){
				while($row = mysqli_fetch_assoc($result)){	
					$row['encryptId'] = base64_encode($row['id']);	
					//$row['encrypt_pId'] = base64_encode($row['proposal_id']);				
					//$row['posted_on'] = date('d, M Y', strtotime($row['created_date']));
					//$row['applied_on'] = date('d, M Y', strtotime($row['applied_date']));		 			
					$resultSet[] =  $row;
				}
			}		   

		   $data = array('status'=>'success', 'msg'=>"Your Jobs Has Been fetched.", 'result'=>$resultSet, 'count'=>$count);
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
    }    


   public static function makeAnOffer($formdata){
		try {  
			//echo"<pre>";print_r($formdata);die;   
			unset($formdata['optional_fields']);
		   if(in_array("", $formdata, true)){
			 throw new exception("All fields are required!");
		   }
		   $query = "SELECT o_id FROM `offers` WHERE user_id = '".base64_decode($formdata['uId'])."' AND offer_to = '".base64_decode($formdata['fId'])."'";	
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");  
		   }
		   if(mysqli_num_rows($result) > 0){  
				throw new exception("Sorry! You have already sent an offer");
		   }  

		   $message = mysqli_real_escape_string(self::$con, $formdata['msg']);
		   $query = "INSERT INTO `offers` (user_id, offer_to, message, offer_date) VALUES ('".base64_decode($formdata['uId'])."', '".base64_decode($formdata['fId'])."', '".$message."', '".date('Y-m-d H:i:s')."')";

		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("Server not responde1!");
		   }
		   $notify = self::addNotification(base64_decode($formdata['uId']), base64_decode($formdata['fId']), NULL, 'offer', 'make an offer for', $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']);

		  $email = self::emailToFreelancerOnMakeAnOffer(base64_decode($formdata['femail']), $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']);  

		   $data = array('status'=>'success', 'msg'=>"Your Offer has been Sent.", 'result'=>'');
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
  }
    

    public static function placeBid($formdata){
		try {  
			//echo"<pre>";print_r($formdata);die;   
			unset($formdata['optional_fields']);
		   if(in_array("", $formdata, true)){
			 throw new exception("All fields are required!");
		   }
		   $query = "SELECT id FROM `job_proposals` WHERE user_id = '".base64_decode($formdata['user_id'])."' AND job_post_id = '".base64_decode($formdata['post_id'])."' AND status != 0";	
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");  
		   }
		   if(mysqli_num_rows($result) > 0){
				throw new exception("Sorry! You have been already placed a bid for this job.");
		   }  

		   $proposal = mysqli_real_escape_string(self::$con, $formdata['proposal']);
		   $query = "INSERT INTO `job_proposals` (user_id, job_post_id, proposal_for, proposal, minimal_rate, delivery_time, applied_date) VALUES ('".base64_decode($formdata['user_id'])."', '".base64_decode($formdata['post_id'])."', '".base64_decode($formdata['client_id'])."', '".$proposal."', '".$formdata['minimal_rate']."', '".$formdata['delivery_qty'].' '.$formdata['delivery_time']."', '".date('Y-m-d H:i:s')."')";

		   $result = dbconfig::run($query);  
		   if(!$result) {
			   throw new exception("Server not responde1!");
		   }
		   $notify = self::addNotification(base64_decode($formdata['user_id']), base64_decode($formdata['client_id']), base64_decode($formdata['post_id']), 'bid', 'placed a bid on your', $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']);

		  $email = self::emailToClientOnSubmitProposal(base64_decode($formdata['email_id']), $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname'], base64_decode($formdata['client_id']), base64_decode($formdata['post_id']));  

		   $data = array('status'=>'success', 'msg'=>"Your proposal has been submitted.", 'result'=>'');
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());  
	   } finally {
		   return $data;
	   }
  }


    public static function updateBid($formdata){
		try {  
			//echo"<pre>";print_r($formdata);die;   
			unset($formdata['optional_fields']);
		   if(in_array("", $formdata, true)){
			 throw new exception("All fields are required!");
		   }		  

		   $proposal = mysqli_real_escape_string(self::$con, $formdata['proposal']);
		   $query = "UPDATE `job_proposals` SET proposal = '".$proposal."', minimal_rate = '".$formdata['minimal_rate']."', delivery_time = '".$formdata['delivery_qty'].' '.$formdata['delivery_time']."' WHERE id = '".base64_decode($formdata['pId'])."'";
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("Server not responde1!");
		   }
		   $notify = self::addNotification(base64_decode($formdata['fId']), base64_decode($formdata['eId']), base64_decode($formdata['jId']), 'bid', 'changed a bid on your', $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']);

		  //$email = self::emailToClientOnSubmitProposal(base64_decode($formdata['email_id']), $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname'], base64_decode($formdata['client_id']), '', $formdata['post_id']);  

		   $data = array('status'=>'success', 'msg'=>"Your proposal has been submitted.", 'result'=>'');
	   } catch (Exception $e) {
		   $data = array('status'=>'error', 'msg'=>$e->getMessage());
	   } finally {
		   return $data;
	   }
  }




     public static function timeago($date) {

     	 try { 

     	 	    $time_ago = "";

			    $timestamp = strtotime($date);	

			   

			    $strTime = array("second", "minute", "hour", "day", "month", "year");

			    $length = array("60","60","24","30","12","10");



			    $currentTime = time();

			    if($currentTime >= $timestamp) {

					$diff     = time()- $timestamp;

					for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {

					$diff = $diff / $length[$i];

					}



					$diff = round($diff);

					$time_ago = $diff . " " . $strTime[$i] ."s ago ";

			   }	

			   //echo"<pre>";print_r($time_ago);die;	

       		$data = array('status'=>'success', 'msg'=>"Time Ago set", 'result'=>$time_ago);	

		} catch (Exception $e) {

			$data = array('status'=>'error', 'msg'=>$e->getMessage());

		} finally {

			return $data;

		}

	}   


		/********* Email template for sending email notifications **********/



      public static function userSignupEmail($id, $email, $fullname, $password){

     	try{

			$bodyContent = '<html><body style="background-color: #eeeeee; margin: 0 !important; padding: 0 !important;">';

			$bodyContent .= '<title></title>';

			$bodyContent .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';

			$bodyContent .= '<meta name="viewport" content="width=device-width, initial-scale=1">';

			$bodyContent .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';

			$bodyContent .= '<style type="text/css">

			/ FONTS /

			@media screen {

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 400;

				  src: local("Lato Regular"), local("Lato-Regular"), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 700;

				  src: local("Lato Bold"), local("Lato-Bold"), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 400;

				  src: local("Lato Italic"), local("Lato-Italic"), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 700;

				  src: local("Lato Bold Italic"), local("Lato-BoldItalic"), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format("woff");

				}

			}

			

			/ CLIENT-SPECIFIC STYLES /

			body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }

			table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }

			img { -ms-interpolation-mode: bicubic; }			

			/ RESET STYLES /

			img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }

			table { border-collapse: collapse !important; }

			body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }



			/ iOS BLUE LINKS /

			a[x-apple-data-detectors] {

				color: inherit !important;

				text-decoration: none !important;

				font-size: inherit !important;

				font-family: inherit !important;

				font-weight: inherit !important;

				line-height: inherit !important;

			}

			

			/ MOBILE STYLES /

			@media screen and (max-width:600px){

				h1 {

					font-size: 32px !important;

					line-height: 32px !important;

				}

			}



			/ ANDROID CENTER FIX /

			div[style*="margin: 16px 0;"] { margin: 0 !important; }

		</style>';



				$bodyContent .= '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

			We"re thrilled to have you here! Get ready to dive into your new account.

		</div>

		';

				$bodyContent .= '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

			We"re thrilled to have you here! Get ready to dive into your new account.

		</div>



		<table border="0" cellpadding="0" cellspacing="0" width="100%">

			<!-- LOGO -->

			<tbody><tr>

				<td bgcolor="#eee" align="center">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

						<tbody><tr>

							<td align="center" valign="top" style="padding: 50px 30px 50px 30px;">

								  <img src="'.SITE_URL.'assets/images/logo.png"/>

							</td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- HERO -->

			<tr>

				<td bgcolor="#eee" align="center" style="padding: 0px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 0px 30px -15px #808080;">

						<tbody><tr>

							<td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 3px; line-height: 48px;">

							  <h1 style="font-size: 48px; font-weight: 400; margin: 0;">Welcome!</h1>

							</td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- COPY BLOCK -->

			<tr>

				<td bgcolor="#fcfcfc" align="center" style="padding: 0px 10px 0px 10px;">

						<!--[if (gte mso 9)|(IE)]>

						<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

							<td align="center" valign="top" width="600">

							<![endif]-->

						<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 20px 30px -15px #808080;">

						  <!-- COPY -->

						  <tbody>

						<tr>

							<td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px; letter-spacing: .4px">
								  <p style="margin: 0;">Dear '.$fullname.',</p>
							</td>

						</tr>'; 


						$bodyContent .= '<tr><td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px; letter-spacing: .4px">

								  <p style="margin: 0;">Thanks for joining PayMyPrice! Your account is under review. We will notify you once your account has been approved.</p>

							</td></tr>'; 
				  

					  $bodyContent .= '<tr>

						<td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

						  <p style="margin: 0;">If you have any questions, then do not hesitate to reply to this email.</p>

				</td>

			</tr>

					  <!-- COPY -->

					   <tr>

						<td bgcolor="#ffffff" align="left" style="padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

						 	 <p style="margin: 0;">Thank you,</p>	
						  </td>

					  </tr>

					  <tr>

						<td bgcolor="#ffffff" align="left" style="padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

						  <p style="margin: 0;">The PayMyPrice team</p>							

						</td>

					  </tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- SUPPORT CALLOUT -->

			<tr>

				<td bgcolor="#ffffff" align="center" style="padding: 30px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 0px 30px -15px #808080;">

						<!-- HEADLINE -->  

						<tbody><tr>

						  <td bgcolor="#ffffff" align="center" style="padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

							<h2 style="font-size: 20px; font-weight: 400; color: #1B2A50; margin: 0;">Need more help?</h2>

							<p style="margin: 0;"><a style="color:#f19d39;" href="tel: 855-469-7742">Schedule a call with us</a></p>

						  </td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- FOOTER -->

			<tr>

				<td bgcolor="#ffffff" align="center" style="padding: 0px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

		</tbody></table>

		</body></html>'; 
		
			$sentEmail = self::SendGridEmail($bodyContent, $email, 'Signup Confirmation', 'info@paymyprice.com');	

			$data = array('status'=>'success', 'msg'=>"You have been registered successfully login now.", 'result'=>'');

    }catch(Exception $e){

       $data = array('status'=>'error', 'msg'=>$e->getMessage());

    }finally{

        return $data;

    }

   }





public static function userForgotPasswordEmail($password, $email){

    try{    	

				$bodyContent = '<html><body style="background-color: #eeeeee; margin: 0 !important; padding: 0 !important;">';

			$bodyContent .= '<title></title>';

			$bodyContent .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';

			$bodyContent .= '<meta name="viewport" content="width=device-width, initial-scale=1">';

			$bodyContent .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';

			$bodyContent .= '<style type="text/css">

			/ FONTS /

			@media screen {

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 400;

				  src: local("Lato Regular"), local("Lato-Regular"), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 700;

				  src: local("Lato Bold"), local("Lato-Bold"), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 400;

				  src: local("Lato Italic"), local("Lato-Italic"), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 700;

				  src: local("Lato Bold Italic"), local("Lato-BoldItalic"), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format("woff");

				}

			}

			

			/ CLIENT-SPECIFIC STYLES /

			body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }

			table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }

			img { -ms-interpolation-mode: bicubic; }			

			/ RESET STYLES /

			img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }

			table { border-collapse: collapse !important; }

			body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }



			/ iOS BLUE LINKS /

			a[x-apple-data-detectors] {

				color: inherit !important;

				text-decoration: none !important;

				font-size: inherit !important;

				font-family: inherit !important;

				font-weight: inherit !important;

				line-height: inherit !important;

			}

			

			/ MOBILE STYLES /

			@media screen and (max-width:600px){

				h1 {

					font-size: 32px !important;

					line-height: 32px !important;

				}

			}



			/ ANDROID CENTER FIX /

			div[style*="margin: 16px 0;"] { margin: 0 !important; }

		</style>';



				$bodyContent .= '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

			We"re thrilled to have you here! Get ready to dive into your new account.

		</div>

		';

				$bodyContent .= "<div style='display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;'>

			We are thrilled to have you here! Get ready to dive into your new account.

				</div>

					<table border='0' cellpadding='0' cellspacing='0' width='100%'>

						<!-- LOGO -->

						<tbody><tr>

							<td bgcolor='#eee' align='center'>

								<!--[if (gte mso 9)|(IE)]>

								<table align='center' border='0' cellspacing='0' cellpadding='0' width='600'>

								<tr>

								<td align='center' valign='top' width='600'>

								<![endif]-->

								<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>

									<tbody><tr>

										<td align='center' valign='top' style='padding: 50px 30px 50px 30px;'>

										    <img src='".SITE_URL."'assets/images/logo.png/>

										</td>

									</tr>

								</tbody></table>

								<!--[if (gte mso 9)|(IE)]>

								</td>

								</tr>

								</table>

								<![endif]-->

							</td>

						</tr>

						<!-- HERO -->

						<tr>

							<td bgcolor='#eee' align='center' style='padding: 0px 10px 0px 10px;'>

								<!--[if (gte mso 9)|(IE)]>

								<table align='center' border='0' cellspacing='0' cellpadding='0' width='600'>

								<tr>

								<td align='center' valign='top' width='600'>

								<![endif]-->

								<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>

									<tbody><tr>

										<td bgcolor='#ffffff' align='center' valign='top' style='padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 3px; line-height: 48px;'>

										  <h1 style='font-size: 48px; font-weight: 400; margin: 0;'>Welcome!</h1>

										</td>

									</tr>

								</tbody></table>

								<!--[if (gte mso 9)|(IE)]>

								</td>

								</tr>

								</table>

								<![endif]-->

							</td>

						</tr>

						<!-- COPY BLOCK -->

						<tr>

							<td bgcolor='#f4f4f4' align='center' style='padding: 0px 10px 0px 10px;'>

								<!--[if (gte mso 9)|(IE)]>

								<table align='center' border='0' cellspacing='0' cellpadding='0' width='600'>

								<tr>

								<td align='center' valign='top' width='600'>

								<![endif]-->

								<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>

								  <!-- COPY -->

								  <tbody><tr>

									<td bgcolor='#ffffff' align='center' style='padding: 20px 30px 40px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px; letter-spacing: .4px'>

									  <p style='margin: 0;'>Please find below your credentials to login.</p>

									</td>

								  </tr>

								  <!-- BULLETPROOF BUTTON -->

								  

								  <!-- COPY -->

								  

								  <!-- COPY -->

					<tr>";





							$bodyContent .= "<td bgcolor='#ffffff' align='left' style='padding: 20px 30px 0px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>

					  

						<b>Your new password is :</b> ".$password."

									  </td>

									</tr>

									<tr>

									  <td bgcolor='#ffffff' align='left' style='padding: 20px 30px 0px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>

										<b>Login Link: </b><a style='color:#f19d39;' href='".SITE_URL."login.php' target='blank'>".SITE_URL."</a>

									  </td>

									</tr>

								  <!-- COPY -->

								  <tr>

									<td bgcolor='#ffffff' align='left' style='padding: 20px 30px 20px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>

									  <p style='margin: 0;'>If you have any questions, just reply to this email. We are always happy to help.</p>

									</td>

								  </tr>

								  <!-- COPY --

								  <tr>

									<td bgcolor='#ffffff' align='left' style='padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>

									  <p style='margin: 0;'>Cheers,<br>PayMyPrice</p></td>

								  </tr>

								</tbody></table>

								<!--[if (gte mso 9)|(IE)]>

								</td>

								</tr>

								</table>

								<![endif]-->

							</td>

						</tr>

						   <!-- SUPPORT CALLOUT -->

					<tr>

						<td bgcolor='#ffffff' align='center' style='padding: 30px 10px 0px 10px;'>

							<!--[if (gte mso 9)|(IE)]>

							<table align='center' border='0' cellspacing='0' cellpadding='0' width='600'>

							<tr>

							<td align='center' valign='top' width='600'>

							<![endif]-->

							<table border='0' cellpadding=0 cellspacing=0' width='100%' style='max-width: 600px;'>

								<!-- HEADLINE -->

								<tbody><tr>

								  <td bgcolor='#ffffff' align='center' style='padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>

									<h2 style='font-size: 20px; font-weight: 400; color: #1B2A50; margin: 0;'>Need more help?</h2>

									 <p style='margin: 0;'><a style='color:#f19d39;' href='tel: 855-469-7742'>Schedule a call with us</a></p>

								  </td>

								</tr>

							</tbody></table>

							<!--[if (gte mso 9)|(IE)]>

							</td>

							</tr>

							</table>

							<![endif]-->

						</td>

					</tr>

					<!-- FOOTER -->

					

						<tr>

							<td bgcolor='#ffffff' align='center' style='padding: 0px 10px 0px 10px;'>

								<!--[if (gte mso 9)|(IE)]>

								<table align='center' border='0' cellspacing='0' cellpadding='0' width='600'>

								<tr>

								<td align='center' valign='top' width='600'>

								<![endif]-->

								

								<!--[if (gte mso 9)|(IE)]>

								</td>

								</tr>

								</table>

								<![endif]-->

							</td>

						</tr>

					</tbody></table>

					</body></html>"; 
    
		$sentEmail = self::SendGridEmail($bodyContent, $email, 'Password recovery');	

		$data = array('status'=>'success', 'msg'=>"password recovery email sent.", 'result'=>'');

    }catch(Exception $e){

       $data = array('status'=>'error', 'msg'=>$e->getMessage());

    }finally{

        return $data;

    }

}







	/************************ Image Upload  ************************/

   

	public static function profileUpload($userId, $image){
		try{
			$target_dir = "../uploads/profile/";
			$upload_name = "p_".time().".jpeg";                        //store in database...

			$new_target_file = $target_dir .basename($upload_name); 

			if(move_uploaded_file($image["tmp_name"], $new_target_file)){
				$query = "UPDATE `users` SET p_img = '".$upload_name."' WHERE id =".$userId;	
				$result = dbconfig::run($query);
				if(!$result) {
					throw new exception("No server response!");
				}
				$_SESSION['user']['result']['p_img'] = $upload_name;
				$data = array('status'=>'success', 'msg'=>"Your Profile Photo Has Been Updated", 'result'=>$upload_name);	
			}else{
				throw new exception("Sorry, there was an error uploading your file."); 
			}
		}catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally {
			return $data;
		}

	}



	/************************ Peronal Id Upload  ************************/

   

	public static function PersonalIdUpload($file){  
		try{  			

			$target_dir = "../uploads/personal/";	
			$target_file = $target_dir . basename($file["name"]);
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			if($file["size"] > 5000000) {  // 5 mb only limit
				throw new exception("Sorry, your file is too large."); 
			}else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"	&& $imageFileType != "gif"){
				throw new exception("Sorry! only JPG, JPEG, PNG & GIF & PDF files are allowed."); 	
			}			

			$upload_name = "Id_".time().".".$imageFileType;                     //store in database...
			$new_target_file = $target_dir.basename($upload_name); 
			if(move_uploaded_file($file["tmp_name"], $new_target_file)){
				$data = array('status'=>'success', 'msg'=>"Image upload Successfully", 'result'=>$upload_name);	
			}else{
				throw new exception("Sorry, there was an error uploading your file."); 
			}				    

		}catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally {
			return $data;
		}  
	}	

		/************************ Image Upload  ************************/

   

	public static function UploadLogo($file){  
		try{  
			//echo"<pre>";print_r($file);die;

			$target_dir = "../uploads/profile/";	
			$target_file = $target_dir . basename($file["name"]);
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			if($file["size"] > 5000000) {  // 5 mb only limit
				throw new exception("Sorry, your file is too large."); 
			}else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"	&& $imageFileType != "gif"){
				throw new exception("Sorry! only JPG, JPEG, PNG & GIF & PDF files are allowed."); 	
			}			

			$upload_name = "logo_".time().".".$imageFileType;                       //store in database...
			$new_target_file = $target_dir.basename($upload_name); 
			if(move_uploaded_file($file["tmp_name"], $new_target_file)){
				$data = array('status'=>'success', 'msg'=>"Image upload Successfully", 'result'=>$upload_name);	
			}else{
				throw new exception("Sorry, there was an error uploading your file."); 
			}				    

		}catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally {
			return $data;
		}  
	}	




	public static function emailToFreelancerOnMakeAnOffer($email, $name){
     	try{
			$bodyContent = '<html><body style="background-color: #eeeeee; margin: 0 !important; padding: 0 !important;">';
			$bodyContent .= '<title></title>';
			$bodyContent .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
			$bodyContent .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
			$bodyContent .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
			$bodyContent .= '<style type="text/css">
			/ FONTS /

			@media screen {

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 400;

				  src: local("Lato Regular"), local("Lato-Regular"), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 700;

				  src: local("Lato Bold"), local("Lato-Bold"), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 400;

				  src: local("Lato Italic"), local("Lato-Italic"), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 700;

				  src: local("Lato Bold Italic"), local("Lato-BoldItalic"), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format("woff");

				}

			}

			

			/ CLIENT-SPECIFIC STYLES /

			body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }

			table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }

			img { -ms-interpolation-mode: bicubic; }			

			/ RESET STYLES /

			img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }

			table { border-collapse: collapse !important; }

			body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }



			/ iOS BLUE LINKS /

			a[x-apple-data-detectors] {

				color: inherit !important;

				text-decoration: none !important;

				font-size: inherit !important;

				font-family: inherit !important;

				font-weight: inherit !important;

				line-height: inherit !important;

			}

			

			/ MOBILE STYLES /

			@media screen and (max-width:600px){

				h1 {

					font-size: 32px !important;

					line-height: 32px !important;

				}

			}



			/ ANDROID CENTER FIX /

			div[style*="margin: 16px 0;"] { margin: 0 !important; }

		</style>';



				$bodyContent .= '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

			We"re thrilled to have you here! Get ready to dive into your new account.

		</div>

		';

				$bodyContent .= '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

			We"re thrilled to have you here! Get ready to dive into your new account.

		</div>



		<table border="0" cellpadding="0" cellspacing="0" width="100%">

			<!-- LOGO -->

			<tbody><tr>

				<td bgcolor="#eee" align="center">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

						<tbody><tr>

							<td align="center" valign="top" style="padding: 50px 30px 50px 30px;">

								  <img src="'.SITE_URL.'assets/img/logo/logo.png"/>

							</td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- HERO -->

			<tr>

				<td bgcolor="#eee" align="center" style="padding: 0px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 0px 30px -15px #808080;">

						<tbody><tr>

							<td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 3px; line-height: 48px;">

							  <h1 style="font-size: 48px; font-weight: 400; margin: 0;">Welcome!</h1>

							</td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- COPY BLOCK -->

			<tr>

				<td bgcolor="#fcfcfc" align="center" style="padding: 0px 10px 0px 10px;">

						<!--[if (gte mso 9)|(IE)]>

						<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

							<td align="center" valign="top" width="600">

							<![endif]-->

						<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 20px 30px -15px #808080;">

						  <!-- COPY -->

						  <tbody>

						<tr>

							<td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #1B2A50; 	font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px; letter-spacing: .4px">

								  <p style="margin: 0;">You have recieved an job offer by '.ucwords($name).' .</p>

							</td>

						</tr>'; 

						

						$bodyContent .= '<tr>

						  <td bgcolor="#ffffff" align="center" style="padding: 20px 30px 0px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

		  

						<b><a style="color:#f19d39;" href="'.SITE_URL.'" target="blank">Please click here to login </a>

						  </b></td>

					</tr>';

				  

					  $bodyContent .= '<tr>

						<td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

						  <p style="margin: 0;">In the meantime, if you have any questions, then do not hesitate to reply to this email.</p>

				</td>

			</tr>

					  <!-- COPY -->

					  <tr>

						<td bgcolor="#ffffff" align="left" style="padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

						  <p style="margin: 0;">Cheers,<br>PayMyPrice</p>							

						</p></td>

					  </tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- SUPPORT CALLOUT -->

			<tr>

				<td bgcolor="#ffffff" align="center" style="padding: 30px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 0px 30px -15px #808080;">

						<!-- HEADLINE -->  

						<tbody><tr>

						  <td bgcolor="#ffffff" align="center" style="padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

							<h2 style="font-size: 20px; font-weight: 400; color: #1B2A50; margin: 0;">Need more help?</h2>

							<p style="margin: 0;"><a style="color:#f19d39;" href="tel: 855-469-7742">Schedule a call with us</a></p>

						  </td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- FOOTER -->

			<tr>

				<td bgcolor="#ffffff" align="center" style="padding: 0px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

		</tbody></table>

		</body></html>'; 

			$sentEmail = self::SendGridEmail($bodyContent,$email, 'You have a new proposal');	

			$data = array('status'=>'success', 'msg'=>"You have been registered successfully login now.", 'result'=>'');

    }catch(Exception $e){

       $data = array('status'=>'error', 'msg'=>$e->getMessage());

    }finally{

        return $data;

    }

   }



   public static function emailToClientOnSubmitProposal($email, $name, $client_id, $post_id){

     	try{

			$bodyContent = '<html><body style="background-color: #eeeeee; margin: 0 !important; padding: 0 !important;">';

			$bodyContent .= '<title></title>';

			$bodyContent .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';

			$bodyContent .= '<meta name="viewport" content="width=device-width, initial-scale=1">';

			$bodyContent .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';

			$bodyContent .= '<style type="text/css">

			/ FONTS /

			@media screen {

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 400;

				  src: local("Lato Regular"), local("Lato-Regular"), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 700;

				  src: local("Lato Bold"), local("Lato-Bold"), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 400;

				  src: local("Lato Italic"), local("Lato-Italic"), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 700;

				  src: local("Lato Bold Italic"), local("Lato-BoldItalic"), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format("woff");

				}

			}

			

			/ CLIENT-SPECIFIC STYLES /

			body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }

			table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }

			img { -ms-interpolation-mode: bicubic; }			

			/ RESET STYLES /

			img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }

			table { border-collapse: collapse !important; }

			body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }



			/ iOS BLUE LINKS /

			a[x-apple-data-detectors] {

				color: inherit !important;

				text-decoration: none !important;

				font-size: inherit !important;

				font-family: inherit !important;

				font-weight: inherit !important;

				line-height: inherit !important;

			}

			

			/ MOBILE STYLES /

			@media screen and (max-width:600px){

				h1 {

					font-size: 32px !important;

					line-height: 32px !important;

				}

			}



			/ ANDROID CENTER FIX /

			div[style*="margin: 16px 0;"] { margin: 0 !important; }

		</style>';



				$bodyContent .= '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

			We"re thrilled to have you here! Get ready to dive into your new account.

		</div>

		';

				$bodyContent .= '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

			We"re thrilled to have you here! Get ready to dive into your new account.

		</div>



		<table border="0" cellpadding="0" cellspacing="0" width="100%">

			<!-- LOGO -->

			<tbody><tr>

				<td bgcolor="#eee" align="center">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

						<tbody><tr>

							<td align="center" valign="top" style="padding: 50px 30px 50px 30px;">

								  <img src="'.SITE_URL.'assets/img/logo/logo.png"/>

							</td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- HERO -->

			<tr>

				<td bgcolor="#eee" align="center" style="padding: 0px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 0px 30px -15px #808080;">

						<tbody><tr>

							<td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 3px; line-height: 48px;">

							  <h1 style="font-size: 48px; font-weight: 400; margin: 0;">Welcome!</h1>

							</td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- COPY BLOCK -->

			<tr>

				<td bgcolor="#fcfcfc" align="center" style="padding: 0px 10px 0px 10px;">

						<!--[if (gte mso 9)|(IE)]>

						<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

							<td align="center" valign="top" width="600">

							<![endif]-->

						<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 20px 30px -15px #808080;">

						  <!-- COPY -->

						  <tbody>

						<tr>

							<td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px 30px; color: #1B2A50; 	font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px; letter-spacing: .4px">

								  <p style="margin: 0;">You have recieved a job proposal by '.ucwords($name).' .</p>

							</td>

						</tr>'; 

						

						$bodyContent .= '<tr>

						  <td bgcolor="#ffffff" align="center" style="padding: 20px 30px 0px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

		  

						<b><a style="color:#f19d39;" href="'.SITE_URL.'helpers/functions.php?type='.base64_encode('client_email_proposal').'&id='.base64_encode($client_id).'&post='.$post_id.'" target="blank">Please click here to view proposal </a>

						  </b></td>

					</tr>';

				  

					  $bodyContent .= '<tr>

						<td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

						  <p style="margin: 0;">In the meantime, if you have any questions, then do not hesitate to reply to this email.</p>

				</td>

			</tr>

					  <!-- COPY -->

					  <tr>

						<td bgcolor="#ffffff" align="left" style="padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

						  <p style="margin: 0;">Cheers,<br>PayMyPrice</p>							

						</p></td>

					  </tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- SUPPORT CALLOUT -->

			<tr>

				<td bgcolor="#ffffff" align="center" style="padding: 30px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 0px 30px -15px #808080;">

						<!-- HEADLINE -->  

						<tbody><tr>

						  <td bgcolor="#ffffff" align="center" style="padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

							<h2 style="font-size: 20px; font-weight: 400; color: #1B2A50; margin: 0;">Need more help?</h2>

							<p style="margin: 0;"><a style="color:#f19d39;" href="tel: 855-469-7742">Schedule a call with us</a></p>

						  </td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- FOOTER -->

			<tr>

				<td bgcolor="#ffffff" align="center" style="padding: 0px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

		</tbody></table>

		</body></html>'; 

			$sentEmail = self::SendGridEmail($bodyContent,$email, 'You have a new proposal');	

			$data = array('status'=>'success', 'msg'=>"You have been registered successfully login now.", 'result'=>'');

    }catch(Exception $e){

       $data = array('status'=>'error', 'msg'=>$e->getMessage());

    }finally{

        return $data;

    }

   }





   public static function emailToFreelancerOnProposalAccept($emailFrom, $emailTo, $name){
     	try{
     		
      		

			$bodyContent = '<html><body style="background-color: #eeeeee; margin: 0 !important; padding: 0 !important;">';

			$bodyContent .= '<title></title>';

			$bodyContent .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';

			$bodyContent .= '<meta name="viewport" content="width=device-width, initial-scale=1">';

			$bodyContent .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';

			$bodyContent .= '<style type="text/css">

			/ FONTS /

			@media screen {

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 400;

				  src: local("Lato Regular"), local("Lato-Regular"), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 700;

				  src: local("Lato Bold"), local("Lato-Bold"), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 400;

				  src: local("Lato Italic"), local("Lato-Italic"), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 700;

				  src: local("Lato Bold Italic"), local("Lato-BoldItalic"), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format("woff");

				}

			}

			

			/ CLIENT-SPECIFIC STYLES /

			body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }

			table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }

			img { -ms-interpolation-mode: bicubic; }			

			/ RESET STYLES /

			img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }

			table { border-collapse: collapse !important; }

			body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }



			/ iOS BLUE LINKS /

			a[x-apple-data-detectors] {

				color: inherit !important;

				text-decoration: none !important;

				font-size: inherit !important;

				font-family: inherit !important;

				font-weight: inherit !important;

				line-height: inherit !important;

			}

			

			/ MOBILE STYLES /

			@media screen and (max-width:600px){

				h1 {

					font-size: 32px !important;

					line-height: 32px !important;

				}

			}



			/ ANDROID CENTER FIX /

			div[style*="margin: 16px 0;"] { margin: 0 !important; }

		</style>';



				$bodyContent .= '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

			We"re thrilled to have you here! Get ready to dive into your new account.

		</div>

		';

				$bodyContent .= '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

			We"re thrilled to have you here! Get ready to dive into your new account.

		</div>



		<table border="0" cellpadding="0" cellspacing="0" width="100%">

			<!-- LOGO -->

			<tbody><tr>

				<td bgcolor="#eee" align="center">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

						<tbody><tr>

							<td align="center" valign="top" style="padding: 50px 30px 50px 30px;">

								  <img src="'.SITE_URL.'assets/img/logo/logo.png"/>

							</td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- HERO -->

			<tr>

				<td bgcolor="#eee" align="center" style="padding: 0px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 0px 30px -15px #808080;">

						<tbody><tr>

							<td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 3px; line-height: 48px;">

							  <h1 style="font-size: 48px; font-weight: 400; margin: 0;">Welcome!</h1>

							</td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- COPY BLOCK -->

			<tr>

				<td bgcolor="#fcfcfc" align="center" style="padding: 0px 10px 0px 10px;">

						<!--[if (gte mso 9)|(IE)]>

						<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

							<td align="center" valign="top" width="600">

							<![endif]-->

						<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 20px 30px -15px #808080;">

						  <!-- COPY -->

						  <tbody>

						<tr>

							<td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #1B2A50; 	font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px; letter-spacing: .4px">
								  <p style="margin: 0;">Dear '.ucwords($name).', </p>
							</td>
						</tr>'; 

						$bodyContent .= '<tr><td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px; letter-spacing: .4px">
								  <p style="margin: 0;">Your proposal has been accepted</p>
							</td></tr>'; 
						

						/*$bodyContent .= '<tr>

						  <td bgcolor="#ffffff" align="center" style="padding: 20px 30px 0px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

		  

						<b><a style="color:#f19d39;" href="'.SITE_URL.'helpers/functions.php?type='.base64_encode('vendor_email_proposal').'&id='.base64_encode($vendor_id).'&job='.base64_encode($job_id).'" target="blank">Please click here to view proposal </a>

						  </b></td>

					</tr>';*/

				  



				 /* 	$bodyContent .= '<tr>

						  <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 0px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

		 						 <p>'.$response.'</p>		

		 				</td>

					</tr>';*/

					  $bodyContent .= '<tr>

						<td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

						  <p style="margin: 0;">In the meantime, if you have any questions, then do not hesitate to reply to this email.</p>

				</td>

			</tr>

					  <!-- COPY -->

					  <tr>

						<td bgcolor="#ffffff" align="left" style="padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

						  <p style="margin: 0;">Cheers,<br>PayMyPrice</p>							

						</p></td>

					  </tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- SUPPORT CALLOUT -->

			<tr>

				<td bgcolor="#ffffff" align="center" style="padding: 30px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 0px 30px -15px #808080;">

						<!-- HEADLINE -->  

						<tbody><tr>

						  <td bgcolor="#ffffff" align="center" style="padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

							<h2 style="font-size: 20px; font-weight: 400; color: #1B2A50; margin: 0;">Need more help?</h2>

							<p style="margin: 0;"><a style="color:#f19d39;" href="tel: 855-469-7742">Schedule a call with us</a></p>

						  </td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- FOOTER -->

			<tr>

				<td bgcolor="#ffffff" align="center" style="padding: 0px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

		</tbody></table>

		</body></html>'; 
			$sentEmail = self::SendGridEmail($bodyContent,$emailTo, 'Client has accept your proposal');	

			$data = array('status'=>'success', 'msg'=>"You have been registered successfully login now.", 'result'=>'');

    }catch(Exception $e){

       $data = array('status'=>'error', 'msg'=>$e->getMessage());

    }finally{

        return $data;

    }

   }







	/************************ Image Upload  ************************/

   

	public static function UploadID($image){  

		try{  

			//echo"<pre>";print_r($image);die;

			$target_dir = "../uploads/ID/";	

			$target_file = $target_dir . basename($image["name"]);			

			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

			if($image["size"] > 5000000) {  // 5 mb only limit

				throw new exception("Sorry, your file is too large."); 				

			}else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"	&& $imageFileType != "gif"){				

				throw new exception("Sorry! only JPG, JPEG, PNG & GIF & PDF files are allowed."); 	

			}			

			$upload_name = time().".".$imageFileType;                        //store in database...

			$new_target_file = $target_dir.basename($upload_name); 

			if(move_uploaded_file($image["tmp_name"], $new_target_file)){

				$data = array('status'=>'success', 'msg'=>"Image upload Successfully", 'result'=>$upload_name);	

			}else{

				throw new exception("Sorry, there was an error uploading your file."); 

			}				    

		}catch (Exception $e) {

			$data = array('status'=>'error', 'msg'=>$e->getMessage());

		}finally {

			return $data;

		}  

	}



	public static function uploadIDFileUnlink($filename){

		try {

			unlink("../uploads/ID/".$filename);			 	

			$data = array('status'=>'success', 'msg'=>"Upload ID file unlinked successfully.", 'result'=>'');			

		}catch(Exception $e) {

			$data = array('status'=>'error', 'msg'=>$e->getMessage());

		}finally{

			return $data;

		}

	}



	public static function ContactUs($formdata){
		try {	
			if($formdata['name']=="" || $formdata['email']=="" || $formdata['message']=="" || $formdata['subject']==""){
				throw new exception("All fields are required!");
			}			
			$subject = mysqli_real_escape_string(self::$con, $formdata['subject']);	
			$message = mysqli_real_escape_string(self::$con, $formdata['message']);	

			$query = "INSERT INTO `contact_us` (name, email, subject, message, created_date) VALUES ('".$formdata['name']."', '".$formdata['email']."', '".$subject."', '".$message."', '".date('Y-m-d H:i:s')."')";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("No server response!");
			}	
			$contact = self::contactUsEmail($formdata['name'], $formdata['email'], $subject, $message);	

			$data = array('status'=>'success', 'msg'=>"Thank you! Your message has been successfully sent. We will contact you very soon!.", 'result'=>'');	
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}



	public static function LoginViaEmail($id){  
     	try {  	     	

			$query = "SELECT * FROM `users` WHERE id ='".$id."' AND status = 1 LIMIT 1"; 

		   	$result = dbconfig::run($query);		   

	        if(mysqli_num_rows($result) > 0){

			   $resultSet = mysqli_fetch_assoc($result);

			   $data = array('status'=>'success', 'msg'=>"User detail fetched successfully.", 'result'=>$resultSet);			  

			}else{

	          $data = array('status'=>'error', 'msg'=>"username/Password Not exist !", 'result'=>array());

			} 

		} catch (Exception $e) {

			$data = array('status'=>'error', 'msg'=>$e->getMessage());

		} finally {

			return $data;

		}  

   }



   public static function infiniteScrollMyPost($formdata){
     	try { 
     		$sort = "";  	
	        $resultSet = array();	
		   	$proposal = self::getProposalsCountOfPost();
		   	//echo "<pre>";print_r($proposal);die; 
			$query = "SELECT j.*, u.country, u.p_img FROM  `job_post` j INNER JOIN `users` u ON(u.id = j.user_id) WHERE j.user_id = '".base64_decode($formdata['scroll_with']['user_id'])."' ".$sort." ORDER BY j.id DESC LIMIT ".$formdata['scroll_with']["start"].", ".$formdata['scroll_with']["limit"].""; 
		   $result = dbconfig::run($query);
		   if(!$result) {
			   throw new exception("No server response!");
		   }
		    $count = mysqli_num_rows($result);
		    if($count > 0){	 
				while($row = mysqli_fetch_assoc($result)){
					$row['proposal_bid'] = $proposal['result'];					
					$row['encryptId'] = base64_encode($row['id']);
					$row['posted_on'] = date('d, M Y', strtotime($row['created_date']));
					$row['expire_on'] = date('d, M Y', strtotime('+1 month',strtotime($row['created_date'])));
					$row['now'] = strtotime('now');
					$row['expired_on'] = strtotime($row['expire_on']);
					//$row['tags'] = '<span class="category-tags">'.$row['category'].'</span>';	  		

					$resultSet[] =  $row;
				}
			}		   

		   $data = array('status'=>'success', 'msg'=>"Your Jobs Has Been fetched.", 'result'=>$resultSet, 'count'=>$count);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }



   	public static function infiniteScrollJobListing($formdata){
     	try {   
     	  	  // echo "<pre>";print_r($formdata);die;
     		$resultSet = array();
     		$yourCategories = '';     		
     		$filter = array();
     		$sort = 'ORDER BY j.id DESC';
     		$filters = "";

     		if(isset($formdata['scroll_with']['category_id']) && ($formdata['scroll_with']['category_id'] !== "")) {
	     		$yourCategories = "WHERE j.category_id IN (".$formdata['scroll_with']['category_id'].")";	  
	     	}		

	     	if(isset($formdata['filter']) && !empty($formdata['filter'])){
	     			$i = 0;
					$filterName = "";					
				 
		     		foreach($formdata['filter'] as $key=>$value){
		     			$nameKey = explode('-', $key);      			
		     			if($i == 0){
		     				if($nameKey[0] == 'tags'){
					     		foreach ($value as $tag) {
					     			$filterSet['tags'][] = "CONCAT(',', j.".$nameKey[0].", ',') like '%,".$tag.",%'";
					     		}		     					
		     				}else if($nameKey[0] == 'category_id'){
		     					$filterSet['category'][]  = "j.".$nameKey[0]." IN (".implode(',', $value).")"; 
		     				}else{
		     					$filterSet[$nameKey[0]][] = "j.".$nameKey[0]." = '".$value."'"; 
		     				}     				
		     			} else{
		     				if($filterName == $nameKey[0]){
		     					if($nameKey[0] == 'tags'){
			     					foreach ($value as $tag) {
						     			$filterSet['tags'][] = "CONCAT(',', j.".$nameKey[0].", ',') like '%,".$tag.",%'";
						     		}	
			     				}else if($nameKey[0] == 'category_id'){
			     					$filterSet['category'][]= "OR j.".$nameKey[0]." IN (".implode(',', $value).")"; 
			     				}else{
			     					$filterSet[$nameKey[0]][] = "OR j.".$nameKey[0]." = '".$value."'";
			     				}     					
		     				}else{
		     					if($nameKey[0] == 'tags'){
			     					foreach ($value as $tag) {
						     			$filterSet['tags'][] = "CONCAT(',', j.".$nameKey[0].", ',') like '%,".$tag.",%'";
						     		}	
			     				}else if($nameKey[0] == 'category_id'){
			     					$filterSet['category'][] = "j.".$nameKey[0]." IN (".implode(',', $value).")"; 
			     				}else{
			     					$filterSet[$nameKey[0]][] = "j.".$nameKey[0]." = '".$value."'";
			     				}  
		     				}
		     				
		     			}  			  
		     			$filterName = $nameKey[0];
		     			$i++; 
		     		}
		     		//echo "<pre>";print_r($filterSet);die;   

		 			if(!empty($filterSet['job_type'])){
			     		$filter[] = "(".implode(' ', $filterSet['job_type']).")";
			     	}	
			     	if(!empty($filterSet['payment_process'])){
			     		$filter[] = "(".implode(' ', $filterSet['payment_process']).")";
			     	}	
			     	if(!empty($filterSet['payment_type'])){
			     		$filter[] = "(".implode(' ', $filterSet['payment_type']).")";
			     	}	
			     	if(!empty($filterSet['tags'])){
			     		$filter[] = "(".implode(' OR ', $filterSet['tags']).")";
			     	}	
			     	if(!empty($filterSet['category'])){
			     		$filter[] = $filterSet['category'][0];
			     	}
			     	
		 					
	 				$filters = "WHERE ".implode(' AND ', $filter);
	 				//echo "<pre>";print_r($filters);die;

			}

			if(isset($formdata['sort_by']) && ($formdata['sort_by'] !== "")) {
				if($formdata['sort_by'] == 'rand')	{
					$sort = 'ORDER BY RAND ()';
				}else{
					$sort = 'ORDER BY j.id '.$formdata['sort_by'];
				}
			}		
		

     		$query = "SELECT j.*,  u.country, u.state, u.p_img, u.fname, u.lname, c.category, p.status FROM `job_post` j INNER JOIN `users` u ON(u.id = j.user_id) INNER JOIN `categories` c ON(c.id = j.category_id) LEFT JOIN `job_proposals` p ON(j.id = p.job_post_id AND p.user_id = '".base64_decode($formdata['scroll_with']["user_id"])."') ".$yourCategories." ".$filters." ".$sort." LIMIT ".$formdata['scroll_with']["start"].", ".$formdata['scroll_with']["limit"]."";  
		    $result = dbconfig::run($query);    
		    if(!$result) {
			   throw new exception("No server response!");
		    }
		    $count = mysqli_num_rows($result);
		    if($count > 0){
				while($row = mysqli_fetch_assoc($result)){
					$job_tag = array();
					$row['time_ago'] = self::timeago($row['created_date']);
					$row['encryptId'] = base64_encode($row['id']);
					/*if($row['tags'] !== ''){
						$tags = explode(',', $row['tags']);
							foreach ($tags as $tag) {
								$job_tags[] = '<span>'.$tag.'</span>';
							}	
						$row['tags'] = implode(' ', $job_tags);
					}*/
					
					$resultSet[] =  $row;
				}
			}
			$data = array('status'=>'success', 'msg'=>"User detail fetched successfully.", 'result'=>$resultSet, 'count'=>$count, 'query'=>$query );
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		} 
   }
   

    public static function infiniteScrollFreelancerListing($formdata){              
     	try { 
     		//echo "<pre>";print_r($formdata);die;
     		$resultSet = array();
     		$yourCategories = '';     		
     		$filter = array();
     		$sort = 'ORDER BY u.id DESC';
     		$filters = "";

     		if(isset($formdata['scroll_with']['category_id']) && ($formdata['scroll_with']['category_id'] !== "")) {
     			$cat = explode(',',$formdata['scroll_with']['category_id']);
     			foreach ($cat as $c) {
	     			$yourCat[]  = "find_in_set(".$c.", d.category_list)"; 
	     		}	
	     		$yourCategories = 'WHERE '.implode(' OR ', $yourCat);	     	 
	     	}	


	     	if(isset($formdata['filter']) && !empty($formdata['filter'])){
	     		
					$filterName = "";					
				 
		     		foreach($formdata['filter'] as $key=>$value){
		     			    			
		     		
		     				if($key== 'tags'){
					     		foreach ($value as $tag) {
					     			$filterSet['tags'][] = "CONCAT(',', d.tags, ',') like '%,".$tag.",%'";
					     		}

		     				}
		     				else if($key == 'category_id'){
		     					foreach ($value as $cat) {
					     			$filterSet['category'][]  = "find_in_set(".$cat.", d.category_list)"; 
					     		}		     					
		     				}

		     				else if($key == 'hourly_rate'){
		     					$filterSet['hourly_rate'] = "d.hourly_rate BETWEEN  ".$value[0]." AND '".$value[1]."'"; 
		     				} 
		     			
		     		}
		     		//echo "<pre>";print_r($filterSet);die;   
		     		

			     	if(!empty($filterSet['tags'])){
			     		$filter[] = "(".implode(' OR ', $filterSet['tags']).")";
			     	}	
			     	if(!empty($filterSet['category'])){
			     		$filter[] = "(".implode(' OR ', $filterSet['category']).")";
			     	}

					if(!empty($filter)){
						$filters = "WHERE (" .$filterSet['hourly_rate'].') AND '. implode(' AND ', $filter);
					}else{
						$filters = "WHERE (" .$filterSet['hourly_rate']." ) ";
					}
			     	
		 					
	 				
	 				//echo "<pre>";print_r($filters);die;

			}

			if(isset($formdata['sort_by']) && ($formdata['sort_by'] !== "")) {
				if($formdata['sort_by'] == 'rand')	{    
					$sort = 'ORDER BY RAND ()';
				}else{
					$sort = 'ORDER BY u.id '.$formdata['sort_by'];
				}
			}		
     		
     		$query = "SELECT u.id, u.fname, u.lname, u.p_img, u.country, u.state, d.tagline, d.nationality, d.hourly_rate, round(IFNULL(AVG(r.rating),0)) rate  FROM `users` u INNER JOIN `user_details` d ON(u.id = d.user_id AND u.account_type = 2) LEFT JOIN `reviews` r ON(u.id = r.review_to) ".$yourCategories." ".$filters."  GROUP BY u.id ".$sort." LIMIT ".$formdata['scroll_with']["start"].", ".$formdata['scroll_with']["limit"]."";        
		    $result = dbconfig::run($query);  
		    if(!$result) {
			   throw new exception("No server response!");
		    }
		    $count = mysqli_num_rows($result);
		    if($count > 0){
				while($row = mysqli_fetch_assoc($result)){	
					$row['encryptId'] = base64_encode($row['id']);	
					$resultSet[] =  $row;
				}
			}
			$data = array('status'=>'success', 'msg'=>"Freelancer list fetched successfully.", 'result'=>$resultSet, 'count'=>$count, 'query'=>$query );	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }

    public static function infiniteScrollFreelancerActivity($formdata){  
     	try { 
     		//echo "<pre>";print_r($formdata);die;
     		$sort = "";
		    $resultSet = array();		   		  
		    if($formdata['sort_by'] !== ""){		   	
		   		$sort = 'AND p.status = '.$formdata['sort_by'];
		    }
     		$query = "SELECT p.*, j.title, j.job_type, u.state, u.country, j.id job_id FROM `job_proposals` p INNER JOIN `job_post` j ON(p.user_id = '".base64_decode($formdata['scroll_with']['user_id'])."' AND p.job_post_id = j.id) INNER JOIN `users` u ON(u.id = j.user_id)  ".$sort." ORDER BY p.id DESC LIMIT ".$formdata['scroll_with']["start"].", ".$formdata['scroll_with']["limit"]."";
		    $result = dbconfig::run($query);
		    if(!$result) {  
			   throw new exception("No server response!");  
			}  
		    $count = mysqli_num_rows($result);
		    if($count > 0){
				while($row = mysqli_fetch_assoc($result)){	
					$row['encryptId'] = base64_encode($row['job_id']);	
					$row['time_ago'] = self::timeago($row['applied_date']);		
					$resultSet[] =  $row;
				}
			}
			$data = array('status'=>'success', 'msg'=>"Freelancer list fetched successfully.", 'result'=>$resultSet, 'count'=>$count);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }


	    public static function addNotification($from, $to, $postId, $type, $notice, $fullname){  
	     	try { 
	     		require_once('pusher_config.php');
	     		require('Pusher.php');
	     		$pusher = new Pusher($key, $secret, $app_id);

	     		$query = "SELECT title FROM `job_post` WHERE id = '".$postId."'";
			    $result = dbconfig::run($query);
			    if(!$result) {
				   throw new exception("No server response!");
			    }
			    if(mysqli_num_rows($result) > 0){   
					$row = mysqli_fetch_assoc($result);		  
				}
					
	     		if($type == 'bid'){
					if($_SESSION['user']['result']['account_type'] == 1){
						$href = 'manage-jobs.php';
					}else if($_SESSION['user']['result']['account_type'] == 2){						
						$href = 'manage-bidders.php?post='.base64_encode($postId);	
					}
					$text = '<strong>'.$fullname.'</strong> '.$notice.' <span class="color">'.$row['title'].'</span> project';
					// if string contains the 'contract' word.. 
					if(strpos($notice, 'contract') !== false){
					   	$icon = 'assignment';
					}else if(strpos($notice, 'hired') !== false){
					   	$icon = 'thumb-up';
					}  else{
					    $icon = 'gavel';   
					}
				
				}else if($type == 'review'){
					$href = 'reviews.php';
					$text = '<strong>'.$fullname.'</strong> '.$notice.' <span class="color">'.$row['title'].'</span> project';
					$icon = 'rate-review';
				}else if($type == 'offer'){
					$href = '';
					$text = '<strong>'.$fullname.'</strong> '.$notice.' <span class="color">'.$row['title'].'</span> project';
					$icon = 'local-offer';
				}else if($type == 'payment'){
					$href = 'manage-bidders.php?post='.base64_encode($postId);	
					$text = '<strong>'.$fullname.'</strong> '.$notice.' <span class="color">'.$row['title'].'</span> project';
					$icon = 'account-balance-wallet';
				}
    
	     		$query = "INSERT INTO `notifications` (notice_from, notice_to, post_id, notice_type, notice, href, icon, notification_date) VALUES ('".$from."', '".$to."', '".$postId."', '".$type."', '".$text."', '".$href."', '".$icon."', '".date('Y-m-d H:i:s')."')";
			    $result = dbconfig::run($query);  
			    if(!$result) { 
				   throw new exception("No server response!");
			    }	

			    $pusher->trigger('presence-mychanel', 'send-notification', array('message' => '', 'from' => $from, 'to' => $to));

				$data = array('status'=>'success', 'msg'=>"Notification msg sent.", 'result'=>'');	
			} catch (Exception $e) {
				$data = array('status'=>'error', 'msg'=>$e->getMessage());
			} finally {
				return $data;
			}  
	   }

    public static function getAllNotificationsByUserId($formdata){   
     	try { 
     		//echo "<pre>";print_r($formdata);die;   
     		$resultSet = array();    	
     	
		   $query = "SELECT notice, href, icon FROM `notifications` WHERE notice_to = '".base64_decode($formdata['scroll_with']['user_id'])."' ORDER BY n_id DESC LIMIT ".$formdata['scroll_with']["start"].", ".$formdata['scroll_with']["limit"]."";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }		 
		    if(mysqli_num_rows($result) > 0){   
				while($row = mysqli_fetch_assoc($result)){											
					$resultSet[] =  $row;
				}
			}	

			$query = "UPDATE `notifications` SET status = 1 WHERE notice_to = '".base64_decode($formdata['scroll_with']['user_id'])."' AND status = 0";
 			$result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }	
			$data = array('status'=>'success', 'msg'=>"All Notifications fetched.", 'result'=>$resultSet);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }   


    public static function getUserAlerts($userId){   
     	try {   
     		
     		$notification = 0;  
     		$message = 0;   	
     	
		   	$query = "SELECT  COUNT(n_id) count FROM `notifications` WHERE notice_to = '".$userId."' AND status = 0 GROUP BY notice_to";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }		 
		    if(mysqli_num_rows($result) > 0){   
				$row = mysqli_fetch_assoc($result);	
				$notification = $row['count'];
			}

			$query = "SELECT  COUNT(m_id) count FROM `messages` WHERE msg_to = '".$userId."' AND status = 0 GROUP BY msg_to";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }		 
		    if(mysqli_num_rows($result) > 0){   
				$row = mysqli_fetch_assoc($result);	
				$message = $row['count'];
			}		

			$data = array('status'=>'success', 'msg'=>"All Notifications fetched.", 'notification'=>$notification, 'message'=>$message);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }  
   


    public static function getCountOfReviewByFreelancer($userId){  
     	try { 
     		$resultSet = 0;  
     		$query = "SELECT COUNT(r_id) count FROM `reviews` WHERE review_to = '".$userId."' GROUP BY review_to";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);
				$resultSet = $row['count'];
			} 
			$data = array('status'=>'success', 'msg'=>"Count Of Reviews", 'result'=>$resultSet);
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }


    public static function getCountOfPostedJobs($userId){  
     	try { 
     		$resultSet = 0;  
     		$query = "SELECT COUNT(id) count FROM `job_post` WHERE user_id = '".$userId."' GROUP BY user_id";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);
				$resultSet = $row['count'];
			} 
			$data = array('status'=>'success', 'msg'=>"Count Of Posted Jobs", 'result'=>$resultSet);
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }

    public static function getCountOfAppliedJobs($userId){  
     	try { 
     		$resultSet = 0; 
     		$query = "SELECT COUNT(id) count FROM `job_proposals` WHERE proposal_for = '".$userId."' GROUP BY proposal_for";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }	
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);
				$resultSet = $row['count'];
			}	    
			$data = array('status'=>'success', 'msg'=>"Count Of Applied Jobs", 'result'=>$resultSet);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }

    public static function getCountOfAppliedBid($userId){  
     	try { 
     		$resultSet = 0; 
     		$query = "SELECT COUNT(id) count FROM `job_proposals` WHERE user_id = '".$userId."' GROUP BY user_id";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }	
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);
				$resultSet = $row['count'];
			}	    
			$data = array('status'=>'success', 'msg'=>"Count Of Applied Jobs", 'result'=>$resultSet);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }

    public static function getCountOfBidWon($userId){  
     	try { 
     		$resultSet = 0; 
     		$query = "SELECT COUNT(id) count FROM `job_proposals` WHERE user_id = '".$userId."' AND status > 1 GROUP BY user_id";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }	
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);
				$resultSet = $row['count'];
			}	    
			$data = array('status'=>'success', 'msg'=>"Count Of Applied Jobs", 'result'=>$resultSet);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }


    public static function getCountOfCompletedJobs($userId){  
     	try { 
     		$resultSet = 0; 
     		$query = "SELECT COUNT(id) count FROM `job_proposals` WHERE proposal_for = '".$userId."' AND status = 5 GROUP BY proposal_for";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }	
		    if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);
				$resultSet = $row['count'];
			}	    
			$data = array('status'=>'success', 'msg'=>"Count Of Applied Jobs", 'result'=>$resultSet);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }

   
    public static function getAllRateToFreelancer($formdata){  
     	try { 
     		$resultSet = array(); 
     		$query = "SELECT j.id, j.user_id, j.title, p.user_id freelancer_id, u.fname, u.lname, r.message, r.rating, r.review_date, r_id FROM `job_post` j INNER JOIN `job_proposals` p ON(j.id = p.job_post_id AND p.proposal_for = '".base64_decode($formdata['scroll_with']['user_id'])."' AND p.rating_status = 1) INNER JOIN `users` u ON(u.id = p.user_id) LEFT JOIN `reviews` r ON(j.id = r.post_id AND review_from = '".base64_decode($formdata['scroll_with']['user_id'])."') ORDER BY r.r_id DESC LIMIT ".$formdata['scroll_with']["start"].", ".$formdata['scroll_with']["limit"]."";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }	
		    $count = mysqli_num_rows($result);
		    if($count > 0){
				while($row = mysqli_fetch_assoc($result)){
					$row['encrypt_id'] = base64_encode($row['id']);	
					$row['encrypt_fId'] = base64_encode($row['freelancer_id']);	
					$row['encrypt_eId'] = $formdata['scroll_with']['user_id'];	
					$row['review_on'] = date('M Y', strtotime($row['review_date']));
					$resultSet[] = $row;
				}
			}	    
			$data = array('status'=>'success', 'msg'=>"Rate To Freelancer Fetched", 'result'=>$resultSet, 'count'=>$count);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }

    public static function getAllRateToEmployer($formdata){  
     	try { 
     		$resultSet = array(); 
     		$query = "SELECT j.id, j.user_id, j.title, u.fname, u.lname, r.message, r.rating, r.review_date, r.r_id FROM `job_post` j INNER JOIN `job_proposals` p ON(j.id = p.job_post_id AND p.user_id = '".base64_decode($formdata['scroll_with']['user_id'])."' AND p.rating_status = 1) INNER JOIN `users` u ON(u.id = j.user_id) LEFT JOIN `reviews` r ON(j.id = r.post_id AND review_from = '".base64_decode($formdata['scroll_with']['user_id'])."') ORDER BY r.r_id DESC LIMIT ".$formdata['scroll_with']["start"].", ".$formdata['scroll_with']["limit"]."";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }	
		    $count = mysqli_num_rows($result);
		    if($count > 0){
				while($row = mysqli_fetch_assoc($result)){
					$row['encrypt_id'] = base64_encode($row['id']);	
					$row['encrypt_eId'] = base64_encode($row['user_id']);	
					$row['encrypt_fId'] = $formdata['scroll_with']['user_id'];	
					$row['review_on'] = date('M Y', strtotime($row['review_date']));
					$resultSet[] = $row;
				}
			}	    
			$data = array('status'=>'success', 'msg'=>"Rate To Freelancer Fetched", 'result'=>$resultSet, 'count'=>$count);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }

   public static function getFreelancerBidsByJob($formdata){  
     	try { 
     		$resultSet = array(); 

     		$query = "SELECT p.id, p.minimal_rate, p.delivery_time, u.fname, u.lname, u.p_img, d.nationality, r.rating, round(IFNULL(AVG(r.rating),0)) rate FROM `job_proposals` p INNER JOIN `job_post` j ON(j.id = p.job_post_id AND p.job_post_id = '".base64_decode($formdata['scroll_with']["post_id"])."' AND p.user_id != '".base64_decode($formdata['scroll_with']["user_id"])."') INNER JOIN `users` u ON(u.id = p.user_id)  INNER JOIN `user_details` d ON(u.id = d.user_id)  LEFT JOIN `reviews` r ON(p.user_id = r.review_to) ORDER BY p.id DESC LIMIT ".$formdata['scroll_with']["start"].", ".$formdata['scroll_with']["limit"]."";
		    $result = dbconfig::run($query);  
		    if(!$result) {
			   throw new exception("No server response!");
		    }	
		    $count = mysqli_num_rows($result);
		    if($count > 0){
				while($row = mysqli_fetch_assoc($result)){
					if($row['id'] !== null){
						$resultSet[] = $row;
					}
				}
			}	    
			$data = array('status'=>'success', 'msg'=>"Freelancer Bid On Job Fetched", 'result'=>$resultSet, 'count'=>$count);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
   }   
   

   	public static function getReviewDetailById($userId, $postId){
		try{
			//echo"<pre>";print_r($formdata);die;
			$resultSet = array();	
		
			$query = "SELECT * FROM `reviews` WHERE review_from = '".base64_decode($userId)."' AND post_id = '".base64_decode($postId)."' LIMIT 1";
			$result = dbconfig::run($query);
			if(!$result) {
				throw new exception("Server not respond. Please try again !");
			}
			if(mysqli_num_rows($result) > 0){
				$row = mysqli_fetch_assoc($result);
				$resultSet = $row;
			}
			$data = array('status'=>'success', 'msg'=>"Your Review Has Been Fetched.", 'result'=>$resultSet);	
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}


	public static function getAllRatingsByUser($formdata){
		try { 
     		$resultSet = array(); 
     		$query = "SELECT r.*, j.title FROM `reviews` r INNER JOIN `job_post` j ON(j.id = r.post_id AND review_to = '".base64_decode($formdata['scroll_with']['user_id'])."') ORDER BY r.r_id DESC LIMIT ".$formdata['scroll_with']["start"].", ".$formdata['scroll_with']["limit"]."";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }	
		    $count = mysqli_num_rows($result);
		    if($count > 0){
				while($row = mysqli_fetch_assoc($result)){				
					$row['review_on'] = date('M Y', strtotime($row['review_date']));
					$resultSet[] = $row;
				}
			}	    
			$data = array('status'=>'success', 'msg'=>"Rate To Freelancer Fetched", 'result'=>$resultSet, 'count'=>$count);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
	}


	public static function getMessageActivateFreelancer($userId){
		try { 
     		$resultSet = array(); 
     		$query = "SELECT fname, lname, id, p_img FROM `users` WHERE status = 1 AND account_type = 2";     		
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }	
		    $count = mysqli_num_rows($result);
		    if($count > 0){
				while($row = mysqli_fetch_assoc($result)){				
					//$row['review_on'] = date('M Y', strtotime($row['review_date']));
					if($row['p_img'] == ""){
						$row['p_img'] = 'user-avatar-placeholder.png';
					}
					$resultSet[$row['id']] = array($row['fname'].' '.$row['lname'], $row['p_img']);
				}
			}	    
			$data = array('status'=>'success', 'msg'=>"Rate To Freelancer Fetched", 'result'=>$resultSet);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
	}


	public static function getMessageActivateEmployer($userId){
		try { 
     		$resultSet = array(); 
     		$query = "SELECT fname, lname, id, p_img FROM `users` WHERE status = 1 AND account_type = 1";    
     		$result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }	
		    $count = mysqli_num_rows($result);
		    if($count > 0){
				while($row = mysqli_fetch_assoc($result)){				
					//$row['review_on'] = date('M Y', strtotime($row['review_date']));
					if($row['p_img'] == ""){
						$row['p_img'] = 'user-avatar-placeholder.png';
					}
					$resultSet[$row['id']] = array($row['fname'].' '.$row['lname'], $row['p_img']);
				}
			}	    
			$data = array('status'=>'success', 'msg'=>"Rate To Freelancer Fetched", 'result'=>$resultSet);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
	}

	public static function addMessages($from, $to, $msg){
		try {   
			$room1 = $from.$to;
			$room2 = $to.$from;
			$message= mysqli_real_escape_string(self::$con, $msg); 		   		
     		$query = "INSERT INTO `messages` (chatroom, msg_from, msg_to, message, created_date) VALUES ('".$room1.','.$room2."', '".$from."', '".$to."', '".$message."', '".date('Y-m-d H:i:s')."')";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }	

			$data = array('status'=>'success', 'msg'=>"User Message Saved Successfully", 'result'=>'');	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
	}


	public static function getMessages($formdata){
		try {   
			$resultSet = array(); 

			$room1 = $formdata['user_id'].$formdata['id'];
			$room2 = $formdata['id'].$formdata['user_id'];  

     		$query = "SELECT * FROM `messages` WHERE FIND_IN_SET (".$room1.", chatroom) OR FIND_IN_SET (".$room2.", chatroom) ORDER BY m_id ASC";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }	
		    $count = mysqli_num_rows($result);
		    if($count > 0){
				while($row = mysqli_fetch_assoc($result)){						
					$resultSet[] = $row;
				}
			}			     
			$data = array('status'=>'success', 'msg'=>"User chatroom Fetched Successfully", 'result'=>$resultSet);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;  
		}  
	}

	public static function getMessagesByuserId($formdata){
		try {   
			$resultSet = array(); 		
     		$query = "SELECT m.message, m.created_date, m.msg_from, u.fname, u.lname, u.p_img FROM `messages` m INNER JOIN `users` u ON(u.id = m.msg_from) WHERE m.msg_to = '".base64_decode($formdata['scroll_with']['user_id'])."' AND m.status = 0 ORDER BY m.m_id DESC";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }			
		    if(mysqli_num_rows($result) > 0){
				while($row = mysqli_fetch_assoc($result)){
					if($row['p_img'] == ""){
						$row['p_img'] = 'user-avatar-placeholder.png';
					}
					$row['encryptId'] = $row['msg_from'];
					$row['time_ago'] = self::timeago($row['created_date']);						
					$resultSet[] = $row;
				}
			}	

			$query = "UPDATE `messages` SET status = 1 WHERE msg_to = '".base64_decode($formdata['scroll_with']['user_id'])."' AND status = 0";
 			$result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }			     
			$data = array('status'=>'success', 'msg'=>"User messages Fetched Successfully", 'result'=>$resultSet);	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
	}

	public static function setMessagesToReadAllByUserId($userId){
		try {   			
     		$query = "UPDATE `messages` SET status = 1 WHERE msg_to = '".$userId."'";
		    $result = dbconfig::run($query);
		    if(!$result) {
			   throw new exception("No server response!");
		    }
				     
			$data = array('status'=>'success', 'msg'=>"User Read All messages Successfully", 'result'=>'');	
		} catch (Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		} finally {
			return $data;
		}  
	}

   public static function addCardPayment($formdata){  
		try{
			$resultSet = array();

			$query = "INSERT INTO `payments` (payment_from, payment_to, charge_id, amount, payment_date) VALUES ('".$formdata['eId']."', '".$formdata['fId']."', '".$formdata['cId']."', '".$formdata['amount']."', '".date('Y-m-d H:i:s')."')";
			$lastInsertId = dbconfig::insertrun($query);
			if(!$lastInsertId) {
				throw new exception("No server response!");
			}
			$query = "UPDATE `job_proposals` SET payment_id = '".$lastInsertId."' WHERE job_post_id = '".$formdata['jId']."' AND user_id = '".$formdata['fId']."'";
			$result = dbconfig::run($query);    
		    if(!$result) {
			   throw new exception("No server response!");
		    }

		    $notify = self::addNotification($formdata['eId'], $formdata['fId'], $formdata['jId'], 'payment', 'make a payment for', $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']);
		
			$data = array('status'=>'success', 'msg'=>"Your payment has charged successfull.", 'result'=>'');
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}

	public static function requestForPayment($formdata){  
		try{

		    $notify = self::addNotification(base64_decode($formdata['fId']), base64_decode($formdata['eId']), base64_decode($formdata['jId']), 'payment', 'request a payment for', $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']);
		
			$data = array('status'=>'success', 'msg'=>"Your request for payment sent successfully.", 'result'=>'');
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data;
		}
	}



	public static function getAddress($coords) {
		try{
	      //google map api url
	        $url = "https://maps.google.com/maps/api/geocode/json?latlng=".$coords['position']['coords']['latitude'].",".$coords['position']['coords']['longitude']."&key=".GOOGLE_API_KEY;
  
	        // send http request
	        $geocode = file_get_contents($url);
	        $json = json_decode($geocode);
	        $address = $json->results[0]->address_components;  
			
			foreach($address as $add){
				//echo "<pre>"; print_r($add);
				
				if($add->types[0] == "locality"){
					
					  $zip = $add->long_name;
					 
				} 
			}
			 
	       // $zip = end($address);      
		
			$data = array('status'=>'success', 'msg'=>"Your request for payment sent successfully.", 'result'=>$zip);
		}catch(Exception $e) {
			$data = array('status'=>'error', 'msg'=>$e->getMessage());
		}finally{
			return $data; 
		}

	}



	 public static function contactUsEmail($name, $email, $subject, $message){

     	try{

			$bodyContent = '<html><body style="background-color: #eeeeee; margin: 0 !important; padding: 0 !important;">';
			$bodyContent .= '<title></title>';
			$bodyContent .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
			$bodyContent .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
			$bodyContent .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
			$bodyContent .= '<style type="text/css">

			/ FONTS /

			@media screen {

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 400;

				  src: local("Lato Regular"), local("Lato-Regular"), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: normal;

				  font-weight: 700;

				  src: local("Lato Bold"), local("Lato-Bold"), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 400;

				  src: local("Lato Italic"), local("Lato-Italic"), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format("woff");

				}

				

				@font-face {

				  font-family: Lato;

				  font-style: italic;

				  font-weight: 700;

				  src: local("Lato Bold Italic"), local("Lato-BoldItalic"), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format("woff");

				}

			}

			

			/ CLIENT-SPECIFIC STYLES /

			body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }

			table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }

			img { -ms-interpolation-mode: bicubic; }			

			/ RESET STYLES /

			img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }

			table { border-collapse: collapse !important; }

			body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }



			/ iOS BLUE LINKS /

			a[x-apple-data-detectors] {

				color: inherit !important;

				text-decoration: none !important;

				font-size: inherit !important;

				font-family: inherit !important;

				font-weight: inherit !important;

				line-height: inherit !important;

			}

			

			/ MOBILE STYLES /

			@media screen and (max-width:600px){

				h1 {

					font-size: 32px !important;

					line-height: 32px !important;

				}

			}



			/ ANDROID CENTER FIX /

			div[style*="margin: 16px 0;"] { margin: 0 !important; }

		</style>';



				$bodyContent .= '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

			We"re thrilled to have you here! Get ready to dive into your new account.

		</div>

		';

				$bodyContent .= '<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Lato, Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

			We"re thrilled to have you here! Get ready to dive into your new account.

		</div>



		<table border="0" cellpadding="0" cellspacing="0" width="100%">

			<!-- LOGO -->

			<tbody><tr>

				<td bgcolor="#eee" align="center">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

						<tbody><tr>

							<td align="center" valign="top" style="padding: 50px 30px 50px 30px;">

								  <img src="'.SITE_URL.'assets/images/logo.png"/>

							</td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- HERO -->

			<tr>

				<td bgcolor="#eee" align="center" style="padding: 0px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 0px 30px -15px #808080;">

						<tbody><tr>

							<td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 3px; line-height: 48px;">

							  <h1 style="font-size: 48px; font-weight: 400; margin: 0;">Contact Us!</h1>

							</td>

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- COPY BLOCK -->

			<tr>

				<td bgcolor="#fcfcfc" align="center" style="padding: 0px 10px 0px 10px;">

						<!--[if (gte mso 9)|(IE)]>

						<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

							<td align="center" valign="top" width="600">

							<![endif]-->

						<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 20px 30px -15px #808080;">

						  <!-- COPY -->

						  <tbody>

						<tr>

							<td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px; letter-spacing: .4px">

								  <p style="margin: 0;">From : '.$name.',</p>

							</td>

							

						</tr>'; 


						$bodyContent .= '<tr><td bgcolor="#ffffff" align="left" style="padding: 0px 30px 20px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px; letter-spacing: .4px">

								  <p style="margin: 0;">Subject : '.$subject.'</p>

							</td></tr>'; 

					$bodyContent .= '<tr>

					 	  <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 20px 30px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

		  

					  <p style="margin: 0;">'.$message.'</p></td>

					 </tr>';

					  


					$bodyContent .= '  <tr>

						<td bgcolor="#ffffff" align="left" style="padding: 0px 30px 20px 30px; border-radius: 0px 0px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
						 	 <p style="margin: 0;">Thanks</p>	
						  </td>
					  </tr>';

				$bodyContent .= '  <tr>
						<td bgcolor="#ffffff" align="left" style="padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #1B2A50; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">

						 	 <p style="margin: 0;">PayMyPrice</p>	
						  </td>
					  </tr>	  

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- SUPPORT CALLOUT -->

			<tr>

				<td bgcolor="#ffffff" align="center" style="padding: 30px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->

					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;box-shadow: 0px 0px 30px -15px #808080;">

						<!-- HEADLINE -->  

						<tbody><tr>

						  

						</tr>

					</tbody></table>

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

			<!-- FOOTER -->

			<tr>

				<td bgcolor="#ffffff" align="center" style="padding: 0px 10px 0px 10px;">

					<!--[if (gte mso 9)|(IE)]>

					<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">

					<tr>

					<td align="center" valign="top" width="600">

					<![endif]-->
  
					

					<!--[if (gte mso 9)|(IE)]>

					</td>

					</tr>

					</table>

					<![endif]-->

				</td>

			</tr>

		</tbody></table>

		</body></html>'; 		

		$sentEmail = self::SendGridEmailForContactUs($bodyContent, $email, 'Contact Us');			 	

		$data = array('status'=>'success', 'msg'=>"You have been submit contact us form.", 'result'=>'');

    }catch(Exception $e){

       $data = array('status'=>'error', 'msg'=>$e->getMessage());

    }finally{

        return $data;
  
    }

   }


      public static function SendGridEmailForContactUs($template, $from, $subject) {
       	   require '../sendgrid/autoload.php'; // If you're using Composer (recommended)
			// Comment out the above line if not using Composer
			// require("<PATH TO>/sendgrid-php.php");
			// If not using Composer, uncomment the above line and
			// download sendgrid-php.zip from the latest release here,
			// replacing <PATH TO> with the path to the sendgrid-php.php file,
			// which is included in the download:
			// https://github.com/sendgrid/sendgrid-php/releases

			$email = new \SendGrid\Mail\Mail(); 
			$email->setFrom($from);
			$email->setSubject($subject);
      $email->addTo("info@paymyprice.com");	
      $bccsArray = [ 
        //user emails       =>  user names  
        "patrick@nationwideautomation.com" => "Patrick",
        "gustavo@echelonwebsites.com" => "Gustavo"
      ];
      $email->addBccs($bccsArray);
			//$email->addContent("text/plain", "and easy to do anywhere, even with PHP");
			$email->addContent("text/html", $template);
			$sendgrid = new \SendGrid('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');  
	    try{
	    	  $response = $sendgrid->send($email);
			    //echo"<pre>";print_r($response);die;
			$data = array('status'=>'success', 'msg'=>"We sent an email to user.", 'result'=>'');
	    }catch (Exception $e) { 
	       $data = array('status'=>'error', 'msg'=>$e->getMessage());
	    }finally {  
	        return $data;
	    }
    }


       public static function SendGridEmail($template, $to, $subject, $bccEmail) {
       	   require '../sendgrid/autoload.php'; // If you're using Composer (recommended)
			// Comment out the above line if not using Composer
			// require("<PATH TO>/sendgrid-php.php");
			// If not using Composer, uncomment the above line and
			// download sendgrid-php.zip from the latest release here,
			// replacing <PATH TO> with the path to the sendgrid-php.php file,
			// which is included in the download:
			// https://github.com/sendgrid/sendgrid-php/releases

			$email = new \SendGrid\Mail\Mail(); 
			$email->setFrom("info@paymyprice.com", "PayMyPrice");
			$email->setSubject($subject);
      $email->addTo($to);
      $bccsArray = [ 
        //user emails       =>  user names  
        "patrick@nationwideautomation.com" => "Patrick",
        "gustavo@echelonwebsites.com" => "Gustavo"
      ];
      $email->addBccs($bccsArray);
			$email->addContent("text/html", $template);
			$sendgrid = new \SendGrid('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');  
	    try{
	    	  $response = $sendgrid->send($email);
			    //echo"<pre>";print_r($response);die;
			$data = array('status'=>'success', 'msg'=>"We sent an email to user.", 'result'=>'');
	    }catch (Exception $e) { 
	       $data = array('status'=>'error', 'msg'=>$e->getMessage());
	    }finally {  
	        return $data;
	    }
    }
			



}