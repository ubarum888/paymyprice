<?php include('../helpers/classes/users.php'); 
$objUser= new USER(); 

if(isset($_SESSION['user']) && ($_SESSION['user']['result']['account_type'] == 1)){  // For Employer...
	if(count($_GET)){
		if(isset($_GET['popup']) && ($_GET['popup']!=="")){
				if($_GET['popup']=="rate-freelancer"){?>	
					<div id="small-dialog-2" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->

						<div class="sign-in-form">
							<ul class="popup-tabs-nav"></ul>
							<div class="popup-tabs-container"> 
								<!-- Tab -->
								<div class="popup-tab-content" id="tab2">	
									<!-- Welcome Text -->
									<div class="welcome-text">
										<h3>Leave a Review</h3>
										<span id="review-popup-header">Rate <a href="javascript:void(0);"></a> for the project <a href="javascript:void(0);"></a> </span>
									</div>	

									<!-- Form -->
									<form id="add_review" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('add_review')?>" data-name="<?= base64_encode('add_review')?>" method="POST" onsubmit="return validateForm(this.id, event)">
										<div class="feedback-yes-no">
											<strong>Was this delivered on budget?</strong>
											<div class="radio">
												<input id="radio-1" name="dob" type="radio" value="yes" required>
												<label for="radio-1"><span class="radio-label"></span> Yes</label>
											</div>

											<div class="radio">
												<input id="radio-2" name="dob" type="radio" value="no"  required>
												<label for="radio-2"><span class="radio-label"></span> No</label>
											</div>
										</div>


										<div class="feedback-yes-no">
											<strong>Was this delivered on time?</strong>
											<div class="radio">
												<input id="radio-3" name="dot" type="radio" value="yes"  required>
												<label for="radio-3"><span class="radio-label"></span> Yes</label>
											</div>

											<div class="radio">
												<input id="radio-4" name="dot" type="radio" value="no"  required>
												<label for="radio-4"><span class="radio-label"></span> No</label>
											</div>
										</div>

										<div class="feedback-yes-no">
											<strong>Your Rating</strong>
											<div class="leave-rating">
												<input type="radio" name="rating" id="rating-radio-1" value="5" required>
												<label for="rating-radio-1" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-2" value="4" required>
												<label for="rating-radio-2" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-3" value="3" required>
												<label for="rating-radio-3" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-4" value="2" required>
												<label for="rating-radio-4" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-5" value="1" required>
												<label for="rating-radio-5" class="icon-material-outline-star"></label>
											</div><div class="clearfix"></div>
										</div>
										<textarea class="with-border" placeholder="Comment" title="message" name="message" id="message2" cols="7" required></textarea>
										<input type="hidden" name="post_id" value="<?= $_GET['pId']?>"/>
										<input type="hidden" name="r_from" value="<?= $_GET['eId']?>"/>
										<input type="hidden" name="r_to" value="<?= $_GET['fId']?>"/>
										<button class="button full-width button-sliding-icon ripple-effect has-spinner" type="submit" name="submit">Leave a Review  <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
									</form>
								</div>    
							</div>
						</div>
					</div>
			<?php } 

			else if($_GET['popup']=="edit-rate-freelancer"){			
				$review = $objUser->getReviewDetailById($_GET['eId'], $_GET['pId']); ?>	
					<div id="small-dialog-2" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->

						<div class="sign-in-form">
							<ul class="popup-tabs-nav"></ul>
							<div class="popup-tabs-container"> 
								<!-- Tab -->
								<div class="popup-tab-content" id="tab2">	
									<!-- Welcome Text -->
									<div class="welcome-text">
										<h3>Change Review</h3>
										<span id="review-popup-header">Rate <a href="javascript:void(0);"></a> for the project <a href="javascript:void(0);"></a> </span>
									</div>	

									<!-- Form -->
									<form id="update_review" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('update_review')?>" data-name="<?= base64_encode('update_review')?>" method="POST" onsubmit="return validateForm(this.id, event)">
										<div class="feedback-yes-no">
											<strong>Was this delivered on budget?</strong>
											<div class="radio">
												<input id="radio-1" name="dob" type="radio" value="yes" <?php if($review['result']['dob'] == 'yes'){echo 'checked';} ?> required>
												<label for="radio-1"><span class="radio-label"></span> Yes</label>
											</div>

											<div class="radio">
												<input id="radio-2" name="dob" type="radio" value="no" <?php if($review['result']['dob'] == 'no'){echo 'checked';} ?> required>
												<label for="radio-2"><span class="radio-label"></span> No</label>
											</div>
										</div>


										<div class="feedback-yes-no">
											<strong>Was this delivered on time?</strong>
											<div class="radio">
												<input id="radio-3" name="dot" type="radio" value="yes" <?php if($review['result']['dot'] == 'yes'){echo 'checked';} ?> required>
												<label for="radio-3"><span class="radio-label"></span> Yes</label>
											</div>

											<div class="radio">
												<input id="radio-4" name="dot" type="radio" value="no" <?php if($review['result']['dot'] == 'no'){echo 'checked';} ?> required>
												<label for="radio-4"><span class="radio-label"></span> No</label>
											</div>
										</div>

										<div class="feedback-yes-no">
											<strong>Your Rating</strong>
												<div class="leave-rating">
												<input type="radio" name="rating" id="rating-radio-1" value="5" <?php if($review['result']['rating'] == 5){echo 'checked';} ?> required>
												<label for="rating-radio-1" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-2" value="4" <?php if($review['result']['rating'] == 4){echo 'checked';} ?> required>
												<label for="rating-radio-2" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-3" value="3" <?php if($review['result']['rating'] == 3){echo 'checked';} ?> required>
												<label for="rating-radio-3" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-4" value="2" <?php if($review['result']['rating'] == 2){echo 'checked';} ?> required>
												<label for="rating-radio-4" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-5" value="1" <?php if($review['result']['rating'] == 1){echo 'checked';} ?> required>
												<label for="rating-radio-5" class="icon-material-outline-star"></label>
											</div><div class="clearfix"></div>
										</div>
										<textarea class="with-border" placeholder="Comment" title="message" name="message" id="message2" cols="7" required><?= $review['result']['message']?></textarea>
										<input type="hidden" name="post_id" value="<?= $_GET['pId']?>"/>
										<input type="hidden" name="r_from" value="<?= $_GET['eId']?>"/>
										<input type="hidden" name="r_to" value="<?= $_GET['fId']?>"/>
										<button class="button full-width button-sliding-icon ripple-effect has-spinner" type="submit" name="submit">Leave a Review  <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
									</form>
								</div>    
							</div>
						</div>
					</div>
			<?php } 

			else if($_GET['popup']=="end-contract"){?>	  
					<div id="small-dialog-2" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->

						<div class="sign-in-form">
							<ul class="popup-tabs-nav">
								<li><a href="#tab">End Contract</a></li>
							</ul>
							<div class="popup-tabs-container"> 
								<!-- Tab -->
								<div class="popup-tab-content" id="tab2">	
									<!-- Welcome Text -->
									<div class="welcome-text">
										<h3>End Contract with <a href="javascript:void(0);"><?= base64_decode($_GET['fname'])?></a> for <a href="javascript:void(0);"><?= base64_decode($_GET['t'])?></a></h3>
									</div>	

									<!-- Form -->
									<form id="employer_end_contract" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('employer_end_contract')?>" data-name="<?= base64_encode('employer_end_contract')?>" method="POST" onsubmit="return validateForm(this.id, event)">								


										<div class="feedback-yes-no">
											<strong>Reason for ending contract</strong>
											<select class="with-border default margin-bottom-20" name="reason" data-size="7" title="reason" required>
												<option value="" selected disabled hidden>Select a reason</option>
												<option>Job completed successfully</option>
												<option>Freelancer not responsive</option>
												<option>Job was completed unsuccessfully</option>
												<option>Another reason</option>
										</select>
										</div>

										<div class="feedback-yes-no">
											<strong>Feedback To Freelancer</strong>
											<div class="leave-rating">
												<input type="radio" name="rating" id="rating-radio-1" value="5" required>
												<label for="rating-radio-1" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-2" value="4" required>
												<label for="rating-radio-2" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-3" value="3" required>
												<label for="rating-radio-3" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-4" value="2" required>
												<label for="rating-radio-4" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-5" value="1" required>
												<label for="rating-radio-5" class="icon-material-outline-star"></label>
											</div><div class="clearfix"></div>
										</div>
										<div class="feedback-yes-no">
											<strong>Share your experience with this freelancer. It may help other clients to understand Freelancer.</strong>
											<textarea class="with-border" placeholder="Comment" title="message" name="message" id="message2" cols="7" required></textarea>
										</div>	
										<input type="hidden" name="pId" value="<?= $_GET['pId']?>"/>	
										<input type="hidden" name="jId" value="<?= $_GET['jId']?>"/>	
										<input type="hidden" name="uId" value="<?= $_GET['uId']?>"/>
										<input type="hidden" name="toId" value="<?= $_GET['fId']?>"/>								
										<button class="button full-width button-sliding-icon ripple-effect has-spinner" type="submit" name="submit">End Contract  <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
									</form>
								</div>    
							</div>
						</div>
					</div>
			<?php } 		

			else if($_GET['popup']=="edit-note"){ 				
				$note = $objUser->getUserNoteById($_GET['id']);?>	
					<div id="small-dialog-2" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->

						<div class="sign-in-form">
							<ul class="popup-tabs-nav">
								<li><a href="#tab">Update Note</a></li>
							</ul>
							<div class="popup-tabs-container"> 
								<!-- Tab -->
								<div class="popup-tab-content" id="tab">										

									<div class="notification error closeable hide" id="validate_msg">
										<p>Please fill in all the fields required</p>
										<a class="close"></a>
									</div>		

									<!-- Form -->

									<form id="update_note" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('update_note')?>" data-name="<?= base64_encode('update_note')?>" method="POST" onsubmit="return validateForm(this.id, event)">
											<input type="hidden" name="n_id" value="<?= $_GET['id']?>"/>
										<select class="with-border default margin-bottom-20" name="priority" data-size="7" title="Priority">
											<option value="low" <?php if($note['result']['priority'] == 'low'){echo 'selected';}?>>Low Priority</option>
											<option value="medium" <?php if($note['result']['priority'] == 'medium'){echo 'selected';}?>>Medium Priority</option>
											<option value="high" <?php if($note['result']['priority'] == 'high'){echo 'selected';}?>>High Priority</option>
										</select>
										<!-- <input type="hidden" id="priority" name="priority"/> -->
										<textarea name="note" cols="10" title="Note" placeholder="Note" class="with-border"><?= $note['result']['note']?></textarea>
										<!-- Button -->

										<button class="button full-width button-sliding-icon ripple-effect has-spinner" type="submit" name="submit">Add Note  <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
									</form>
								</div>    
							</div>
						</div>
					</div>
			<?php } 


			else if($_GET['popup']=="delete-note"){ ?>					
				<div id="small-dialog" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->
						<div class="sign-in-form">  
							<ul class="popup-tabs-nav">
								<li><a href="#tab">Delete Note</a></li>
							</ul>
							<div class="popup-tabs-container">
								<!-- Tab -->
								<div class="popup-tab-content" id="tab">									
									<!-- Welcome Text -->  
									<div class="welcome-text">
										<h3>Do you really want to delete this note ?</h3>
									</div>																	
									<!-- Button -->
									<a href="<?= SITE_URL ?>helpers/functions.php?type=<?= base64_encode('delete_note')?>&del_id=<?= $_GET['id'] ?>" class="button full-width button-sliding-icon ripple-effect">Yes </a>									
								</div>
							</div>
						</div>
					</div>
			<?php } 

			else if($_GET['popup']=="delete-proposal"){ ?>					
				<div id="small-dialog" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->
						<div class="sign-in-form">  
							<ul class="popup-tabs-nav">
								<li><a href="#tab">Delete Proposal</a></li>
							</ul>
							<div class="popup-tabs-container">
								<!-- Tab -->
								<div class="popup-tab-content" id="tab">									
									<!-- Welcome Text -->  
									<div class="welcome-text">
										<h3>Do you really want to delete this proposal ?</h3>
									</div>																	
									<!-- Button -->
									<a href="<?= SITE_URL ?>helpers/functions.php?type=<?= base64_encode('delete_proposal')?>&del_id=<?= $_GET['pId'] ?>&fId=<?= $_GET['fId'] ?>&uId=<?= $_GET['uId'] ?>&jId=<?= $_GET['jId'] ?>" class="button full-width button-sliding-icon ripple-effect">Yes </a>									
								</div>
							</div>
						</div>
					</div>
			<?php } 


			else if($_GET['popup']=="accept-proposal"){ ?>					
				<div id="small-dialog" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->
						<div class="sign-in-form">  
							<ul class="popup-tabs-nav">
								<li><a href="#tab">Accept Offer</a></li>
							</ul>
							<div class="popup-tabs-container">
								<!-- Tab -->
								<div class="popup-tab-content" id="tab">

   
										<!-- Welcome Text -->
										<div class="welcome-text">
											<h3>Accept Offer From <?= base64_decode($_GET['fname'])?></h3>
											<div class="bid-acceptance margin-top-15">
												$<?= $_GET['b']?>
											</div>

										</div>  

										<form action="<?= SITE_URL ?>helpers/functions.php?type=<?= base64_encode('accept_proposal')?>&pId=<?= $_GET['pId'] ?>&fId=<?= $_GET['fId'] ?>&uId=<?= $_GET['uId'] ?>&jId=<?= $_GET['jId'] ?>" method="POST">
											<div class="radio">
												<input id="radio-1" name="radio" type="radio" required>
												<label for="radio-1"><span class="radio-label"></span>  I have read and agree to the Terms and Conditions</label>
											</div>
											<button class="margin-top-15 button full-width button-sliding-icon ripple-effect" type="submit" >Accept <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
										</form>								
								</div>
							</div>
						</div>
					</div>
			<?php } 


			else if($_GET['popup']=="hire"){ ?>					
				<div id="small-dialog" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->
						<div class="sign-in-form">  
							<ul class="popup-tabs-nav">
								<li><a href="#tab">Hire To Freelancer</a></li>
							</ul>
							<div class="popup-tabs-container">
								<!-- Tab -->
								<div class="popup-tab-content" id="tab">
   
										<!-- Welcome Text -->
										<div class="welcome-text">
											<h3>Hire <span class="hire-process"><?= ucfirst(base64_decode($_GET['fname']))?></span> for <span class="hire-process"> <?= $_GET['title']?></span></h3>
										</div>  

										<form id="hire_to_freelancer" action="<?= SITE_URL ?>helpers/functions.php?type=<?= base64_encode('hire_to_freelancer')?>&pId=<?= $_GET['pId'] ?>&fId=<?= $_GET['fId'] ?>&uId=<?= $_GET['uId'] ?>&jId=<?= $_GET['jId'] ?>" data-name="<?= base64_encode('hire_to_freelancer')?>" method="POST" onsubmit="return validateForm(this.id, event)">
									

											<textarea cols="10" title="Message" placeholder="Share your Project detail with Freelancer" class="with-border" name="msg"></textarea>											
			                            	<input type="hidden" name="from" title="Reload page" class="from" value="<?= base64_decode($_GET['uId']) ?>"/>
			                            	<input type="hidden" name="to" title="Reload page" class="to" value="<?=  base64_decode($_GET['fId']) ?>" />
			                            	<input type="hidden" name="typing" class="typing" value="false"/>

			                            	<div class="notification notice">
												<p>We are sending this details to <?= base64_decode($_GET['fname'])?> message box directly.</p>
											</div>

											<button class="margin-top-15 button full-width button-sliding-icon ripple-effect" name="submit" type="submit" >Send Message <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
										</form>								
								</div>
							</div>
						</div>
					</div>
			<?php } 

			else if($_GET['popup']=="make-offer"){ ?>					
				<!-- Make an Offer Popup
						================================================== -->

						<div id="small-dialog" class="zoom-anim-dialog dialog-with-tabs">
							<!--Tabs -->
							<div class="sign-in-form">
								<ul class="popup-tabs-nav">  
									<li><a href="#tab">Make an Offer</a></li>
								</ul>
								<div class="popup-tabs-container">			<!-- Tab -->
									<div class="popup-tab-content" id="tab">	
										<!-- Welcome Text -->
										<div class="welcome-text">											
											<h3>Discuss your project with  <a href="javascript:void(0);"><?= base64_decode($_GET['fname'])?></a></h3>
										</div>											

										<!-- Form -->
										<form id="make_offer" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('make_offer')?>&uId=<?= $_GET['uId'] ?>&fId=<?= $_GET['fId'] ?>&femail<?= $_GET['femail']?>" data-name="<?= base64_encode('make_offer')?>" method="POST" onsubmit="return validateForm(this.id, event)">  

											<textarea cols="10" placeholder="Message" title="message" name="msg" class="with-border" title="Required Field"></textarea>							
			                            	<input type="hidden" name="from" title="Reload page" class="from" value="<?= base64_decode($_GET['uId']) ?>"/>
			                            	<input type="hidden" name="to" title="Reload page" class="to" value="<?=  base64_decode($_GET['fId']) ?>" />
			                            	<input type="hidden" name="typing" class="typing" value="false"/>

			                            	<div class="notification notice">
												<p>We are sending this details to <?= base64_decode($_GET['fname'])?> message box directly.</p>
											</div>
											<button class="button margin-top-35 full-width button-sliding-icon ripple-effect has-spinner" type="submit" name="submit">Make an Offer <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
										</form>							
									</div> 

								</div>
							</div>
						</div>
						<!-- Make an Offer Popup / End -->
			<?php } 

			else if($_GET['popup']=="make-payment"){ 
				$make_payment = $objUser->getUserCardDetailByUserId($_GET['eId']);?>	

				<!-- Make an Offer Popup
						================================================== -->

						<div id="small-dialog" class="zoom-anim-dialog dialog-with-tabs">
							<!--Tabs -->
							<div class="sign-in-form">
								<ul class="popup-tabs-nav">
									<li><a href="#tab">Make Payment</a></li>
								</ul>
								<div class="popup-tabs-container">			<!-- Tab -->
									<div class="popup-tab-content" id="tab">	
										<!-- Welcome Text -->

										<div class="welcome-text">
											<h3>Make a payment for <a href="javascript:void(0);"><?= base64_decode($_GET['t'])?></a> to <a href="javascript:void(0);"><?= base64_decode($_GET['n'])?></a></h3>
											
											<div class="bidding-inner">
												<span class="bidding-detail">Activate Card </span>	


												<div class="input-with-icon">
													<span><img src="<?= SITE_URL?>uploads/brand/<?= $make_payment['result']['brand'] ?>.png" width="50"></span>
													<span>************<?= $make_payment['result']['account_no']?></span>
													<span><?= $make_payment['result']['expire_card']?></span>
												</div>
										
												<a href="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('make_payment')?>&a=<?= $_GET['a']?>&aId=<?= $_GET['aId']?>&cId=<?= $_GET['cId']?>&fId=<?= $_GET['fId']?>&eId=<?= $_GET['eId']?>&jId=<?= $_GET['jId'] ?>" class="button margin-top-35 full-width button-sliding-icon ripple-effect has-spinner">Pay $<?= base64_decode($_GET['a'])?> </a>											
											</div>	
										</div>	


									</div>

								</div>
							</div>
						</div>
						<!-- Make an Offer Popup / End -->
			<?php } 

			else if($_GET['popup']=="delete-card"){ ?>					
				<div id="small-dialog" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->
						<div class="sign-in-form">  
							<ul class="popup-tabs-nav">
								<li><a href="#tab">Delete Card</a></li>
							</ul>
							<div class="popup-tabs-container">
								<!-- Tab -->
								<div class="popup-tab-content" id="tab">									
									<!-- Welcome Text -->  
									<div class="welcome-text">
										<h3>Do you really want to delete this card ?</h3>
									</div>																	
									<!-- Button -->
									<a href="<?= SITE_URL ?>helpers/functions.php?type=<?= base64_encode('delete_card_customer')?>&del_id=<?= $_GET['id'] ?>" class="button full-width button-sliding-icon ripple-effect">Yes </a>	
								</div>
							</div>
						</div>
					</div>
			<?php } 
		} 
	} 
}



if(isset($_SESSION['user']) && ($_SESSION['user']['result']['account_type'] == 2)){  // For Freelancer...
	if(count($_GET)){
		if(isset($_GET['popup']) && ($_GET['popup']!=="")){
				if($_GET['popup']=="rate-employer"){?>	
					<div id="small-dialog-2" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->

						<div class="sign-in-form">
							<ul class="popup-tabs-nav"></ul>
							<div class="popup-tabs-container"> 
								<!-- Tab -->
								<div class="popup-tab-content" id="tab2">	
									<!-- Welcome Text -->
									<div class="welcome-text">
										<h3>Leave a Review</h3>
										<span id="review-popup-header">Rate <a href="javascript:void(0);"></a> for the project <a href="javascript:void(0);"></a> </span>
									</div>	

									<!-- Form -->
									<form id="add_review" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('add_review')?>" data-name="<?= base64_encode('add_review')?>" method="POST" onsubmit="return validateForm(this.id, event)">
										<div class="feedback-yes-no">
											<strong>Was this delivered on budget?</strong>
											<div class="radio">
												<input id="radio-1" name="dob" type="radio" value="yes" required>
												<label for="radio-1"><span class="radio-label"></span> Yes</label>
											</div>

											<div class="radio">
												<input id="radio-2" name="dob" type="radio" value="no"  required>
												<label for="radio-2"><span class="radio-label"></span> No</label>
											</div>
										</div>


										<div class="feedback-yes-no">
											<strong>Was this delivered on time?</strong>
											<div class="radio">
												<input id="radio-3" name="dot" type="radio" value="yes"  required>
												<label for="radio-3"><span class="radio-label"></span> Yes</label>
											</div>

											<div class="radio">
												<input id="radio-4" name="dot" type="radio" value="no"  required>
												<label for="radio-4"><span class="radio-label"></span> No</label>
											</div>
										</div>

										<div class="feedback-yes-no">
											<strong>Your Rating</strong>
											<div class="leave-rating">
												<input type="radio" name="rating" id="rating-radio-1" value="5" required>
												<label for="rating-radio-1" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-2" value="4" required>
												<label for="rating-radio-2" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-3" value="3" required>
												<label for="rating-radio-3" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-4" value="2" required>
												<label for="rating-radio-4" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-5" value="1" required>
												<label for="rating-radio-5" class="icon-material-outline-star"></label>
											</div><div class="clearfix"></div>
										</div>
										<textarea class="with-border" placeholder="Comment" title="message" name="message" id="message2" cols="7" required></textarea>
										<input type="hidden" name="post_id" value="<?= $_GET['pId']?>"/>
										<input type="hidden" name="r_from" value="<?= $_GET['fId']?>"/>
										<input type="hidden" name="r_to" value="<?= $_GET['eId']?>"/>
										<button class="button full-width button-sliding-icon ripple-effect has-spinner" type="submit" name="submit">Leave a Review  <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
									</form>
								</div>    
							</div>
						</div>
					</div>
			<?php } 

			else if($_GET['popup']=="edit-rate-employer"){			
				$review = $objUser->getReviewDetailById($_GET['fId'], $_GET['pId']); ?>	
					<div id="small-dialog-2" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->

						<div class="sign-in-form">
							<ul class="popup-tabs-nav"></ul>
							<div class="popup-tabs-container"> 
								<!-- Tab -->
								<div class="popup-tab-content" id="tab2">	
									<!-- Welcome Text -->
									<div class="welcome-text">
										<h3>Change Review</h3>
										<span id="review-popup-header">Rate <a href="javascript:void(0);"></a> for the project <a href="javascript:void(0);"></a> </span>
									</div>	

									<!-- Form -->
									<form id="update_review" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('update_review')?>" data-name="<?= base64_encode('update_review')?>" method="POST" onsubmit="return validateForm(this.id, event)">
										<div class="feedback-yes-no">
											<strong>Was this delivered on budget?</strong>
											<div class="radio">
												<input id="radio-1" name="dob" type="radio" value="yes" <?php if($review['result']['dob'] == 'yes'){echo 'checked';} ?> required>
												<label for="radio-1"><span class="radio-label"></span> Yes</label>
											</div>

											<div class="radio">
												<input id="radio-2" name="dob" type="radio" value="no" <?php if($review['result']['dob'] == 'no'){echo 'checked';} ?> required>
												<label for="radio-2"><span class="radio-label"></span> No</label>
											</div>
										</div>


										<div class="feedback-yes-no">
											<strong>Was this delivered on time?</strong>
											<div class="radio">
												<input id="radio-3" name="dot" type="radio" value="yes" <?php if($review['result']['dot'] == 'yes'){echo 'checked';} ?> required>
												<label for="radio-3"><span class="radio-label"></span> Yes</label>
											</div>

											<div class="radio">
												<input id="radio-4" name="dot" type="radio" value="no" <?php if($review['result']['dot'] == 'no'){echo 'checked';} ?> required>
												<label for="radio-4"><span class="radio-label"></span> No</label>
											</div>
										</div>

										<div class="feedback-yes-no">
											<strong>Your Rating</strong>
												<div class="leave-rating">
												<input type="radio" name="rating" id="rating-radio-1" value="5" <?php if($review['result']['rating'] == 5){echo 'checked';} ?> required>
												<label for="rating-radio-1" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-2" value="4" <?php if($review['result']['rating'] == 4){echo 'checked';} ?> required>
												<label for="rating-radio-2" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-3" value="3" <?php if($review['result']['rating'] == 3){echo 'checked';} ?> required>
												<label for="rating-radio-3" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-4" value="2" <?php if($review['result']['rating'] == 2){echo 'checked';} ?> required>
												<label for="rating-radio-4" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-5" value="1" <?php if($review['result']['rating'] == 1){echo 'checked';} ?> required>
												<label for="rating-radio-5" class="icon-material-outline-star"></label>
											</div><div class="clearfix"></div>
										</div>
										<textarea class="with-border" placeholder="Comment" title="message" name="message" id="message2" cols="7" required><?= $review['result']['message']?></textarea>
										<input type="hidden" name="post_id" value="<?= $_GET['pId']?>"/>
										<input type="hidden" name="r_from" value="<?= $_GET['fId']?>"/>
										<input type="hidden" name="r_to" value="<?= $_GET['eId']?>"/>
										<button class="button full-width button-sliding-icon ripple-effect has-spinner" type="submit" name="submit">Leave a Review  <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
									</form>
								</div>    
							</div>
						</div>
					</div>
			<?php } 

			else if($_GET['popup']=="end-contract"){?>	  
					<div id="small-dialog-2" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->

						<div class="sign-in-form">
							<ul class="popup-tabs-nav">
								<li><a href="#tab">End Contract</a></li>
							</ul>
							<div class="popup-tabs-container"> 
								<!-- Tab -->
								<div class="popup-tab-content" id="tab2">	
									<!-- Welcome Text -->
									<div class="welcome-text">									
										<h3>End Contract for <a href="javascript:void(0);"><?= base64_decode($_GET['t'])?></a></h3>
									</div>	

									<!-- Form -->
									<form id="freelancer_end_contract" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('freelancer_end_contract')?>" data-name="<?= base64_encode('freelancer_end_contract')?>" method="POST" onsubmit="return validateForm(this.id, event)">

										<div class="feedback-yes-no">
											<strong>Reason for ending contract</strong>
											<select class="with-border default margin-bottom-20" name="reason" data-size="7" title="reason" required>
												<option value="" selected disabled hidden>Select a reason</option>
												<option>Job completed successfully</option>
												<option>Client not responsive</option>
												<option>Job was completed unsuccessfully</option>
												<option>Another reason</option>
										</select>
										</div>

										<div class="feedback-yes-no">
											<strong>Feedback To Client</strong>
											<div class="leave-rating">
												<input type="radio" name="rating" id="rating-radio-1" value="5" required>
												<label for="rating-radio-1" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-2" value="4" required>
												<label for="rating-radio-2" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-3" value="3" required>
												<label for="rating-radio-3" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-4" value="2" required>
												<label for="rating-radio-4" class="icon-material-outline-star"></label>
												<input type="radio" name="rating" id="rating-radio-5" value="1" required>
												<label for="rating-radio-5" class="icon-material-outline-star"></label>
											</div><div class="clearfix"></div>
										</div>
										<div class="feedback-yes-no">
											<strong>Share your experience with this client. It may help other freelancer to understand client.</strong>
											<textarea class="with-border" placeholder="Comment" title="message" name="message" id="message2" cols="7" required></textarea>
										</div>	
										<input type="hidden" name="pId" value="<?= $_GET['pId']?>"/>	
										<input type="hidden" name="jId" value="<?= $_GET['jId']?>"/>	
										<input type="hidden" name="uId" value="<?= $_GET['uId']?>"/>
										<input type="hidden" name="toId" value="<?= $_GET['eId']?>"/>								
										<button class="button full-width button-sliding-icon ripple-effect has-spinner" type="submit" name="submit">End Contract  <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
									</form>
								</div>    
							</div>
						</div>
					</div>
			<?php }

			else if($_GET['popup']=="edit-proposal"){ 
				$bid = $objUser->getBidDetailById($_GET['pId']);
				$deliver = explode(' ', $bid['result']['delivery_time']);
				?>						
				<div id="small-dialog" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->
						<div class="sign-in-form">  
							<ul class="popup-tabs-nav">
								<li><a href="#tab">Update a Bid</a></li>
							</ul>
							<div class="popup-tabs-container">
								<!-- Tab -->
								<div class="popup-tab-content" id="tab">

   
										<!-- Welcome Text -->
									<div class="welcome-text">
										<h3 class="margin-bottom-25">Bid on this job! </h3>
									</div>  
										
									<!-- Form -->
									<form id="edit_bid_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('edit_bid_form')?>&job=<?= $_GET['job']?>" data-name="<?= base64_encode('edit_bid_form')?>" method="POST" onsubmit="return validateForm(this.id, event)" autocomplete="off">	
											<input type="hidden" name="pId" value="<?= $_GET['pId']?>"/>	
											<input type="hidden" name="eId" value="<?= base64_encode($bid['result']['proposal_for'])?>"/>	
											<input type="hidden" name="fId" value="<?= base64_encode($bid['result']['user_id'])?>"/>
											<input type="hidden" name="jId" value="<?= base64_encode($bid['result']['job_post_id'])?>"/>								
											<div class="bidding-inner">
												<span class="bidding-detail">Set your <strong>minimal rate</strong></span>	
										
												<div class="input-with-icon">
													<input class="with-border" type="text" name="minimal_rate" title="Minimal Rate" onkeypress="return isNumberKey(event)" placeholder="Minimal Rate" value="<?= $bid['result']['minimal_rate']?>">
													<i class="currency">USD</i>
												</div>		  
												
												
												<!-- Headline -->
												<span class="bidding-detail margin-top-30">Set your <strong>delivery time</strong></span>
												<!-- Fields -->
												<div class="bidding-fields row">
													<div class="bidding-field col-xl-6">
														<!-- Quantity Buttons -->
														<div class="qtyButtons">
															<div class="qtyDec"></div>
															<input type="text" name="delivery_qty" value="<?= $deliver[0]?>">
															<div class="qtyInc"></div>
														</div>
													</div>
													<div class="bidding-field col-xl-6">
														<select class="default" name="delivery_time">
															<option value="days" <?php if($deliver[1] == 'days'){echo 'selected';}?>>Days</option>
															<option value="hours" <?php if($deliver[1] == 'hours'){echo 'selected';}?>>Hours</option>
														</select>
													</div>
												</div>

												<!-- Headline -->
												<span class="bidding-detail margin-top-30">Write your <strong>porposal</strong> to win this job</span>		

												<div class="form-group margin-top-30">
													<textarea class="form-control" name="proposal" rows="10" cols="95" title="write your porposal to win this job"><?= $bid['result']['proposal']?></textarea>
												</div>	
											</div>		
												
										<!-- Button -->				
										<button type="submit" name="submit" class="button margin-top-35 full-width button-sliding-icon ripple-effect has-spinner">Place a Bid <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>

									</form>							
								</div>
							</div>
						</div>
					</div>
			<?php } 

			else if($_GET['popup']=="delete-card"){ ?>					
				<div id="small-dialog" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->
						<div class="sign-in-form">  
							<ul class="popup-tabs-nav">
								<li><a href="#tab">Delete Card</a></li>
							</ul>
							<div class="popup-tabs-container">
								<!-- Tab -->
								<div class="popup-tab-content" id="tab">									
									<!-- Welcome Text -->  
									<div class="welcome-text">
										<h3>Do you really want to delete this card ?</h3>
									</div>																	
									<!-- Button -->
									<a href="<?= SITE_URL ?>helpers/functions.php?type=<?= base64_encode('delete_card_account')?>&del_id=<?= $_GET['id'] ?>" class="button full-width button-sliding-icon ripple-effect">Yes </a>	
								</div>
							</div>
						</div>
					</div>
			<?php } 
		} 
	} 
}

if(isset($_SESSION['admin']) && ($_SESSION['admin']['result']['account_type'] == 3)){  // For Employer... // For Admin...
	if(count($_GET)){
		if(isset($_GET['popup']) && ($_GET['popup']!=="")){
			if($_GET['popup']=="delete_user"){?>	
				<div id="small-dialog" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->
						<div class="sign-in-form">  
							<ul class="popup-tabs-nav">
								<li><a href="#tab">Delete User</a></li>
							</ul>
							<div class="popup-tabs-container">
								<!-- Tab -->
								<div class="popup-tab-content" id="tab">									
									<!-- Welcome Text -->  
									<div class="welcome-text">
										<h3>Do you really want to delete a user ?</h3>
									</div>																	
									<!-- Button -->
									<a href="<?= SITE_URL ?>helpers/functions.php?type=<?= base64_encode('delete_user')?>&del_id=<?= $_GET['id'] ?>" class="button full-width button-sliding-icon ripple-effect">Yes </a>									
								</div>
							</div>
						</div>
					</div>
			<?php }	

			if($_GET['popup'] == "delete_category"){?>	
				<div id="small-dialog" class="zoom-anim-dialog dialog-with-tabs">
						<!--Tabs -->
						<div class="sign-in-form">  
							<ul class="popup-tabs-nav">
								<li><a href="#tab">Delete Category</a></li>
							</ul>
							<div class="popup-tabs-container">
								<!-- Tab -->
								<div class="popup-tab-content" id="tab">									
									<!-- Welcome Text -->  
									<div class="welcome-text">
										<h3>Do you really want to delete a category ?</h3>
									</div>																	
									<!-- Button -->
									<a href="<?= SITE_URL ?>helpers/functions.php?type=<?= base64_encode('delete_category')?>&del_id=<?= $_GET['id'] ?>" class="button full-width button-sliding-icon ripple-effect">Yes </a>									
								</div>
							</div>
						</div>
					</div>
			<?php }	

		}	
	}
}		

?>