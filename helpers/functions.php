<?php require_once('classes/users.php'); 
require_once('classes/admin.php');   
require_once('classes/stripe.php');   
   
$type = base64_decode($_GET['type']);
//$type = $_GET['type'];
//echo "<pre>";print_r($type);  
if($type == 'signin_form'){  
	$call =  USER::login($_POST); 
		
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
	}else{
		$_SESSION['user'] = $call;  
		if($call['result']['account_type'] == 1){
			$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'off', 'redirect'=>'on', 'href'=>CLIENT_URL);
		}else if($call['result']['account_type'] == 2){
			$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'off', 'redirect'=>'on', 'form_reset'=>'on', 'href'=>VENDOR_URL);
		}
	}	
	echo json_encode($show); 	   
}


else if($type == 'admin_signin_form'){  
	$call =  ADMIN::login($_POST); 		
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
	}else{
		$_SESSION['admin'] = $call; 
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'off', 'redirect'=>'on', 'href'=>ADMIN_URL);	
	}	
	echo json_encode($show); 	   
}


else if($type == "user_account_activate"){
	$call = ADMIN::userAccountActivate($_POST);	
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on');
	}	
	echo json_encode($show);    
}  


else if($type == 'logout'){		
	unset($_SESSION["user"]);	
	header("location:".SITE_URL);	
}

else if($type == 'admin_logout'){		
	unset($_SESSION["admin"]);	
	header("location:".ADMIN_URL.'login.php');	
}

else if($type == "freelancer_signup_form"){	
	//echo "<pre>";print_r($_POST);die; 
	$stripe = STRIPE::createFreelancerStripeAccount($_POST);	  
		if($stripe['status'] == 'error'){
			$show = array("msg" => 'error', 'notice'=>$stripe['msg'], 'notice_show'=>'on');				
	}else{		
		$call = USER::freelancerSignUp($_POST, $stripe['result']);
		if($call['status'] == 'error'){
			$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
		}else{
			$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'off', 'redirect'=>'off', 'href'=>VENDOR_URL.'signup-success.php', 'form_reset'=>'on');
		}	
	}
	echo json_encode($show);  
}

else if($type == "employer_signup_form"){	
	$stripe = STRIPE::addEmployerAsCustomer($_POST['email']);	
		if($stripe['status'] == 'error'){
			$show = array("msg" => 'error', 'notice'=>$stripe['msg'], 'notice_show'=>'on');				
	}else{		
		$call = USER::employerSignUp($_POST, $stripe['result']);
		if($call['status'] == 'error'){
			$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
		}else{
			$show = array("msg" => 'success', 'notice'=>$call['msg'], 'redirect'=>'off', 'href'=>CLIENT_URL.'signup-success.php', 'form_reset'=>'on');
		}	
	}
	echo json_encode($show);  
}


else if($type == "forgot_form"){	
	$call = USER::forgotPassword($_POST['email']);
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'redirect'=>'off', 'href'=>'', 'form_reset'=>'on');
	}	
	echo json_encode($show);  
}

else if($type == "job_posting_form"){	
	//echo "<pre>";print_r($_POST);die;  
	$call = USER::jobPost($_POST);
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'redirect'=>'off', 'href'=>'', 'form_reset'=>'on');
	}	
	echo json_encode($show);  
}


else if($type == "job_update_form"){	
	//echo "<pre>";print_r($_POST);die;  
	$call = USER::updateJobPost($_POST);
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'redirect'=>'on', 'href'=>CLIENT_URL.'manage-posts.php', 'form_reset'=>'on');
	}	
	echo json_encode($show);  
}


else if($type == "email_verify"){	
	$call = USER::userVerify(base64_decode($_GET['id']));	
	if($call['status'] == 'error') {
		FD_add_notices('Please check Username or Password', 'error');
		header("location:".SITE_URL);  
		exit;
	}else{
		$_SESSION['user'] = $call;
		if($call['result']['account_type'] == 1){
			header("location:".CLIENT_URL);
			exit;
		}else if($call['result']['account_type'] == 2){
			header("location:".VENDOR_URL);
			exit;
		}		
	} 	
}

else if($type == "make_offer"){
	$call = USER::makeAnOffer($_REQUEST);	
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
	}else{		
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'form_reset'=>'on', 'result'=>$call['result']);
	}	
	echo json_encode($show);  
}

else if($type == "bid_form"){
	$call = USER::placeBid($_POST);	 
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'redirect'=>'on', 'notice_show'=>'on', 'href'=>VENDOR_URL.'job-detail.php?job='.$_POST['post_id'], 'form_reset'=>'on');
	}	
	echo json_encode($show);  
}

else if($type == "edit_bid_form"){
	$call = USER::updateBid($_POST);	
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'redirect'=>'on', 'notice_show'=>'on', 'href'=>VENDOR_URL.'job-detail.php?job='.$_GET['job'], 'form_reset'=>'on');
	}	
	echo json_encode($show);  
}

else if($type == "make_payment"){
	//echo "<pre>";print_r($_GET); 	die;
	$stripe = STRIPE::makeCardPayment($_GET);	

	if($stripe['status'] == 'error'){
		$_SESSION['alert_msg'] = array("msg" => 'error', 'notice'=>$stripe['msg']);		
	}else{
		$call = USER::addCardPayment($stripe['result']);
		if($call['status'] == 'error'){
			$_SESSION['alert_msg'] = array("msg" => 'error', 'notice'=>$call['msg']);			
		}else{			
			$_SESSION['alert_msg'] = array("msg" => 'success', 'notice'=>$call['msg']);
		}
	}	
	//echo json_encode($show); 
	header("location:".CLIENT_URL.'manage-bidders.php?post='.$_GET['jId']); 
} 

else if($type == "request_for_payment"){
	//echo "<pre>";print_r($_GET); 	die;
	
		$call = USER::requestForPayment($_GET);
		if($call['status'] == 'error'){
			$_SESSION['alert_msg'] = array("msg" => 'error', 'notice'=>$call['msg']);			
		}else{			
			$_SESSION['alert_msg'] = array("msg" => 'success', 'notice'=>$call['msg']);
		}
	header("location:".VENDOR_URL.'manage-jobs.php'); 
} 

else if($type == "subcategory_list"){
	$call = USER::getAllSubcategoriesByCategoryId($_POST);	
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'result'=>$call['result'], 'redirect'=>'off', 'href'=>'');
	}	
	echo json_encode($show);    
}

else if($type == "user_password_change"){		
	$call = USER::passwordChange($_REQUEST);		
	if($call['status'] == 'error'){			
		FD_add_notices($call['msg'], 'error');
		header('location:'.SITE_URL.'account.php?tab=password');	
	}else{
		FD_add_notices($call['msg'], 'success');	
		header('location:'.SITE_URL.'account.php?tab=password');	
	}
}


else if($type == "delete_upload_id"){	  
	$call = USER::uploadIDFileUnlink($_POST['name']);		  
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
	}else{
		$show = array("msg" => 'success');
	}	
	echo json_encode($show);    
}

else if($type == "accept_proposal"){	  
	$call = USER::acceptProposal($_GET);			  
	if($call['status'] == 'error'){			
		$_SESSION['alert_msg'] = array("msg" => 'error', 'notice'=>$call['msg']);		
	}else{
		$_SESSION['alert_msg'] = array("msg" => 'success', 'notice'=>$call['msg']);
	}  
	header('location:'.CLIENT_URL.'manage-bidders.php?post='.$_GET['jId']);	   
}  

else if($type == "delete_proposal"){	  
	$call = USER::deleteProposal($_GET);		  
	if($call['status'] == 'error'){			
		$_SESSION['alert_msg'] = array("msg" => 'error', 'notice'=>$call['msg']);		
	}else{
		$_SESSION['alert_msg'] = array("msg" => 'success', 'notice'=>$call['msg']);
	}  	  
	header('location:'.CLIENT_URL.'manage-bidders.php?post='.$_GET['jId']);	   
} 


else if($type == "delete_card_account"){	
	$stripe = STRIPE::deleteCardToFreelancerAccount(base64_decode($_GET['del_id']), $_SESSION['user']['result']['id']);	
	if($stripe['status'] == 'error'){
			$_SESSION['alert_msg'] = array("msg" => 'error', 'notice'=>$stripe['msg']);				
	}else{		
		$call = USER::deleteCard($_GET['del_id']);		  
		if($call['status'] == 'error'){			
			$_SESSION['alert_msg'] = array("msg" => 'error', 'notice'=>$call['msg']);		
		}else{
			$_SESSION['alert_msg'] = array("msg" => 'success', 'notice'=>$call['msg']);
		}
	}	
	header('location:'.VENDOR_URL.'settings.php');   
} 

else if($type == "delete_card_customer"){	
	$stripe = STRIPE::deleteCardToEmployerCustomer(base64_decode($_GET['del_id']), $_SESSION['user']['result']['id']);	
	if($stripe['status'] == 'error'){
			$_SESSION['alert_msg'] = array("msg" => 'error', 'notice'=>$stripe['msg']);				
	}else{		
		$call = USER::deleteCard($_GET['del_id']);		  
		if($call['status'] == 'error'){			
			$_SESSION['alert_msg'] = array("msg" => 'error', 'notice'=>$call['msg']);		
		}else{
			$_SESSION['alert_msg'] = array("msg" => 'success', 'notice'=>$call['msg']);
		}
	}	
	header('location:'.CLIENT_URL.'settings.php');   
} 

else if($type == "contact_form"){	  
	$call = USER::contactUs($_POST);		  
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'redirect'=>'off', 'form_reset'=>'on');
	}	
	echo json_encode($show);    
}

else if($type == "update_account_form"){
	if($_SESSION['user']['result']['account_type'] == 2){
		$stripe = STRIPE::updateFreelancerConnectedAccount($_POST, $_SESSION['user']['result']['id'], $_FILES['personal_id']);  	
		if($stripe['status'] == 'error'){
			$show = array("msg" => 'error', 'notice'=>$stripe['msg'], 'notice_show'=>'on');				
		}else{		
			$call = USER::updateAccount($_POST, $_SESSION['user']['result']['id'], null);	  	  
			if($call['status'] == 'error'){
				$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
			}else{		
				$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'form_reset'=>'off');
			}
		}
	}else{
		$call = USER::updateAccount($_POST, $_SESSION['user']['result']['id'], $_FILES['logo']);	  	  
		if($call['status'] == 'error'){
			$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
		}else{		
			$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'form_reset'=>'off');
		}
	}	  
 echo json_encode($show);  
}

else if($type == "verify_connected_account"){

	$stripe = STRIPE::verifyFreelancerConnectedAccount($_POST, $_SESSION['user']['result']['id'], $_FILES['personal_id']);  	
	if($stripe['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$stripe['msg'], 'notice_show'=>'on');				
	}else{		
		$call = USER::updateConnectedAccount($_POST, $_SESSION['user']['result']['id'], $_FILES['personal_id']);	  	  
		if($call['status'] == 'error'){
			$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
		}else{		
			$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'form_reset'=>'off', 'redirect'=>'on', 'href'=>VENDOR_URL.'settings.php');
		}
	}	  
 echo json_encode($show);  
}

else if($type == "add_detail_form"){	  
	$call = USER::addDetails($_POST, $_SESSION['user']['result']['id']);		  
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
	}else{		
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'form_reset'=>'off');
	}	
	echo json_encode($show);    
}	    

else if($type == "add_password_form"){	  
	$call = USER::changePassword($_POST, $_SESSION['user']['result']['id']);		  
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
	}else{		
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'form_reset'=>'on');
	}	
	echo json_encode($show);    
}


else if($type == "add_card_form"){	
	if($_SESSION['user']['result']['account_type'] == 1){
		$stripe = STRIPE::addCardToEmployerCustomer($_POST['token'], $_SESSION['user']['result']['id']);	
	}else if($_SESSION['user']['result']['account_type'] == 2){
		$stripe = STRIPE::addCardToFreelancerAccount($_POST['token'], $_SESSION['user']['result']['id']);	
	}else{
		$show = array("msg" => 'error', 'notice'=>'Account User Type not set', 'notice_show'=>'on');	
		echo json_encode($show); 
		exit;
	}
	
	if($stripe['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$stripe['msg'], 'notice_show'=>'on');				
	}else{		
		$call = USER::addCard($_POST, $stripe['result'], $_SESSION['user']['result']['id']);
		if($call['status'] == 'error'){
			$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
		}else{		
			$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'form_reset'=>'on', 'result'=>$call['result']);
		}	
	}
	echo json_encode($show);    
}

else if($type == "set_default_card"){	
	if($_SESSION['user']['result']['account_type'] == 1){
		$stripe = STRIPE::setDefaultCardToCustomer($_POST['id'], $_SESSION['user']['result']['id']);	
	}else if($_SESSION['user']['result']['account_type'] == 2){
		$stripe = STRIPE::setDefaultCardToAccount($_POST['id'], $_SESSION['user']['result']['id']);	
	}else{
		$show = array("msg" => 'error', 'notice'=>'Account User Type not set', 'notice_show'=>'on');	
		echo json_encode($show); 
		exit;
	}	  
	if($stripe['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$stripe['msg'], 'notice_show'=>'on');				
	}else{		
		$call = USER::updateCard($_POST['id'], $_SESSION['user']['result']['id']);
		if($call['status'] == 'error'){
			$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
		}else{		
			$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'form_reset'=>'off');
		}	
	}
	echo json_encode($show);    
}

else if($type == "add_note"){	  
	$call = USER::addNote($_POST, $_SESSION['user']['result']['id']);		  
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
	}else{		
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'form_reset'=>'on', 'result'=>$call['result']);
	}	
	echo json_encode($show);    
}

else if($type == "delete_note"){	  
	$call = USER::deleteNote($_GET['del_id']);	  	  
	// if($call['status'] == 'error'){
	// 	$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
	// }else{		
	// 	$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'form_reset'=>'on', 'result'=>$call['result']);
	// }	
	header('location:'.CLIENT_URL); 
}

else if($type == "update_note"){	  
	$call = USER::updateNote($_POST);		  
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
	}else{		
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'form_reset'=>'on', 'result'=>$call['result']);
	}	
	echo json_encode($show);    
}

else if($type == "add_review"){	  
	$call = USER::addReview($_POST);		  
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
	}else{		
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'form_reset'=>'on', 'result'=>$call['result']);
	}	
	echo json_encode($show);    
}


else if($type == "update_review"){	  
	$call = USER::updateReview($_POST);		  
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
	}else{		
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'form_reset'=>'on', 'result'=>$call['result']);
	}	
	echo json_encode($show);    
}  

elseif($type == "user_profile_upload"){	
	//echo "<pre>";print_r($_FILES);die;	
	$call = USER::profileUpload($_SESSION['user']['result']['id'], $_FILES['profile']);	
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');		
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'result'=>$call['result']);	
	}
	echo json_encode($show);
}

else if($type == "client_email_proposal"){	
	
	$call = USER::LoginViaEmail(base64_decode($_GET['id']));		
	if($call['status'] == 'error'){			
		header('location:'.SITE_URL);	
	}else{		
		$_SESSION['user'] = $call; 	
		header('location:'.CLIENT_URL.'manage-bidders.php?post='.$_GET['post']);	
	}
}

else if($type == "vendor_email_proposal"){	
//echo "<pre>";print_r($_REQUEST);die;  
	$call = USER::LoginViaEmail(base64_decode($_GET['id']));		
	if($call['status'] == 'error'){			
		header('location:'.SITE_URL);	
	}else{		
		$_SESSION['user'] = $call; 	
		header('location:'.VENDOR_URL.'job-detail.php?job='.$_GET['job']);	
	}  
}

else if($type == "get_messages"){	
//echo "<pre>";print_r($_REQUEST);die;  
	$call = USER::getMessages($_POST);		
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');		
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'result'=>$call['result']);	
	}
	echo json_encode($show);
}

else if($type == "infinite_scroll"){
	//echo "<pre>";print_r($_POST);die; 
	if($_POST['scroll_with']['scroll'] == 'my-jobs') { 
		$call = USER::getMyJobs($_POST);	  
	}else if($_POST['scroll_with']['scroll'] == 'my-posts') {      
		$call = USER::infiniteScrollMyPost($_POST);	  
	}else if($_POST['scroll_with']['scroll'] == 'job-listing') { 
		$call = USER::infiniteScrollJobListing($_POST);	
	}else if($_POST['scroll_with']['scroll'] == 'freelancer-listing') { 
		$call = USER::infiniteScrollFreelancerListing($_POST);	
	}else if($_POST['scroll_with']['scroll'] == 'freelancer-activity') { 
		$call = USER::infiniteScrollFreelancerActivity($_POST);	
	}else if($_POST['scroll_with']['scroll'] == 'my-notes') { 
		$call = USER::getAllNotesByUserId($_POST);	
	}else if($_POST['scroll_with']['scroll'] == 'rate-freelancer') { 
		$call = USER::getAllRateToFreelancer($_POST);	
	}else if($_POST['scroll_with']['scroll'] == 'rate-employer') { 
		$call = USER::getAllRateToEmployer($_POST);	
	}else if($_POST['scroll_with']['scroll'] == 'rate-from-user') { 
		$call = USER::getAllRatingsByUser($_POST);	
	}else if($_POST['scroll_with']['scroll'] == 'freelancer-rating') { 
		$call = USER::getAllRatingsByUser($_POST);	
	}else if($_POST['scroll_with']['scroll'] == 'freelancer-bid') { 
		$call = USER::getFreelancerBidsByJob($_POST);	
	}

	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');	  		
	}else{
		$show = array("msg" => 'success', 'data'=>$call['result'], 'content'=>$_POST['scroll_with']['scroll'], 'count'=>$call['count']);
	}	
	echo json_encode($show);  
}


else if($type == "view_notifications"){
	$call = USER::getAllNotificationsByUserId($_POST);	
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'result'=>$call['result']);
	}	
	echo json_encode($show);    
}  

else if($type == "view_messages"){
	$call = USER::getMessagesByuserId($_POST);	   
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'result'=>$call['result']);
	}	
	echo json_encode($show);    
}  

else if($type == "delete_user"){
	$call = ADMIN::deleteUser($_GET['del_id']);		
	header('location:'.ADMIN_URL.'manage-users.php');	
}

else if($type == "delete_category"){
	$call = ADMIN::deleteCategory($_GET['del_id']);		
	header('location:'.ADMIN_URL.'view-categories.php');	
}

else if($type == "hire_to_freelancer"){	
//echo "<pre>";print_r($_REQUEST);die;  
	$call = USER::hireToFreelancer($_REQUEST);		
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
	}else{		
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'redirect'=>'on', 'notice_show'=>'on', 'href'=>CLIENT_URL.'manage-bidders.php?post='.$_GET['jId'], 'form_reset'=>'on');
	}	
	echo json_encode($show);
}

else if($type == "start_contract"){	
//echo "<pre>";print_r($_REQUEST);die;  
	$call = USER::startContract($_GET);		
	header('location:'.VENDOR_URL.'manage-jobs.php');	
	/*if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
	}else{		
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'redirect'=>'on', 'notice_show'=>'on', 'href'=>CLIENT_URL.'manage-jobs.php', 'form_reset'=>'on');
	}	
	echo json_encode($show);*/
}
else if($type == "employer_end_contract"){	 
	$call = USER::endContract($_POST);
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
	}else{		
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'redirect'=>'on', 'notice_show'=>'on', 'href'=>CLIENT_URL.'manage-bidders.php?post='.$_POST['jId'], 'form_reset'=>'on');
	}	
	echo json_encode($show);
}

else if($type == "freelancer_end_contract"){	 
	$call = USER::endContract($_POST);
	echo "<pre>";print_r($call);die;  
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');				
	}else{		
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'redirect'=>'on', 'notice_show'=>'on', 'href'=>VENDOR_URL.'manage-jobs.php', 'form_reset'=>'on');
	}	
	echo json_encode($show);
}



else if($type == "add_main_categories"){
	//echo"<pre>";print_r($category);

	$call = ADMIN::addMainCategory($_POST, $_FILES['image'], $_FILES['icon']);	
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'redirect'=>'on', 'form_reset'=>'on', 'href'=>ADMIN_URL.'view-categories.php');
	}	
	echo json_encode($show);    
} 

else if($type == "update_main_category"){	
	$call = ADMIN::updateMainCategory($_POST, $_FILES['image'], $_FILES['icon']);	
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg'], 'notice_show'=>'on');			
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'notice_show'=>'on', 'redirect'=>'on', 'form_reset'=>'on', 'href'=>ADMIN_URL.'view-categories.php');
	}	
	echo json_encode($show);    
}  


else if($type == "categories_suggestion"){	
	$call = ADMIN::autoSuggestionCategory($_GET['q']);	
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg']);			
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'result'=>$call['result']);
	}	
	echo json_encode($show);    
}  

else if($type == "share_address"){	
	$call = USER::getAddress($_POST);	
	if($call['status'] == 'error'){
		$show = array("msg" => 'error', 'notice'=>$call['msg']);			
	}else{
		$show = array("msg" => 'success', 'notice'=>$call['msg'], 'result'=>$call['result']);
	}	
	echo json_encode($show);    
}  



?>