/* ----------------- Start Document ----------------- */

var SITE_URL = 'https://paymyprice.com/';  
var CLIENT_URL = 'https://paymyprice.com/employer/'; 
var VENDOR_URL = 'https://paymyprice.com/freelancer/';   

function sendRequest(requestData, requestType, form, formId) {
    try {
        if(formId == "hire_to_freelancer" || formId == "make_offer"){ 
          var send_message = $('#'+formId).serialize();
        } 

        $.ajax({
            url:requestType,
            type:"POST",
            data:requestData,
            processData: false,
            contentType: false,
            success:function(response) {
              var obj = $.parseJSON(response);  
              form['submit'].classList.remove('active'); 
              form['submit'].disabled = false;                        
              if(obj.msg == 'success'){
                
                  if(obj.form_reset == 'on'){form.reset();}               
                  
                  $('.mfp-close').click();                  
                        
                  if(formId == "add_note" || formId == "update_note"){                                     
                    $('#load_data').html('');   
                    load_infinite_data(scrollInfinite);
                    popupManage(); 
                  }

                  else if(formId == "hire_to_freelancer"){                                 
                    $.post(SITE_URL+'helpers/classes/pusher_server.php', send_message);
                  }  
                  else if(formId == "make_offer"){        
                     $.post(SITE_URL+'helpers/classes/pusher_server.php', send_message);          
                    $('#make_offer a').addClass('hide');                 
                    $('#make_offer button').removeClass('hide'); 
                  }
                  else if(formId == "add_review" || formId == "update_review" ){                             
                    $('#rate_to_user').html('');   
                    loadRateToUser(rate_to_user);
                  }
                  else if(formId == "employer_signup_form"){
                      $("section").hide();
                      $(window).scrollTop(0);
                      $("#step5").css({"animation-name": "slideInRight" });
                      $("#step5").show();   
                  }
                   else if(formId == "freelancer_signup_form"){
                      $("section").hide();
                      $(window).scrollTop(0);
                      $("#step4").css({"animation-name": "slideInRight" });
                      $("#step4").show();   
                  }
               if(obj.redirect == 'on'){window.location.replace(obj.href);} 

              }
              if(obj.notice_show == 'on'){
                alertNotificationMsg(obj.notice);
              }
              
            }
        }); 
    }catch (e) {
        console.log(e);
    }
}

function alertNotificationMsg(msg) {
  try {      
      Snackbar.show({
          text: msg,
          pos: 'bottom-center',
          showAction: false,
          actionText: "Dismiss",
          duration: 5000,
          textColor: '#fff',
          backgroundColor: '#383838'
      }); 
    }catch (e) {
        console.log(e);
    } 
}

function popupManage(){
      $('.popup-with-zoom-anim-ajax').magnificPopup({
          type: 'ajax',
          fixedContentPos: false,
          fixedBgPos: true,
          overflowY: 'auto',
          closeOnBgClick:false,
          closeBtnInside: true,
          showCloseBtn:true,
          preloader: true,
          midClick: true,
          removalDelay: 300,
          mainClass: 'my-mfp-zoom-in',
          ajax: {
            settings: null, // Ajax settings object that will extend default one - http://api.jquery.com/jQuery.ajax/#jQuery-ajax-settings
            // For example:
            // settings: {cache:false, async:false}

            cursor: 'mfp-ajax-cur', // CSS class that will be added to body during the loading (adds "progress" cursor)
            tError: '<a href="%url%">The content</a> could not be loaded.' //  Error message, can contain %curr% and %total% tags if gallery is enabled
          },          
      });
}


function validateForm(id, event) {
  try {    
     //console.log($('#'+id).serialize());
    if(event.keyCode == 13) {
      event.preventDefault();
      console.log('Hit enter Not submit form');
      return false;
    }
    event.preventDefault();        
    var onlyletter = /^[A-Za-z]+$/; 
    var onlyNumber = /^[0-9]+$/;        
    var isValid = true; 
    $('#validate_msg').addClass('hide');   
    // Access the form element...         
    var form = document.forms[id];    
    form['submit'].classList.add("active");
    form['submit'].disabled = true;
    var formData = new FormData(form);
    var reqType = form.action; 
    var optional_fields = []; 

     // Display the key/value pairs and validate one by one..
    for (var pair of formData.entries()) {
         //console.log(pair[0]  +' ='+pair[1]);
      if(pair[1] == '') { 
        //console.log(form[pair[0]].title);
          if(form[pair[0]].title) { 
            if(form[pair[0]].classList.contains("selectpicker")){          
                form[pair[0]].parentElement.getElementsByTagName('button')[0].style.border = '1px solid #ff0000';              
            }else{          
              form[pair[0]].style.border = '1px solid #ff0000';
              form[pair[0]].focus();
              isValid = false;  
            }
          }else{
            optional_fields.push(pair[0]);
          }
       }else{
          if(form[pair[0]].title) { 
            if(form[pair[0]].classList.contains("selectpicker")){
              form[pair[0]].parentElement.getElementsByTagName('button')[0].style.border = '1px solid #e0e0e0';        
            }else{          
                form[pair[0]].style.border = '1px solid #e0e0e0';
            }   
            
          }
       }   
    } 

    formData.append('optional_fields', optional_fields);
      
      //Return error if term and condition checkbox unchecked...
      if(id == 'freelancer_signup_form'){
         if(form['checkbox_agree'].checked === false)  {   
            form.getElementsByClassName('agree-chkbox')[0].style.border = '2px solid #ff0000'; 
            form.getElementsByClassName('chkbox-agree')[0].style.color = '#ff0000';
            isValid = false;
         }else{
            form.getElementsByClassName('agree-chkbox')[0].style.border = '2px solid #b4b4b4';   
            form.getElementsByClassName('chkbox-agree')[0].style.color = '#212529'; 
         }        
      }  

      if(id == 'employer_signup_form'){
         if(form['checkbox_agree'].checked === false)  {   
            form.getElementsByClassName('agree-chkbox')[0].style.border = '2px solid #ff0000'; 
            form.getElementsByClassName('chkbox-agree')[0].style.color = '#ff0000';
            isValid = false;
         }else{
            form.getElementsByClassName('agree-chkbox')[0].style.border = '2px solid #b4b4b4';   
            form.getElementsByClassName('chkbox-agree')[0].style.color = '#212529'; 
         }        
      } 

      if(id == 'job_posting_form' || id == 'job_update_form'){       
          if($('.payment-type input:checkbox:checked').length == 0){
            form.getElementsByClassName('pt')[0].style.border = '2px solid #ff0000';               
            isValid = false;
          }else{
            form.getElementsByClassName('pt')[0].style.border = '2px solid #b4b4b4'; 
            var value = $('.payment-type input:checkbox:checked').val();
            formData.append ('payment_type', value);
          }
          if($('.payment-process input:checkbox:checked').length == 0){
            form.getElementsByClassName('pp')[0].style.border = '2px solid #ff0000';             
            isValid = false;
          }else{
            form.getElementsByClassName('pp')[0].style.border = '2px solid #b4b4b4';   
             var value = $('.payment-process input:checkbox:checked').val();
             formData.append ('payment_process', value);
          } 
          var tags = [];
          $('.keywords-list .keyword-text').each(function( index ) {
              tags.push($(this).text());
          });
          if(tags.length > 9){            
            isValid = false;
          }else if(tags.length > 0){
              formData.append('tags', tags);
          }
      }

       if(id == 'add_detail_form'){
          var tags = [];
          $('.keywords-list .keyword-text').each(function( index ) {
              tags.push($(this).text());
          });
          if(tags.length > 9 ){           
            isValid = false;
          }else if(tags.length == 0){
            form.getElementsByClassName('tags-type')[0].style.border = '1px solid #ff0000';             
            isValid = false;
          }else{
            form.getElementsByClassName('tags-type')[0].style.border = '1px solid #b4b4b4'; 
          }
          formData.append('tags', tags);
      }

      if(id == 'add_main_categories' || id == 'update_main_category'){
          var subcat = [];
          $('.keywords-list .keyword-text').each(function( index ) {
              subcat.push($(this).text());
          });
          if(subcat.length == 0){
            form.getElementsByClassName('subcat-list')[0].style.border = '1px solid #ff0000';             
            isValid = false;
          }else{
            form.getElementsByClassName('subcat-list')[0].style.border = '1px solid #b4b4b4'; 
          }
          formData.append('sub_categories', subcat);
      }
           
     
      if(isValid == false){
        form['submit'].classList.remove("active");
        form['submit'].disabled = false; 
          throw 'Client end error';
      }       
      sendRequest(formData, reqType, form, id); 
    }catch (err) { 
     console.log(err);          
     // validateErrorMsg(err);
     $('#validate_msg').removeClass('hide').fadeTo(100,1);
      return false;  
    }  
}  


 // $('.selectpicker').change(function () {
 //     var id = $(this).data('name');
 //     var selectedValue= $(this).val();
 //     $('#'+id).val(selectedValue);
 //  });

// When the user clicks on the password field, show the message box
function validateErrorMsg(err) {    
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',  
      showCloseButton: true,
      showConfirmButton: false,    
      customClass: {
        container: 'validation-error-show',        
      },   
    })
    Toast.fire({     
      title: '<div id="validate_msg"><ul>'+err.join("")+'</ul></div>'
    })
}

function visiblePassword() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}

  // only checked one checkbox at a time in question 3....  
  $('.payment-type .one_checked').on('change', function() {
      $('.payment-type .one_checked').not(this).prop('checked', false); 

      var value = $(this).val();
    
      $('#payment_type').text('Project '+value+' Budget');
     
     
  });
  
  // only checked one checkbox at a time in question 1....
  $('.payment-process .one_checked').on('change', function() {
      $('.payment-process .one_checked').not(this).prop('checked', false);
  });


function isNumberKey(evt) {
  try {
      var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
    }catch (err) {       
      console.log(err);
        return false;  
    } 
}


$('#category-select .nice-select li').click(function(){
  $('#category').val($(this).data('value'));
});



  function hideExtraContent() {
    var showChar = 100;
    var ellipsestext = "...";
    var moretext = "more";
    var lesstext = "less";
    $('#load_data .more').each(function() {
        var content = $(this).html();

        if(content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar-1, content.length - showChar);

            var html = c + '<span class="moreelipses">'+ellipsestext+'</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

            $(this).html(html);
        }

    });

    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });   
  }



 function rateFromUser(value){

  if(value.message == null){
    var msg ='<span class="company-not-rated margin-bottom-5">Not Rated</span>';
   
  }else{
    var msg = '<div class="item-details margin-top-10">'+
          '<div class="star-rating" data-rating="'+value.rating+'.0"></div>'+
          '<div class="detail-item"><i class="icon-material-outline-date-range"></i> '+value.review_on+'</div>'+
        '</div>'+
        '<div class="item-description">'+value.message+'</div>';
     }

   $('#load_data').append('<li>'+
          '<div class="boxed-list-item">'+
            '<div class="item-content">'+
              '<h4 class="post_title">'+value.title+'</h4>'+
              msg
            +'</div>'+
          '</div>'+       
        '</li>');
  } 

function myNotes(value){
   $('#load_data').append('<div class="dashboard-note">'+
        '<p>'+value.note+'</p>'+
        '<div class="note-footer">'+
          '<span class="note-priority '+value.priority+'">'+value.priority+' Priority</span>'+
          '<div class="note-buttons">'+
            '<a href="'+SITE_URL+'helpers/magnific-popup.php?popup=edit-note&id='+value.encryptId+'" class="popup-with-zoom-anim-ajax" title="Edit" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>'+
            '<a href="'+SITE_URL+'helpers/magnific-popup.php?popup=delete-note&id='+value.encryptId+'" class="popup-with-zoom-anim-ajax" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>'+
          '</div>'+
        '</div>'+
      '</div>');
 }

function myJobsContent(value){  
    var display_edit = '';
    var status = "";        
     if(value.status == '5'){  
        status = '<span class="dashboard-status-button green">Job Ended</span>'; 
        if(value.payment_id == null)  {         
          display_edit = '<a href="'+SITE_URL+'helpers/functions.php?type=cmVxdWVzdF9mb3JfcGF5bWVudA==&jId='+value.encryptId+'&eId='+value.encrypt_eId+'&fId='+value.encrypt_fId+'" class="button ripple-effect"><i class="icon-material-outline-launch"></i> Request Payment</a>';  
        }else{
          display_edit = '<a href="javascript:void(0)" class="button dark ripple-effect"><i class="icon-feather-credit-card"></i> Payed</a>';  
        }           
    }
    if(value.status == '4'){
      status = '<span class="dashboard-status-button green">Job Started</span>'; 
      display_edit = '<a href="'+SITE_URL+'helpers/magnific-popup.php?popup=end-contract&jId='+value.encryptId+'&pId='+value.encrypt_pId+'&eId='+value.encrypt_eId+'&eId='+value.encrypt_eId+'&uId='+value.encrypt_fId+'&t='+value.encrypt_t+'" class="button popup-with-zoom-anim-ajax red ripple-effect"><i class="icon-feather-x"></i> End Job </a>';
    }else if(value.status == '3'){
      status = '<span class="dashboard-status-button green">Hired</span>'; 
      display_edit = '<a href="'+SITE_URL+'helpers/functions.php?type=c3RhcnRfY29udHJhY3Q=&jId='+value.encryptId+'&pId='+value.encrypt_pId+'&eId='+value.encrypt_eId+'&fId='+value.encrypt_fId+'" class="button ripple-effect"><i class="glyphicon icon-material-outline-assignment"></i> Start Job </a>';     
    }else if(value.status == '2'){
      status = '<span class="dashboard-status-button green">Accepted</span>';   
    }else if(value.status == '1'){
      status = '<span class="dashboard-status-button green">Pending Approval</span>';
      display_edit =  '<a href="'+VENDOR_URL+'view-proposal.php?pId='+value.encrypt_pId+'" class="button ripple-effect"><i class="icon-material-outline-drafts"></i> View Proposal</a> <a href="'+VENDOR_URL+'job-detail.php?job='+value.encryptId+'" class="button gray ripple-effect ico" title="Edit Bid" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>';
    }else if(value.status == '0'){
      status = '<span class="dashboard-status-button red">Rejected</span>';
       display_edit =  '<a href="'+VENDOR_URL+'view-proposal.php?pId='+value.encrypt_pId+'" class="button ripple-effect"><i class="icon-material-outline-drafts"></i> View Proposal</a>';  
     
    }
       
    
    $('#load_data').append('<li>'+
            '<div class="job-listing freelancer">'+                   
              '<div class="job-listing-details">'+
                '<div class="job-listing-description">'+
                    '<h3 class="job-listing-title"><a href="'+VENDOR_URL+'job-detail.php?job='+value.encryptId+'">'+value.title+'</a> '+status+'</h3>'+       
                  '<p class="more">'+value.description+'</p>'+
                  '<div class="job-listing-footer padt_30">'+
                    '<ul>'+
                      '<li><i class="icon-material-outline-date-range"></i> Posted on '+value.posted_on+'</li>'+
                      '<li><i class="icon-material-outline-date-range"></i> Applied on '+value.applied_on+'</li>'+
                    '</ul>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>'+
            '<div class="buttons-to-right always-visible">'+ 
            '<a href="'+VENDOR_URL+'messages.php?eId='+value.encrypt_eId+'" class="button dark ripple-effect"><i class="icon-feather-message-square"></i> Send Message</a>'+             
             display_edit
            +'</div>'+
        '</li>');
}

function myJobListingContent(value){ 

    if(value.status == null || value.status == '0') {
         var btnText = 'Bid Now'; 
      }else  {
         var btnText = 'Bid Placed';
      }
    var tag_show = "";
    var tags = []
  
     if(value.tags){
        var job_tags = value.tags.split(',');    

       $.each(job_tags, function( index, value ) {
          tags.push('<span>'+value+'</span>');
       });
   
      tag_show = tags.join('');
    }

  if(value.min_budget !== '' && value.max_budget !== ''){
    var budget_price = '$'+value.min_budget+' - '+'$'+value.max_budget;
  } else if(value.min_budget !== ''){
    var budget_price = '$'+value.min_budget;
  } else if(value.max_budget !== ''){
    var budget_price = '$'+value.max_budget;
  } 

  if(value.payment_type == 'Fixed'){
      var budget_type = 'Fixed Price';
  }else if(value.payment_type == 'Hourly'){
      var budget_type = 'Hourly Rate';
  }
   
  $('#load_data').append('<a href="'+VENDOR_URL+'job-detail.php?job='+value.encryptId+'" class="task-listing">'+          
          '<div class="task-listing-details">'+
            '<div class="task-listing-description">'+
              '<h3 class="task-listing-title">'+value.title+'</h3>'+
              '<ul class="task-icons">'+
                '<li><i class="icon-material-outline-location-on"></i> '+value.state+', '+value.country+'</li>'+
                '<li><i class="icon-material-outline-access-time"></i> '+value.time_ago.result+'</li>'+
              '</ul>'+
              '<p class="task-listing-text more">'+value.description+'</p>'+
              '<div class="task-tags">'+ tag_show +'</div>'+
            '</div>'+
          '</div>'+

          '<div class="task-listing-bid">'+
            '<div class="task-listing-bid-inner">'+
              '<div class="task-offers">'+
                '<strong>'+budget_price+'</strong>'+
                '<span>'+budget_type+'</span>'+
              '</div>'+
              '<span class="button button-sliding-icon ripple-effect">'+  btnText+' <i class="icon-material-outline-arrow-right-alt"></i></span>'+
            '</div>'+
          '</div>'+
        '</a>');
}


function myPostsContent(value){
  //console.log(value);
    var count = 0;   
    if(value.payment_type == 'Fixed' ){
      var budget_type = 'Fixed Price';
    }else if(value.payment_type == 'Hourly' ){
      var budget_type = 'Hourly Rate';
    }
    var btn_manage = '';


    if(value.proposal_bid[value.id]){
       count = value.proposal_bid[value.id].count;      
    }  

    if(count == 0){
      btn_manage = 'hide';
    }

    if(value.min_budget !== '' && value.max_budget !== ''){
      var budget_price = '$'+value.min_budget+' - '+'$'+value.max_budget;
    } else if(value.min_budget !== ''){
      var budget_price = '$'+value.min_budget;
    } else if(value.max_budget !== ''){
      var budget_price = '$'+value.max_budget;
    } 
    $('#load_data').append('<li>'+
          '<div class="job-listing">'+
            '<div class="job-listing-details">'+
              '<div class="job-listing-description">'+
                '<h3 class="job-listing-title"><a href="#">'+value.title+'</a></h3>'+              
                '<div class="job-listing-footer">'+
                  '<ul>'+
                   '<li class=""><i class="icon-material-outline-date-range"></i> Posted on '+value.posted_on+'</li>'+
                    '<li><i class="icon-material-outline-date-range"></i> Expiring on '+value.expire_on+'</li>'+
                  '</ul>'+
               '</div>'+
              '</div>'+
            '</div>'+
          '</div>'+ 
          '<ul class="dashboard-task-info">'+
              '<li><strong>'+count+'</strong><span>Bids</span></li>'+             
              '<li><strong>'+budget_price+'</strong><span>'+budget_type+'</span></li>'+
          '</ul>'+                
          '<div class="buttons-to-right always-visible">'+
            '<a href="'+CLIENT_URL+'manage-bidders.php?post='+value.encryptId+'" class="button ripple-effect '+btn_manage+'"><i class="icon-material-outline-supervisor-account"></i> Manage Bidders <span class="button-info">'+count+'</span></a>'+
            '<a href="'+CLIENT_URL+'edit-post.php?post='+value.encryptId+'" class="button gray ripple-effect ico" title="Edit" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>'+
            // '<a href="#" class="button gray ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>'+
          '</div>'+
        '</li>');
}

 function freelancerBidContent(value){ 
 var nation = ""; 
   if(value.p_img == null){
      var photo = 'user-avatar-placeholder.png';
    }else{
      var photo = value.p_img;
    } 

   if(value.nationality !== ""){
       nation = value.nationality;
    }

      $('#load_data').append('<li>'+
            '<div class="bid">'+            
              '<div class="bids-avatar">'+
                '<div class="freelancer-avatar">'+
                  '<div class="verified-badge"></div>'+
                  '<a href="single-freelancer-profile.html"><img src="'+SITE_URL+'uploads/profile/'+photo+'" alt=""></a>'+
                '</div>'+
              '</div>'+             
            
              '<div class="bids-content">'+             
                '<div class="freelancer-name">'+
                  '<h4><a href="#">'+value.fname+' '+value.lname+' <img class="flag" src="https://www.countryflags.io/'+nation+'/shiny/64.png" alt="" title="'+nation+'" data-tippy-placement="top"></a></h4>'+
                  '<div class="star-rating" data-rating="'+value.rate+'.0"></div>'+
                '</div>'+
              '</div>'+
              
              '<div class="bids-bid">'+
                '<div class="bid-rate">'+
                  '<div class="rate">$'+value.minimal_rate+'</div>'+
                  '<span>in '+value.delivery_time+'</span>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</li>');
         }    


 function freelancerActivityContent(value){
    if(value.status == '0'){
      var status = '<span class="rejected">Rejected</span>';
    }else if(value.status == '2'){
      var status = '<span class="accepted">Accepted</span>';
    }else if(value.status  == '3'){
      var status = '<span class="accepted">Hired</span>';
    }else if(value.status == '1'){
      var status = '<span class="applied">Applied</span>';
    }else if(value.status == '4'){
      var status = '<span class="accepted">In Progress</span>';
    }else if(value.status == '5'){
      var status = '<span class="accepted">Completed</span>';
    }    

    $('#load_data').append('<a href="'+VENDOR_URL+'job-detail.php?job='+value.encryptId+'" class="job-listing">'+
          '<div class="job-listing-details">'+
            '<div class="job-listing-company-logo">'+
              '<img src="'+SITE_URL+'assets/images/company-logo-01.png" alt="">'+
            '</div>'+
            '<div class="job-listing-description">'+
              '<h3 class="job-listing-title">'+value.title+'</h3>'+
              '<div class="job-listing-footer">'+
                '<ul>'+
                  '<li><i class="icon-material-outline-business"></i> Hexagon <div class="verified-badge" data-tippy-placement="top" data-tippy="" data-original-title="Verified Employer"></div></li>'+
                  '<li><i class="icon-material-outline-location-on"></i> '+value.state+', '+value.country+'</li>'+
                  '<li><i class="icon-material-outline-business-center"></i> '+value.job_type+'</li>'+
                  '<li><i class="icon-material-outline-access-time"></i> '+value.time_ago.result+'</li>'+
                '</ul>'+
              '</div>'+
            '</div>'+
            status
          +'</div>'+
        '</a>');
}


function freelancerListing(value){
  var tagline = "";
    if(value.p_img == null){
      var photo = 'user-avatar-placeholder.png';
    }else{
      var photo = value.p_img;
    } 

    if(value.tagline !== null){
      tagline = '<span>'+value.tagline+'</span>';
    }

   /* if(value.votes > 3){
      var display = '<div class="freelancer-rating"><div class="star-rating" data-rating="'+value.rate+'.0"></div></div>';      
    } else{
       var display = '<span class="company-not-rated margin-bottom-5">Minimum of 3 votes required</span>';
    }*/
    $('#load_data').append('<div class="freelancer">'+
          '<div class="freelancer-overview">'+
            '<div class="freelancer-overview-inner">'+             
             '<div class="freelancer-avatar">'+  
                '<div class="verified-badge"></div>'+
                '<a href="freelancer-profile.php?f='+value.encryptId+'"><img src="'+SITE_URL+'uploads/profile/'+photo+'" alt=""></a>'+
              '</div>'+
              '<div class="freelancer-name">'+
                '<h4><a href="freelancer-profile.php?f='+value.encryptId+'">'+value.fname+' '+value.lname+'<img class="flag" src="https://www.countryflags.io/'+value.country+'/shiny/64.png" alt="" title="'+value.country+'" data-tippy-placement="top"></a></h4>'+
                tagline
             +'</div>'+
              '<div class="freelancer-rating">'+
                '<div class="star-rating" data-rating="'+value.rate+'.0"></div>'+
              '</div>'+
            '</div>'+
          '</div>'+
          '<div class="freelancer-details">'+
            '<div class="freelancer-details-list">'+
              '<ul>'+
                '<li>Location <strong><i class="icon-material-outline-location-on"></i> '+value.state+', '+value.country+'</strong></li>'+
                '<li>Rate <strong>$'+value.hourly_rate+'/ hr</strong></li>'+                
              '</ul>'+
            '</div>'+
            '<a href="freelancer-profile.php?fId='+value.encryptId+'" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>'+
          '</div>'+
        '</div>');
}


$('#post-detail-more').click(function(){    
      $('#post-detail-view').slideToggle('slow'); 
    if ($(this).text() == "View More Detail")
       $(this).text("View Less Detail")
    else
       $(this).text("View More Detail");  
});  



//$(document).ready(function(){ 


 function e_sortBy() {
    try { 
      var scrollInfinite = {};  
        scrollInfinite['limit'] = 8;
        scrollInfinite['start'] = 0;
        scrollInfinite['scroll'] = $("#load_data").data('name');
        scrollInfinite['user_id'] = $('#user_id').val();

       var set_filter = checkActivateFilterForEmployer();
       var sort = $('#sort_by').val();           
         
        $('#load_data').html('');      
        load_infinite_data(scrollInfinite,set_filter, sort);  
    }catch (err) {       
      console.log(err);
        return false;  
    } 
}


 function sortBy() {
    try { 
      var scrollInfinite = {};  
        scrollInfinite['limit'] = 8;
        scrollInfinite['start'] = 0;
        scrollInfinite['scroll'] = $("#load_data").data('name');
        scrollInfinite['user_id'] = $('#user_id').val();

       var set_filter = checkActivateFilter();
       var sort = $('#sort_by').val();           
         
        $('#load_data').html('');      
        load_infinite_data(scrollInfinite,set_filter, sort);  
    }catch (err) {       
      console.log(err);
        return false;  
    } 
}


/*******************************************

      Job Listing Left Side Filter Set..

*****************************************/
$('#job_type input[type="checkbox"]').change(function(e){
   e.preventDefault();
  var scrollInfinite = {};  
      scrollInfinite['limit'] = 8;
      scrollInfinite['start'] = 0;
      scrollInfinite['scroll'] = $("#load_data").data('name');
      scrollInfinite['user_id'] = $('#user_id').val();

       var set_filter = checkActivateFilter();
       var sort = $('#sort_by').val();           
         
        $('#load_data').html('');      
        load_infinite_data(scrollInfinite,set_filter, sort); 
});

$('#payment_type input[type="checkbox"]').change(function(e){
  e.preventDefault(); 
    var scrollInfinite = {};
      scrollInfinite['limit'] = 8;
      scrollInfinite['start'] = 0;
      scrollInfinite['scroll'] = $("#load_data").data('name');
      scrollInfinite['user_id'] = $('#user_id').val();  

      var set_filter = checkActivateFilter();

      var sort = $('#sort_by').val();           
         
      $('#load_data').html('');      
      load_infinite_data(scrollInfinite,set_filter, sort);   
});

$('#payment_process input[type="checkbox"]').change(function(e){
     e.preventDefault();
    var scrollInfinite = {};
      scrollInfinite['limit'] = 8;
      scrollInfinite['start'] = 0;
      scrollInfinite['scroll'] = $("#load_data").data('name');
      scrollInfinite['user_id'] = $('#user_id').val();

      var set_filter = checkActivateFilter();  
      
      var sort = $('#sort_by').val();           
         
      $('#load_data').html('');      
      load_infinite_data(scrollInfinite,set_filter, sort);  
});

/*$('#tags input[type="checkbox"]').click(function(){
  // e.preventDefault();
    var scrollInfinite = {};
      scrollInfinite['limit'] = 8;
      scrollInfinite['start'] = 0;
      scrollInfinite['scroll'] = $("#load_data").data('name');
      scrollInfinite['user_id'] = $('#user_id').val();

      var set_filter = checkActivateFilter();
        
      var sort = $('#sort_by').val();           
         
      $('#load_data').html('');      
      load_infinite_data(scrollInfinite,set_filter, sort);      
   
});*/

$('#set_tags .keyword-input-button').click(function(e){    
  e.preventDefault();
      var scrollInfinite = {};
      scrollInfinite['limit'] = 8;
      scrollInfinite['start'] = 0;
      scrollInfinite['scroll'] = $("#load_data").data('name');
      scrollInfinite['user_id'] = $('#user_id').val();

      setTimeout(function(){
         var set_filter = checkActivateFilter(); 
          var sort = $('#sort_by').val();           
             
          $('#load_data').html('');      
          load_infinite_data(scrollInfinite, set_filter, sort);   
       }, 500);  
      //removeTag();
});


$('#e_set_tags .keyword-input-button').click(function(e){    
  e.preventDefault();
      var scrollInfinite = {};
      scrollInfinite['limit'] = 8;
      scrollInfinite['start'] = 0;
      scrollInfinite['scroll'] = $("#load_data").data('name');
      scrollInfinite['user_id'] = $('#user_id').val();

      setTimeout(function(){
         var set_filter = checkActivateFilterForEmployer(); 
          var sort = $('#sort_by').val();           
             
          $('#load_data').html('');      
          load_infinite_data(scrollInfinite, set_filter, sort);   
       }, 500);  
      //removeTag();
});




function removeTag() {
  console.log('ass');
  $('.keyword-remove').click(function(){   
      console.log('eee'); 
       /* e.preventDefault();
            var scrollInfinite = {};
            scrollInfinite['limit'] = 8;
            scrollInfinite['start'] = 0;
            scrollInfinite['scroll'] = $("#load_data").data('name');
            scrollInfinite['user_id'] = $('#user_id').val();
            
            var set_filter = checkActivateFilter(); 
            var sort = $('#sort_by').val();           
               
            $('#load_data').html('');      
            load_infinite_data(scrollInfinite, set_filter, sort); */
      });
}


$('#category_id select').change(function(e){
  e.preventDefault();
    var scrollInfinite = {};
      scrollInfinite['limit'] = 8;
      scrollInfinite['start'] = 0;
      scrollInfinite['scroll'] = $("#load_data").data('name');
      scrollInfinite['user_id'] = $('#user_id').val();

      var set_filter = checkActivateFilter();  

      var sort = $('#sort_by').val();           
         
      $('#load_data').html('');      
      load_infinite_data(scrollInfinite,set_filter, sort);     
    
});


$('#e_category_id select').change(function(e){
  e.preventDefault();
    var scrollInfinite = {};
      scrollInfinite['limit'] = 8;
      scrollInfinite['start'] = 0;
      scrollInfinite['scroll'] = $("#load_data").data('name');
      scrollInfinite['user_id'] = $('#user_id').val();

      var set_filter = checkActivateFilterForEmployer();  

      var sort = $('#sort_by').val();           
         
      $('#load_data').html('');      
      load_infinite_data(scrollInfinite,set_filter, sort);     
    
});
    

  function checkActivateFilterForEmployer() { 
    try { 
        var setFilter = {};  

        var tags = [];
        $('#e_set_tags .keywords-list .keyword-text').each(function( index ) {
            tags.push($(this).text());
        });
        setFilter['tags'] = tags; 

        var category_id =  $("#e_category_id select").val();
        //console.log(category_id);
        if(category_id.length > 0){
             setFilter['category_id'] = category_id;
        }

        setFilter['hourly_rate'] = $('#set_hourly_rate .range-slider').data('slider').getValue();     
       
       return setFilter;
    }catch (err) {       
      console.log(err);
        return false;  
    } 
}

$('.activ_deactiv_user').change(function(e){
  e.preventDefault();
  if($(this).is(":checked")){
    var status = 1;      
  }else{
    var status = 0;
  }
  $.ajax({
        type:'POST',
        url:'../helpers/functions.php?type=dXNlcl9hY2NvdW50X2FjdGl2YXRl',
        data:{'id':$(this).attr('data-id'), 'status':status},
        success:function(response){
          var obj = $.parseJSON(response);
          alertNotificationMsg(obj.notice);
        }
    });
});



function checkActivateFilter() { 
    try { 
        var setFilter = {};   
        $(".set-filter input:checkbox:checked").each(function(){
         
          var filter = $(this).closest('.sidebar-widget').attr('id');  
          var value = $(this).val();
          var data_name = $(this).data('name');
          setFilter[filter+'-'+data_name] = value;
        });

        var category_id =  $(".set-filter #category_id select").val();
        //console.log(category_id);
        if(category_id.length > 0){
             setFilter['category_id'] = category_id;
        }
   
        var tags = [];
        $('#set_tags .keywords-list .keyword-text').each(function( index ) {
            tags.push($(this).text());
        });
        setFilter['tags'] = tags;
       
       return setFilter;
    }catch (err) {       
      console.log(err);
        return false;  
    } 
}

function showSubcategoryList(categoryId){     
      // Retrieve all sub-categories of the current category
      var category = [categoryId];      
      $.ajax({  
          url:SITE_URL+"helpers/functions.php?type=c3ViY2F0ZWdvcnlfbGlzdA==",
          method:"POST",
          data:{'category':category, 'view_only':'sub_categories'},
         // dataType:"json",
          success:function(response) {             
              var obj = $.parseJSON(response);     
              $('#subcat-list').empty();
              //console.log(obj.result[categoryId]);                                
              if(obj.msg == 'success'){
                  // Your API must return the sub-categories in Json format
                  $.each(obj.result[categoryId], function( index, subcategory ) {                    
                    $('#subcat-list').append($('<option>', { 
                        value: subcategory.id,
                        text : subcategory.subcategory 
                    }));
                     
                  });                 
                  $('#subcat-list').selectpicker('refresh');
              }
          }
    });

}


$('#link_new_card').click(function(){   
      $('#addCard').removeClass('hide'); 
 });

$('.set-default-card').click(function(){  
      $('#card_list .content-loader').show();
      var id = $(this).val(); 
      $.ajax({  
          url:SITE_URL+"helpers/functions.php?type=c2V0X2RlZmF1bHRfY2FyZA==",
          method:"POST",
          data:{'id':id},
          dataType:"json",
          success:function(response) { 
             $('#card_list .content-loader').show();
             alertNotificationMsg(response.notice);
             location.reload();
          }
    });
 });




// Function to preview image after validation
      $(function() {
            var reset_img = $('#image_preview img').attr('src');
            //console.log(reset_img);
            var $image_crop;
            $(".change-photo-btn.reset").click(function() {              
                $image_crop.croppie('destroy');
                  //$image_crop = "";
                $('.change-photo-btn').hide();                 
                $('#crop_preview').addClass('hide');
                $('#image_preview img').attr('src', reset_img);     
                $('#image_preview').fadeIn();              
               // location.reload();
            });
            $(".avatar-wrapper .file-upload").change(function() {
             // $("#message").empty(); // To remove the previous error message
              $('.content-loader').show();
              var file = this.files[0];
              var imagefile = file.type;
              var match= ["image/jpeg","image/png","image/jpg"];
              if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))) {  
                //$("#message").html(errorMsg('Please Select A valid Image File.Only jpeg, jpg and png Images type allowed')); 
               // $('.content-loader').hide();    
               console.log('errror');          
                return false;
              }else {                            
                $image_crop = $('#crop_preview').croppie({
                    enableExif: true,
                    enableOrientation: true,  
                    viewport: {
                        width: 155,
                        height: 150,
                        type: 'square'  
                    }                  
                });
                setTimeout(function(){ 
                 // console.log($image_crop);
                  var reader = new FileReader();                        
                  reader.onload = function (event) {
                      $image_crop.croppie('bind', {
                        url: event.target.result,
                        zoom : 0,                     
                      }).then(function(){
                        console.log('jQuery bind complete');
                      });        
                  }
                  reader.readAsDataURL(file); 
                  $('#image_preview').hide();                 
                  $('.content-loader').hide();                    
                  $('#crop_preview').removeClass('hide').fadeIn();
                  $('.avatar-wrapper').css('overflow', 'visible');  
                  $('.change-photo-btn.save').fadeIn();   
                  $('.change-photo-btn.reset').fadeIn();   
                }, 1000);
                 
              }              
               
                $('.change-photo-btn.save').click(function(event){
                   $('.content-loader').show(); 
                      $image_crop.croppie('result', {
                      type: 'blob',
                      size: 'viewport',
                    }).then(function(response){                      
                      var formData = new FormData();
                      formData.append('profile', response);
                      $.ajax({
                          url:SITE_URL+"helpers/functions.php?type=dXNlcl9wcm9maWxlX3VwbG9hZA==",
                          type: "POST",
                          data:formData,
                          processData: false,
                          contentType: false,
                          success:function(response) {
                            var obj = $.parseJSON(response);  
                            $('.content-loader').hide();              
                            if(obj.msg == 'success'){
                                $image_crop.croppie('destroy');                                  
                                $('.change-photo-btn').hide();                 
                                $('#image_preview img').attr('src', SITE_URL+'uploads/profile/'+obj.result); 
                                $('.user-menu .user-avatar img').attr('src', SITE_URL+'uploads/profile/'+obj.result); 
                                $('#crop_preview').hide();
                                $('#image_preview').fadeIn();    
                            }else{
                              $(".change-photo-btn.reset").click(); 
                            }
                            alertNotificationMsg(obj.notice);  
                          }
                      });
                    })
                });
            }); 
      });

    $(".toggle-password").click(function() {
      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });  

    $('body').on('keyup', '.expire_card', function(e){   
       //console.log($(this).val());
      var val = $(this).val();
      if(!isNaN(val)) {
          if(val > 1 && val < 10 && val.length == 1) {
              temp_val = "0" + val + "/";
              $(this).val(temp_val);
          }
          else if (val >= 1 && val < 10 && val.length == 2 && e.keyCode != 8) {
              temp_val = val + "/";
              $(this).val(temp_val);                
          }
          else if(val > 9 && val.length == 2 && e.keyCode != 8) {
              temp_val = val + "/";
              $(this).val(temp_val);
          }
      }      
    });



    /************ Notification alerts ********************/


 var scrollNotificationInfinite = {};
      scrollNotificationInfinite['limit'] = 8;
      scrollNotificationInfinite['start'] = 0;
      //scrollNotificationInfinite['scroll'] = $("#load_data").data('name');
      scrollNotificationInfinite['user_id'] = $('#user_id').val();

/* function load_unseen_notification_count(view = '')
{  
  $('#load_notify').html('');

  $.ajax({  
      url:SITE_URL+"helpers/functions.php?type=dmlld19ub3RpZmljYXRpb25z",
      method:"POST",
      data:{'view':view},
      dataType:"json",
      success:function(response)
      {        
         //var obj = $.parseJSON(response); 
        //console.log(response.result.length);
        if(response.result.length > 0){
          $.each(response.result, function( index, value ) {

              $('#load_notify').append('<li class="notifications-not-read">'+
                  '<a href="'+value.href+'">'+  
                    '<span class="notification-icon"><i class="icon-material-outline-group"></i></span>'+
                    '<span class="notification-text">'+
                      '<strong>'+value.fname+' '+value.lname+'</strong> '+value.notice+' for a job <span class="color">'+value.title+'</span>'+
                    '</span>'+
                  '</a>'+
                '</li>');             
          }); 
        }       
         
        if(response.unread > 0){
           $('.notify-alert .notify-unread').text(response.unread).removeClass('hide');
        }        
      }
  });
}
if(userId){
    load_unseen_notification_count();

    setInterval(function(){
      load_unseen_notification_count();
    }, 60000); // call function after 60 sec repeatly....
}*/

function load_notification(scrollWith)
{  
  $('#load_notify').html('');

  $.ajax({  
      url:SITE_URL+"helpers/functions.php?type=dmlld19ub3RpZmljYXRpb25z",
      method:"POST",
      data:{'scroll_with':scrollWith},
      dataType:"json",
      success:function(response)
      {     
  
        $('.notify-alert .content-loader').hide();  

          if(response.result.length > 0){
            $.each(response.result, function( index, value ) {

                $('#load_notify').append('<li class="notifications-not-read">'+
                    '<a href="'+value.href+'">'+  
                      '<span class="notification-icon"><i class="icon-material-outline-'+value.icon+'"></i></span>'+
                      '<span class="notification-text">'+value.notice+'</span>'+
                    '</a>'+
                  '</li>');             
            });   

            if(response.result.length > 7){
                $('#load_data_notify').html("<button type='button' class='button dark ripple-effect load_more mb-20'><i class='icon-feather-rotate-ccw'></i> Load More</button>"); 
                $('#load_data_notify .load_more').click(function(){
                    scrollNotificationInfinite['start'] = scrollNotificationInfinite['start'] + scrollNotificationInfinite['limit'];  
                    load_notification(scrollNotificationInfinite);  
                }); 
            } 
            $('.header-notifications-scroll').css('height', '270px');
          } else {
            $('#load_data_notify').show().html("<span class='not-found-message'>You have no notification</span>");
          }
            
      }
  });
}

var scrollMessages = {}; 
    scrollMessages['user_id'] = $('#user_id').val();

function load_messages(scrollWith)
{  
  $('#load_message').html('');

  $.ajax({  
      url:SITE_URL+"helpers/functions.php?type=dmlld19tZXNzYWdlcw==",  
      method:"POST",
      data:{'scroll_with':scrollWith},
      dataType:"json",
      success:function(response)
      {     
  
        $('.messages-alert .content-loader').hide();  

          if(response.result.length > 0){
            $.each(response.result, function( index, value ) {

                $('#load_message').append('<li class="notifications-not-read">'+
                  '<a href="messages.php?eId='+value.encryptId+'">'+
                    '<span class="notification-avatar status-online"><img src="'+SITE_URL+'uploads/profile/'+value.p_img+'" alt=""></span>'+
                    '<div class="notification-text">'+
                      '<strong>'+value.fname+' '+value.lname+'</strong>'+
                      '<p class="notification-msg-text">'+value.message+'</p>'+
                      '<span class="color">'+value.time_ago.result+'</span>'+
                    '</div>'+
                  '</a>'+
                '</li>');             
            });  

           
            $('.header-notifications-scroll').css('height', '270px');
          } else {
            $('#load_data_messages').show().html("<span class='not-found-message'>You have no messages</span>");
          }
            
      }
  });
}

$(document).on('click', '.notify-alert .header-notifications-trigger', function(){
    if($('.notify-alert').hasClass('active')){
      $('#load_data_notify').hide();
      $('.notify-alert .content-loader').show();
      $('.notify-alert .notify-unread').text('0').addClass('hide');
      load_notification(scrollNotificationInfinite);
    }
});

$(document).on('click', '.messages-alert .header-notifications-trigger', function(){
    if($('.messages-alert').hasClass('active')){
      $('#load_data_messages').hide();
      $('.messages-alert .content-loader').show();
      $('.messages-alert .message-unread').text('0').addClass('hide');
      load_messages(scrollMessages);
    }
});

// ------------------ End Document ------------------ //
