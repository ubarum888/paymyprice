<?php include('helpers/classes/admin.php');  

include('helpers/classes/users.php'); 



$objAdmin = new ADMIN();

$objUser= new USER(); 



$categorySlug = $objAdmin->getMainCategoryBySlug($_GET['category']);



$category = $objAdmin->getAllMainCategoryById( base64_encode($categorySlug['result']['id']) );  



$subcategories = $objAdmin->getSubcategoriesByCategoryId( base64_encode($categorySlug['result']['id']) );



?>

<!doctype html>

<html lang="en">

<head>

<!--  Essential META Tags -->
<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1">
<link rel="canonical" href="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">

<meta property="og:locale" content="en_US">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
<meta property="og:site_name" content="PayMyPrice">

<title><?php echo $category['result']['category']; ?> Near Me - PayMyPrice</title>
<meta property="og:title" content="<?php echo $category['result']['category']; ?> Near Me - PayMyPrice">
<meta name="twitter:title" content="<?php echo $category['result']['category']; ?> Near Me - PayMyPrice">

<meta name="description" content="<?php echo $category['result']['description']; ?>">
<meta property="og:description" content="<?php echo $category['result']['description']; ?>">
<meta name="twitter:description" content="<?php echo $category['result']['description']; ?>">

<meta property="og:image" content="<?php echo SITE_URL.'uploads/category/'.$category['result']['banner_img']; ?>">
<meta property="og:image:secure_url" content="<?php echo SITE_URL.'uploads/category/'.$category['result']['banner_img']; ?>">
<meta name="twitter:image" content="<?php echo SITE_URL.'uploads/category/'.$category['result']['banner_img']; ?>">
<meta name="twitter:card" content="summary_large_image">
<meta property="og:image:width" content="400">
<meta property="og:image:height" content="50">

 	<?php include_once('elements/head.php');?>



</head>

<body>



<!-- Wrapper -->

<div id="wrapper">



<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth">

	<?php include_once('elements/header.php');?>	 



</header>

<div class="clearfix"></div>

<!-- Header Container / End -->







<!-- Titlebar

================================================== -->

<div class="single-page-header" data-background-image="<?= SITE_URL.'uploads/category/'.$category['result']['banner_img']?>">

	<div class="container">

		<div class="row">

			<div class="col-md-12">

				<div class="single-page-header-inner">

					<div class="left-side">

						<div class="header-image"><a href="#"><img src="<?= SITE_URL.'uploads/category/'.$category['result']['icon']?>" alt=""></a></div>

						<div class="header-details">

							<h1><?= $category['result']['category']?> Near Me</h1>

						</div>

					</div>

					

				</div>

			</div>

		</div>

	</div>

</div>





<!-- Page Content

================================================== -->

<div class="container">

	<div class="row">

		

		<!-- Content -->

		<div class="col-xl-12 col-lg-12 content-right-offset">



			<div class="single-page-section">

				<h3 class="margin-bottom-25">Description</h3>

				<p><?= $category['result']['description']?></p>

			</div>



			<div class="single-page-section">

				<h3 class="margin-bottom-25">Sub Categories</h3>



				<!-- Listings Container -->

				<div class="listings-container grid-layout">



						<?php 

							if(!empty($category['result']['subcat'])){

							//echo"<pre>";print_r($category['result']['subcat']);

							//$subcat = explode(',', $category['result']['subcat']);

							//foreach ($category['result']['subcat'] as $subcat) { 

							foreach ($subcategories['result'] as $subcat) {

						?>

								<a href="#" class="job-listing">							

									<div class="job-listing-details">						

										<div class="job-listing-company-logo">

											<img src="<?= SITE_URL.'uploads/category/'.$category['result']['icon']?>" alt="">

										</div>							

										<div class="job-listing-description">

											<h4 class="job-listing-company"><?=  $subcat ?></h4>								

										</div>

									</div>

								</a>

						<?php }	

						} ?>

					

					

					</div>

					<!-- Listings Container / End -->



				</div>



		

		</div>



	</div>

</div>





<!-- Footer

================================================== -->

<div id="footer">	 

 <?php include_once('elements/footer.php');?>	

 

</div>

<!-- Footer / End -->



</div>

<!-- Wrapper / End -->







</body>

</html>