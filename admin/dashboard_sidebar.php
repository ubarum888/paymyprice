<div class="dashboard-sidebar-inner" data-simplebar>
			<div class="dashboard-nav-container">

				<!-- Responsive Navigation Trigger -->
				<a href="#" class="dashboard-responsive-nav-trigger">
					<span class="hamburger hamburger--collapse" >
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</span>
					<span class="trigger-title">Dashboard Navigation</span>
				</a>
				
				<!-- Navigation -->
				<div class="dashboard-nav">
					<div class="dashboard-nav-inner">

						<!--<ul data-submenu-title="Start">
							<li class="active"><a href="<?= ADMIN_URL?>"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>							
						
						</ul>-->
						
						<ul data-submenu-title="Organize and Manage">
							<li><a href="#"><i class="icon-material-outline-business-center"></i> Users</a>
								<ul>
									<li><a href="<?= ADMIN_URL?>">Manage Users </a></li> 									
								</ul>	
							</li>	
							<li><a href="#"><i class="icon-material-outline-business-center"></i> Categories</a>
								<ul>
									<li><a href="<?= ADMIN_URL?>add-category.php">Add Category </a></li> 		
									<li><a href="<?= ADMIN_URL?>view-categories.php">View Categories </a></li> 									
								</ul>	
							</li>						
						</ul>

						<ul data-submenu-title="Account">							
							<li><a href="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('admin_logout')?>"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
						</ul>
						
					</div>
				</div>
				<!-- Navigation / End -->

			</div>
		</div>