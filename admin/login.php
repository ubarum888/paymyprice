<?php include('../helpers/classes/config.php');   ?>
<!doctype html>
<html lang="en">
<head>

<!--  Essential META Tags -->

<title>Administrator Login - PayMyPrice</title>

<meta property="og:title" content="Administrator Login - PayMyPrice">

<meta property="og:description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">

<meta property="og:image" content="https://paymyprice.com/assets/images/logo.svg">

<meta property="og:url" content="https://paymyprice.com">

<meta name="twitter:card" content="summary_large_image">





<!--  Non-Essential, But Recommended -->



<meta property="og:site_name" content="PayMyPrice">

<meta name="twitter:image:alt" content="PayMyPrice logo">

	<?php include_once('../elements/head.php');?>  
</head>
<body>
<!-- Wrapper -->

<div id="wrapper">

<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth">
 <div id="header">
		<div class="container">			
			<!-- Left Side Content -->

			<div class="left-side">
				<!-- Logo -->

				<div id="logo">
					<a href="<?= SITE_URL ?>"><img src="<?= SITE_URL ?>assets/images/logo.svg" alt=""></a>
				</div>
			</div>
	</div>
</header>
<div class="clearfix"></div>

<!-- Header Container / End -->


<!-- Titlebar

================================================== -->

<!-- Page Content

================================================== -->

<div class="container padt_70">
	<div class="row">

		<div class="col-xl-5 center">

			<div class="login-register-page">
				<!-- Welcome Text -->

				<div class="welcome-text">
					<h3>We're glad to see you again!</h3>
				</div>			

				<!-- Form -->

				<form id="admin_signin_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('admin_signin_form')?>" data-name="<?= base64_encode('admin_signin_form')?>" method="POST" onsubmit="return validateForm(this.id, event)">

					<div class="input-with-icon-left">
						<i class="icon-material-baseline-mail-outline"></i>

						<input type="text" class="input-text with-border" name="email" id="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'" maxlength="50" placeholder="Email Address" title="Enter Email Address"/>
					</div>

					<div class="input-with-icon-left">
						<i class="icon-material-outline-lock"></i>
						<input type="password" class="input-text with-border" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'" maxlength="15" name="password" id="password" placeholder="Password" title="Enter Password"/>
					</div>

					<a href="<?= SITE_URL?>forgot-password.php" class="forgot-password">Forgot Password?</a>
					<button type="submit" name="submit" class="button full-width button-sliding-icon ripple-effect margin-top-10 has-spinner" >Log In <i class="icon-material-outline-arrow-right-alt"></i> <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>  
				</form>	


			</div>
		</div>
	</div>
</div>

<!-- Spacer -->

<div class="margin-top-70"></div>
<!-- Spacer / End-->
<!-- Footer

================================================== -->
<div id="footer">
<div class="footer-top-section">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">

					<!-- Footer Rows Container -->
					<div class="footer-rows-container">
						
						<!-- Left Side -->
						<div class="footer-rows-left">
							<div class="footer-row">
								<div class="footer-row-inner footer-logo">
                  <a href="<?= SITE_URL ?>">
                    <img src="<?= SITE_URL ?>assets/images/logo.svg" alt="PayMyPrice logo">
                  </a>
								</div>
							</div>
						</div>
						
						<!-- Right Side -->
						<div class="footer-rows-right">

							<!-- Social Icons -->
							<div class="footer-row">
								<div class="footer-row-inner">
									<ul class="footer-social-links">
										<li>
											<a href="https://www.tiktok.com/@paymyprice" target="_blank" title="TikTok" data-tippy-placement="bottom" data-tippy-theme="light">
                        <img class="tiktok_icon" src="<?php echo SITE_URL . 'assets/images/tik-tok.svg';?>" title="TikTok"/> 
											</a>
										</li>
										<li>
											<a href="https://www.facebook.com/paymyprice" target="_blank" title="Facebook" data-tippy-placement="bottom" data-tippy-theme="light">
												<i class="icon-brand-facebook-f"></i>
											</a>
										</li>
										<li>
											<a href="https://twitter.com/paymyprice1" target="_blank" title="Twitter" data-tippy-placement="bottom" data-tippy-theme="light">
												<i class="icon-brand-twitter"></i>
											</a>
										</li>
										<li>
											<a href="https://www.instagram.com/paymyprice/" target="_blank" title="Instagram" data-tippy-placement="bottom" data-tippy-theme="light">
												<i class="icon-brand-instagram"></i>
											</a>
										</li>
										<li>
											<a href="https://www.linkedin.com/company/paymyprice/" target="_blank" title="LinkedIn" data-tippy-placement="bottom" data-tippy-theme="light">
												<i class="icon-brand-linkedin-in"></i>
											</a>
										</li>
									</ul>
									<div class="clearfix"></div>
								</div>
							</div>
							
							<!-- Language Switcher -->
							 
						</div>

					</div>
					<!-- Footer Rows Container / End -->
				</div>
			</div>
		</div>	
	<!-- Footer Top Section / End -->

	<!-- Footer Middle Section -->
	<div class="footer-middle-section">
		<div class="container">
			<div class="row">

        <div class="col-xl-4 col-lg-4 col-md-12"></div>

				<!-- Links -->
				<div class="col-xl-2 col-lg-2 col-md-3">
					<div class="footer-links">
						<a href="<?php echo SITE_URL . 'employer/signup-step.php';?>"><h3>Hire a Pro</h3></a>
						<!--<ul>
							<li><a href="#"><span>Browse Jobs</span></a></li>
							<li><a href="#"><span>Add Resume</span></a></li>
							<li><a href="#"><span>Job Alerts</span></a></li>
							<li><a href="#"><span>My Bookmarks</span></a></li>
						</ul>-->
					</div>
				</div>

				<!-- Links -->
				<div class="col-xl-2 col-lg-2 col-md-3">
					<div class="footer-links">
          <a href="<?php echo SITE_URL . 'freelancer/signup-step.php';?>"><h3>Find Customers</h3>
						<!--<ul>
							<li><a href="#"><span>Browse Candidates</span></a></li>
							<li><a href="#"><span>Post a Job</span></a></li>
							<li><a href="#"><span>Post a Task</span></a></li>
							<li><a href="#"><span>Plans & Pricing</span></a></li>
						</ul>-->
					</div>
				</div>

				<!-- Links -->
				<div class="col-xl-2 col-lg-2 col-md-3">
					<div class="footer-links">
						<h3>Helpful Links</h3>
						<ul>
							<li><a href="<?php echo SITE_URL . 'how-it-works.php';?>"><span>How It Works</span></a></li>
							<li><a href="<?php echo SITE_URL . 'who-we-are.php';?>"><span>Who We Are</span></a></li>
							<li><a href="<?php echo SITE_URL . 'contact-us.php';?>"><span>Contact Us</span></a></li>
						</ul>
					</div>
				</div>

				<!-- Links -->
				<div class="col-xl-2 col-lg-2 col-md-3">
					<div class="footer-links">
						<h3>Account</h3>
						<ul>
							<li><a href="#sign-in-dialog" class="popup-with-zoom-anim-inline"><span>Log In / Register</span></a></li>
						</ul>
					</div>
				</div>

				<!-- Newsletter -->
				<!-- <div class="col-xl-4 col-lg-4 col-md-12">
					<h3><i class="icon-feather-mail"></i> Sign Up For a Newsletter</h3>
					<p>Weekly breaking news, analysis and cutting edge advices on job searching.</p>
					<form action="#" method="get" class="newsletter">
						<input type="text" name="fname" placeholder="Enter your email address">
						<button type="submit"><i class="icon-feather-arrow-right"></i></button>
					</form> -->
				</div>
			</div>
		</div>
	</div>
	<!-- Footer Middle Section / End -->
	
	<!-- Footer Copyrights -->
	<div class="footer-bottom-section">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 text-left">
					© <?php echo date("Y"); ?><strong> PayMyPrice</strong>. All Rights Reserved.
				</div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 text-right">
          <div class="footer-links">
						<ul>
							<li><a href="<?php echo SITE_URL . 'terms-of-service.php';?>"><span>Terms of Service</span></a></li>
						</ul>
					</div>
        </div>
			</div>
		</div>
	</div>
	<!-- Footer Copyrights / End -->
	<!-- Footer Top Section -->
</div>
<!-- Footer / End -->

</div>
<!-- Wrapper / End -->


<!-- Scripts
================================================== -->
<?php include_once('../elements/foot-script.php');?>
</div>
<!-- Footer / End -->
</div>
<!-- Wrapper / End -->
</body>
</html>