<?php include('../helpers/classes/users.php');
require_once('restricted.php');
$objUser= new USER();
$profile = $objUser->getMyProfile($_SESSION['user']['result']['id']);  
//echo "<pre>";print_r($profile);die;  
if(isset($profile['result']['p_img']) && !empty($profile['result']['p_img'])){  
   $profile_img = SITE_URL.'uploads/profile/'.$_SESSION['user']['result']['p_img'];
}else{
   $profile_img = '';
}
?>  
<!doctype html>

<html lang="en">

<head>
	<?php include_once('../elements/head.php');?>
</head>
<body class="gray">
<!-- Wrapper -->

<div id="wrapper">
<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth dashboard-header not-sticky">  
  <?php include_once('dashboard_header.php');?>
</header>

<div class="clearfix"></div>
<!-- Header Container / End -->

<!-- Dashboard Container -->

<div class="dashboard-container">

	 <div class="dashboard-sidebar">
		<?php include_once('dashboard_sidebar.php');?>
	</div>
	<!-- Dashboard Content

	================================================== -->

	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >			

			<!-- Dashboard Headline -->

			<div class="dashboard-headline">
				<h3>Settings</h3> 
			</div>

			<!-- Row -->

			<div class="row">
				<!-- Dashboard Box -->
				<form id="update_account_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('update_account_form')?>" data-name="<?= base64_encode('update_account_form')?>" method="POST" onsubmit="return validateForm(this.id, event)" autocomplete="off">
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-account-circle"></i> My Account</h3>
						</div>

						<div class="content with-padding padding-bottom-0">
							<div class="row">
									<div class="col-auto">
										<div class="avatar-wrapper" data-tippy-placement="top" title="Change Avatar">
											<span id='image-loading'></span>    
						                    <div id="crop_preview" class="hide"> </div>                
						                    <div id="image_preview">
						                      	<img class="profile-pic" src="<?=$profile_img?>" alt="profile image" />
						                    </div>												
											<div class="upload-button"></div>
											<input class="file-upload" type="file" accept="image/*"/>
										</div>	
										<div class="change-photo-btn save" style="display: none">  
					                        <div class="photoUpload">
					                            <span><i class="fa fa-upload"></i> Crop & Save</span>                               
					                        </div>
					                    </div>
					                    <div class="change-photo-btn reset" style="display: none">    
					                        <div class="photoUpload">
					                            <span><i class="fa fa-close"></i> Reset</span>                            
					                        </div>
					                    </div> 									
									</div> 						
								<div class="col">
									<div class="row">
										<div class="col-xl-6">
											<div class="submit-field">
												<h5>First Name</h5>
												<input type="text" class="with-border" name="fname" maxlength="20" title="Please enter first name" value="<?= $profile['result']['fname']?>">
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Last Name</h5>
												<input type="text" class="with-border" name="lname" maxlength="20" title="Please enter last name" value="<?= $profile['result']['lname']?>">
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Email</h5>
												<input type="text" class="with-border" value="<?= $profile['result']['email']?>" readonly>
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Phone </h5>
												<input type="text" class="with-border" name="phone" maxlength="15" onkeypress="return isNumberKey(event)" title="Please enter Phone Number" value="<?= $profile['result']['phone']?>">
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Address </h5>
												<input type="text" class="with-border" id="autocomplete" name="address" title="Please enter address" value="<?= $profile['result']['address']?>">
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Country </h5>
												<input type="text" class="with-border" name="country" maxlength="20" id="country" title="Please enter country" value="<?= $profile['result']['country']?>">
											</div>
										</div>  

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>State </h5>
												<input type="text" class="with-border" name="state" maxlength="20" id="administrative_area_level_1" title="Please enter state" value="<?= $profile['result']['state']?>">
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Zip </h5>
												<input type="text" class="with-border" name="zip" onkeypress="return isNumberKey(event)" id="postal_code" title="Please enter Postal code" maxlength="10" value="<?= $profile['result']['zip']?>">
											</div>
										</div>

										 <div class="col-xl-6">	
											<div class="submit-field">
												<button type="submit" name="submit" class="button ripple-effect big margin-top-30 has-spinner">Save Changes <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
											</div>
										</div>															
									</div>
								</div>
							
							</div>
						</div>
					</div>
				</div>
			</form>
				<!-- Dashboard Box -->
			<form id="add_detail_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('add_detail_form')?>" data-name="<?= base64_encode('add_detail_form')?>" method="POST" onsubmit="return validateForm(this.id, event)" autocomplete="off">
			 <div class="col-xl-12">
					<div class="dashboard-box">					 
   
						<div class="headline">
							<h3><i class="icon-material-outline-face"></i> My Profile</h3>
						</div>
						<div class="content">
							<ul class="fields-ul">
								<li>
									<div class="row">
										<div class="col-xl-4">
											<div class="submit-field">
												<div class="bidding-widget">
													<span class="bidding-detail">Set your <strong>minimal hourly rate</strong></span>
													<div class="bidding-value margin-bottom-10">$<span id="biddingVal"></span></div>
													<input class="bidding-slider" name="hourly_rate" type="text" value="" data-slider-handle="custom" data-slider-currency="$" data-slider-min="5" data-slider-max="150" data-slider-value="<?= $profile['result']['hourly_rate']?>" data-slider-step="1" data-slider-tooltip="hide" />
												</div>
											</div>
										</div>


										<div class="col-xl-8">
											<div class="submit-field">
												<h5>Tags <i class="help-icon" data-tippy-placement="right"></i></h5>	
												<div class="keywords-container">
													<div class="keyword-input-container">
														<input type="text" class="keyword-input with-border tags-type" placeholder="e.g. Angular, Laravel"/>
														<button type="button" class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button>
													</div>
													<div class="keywords-list">
														<?php if($profile['result']['tags'] !== ""){
															foreach($profile['result']['tags'] as $tag){?>
																<span class="keyword"><span class="keyword-remove"></span><span class="keyword-text"><?= $tag?></span></span>
														<?php } } ?>							
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
								</li>

							<li>
								<div class="row">
									<div class="col-xl-6">
										<div class="submit-field">
											<h5>Tagline</h5>
											<input type="text" class="with-border" name="tagline" title="Please enter tagline" maxlength="30" value="<?= $profile['result']['tagline']?>">
										</div>
									</div>

									<div class="col-xl-6">
										<div class="submit-field">
											<h5>Nationality</h5>
											<select class="selectpicker with-border" data-size="7" name="nationality" data-live-search="true">
												<option value="AR">Argentina</option>
												<option value="AM">Armenia</option>
												<option value="AW">Aruba</option>
												<option value="AU">Australia</option>
												<option value="AT">Austria</option>
												<option value="AZ">Azerbaijan</option>
												<option value="BS">Bahamas</option>
												<option value="BH">Bahrain</option>
												<option value="BD">Bangladesh</option>
												<option value="BB">Barbados</option>
												<option value="BY">Belarus</option>
												<option value="BE">Belgium</option>
												<option value="BZ">Belize</option>
												<option value="BJ">Benin</option>
												<option value="BM">Bermuda</option>
												<option value="BT">Bhutan</option>
												<option value="BG">Bulgaria</option>
												<option value="BF">Burkina Faso</option>
												<option value="BI">Burundi</option>
												<option value="KH">Cambodia</option>
												<option value="CM">Cameroon</option>
												<option value="CA">Canada</option>
												<option value="CV">Cape Verde</option>
												<option value="KY">Cayman Islands</option>
												<option value="CO">Colombia</option>
												<option value="KM">Comoros</option>
												<option value="CG">Congo</option>
												<option value="CK">Cook Islands</option>
												<option value="CR">Costa Rica</option>
												<option value="CI">Côte d'Ivoire</option>
												<option value="HR">Croatia</option>
												<option value="CU">Cuba</option>
												<option value="CW">Curaçao</option>
												<option value="CY">Cyprus</option>
												<option value="CZ">Czech Republic</option>
												<option value="DK">Denmark</option>
												<option value="DJ">Djibouti</option>
												<option value="DM">Dominica</option>
												<option value="DO">Dominican Republic</option>
												<option value="EC">Ecuador</option>
												<option value="EG">Egypt</option>
												<option value="GP">Guadeloupe</option>
												<option value="GU">Guam</option>
												<option value="GT">Guatemala</option>
												<option value="GG">Guernsey</option>
												<option value="GN">Guinea</option>
												<option value="GW">Guinea-Bissau</option>
												<option value="GY">Guyana</option>
												<option value="HT">Haiti</option>
												<option value="HN">Honduras</option>
												<option value="HK">Hong Kong</option>
												<option value="HU">Hungary</option>
												<option value="IS">Iceland</option>
												<option value="IN">India</option>
												<option value="ID">Indonesia</option>
												<option value="NO">Norway</option>
												<option value="OM">Oman</option>
												<option value="PK">Pakistan</option>
												<option value="PW">Palau</option>
												<option value="PA">Panama</option>
												<option value="PG">Papua New Guinea</option>
												<option value="PY">Paraguay</option>
												<option value="PE">Peru</option>
												<option value="PH">Philippines</option>
												<option value="PN">Pitcairn</option>
												<option value="PL">Poland</option>
												<option value="PT">Portugal</option>
												<option value="PR">Puerto Rico</option>
												<option value="QA">Qatar</option>
												<option value="RE">Réunion</option>
												<option value="RO">Romania</option>
												<option value="RU">Russian Federation</option>
												<option value="RW">Rwanda</option>
												<option value="SZ">Swaziland</option>
												<option value="SE">Sweden</option>
												<option value="CH">Switzerland</option>
												<option value="TR">Turkey</option>
												<option value="TM">Turkmenistan</option>
												<option value="TV">Tuvalu</option>
												<option value="UG">Uganda</option>
												<option value="UA">Ukraine</option>
												<option value="GB">United Kingdom</option>
												<option value="US" selected>United States</option>
												<option value="UY">Uruguay</option>
												<option value="UZ">Uzbekistan</option>
												<option value="YE">Yemen</option>
												<option value="ZM">Zambia</option>
												<option value="ZW">Zimbabwe</option>
											</select>
										</div>
									</div>

									<div class="col-xl-12">
										<div class="submit-field">
											<h5>Introduce Yourself</h5>
											<textarea cols="30" rows="5" name="your_self" title="Please introduce yourself" class="with-border"><?= $profile['result']['your_self']?></textarea>

										</div>
									</div>

									 <div class="col-xl-6">	
										<div class="submit-field">
											<button type="submit" name="submit" class="button ripple-effect big margin-top-30 has-spinner">Save Changes <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
										</div>
									</div>
								</div>
							</li>							
						</ul>
					</div>
				</div>
			</div> 
		</form>	

		<form id="add_card_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('add_card_form')?>" data-name="<?= base64_encode('add_card_form')?>" method="POST" onsubmit="return validateForm(this.id, event)" autocomplete="off">
			<div class="col-xl-12">
				<div id="test1" class="dashboard-box">
					<!-- Headline -->
					<div class="headline">
						<h3><i class="icon-material-outline-credit-card"></i> Payment Methods</h3>
					</div>

					<div class="content with-padding">
						<div class="row <?php if(!empty($profile['result']['account_no'])){echo 'hide';}?>" id="addCard">											
							<div class="col-xl-4">
								<div class="submit-field">
									<h5>Card Number</h5>
									<input type="text" name="account_no" class="with-border" title="Please enter card number" onkeypress="return isNumberKey(event)" maxlength="20"/>
								</div>
							</div>

							<div class="col-xl-3">
								<div class="submit-field">
									<h5>Expiry Date</h5>
									<input type="text" name="expire_card" class="with-border expire_card" title="Please enter card expire date" onkeypress="return isNumberKey(event)" maxlength="5" placeholder="MM/YY"/>
								</div>
							</div>

							<div class="col-xl-2">
								<div class="submit-field">
									<h5>CVV</h5>
									<input type="text" name="cvv" class="with-border" title="Please enter cvv" onkeypress="return isNumberKey(event)" maxlength="3"/>
								</div>
							</div>

							<div class="col-xl-2">
								<div class="submit-field">
									<button class="button full-width button-sliding-icon ripple-effect margin-top-30 has-spinner" type="submit" name="submit" style="width: 475px;">Add<i class="icon-material-outline-arrow-right-alt"></i> <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
								</div>
							</div>	
						</div>					
						<div class="row <?php if(empty($profile['result']['account_no'])){echo 'hide';}?>" id="card_list">
							<div class="col-xl-12">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col">Card No</th>
											<th scope="col">Expiry Date</th>
											<th scope="col">Action</th>
										</tr>
									</thead>
									<tbody>	
										<td>1</td>	
										<td><?= $profile['result']['account_no'] ?></td>
										<td><?= $profile['result']['expire_card'] ?></td>
										<td><a href="<?= SITE_URL?>helpers/magnific-popup.php?popup=edit-card&id=<?= base64_encode($profile['result']['c_id'])?>" class="popup-with-zoom-anim-ajax"><i class="icon-line-awesome-pencil"></i></a></td>
									</tbody>
								</table>
							</div>	
						</div>
					</div>
				</div>
			</div>				
		</form>	
				<!-- Dashboard Box -->
		<form id="add_password_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('add_password_form')?>" data-name="<?= base64_encode('add_password_form')?>" method="POST" onsubmit="return validateForm(this.id, event)" autocomplete="off">		
			<div class="col-xl-12">
				<div id="test" class="dashboard-box">
					<!-- Headline -->
					<div class="headline">
						<h3><i class="icon-material-outline-lock"></i> Password & Security</h3>
					</div>
					<div class="content with-padding">
						<div class="row">								
							<div class="col-xl-3">
								<div class="submit-field">
									<h5>Current Password</h5>
									<input type="password" name="current_pass" id="current_pass" title="Please enter current password" maxlength="15" class="with-border"/>
									<span toggle="#current_pass" class="fa fa-fw fa-eye field-icon toggle-password"></span>
								</div>
							</div>

							<div class="col-xl-3">
								<div class="submit-field">
									<h5>New Password <i class="help-icon" data-tippy-placement="right" title="Password should be at least 8 characters in length and should include at least one upper case letter, one number."></i></h5>
									<input type="password" id="psw" name="new_pass" title="Please enter new password" class="with-border" maxlength="15"/>
									<span toggle="#psw" class="fa fa-fw fa-eye field-icon toggle-password"></span>
								</div>
							</div>

							<div class="col-xl-3">
								<div class="submit-field">  
									<h5>Repeat New Password</h5>
									<input type="password" class="with-border" name="confirm_pass" title="Please confirm new password" id="confirm_pass" maxlength="15" />
									<span toggle="#confirm_pass" class="fa fa-fw fa-eye field-icon toggle-password"></span>
								</div>
							</div>

							<div class="col-xl-2">
								<div class="submit-field">
									<button class="button full-width button-sliding-icon ripple-effect margin-top-30 has-spinner" type="submit" name="submit" style="width: 475px;">Save<i class="icon-material-outline-arrow-right-alt"></i> <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		</div>
			<!-- Row / End -->

			<!-- Footer -->

			<div class="dashboard-footer-spacer"></div>

			<div class="small-footer margin-top-15">

				 <?php include_once('dashboard_footer.php');?>

			</div>
			<!-- Footer / End -->
		</div>

	</div>

	<!-- Dashboard Content / End -->

</div>

<!-- Dashboard Container / End -->


</div>
<!-- Wrapper / End -->

<!-- Scripts

================================================== -->

<?php include_once('../elements/foot-script.php');?>

<!-- Google Autocomplete -->
<?php include_once('../elements/google-provider.php'); ?>

</body>

</html>