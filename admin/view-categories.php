<?php include('../helpers/classes/admin.php');  
require_once('restricted.php');
$objAdmin = new ADMIN(); 

$categoryList = $objAdmin->getAllMainCategories();

//echo "<pre>";print_r($userList );die;   
  ?>  
<!doctype html>
<html lang="en">
<head>
	<?php include_once('../elements/head.php');?>
</head>
<body class="gray">
<!-- Wrapper -->

<div id="wrapper">
<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth dashboard-header not-sticky">
      <?php include_once('dashboard_header.php');?>
</header>

<div class="clearfix"></div>
<!-- Header Container / End -->

<!-- Dashboard Container -->

<div class="dashboard-container">
	<!-- Dashboard Sidebar

	================================================== -->
	<div class="dashboard-sidebar">
		<?php include_once('dashboard_sidebar.php');?>
	</div>

	<!-- Dashboard Content

	================================================== -->

	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >		

			<!-- Dashboard Headline -->

			<div class="dashboard-headline">
				<h3>Manage Main Categories</h3>
				<!-- Breadcrumbs -->		 

			</div>

			<!-- Row -->

			<div class="row">
				<!-- Dashboard Box -->

				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">
						<!-- Headline -->

						<div class="headline">
							<h3><i class="icon-material-outline-supervisor-account"></i><?= $categoryList['count']?> Categories</h3>
						</div>


						<div class="content">								
							<table class="basic-table">
								<tr>
									<th>Banner</th> 
									<th>Icon</th>  
									<th>Category Title</th>
									<th>Description</th>
									<th>Sub Categories</th>
									<th>Action</th>  
								</tr>

							<?php foreach ($categoryList['result'] as $key => $value) {?>
								<tr>
									<td data-label="Column 1"><div class=""><img src="<?= SITE_URL.'uploads/category/'.$value['banner_img'] ?>" alt=""></div></td>
									<td data-label="Column 2"><div class=""><img src="<?= SITE_URL.'uploads/category/'.$value['icon'] ?>" alt=""></div></td>
									<td data-label="Column 3"><?= $value['category']?></td>
									<td data-label="Column 4"><?= $value['description']?></td>
									<td data-label="Column 5"><?= $value['subcat']?></td>
									
									<td data-label="Column 6">	
										<a href="<?= ADMIN_URL?>edit-category.php?cId=<?= base64_encode($value['id'])?>" class="button gray ripple-effect ico" data-tippy-placement="top" data-tippy="" data-original-title="Edit"><i class="icon-feather-edit"></i></a>
										<a href="<?= SITE_URL?>helpers/magnific-popup.php?popup=delete_category&id=<?= base64_encode($value['id'])?>" class="button ico popup-with-zoom-anim-ajax gray ripple-effect" data-tippy-placement="top" data-tippy="" data-original-title="Remove"><i class="icon-feather-trash-2"></i></a>
									</td>
								</tr>
							<?php } ?>
							</table>
							
						</div>
					</div>
				</div>
			</div>

			<!-- Row / End -->
			<!-- Footer -->

			<div class="dashboard-footer-spacer"></div>
			<div class="small-footer margin-top-15">
				 <?php include_once('dashboard_footer.php');?>
			</div>			 

			<!-- Footer / End -->
		</div>
	</div>

	<!-- Dashboard Content / End -->
</div>
<!-- Dashboard Container / End -->
</div>
<!-- Wrapper / End -->

<!-- Scripts

================================================== -->

<?php include_once('../elements/foot-script.php');?>

</body>
</html>