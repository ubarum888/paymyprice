<?php  
$page_file_temp = $_SERVER["PHP_SELF"];
$page_directory = dirname($page_file_temp);

if(basename($page_directory) !== 'admin') {
	FD_add_notices('You have not access for admin. Please log in again', 'error');	
	header('location:'.SITE_URL);
	exit;
}

if(!isset($_SESSION['admin']['result']['id'])){	
	FD_add_notices('Your session id not set or expired. Please log in again', 'error');	
	header('location:'.ADMIN_URL.'login.php');
	exit;
}

if(isset($_SESSION['admin']['result']['id']) && ($_SESSION['admin']['result']['id'] == '')){	
	FD_add_notices('Your session id expired', 'error');	
	header('location:'.ADMIN_URL.'login.php');
	exit;
}

if(isset($_SESSION['admin']['result']['account_type']) && ($_SESSION['admin']['result']['account_type'] !== '3')) {
	FD_add_notices('Account Type not matched', 'error');	
    header('location:'.SITE_URL);
    exit;
}
?>