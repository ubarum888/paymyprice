<?php include('../helpers/classes/admin.php');

require_once('restricted.php');
$objAdmin = new ADMIN();
/*$noOfPost = $objUser->getCountOfPostedJobs($_SESSION['user']['result']['id']);  
$notes = $objUser->getAllNotesByUserId($_SESSION['user']['result']['id']); */

?>   
<!doctype html>

<html lang="en">

<head>

	<?php include_once('../elements/head.php');?>

</head>

<body class="gray">



<!-- Wrapper -->

<div id="wrapper">



<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth dashboard-header not-sticky">

   <?php include_once('dashboard_header.php');?>

</header>

<div class="clearfix"></div>

<!-- Header Container / End -->





<!-- Dashboard Container -->

<div class="dashboard-container">



	<!-- Dashboard Sidebar

	================================================== -->

	<div class="dashboard-sidebar">

		<?php include_once('dashboard_sidebar.php');?>

	</div>

	<!-- Dashboard Sidebar / End -->





	<!-- Dashboard Content

	================================================== -->

	<div class="dashboard-content-container" data-simplebar>

		<div class="dashboard-content-inner" >

			

			<!-- Dashboard Headline -->

			<div class="dashboard-headline">

				<h3>Howdy, Admin!</h3>

				<span>We are glad to see you again!</span>



				<!-- Breadcrumbs -->

				 

			</div>

	

			<!-- Fun Facts Container -->

			<div class="fun-facts-container">

				<div class="fun-fact" data-fun-fact-color="#36bd78">

					<div class="fun-fact-text">

						<span>Task Bids Won</span>

						<h4>22</h4>

					</div>

					<div class="fun-fact-icon"><i class="icon-material-outline-gavel"></i></div>

				</div>

				<div class="fun-fact" data-fun-fact-color="#b81b7f">

					<div class="fun-fact-text">

						<span>Jobs Applied</span>

						<h4>44</h4>

					</div>

					<div class="fun-fact-icon"><i class="icon-material-outline-business-center"></i></div>

				</div>

				<div class="fun-fact" data-fun-fact-color="#efa80f">

					<div class="fun-fact-text">

						<span>Reviews</span>

						<h4>28</h4>

					</div>

					<div class="fun-fact-icon"><i class="icon-material-outline-rate-review"></i></div>

				</div>
		

			</div>	

			<!-- Footer -->

			<div class="dashboard-footer-spacer"></div>

			<div class="small-footer margin-top-15">

				<?php include_once('dashboard_footer.php');?>

			</div>

			<!-- Footer / End -->



		</div>

	</div>

	<!-- Dashboard Content / End -->



</div>

<!-- Dashboard Container / End -->



</div>

<!-- Wrapper / End -->


<!-- Scripts

================================================== -->

<?php include_once('../elements/foot-script.php');?>
</body>

</html>