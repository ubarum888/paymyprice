
<script src="<?= SITE_URL ?>assets/js/jquery-3.3.1.min.js"></script>
<script src="<?= SITE_URL ?>assets/js/jquery-migrate-3.0.0.min.js"></script>
<script src="<?= SITE_URL ?>assets/js/mmenu.min.js"></script>
<script src="<?= SITE_URL ?>assets/js/tippy.all.min.js"></script>
<script src="<?= SITE_URL ?>assets/js/simplebar.min.js"></script>
<!--   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script> -->
<!-- <script src="<?php //SITE_URL ?>assets/js/bootstrap.min.js"></script> -->
<script src="<?= SITE_URL ?>assets/js/bootstrap-slider.min.js"></script>
 <script src="<?= SITE_URL ?>assets/js/bootstrap-select.min.js"></script> 
<script src="<?= SITE_URL ?>assets/js/snackbar.js"></script>
<script src="<?= SITE_URL ?>assets/js/clipboard.min.js"></script>
<script src="<?= SITE_URL ?>assets/js/counterup.min.js"></script>
<script src="<?= SITE_URL ?>assets/js/magnific-popup.min.js"></script>
<script src="<?= SITE_URL ?>assets/js/slick.min.js"></script>   
<!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>  -->
<script src="<?= SITE_URL ?>assets/js/wow.js"></script>  
<script src="<?= SITE_URL ?>assets/js/croppie.js"></script>  
<script src="<?= SITE_URL ?>assets/js/custom.js"></script>
<script src="<?= SITE_URL ?>services.js"></script>

<?php if(isset($_SESSION['user']['result']['id']) && !empty($_SESSION['user']['result']['id'])){?>
    <script src="http://js.pusher.com/1.12/pusher.min.js" type="text/javascript"></script> 
    <script src="<?= SITE_URL?>assets/js/jquery.pusherchat.js" type="text/javascript"></script>

    <script type="text/javascript">
    	
    	var user_id =  $('#user_id').data('id');
        var name = $('#user_id').data('name');
        $.fn.pusherChat({
            'pusherKey':'bbcf715b9a4eaafd9671',
            'authPath':SITE_URL+'helpers/classes/pusher_auth.php?user_id='+user_id+'&name='+name,
            'friendsList' : <?php echo $friendsList ?>,
            'serverPath' : SITE_URL+'helpers/classes/pusher_server.php',
            'profilePage':true,
            'onFriendConnect': function(member){                	 
                
            },
            'onFriendLogOut': function(member){
               
            },
            'onSubscription':function(members){
                          
            }
        });
    </script>
 <?php } ?>   

 

 
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
// Snackbar for user status switcher
$('#snackbar-user-status label').click(function() { 
	Snackbar.show({
		text: 'Your status has been changed!',
		pos: 'bottom-center',
		showAction: false,
		actionText: "Dismiss",
		duration: 3000,
		textColor: '#fff',
		backgroundColor: '#383838'
	}); 
}); 
</script>