<?php 

include('../helpers/classes/admin.php'); 

include('../helpers/classes/users.php'); 


require_once('restricted.php');
$objAdmin = new ADMIN(); 

//echo "<pre>";print_r($userList );die;   
  ?>  
<!doctype html>
<html lang="en">
<head>
	<?php include_once('../elements/head.php');?>
</head>
<body class="gray">
<!-- Wrapper -->

<div id="wrapper">
<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth dashboard-header not-sticky">
      <?php include_once('dashboard_header.php');?>
</header>

<div class="clearfix"></div>
<!-- Header Container / End -->

<!-- Dashboard Container -->

<div class="dashboard-container">
	<!-- Dashboard Sidebar

	================================================== -->
	<div class="dashboard-sidebar">
		<?php include_once('dashboard_sidebar.php');?>
	</div>

	<!-- Dashboard Content

	================================================== -->

	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >		

			<!-- Dashboard Headline -->

			<div class="dashboard-headline">
				<h3>Manage Users</h3>
				<!-- Breadcrumbs -->		 

			</div>

	

			<!-- Row -->

			<div class="row">
				<!-- Dashboard Box -->

				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">
						<!-- Headline -->

						<div class="headline">
							<h3><i class="icon-material-outline-supervisor-account"></i><?= $userList['count']?> Users</h3>
						</div>


						<div class="content manage_users_content">								
							<table class="basic-table">
								<tr>
									<th></th>  
									<th>Full Name</th>
									<th>Email</th>
									<th>Phone</th>
                  <th>Address</th>
                  <th>Zip Code</th>
									<th>User Type</th>
                  <th>Categories</th>
                  <th>Sub-Categories</th>
                  <th>Status</th> 
									<th>Action</th>  
								</tr>

							<?php foreach ($userList['result'] as $key => $value) {
                $objUser= new USER(); 
                $userCategoriesList = $objUser->getSelectedCategories($value['id']); 
                $userSubCategoriesList = $objUser->getSelectedSubCategories($value['id']);

								if($value['p_img'] !== null){
									$img = $value['p_img'];
								}else{
									$img = 'user-avatar-placeholder.png';
								}
								?>
								<tr>
									<td data-label="Column 1"><div class="user-avatar user-active-status-<?= $value['id']?> status-offline"><img src="<?= SITE_URL.'uploads/profile/'.$img ?>" alt=""></div></td>
									<td data-label="Column 2"><?= $value['fname'].' '.$value['lname']?></td>
									<td data-label="Column 3"><?= $value['email']?></td>
									<td data-label="Column 4"><?= $value['phone']?></td>
									<td data-label="Column 5"><?= $value['address']?></td>
									<td data-label="Column 6"><?= $value['zip']?></td>
									<td data-label="Column 7"><?php if($value['account_type']==1){echo 'Employer';}else if($value['account_type']==2){echo 'Freelancer';}?></td>
                  <td data-label="Column 8">
                    <ul>
                      <?php 
                        // print_r($userCategoriesList); 
                        foreach($userCategoriesList['result'] as $key=>$cat){
                          echo '<li>'.$cat['category'].'</li>';
                        }
                      ?>
                    </ul>
                  </td>
                  <td data-label="Column 9">
                    <ul>
                      <?php
                        // print_r($userSubCategoriesList);
                        foreach($userSubCategoriesList['result'] as $key=>$sub_cat){
                          echo '<li>'.$sub_cat['subcategory'].'</li>';
                        }
                      ?>
                    </ul>
                  </td>
                  <td data-label="Column 10"><label class="switch"><input type="checkbox" class="activ_deactiv_user" data-id="<?= base64_encode($value['id'])?>" <?php if($value['status']==1){echo 'checked';}?>><span class="switch-button"></span> </label></td>
									<td data-label="Column 11"><a href="<?= SITE_URL?>helpers/magnific-popup.php?popup=delete_user&id=<?= base64_encode($value['id'])?>" class="button ico popup-with-zoom-anim-ajax gray ripple-effect" data-tippy-placement="top" data-tippy="" data-original-title="Remove"><i class="icon-feather-trash-2"></i></a></td>
								</tr>
							<?php } ?>
							</table>
							
						</div>
					</div>
				</div>
			</div>

			<!-- Row / End -->
			<!-- Footer -->

			<div class="dashboard-footer-spacer"></div>
			<div class="small-footer margin-top-15">
				 <?php include_once('dashboard_footer.php');?>
			</div>			 

			<!-- Footer / End -->
		</div>
	</div>

	<!-- Dashboard Content / End -->
</div>
<!-- Dashboard Container / End -->
</div>
<!-- Wrapper / End -->

<!-- Scripts

================================================== -->

<?php include_once('../elements/foot-script.php');?>

</body>
</html>