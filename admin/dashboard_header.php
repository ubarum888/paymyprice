<?php $userList =  $objAdmin->getAllUsersList();
$friendsList = json_encode($userList['userList']);

$userAlerts = $objAdmin->getUserAlerts($_SESSION['admin']['result']['id']);  
?>

<div id="header">
		<div class="container">
			<!-- Left Side Content -->

			<div class="left-side">			
				<!-- Logo -->


				<div id="logo">
					<a href="<?= ADMIN_URL?>"><img src="<?= SITE_URL ?>assets/images/logo.svg" alt=""></a>
				</div>			

				<div class="clearfix"></div>
				<!-- Main Navigation / End -->

			</div>

			<!-- Left Side Content / End -->

			<!-- Right Side Content / End -->	

			<div class="right-side">
				<!--  User Notifications -->

				<div class="header-widget hide-on-mobile">	
					<!-- Notifications -->
							<!-- Notifications -->
					<div class="header-notifications notify-alert">
						<!-- Trigger -->
						<div class="header-notifications-trigger">
							<a href="#"><i class="icon-feather-bell"></i><span class="notify-unread <?php if($userAlerts['notification'] == 0){echo 'hide';}?>"><?= $userAlerts['notification']?></span></a> 
						</div>

						<!-- Dropdown -->
						<div class="header-notifications-dropdown">
							<div class="header-notifications-headline">
								<h4>Notifications</h4>
								<!-- <button class="mark-as-read ripple-effect-dark" title="Mark all as read" data-tippy-placement="left">
									<i class="icon-feather-check-square"></i>
								</button> -->
							</div>

							<div class="header-notifications-content">
								<div class="header-notifications-scroll" data-simplebar>
									<ul>
										<span id='image-loading'></span> 
										<div id="load_notify"></div> 
               							<div id="load_data_notify" class="text-center"></div> 							
									</ul>
								</div>
							</div>
						</div>
					</div>	
				</div>

				<!--  User Notifications / End -->
				<!-- User Menu -->

				<div class="header-widget">
									<!-- Messages -->

					<div class="header-notifications user-menu">
						<div class="header-notifications-trigger">
							<a href="#"><div class="user-avatar status-online"><img src="<?= SITE_URL ?>assets/images/user-avatar-small-01.jpg" alt=""></div></a>
						</div>
						<!-- Dropdown -->



						<div class="header-notifications-dropdown">
									<!-- User Status -->

							<div class="user-status">
								<!-- User Name / Avatar -->

								<div class="user-details">
									<div class="user-avatar status-online"><img src="<?= SITE_URL ?>assets/images/user-avatar-small-01.jpg" alt=""></div>

									<div class="user-name">
										<?= ucwords($_SESSION['admin']['result']['fname'].' '.$_SESSION['admin']['result']['lname'])?><span>Adminstrator</span>
									</div>
								</div>

								<!-- User Status Switcher -->



								<div class="status-switch" id="snackbar-user-status">
									<label class="user-online current-status">Online</label>
									<label class="user-invisible">Invisible</label>
									<!-- Status Indicator -->
									<span class="status-indicator" aria-hidden="true"></span>
								</div>
						</div>					



						<ul class="user-menu-small-nav">
							<li><a href="<?= ADMIN_URL?>"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>					
							<li><a href="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('admin_logout')?>"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
						</ul>

						</div>
					</div>
				</div>



				<!-- User Menu / End -->

				<!-- Mobile Navigation Button -->	
			</div>

	

			<!-- Right Side Content / End -->

		</div>



	</div>

	<input type="hidden" id="user_id" data-id="<?= $_SESSION['admin']['result']['id']?>" data-name="<?= $_SESSION['admin']['result']['fname'].' '.$_SESSION['admin']['result']['lname']?>" value="<?= base64_encode($_SESSION['admin']['result']['id'])?>"/>

	