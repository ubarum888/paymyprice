<?php include('helpers/classes/config.php');   ?>
<!doctype html>

<html lang="en">

<head>

	<?php include_once('elements/head.php');?>



</head>

<body>



<!-- Wrapper -->

<div id="wrapper">



<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth">

 <?php include_once('elements/header.php');?>

</header>

<div class="clearfix"></div>

<!-- Header Container / End -->



<!-- Titlebar

================================================== -->

 





<!-- Page Content

================================================== -->

<div class="container padt_70">

	<div class="row">

		<div class="col-xl-5  center">





			<div class="login-register-page">

				<!-- Welcome Text -->

				<div class="welcome-text">

					<h3>Recover your password?</h3>

					<span>Don't have an account? <a href="signup.php">Sign Up!</a></span>

				</div>

					

				<!-- Form -->

				<form id="forgot_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('forgot_form')?>" data-name="<?= base64_encode('forgot_form')?>" method="POST" onsubmit="return validateForm(this.id, event)">

					<div class="input-with-icon-left">
						<i class="icon-material-baseline-mail-outline"></i>
						<input type="text" class="input-text with-border" name="email" id="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'" maxlength="50" placeholder="Email Address" title="Enter Email Address"/>

					</div>
					
					<button type="submit" name="submit" class="button full-width button-sliding-icon ripple-effect margin-top-10 has-spinner" >Recover Password <i class="icon-material-outline-arrow-right-alt"></i> <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>  
				</form>

				

				<!-- Button -->

				

				

				<!-- Social Login 

				<div class="social-login-separator"><span>or</span></div>

				<div class="social-login-buttons">

					<button class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> Log In via Facebook</button>

					<button class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i> Log In via Google+</button>

				</div>-->

			</div>



		</div>

	</div>

</div>





<!-- Spacer -->

<div class="margin-top-70"></div>

<!-- Spacer / End-->



<!-- Footer

================================================== -->

<div id="footer">

 <?php include_once('elements/footer.php');?>

</div>

<!-- Footer / End -->

</div>

<!-- Wrapper / End -->



</body>

</html>