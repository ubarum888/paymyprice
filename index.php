<?php include('helpers/classes/admin.php');  
require('user-session.php');
include('helpers/classes/users.php'); 
$objAdmin = new ADMIN();
$objUser= new USER(); 
$category = $objAdmin->getAllMainCategories();
$featuredJob = $objUser->getRecentJobs();
?>
<!doctype html>
<html lang="en">
<head>
<!--  Essential META Tags -->
<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1">
<link rel="canonical" href="https://paymyprice.com/">
<meta property="og:locale" content="en_US">
<meta property="og:type" content="website">
<meta property="og:url" content="https://paymyprice.com/">
<meta property="og:site_name" content="PayMyPrice">
<title>PayMyPrice - Save Money. Get Connected.</title>
<meta property="og:title" content="PayMyPrice - Save Money. Get Connected.">
<meta name="twitter:title" content="PayMyPrice - Save Money. Get Connected.">
<meta name="description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">
<meta property="og:description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">
<meta name="twitter:description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">
<meta property="og:image" content="https://paymyprice.com/assets/images/logo.svg">
<meta property="og:image:secure_url" content="https://paymyprice.com/assets/images/logo.svg">
<meta name="twitter:image" content="https://paymyprice.com/assets/images/logo.svg">
<meta name="twitter:card" content="summary_large_image">
<meta property="og:image:width" content="400">
<meta property="og:image:height" content="50">
  
<?php include_once('elements/head.php');?>
</head>
<body>
<!-- Wrapper -->
<div id="wrapper">
<!-- Header Container
================================================== -->
<header id="header-container" class="fullwidth">
	 <?php include_once('elements/header.php');?>
</header>
<div class="clearfix"></div>
<!-- Header Container / End -->
<!-- Intro Banner
================================================== -->
<!-- add class "disable-gradient" to enable consistent background overlay -->
<div class="intro-banner" data-background-image="<?= SITE_URL ?>assets/images/banner1.jpg">
	<div class="container">
		
		<!-- Intro Headline -->
		<div class="row">
			<div class="col-md-12">
             <div class="banner-headline">
					 
				<h1>Save Money. Get Connected.</h1>
			</div>
				<div class="home tabs">
				<div class="tabs-header">
					<ul>
						<li class="active"><a href="#tab-1" data-tab-id="1">Hire a pro</a></li>
						<li><a href="#tab-2" data-tab-id="2">Find Customers</a></li>
						 
					</ul> 
				</div>
				<!-- Tab Content -->
				<!--<div class="tabs-content" style="height: 185px;">-->
				<div class="tabs-content">
					<form action="<?= CLIENT_URL?>signup-step.php" type="GET">
					<div class="tab active" data-tab-id="1">
						<!--<h1>Find local professionals for pretty much anything.</h1>-->
						<!--<div class="intro-banner-search-form mt-30">-->
            <div class="intro-banner-search-form">
						<!-- Search Field -->
						
							<!-- Search Field -->
							<div class="intro-search-field">
								<div class="ui-widget">
								  <input id="cat-suggestion" type="text" placeholder="What Service Do You Need?" required>
								</div>
							</div>  
							<div class="intro-search-field with-autocomplete">
								<div class="input-with-icon">
									<input id="postal_code" name="location" type="text" placeholder="Location">
									<i class="icon-material-outline-location-on"></i>
								</div>
							</div>
							<!-- Button -->
							<div class="intro-search-button">  
								 <input id="category-id" name="category" type="hidden">
								<button type="submit" class="button ripple-effect">Search</button>
							</div>
						
						</div>
					</div>
				</form>
					
					<div class="tab" data-tab-id="2">
					<h1>Meet new Customers in your area.</h1>
					<p>Sign up for a free profile on PayMyPrice to start growing your business. </p>
					<div class="intro-search-button">
							<a href="<?= VENDOR_URL?>signup-step.php" class="button ripple-effect">Get Started</a>
						</div>
					</div>
					 
				</div>
			</div>
			<div class="banner-headline">
					 
				<!--<h1>Save Money. Get Connected.</h1>-->
				<br>
				<h3>Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.</h3>
			</div>
			</div>
		</div>
		
		<!-- Search Bar -->
		 
		<!-- Stats -->
		<div class="row">
			<div class="col-md-12">
				<ul class="intro-stats margin-top-70 hide-under-992px">
					<li>
						<strong class="counter">1,586</strong>
						<span>Jobs Posted</span>
					</li>
					<li>
						<strong class="counter">3,543</strong>
						<span>Tasks Posted</span>
					</li>
					<li>
						<strong class="counter">1,232</strong>
						<span>Freelancers</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- Content
================================================== -->
<!-- Category Boxes -->
<div class="section margin-top-65">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="section-headline centered margin-bottom-15">
					<h3>Popular Job Categories</h3>
				</div>
				<!-- Category Boxes Container -->
				<div class="categories-container">
					<!-- Category Box -->
					<?php if(!empty($category['result'])) {
						foreach ($category['result'] as $key => $value) {
							if(empty($value['icon'])){
								$icon = '<i class="icon-line-awesome-image"></i>';
							}else{
								$icon = '<img src="'.SITE_URL.'uploads/category/'.$value['icon'].'" width="50">';
							}
							$subcat = explode(',', $value['subcat']);
							?>
							<!-- <a href="category-detail.php?c=<?= base64_encode($value['id'])?>" class="category-box"> -->
							<?php 
								$category_link = '/contractors/' . $value['slug'];
							?>
							<a href="<?php echo $category_link; ?>" class="category-box">
								<div class="category-box-icon">
									<?= $icon ?>
								</div>								
								<div class="category-box-content">
									<h3><?= $value['category']?></h3>
									<p><?= $subcat[0].', '.$subcat[1].' & more'?></p>
								</div>
							</a>
					<?php } } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Category Boxes / End -->
<!-- Features Jobs -->
<div class="section gray margin-top-45 padding-top-65 padding-bottom-75">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				
				<!-- Section Headline -->
				<div class="section-headline margin-top-0 margin-bottom-35">
					<h3>Featured Jobs</h3>					
				</div>
				
				<!-- Jobs Container -->
				<div class="listings-container compact-list-layout margin-top-35">
					
					<!-- Job Listing -->
				 <?php if(!empty($featuredJob['result'])){
				 	foreach ($featuredJob['result'] as $key => $value) {
				 		  if(!empty($value['c_logo'])){
								$cLogo = $value['c_logo'];
							}else{
								$cLogo = 'company-logo.png';
							}
				 		?>    	
						<a href="<?= SITE_URL?>job-detail.php?job=<?= $value['encryptId']?>" class="job-listing with-apply-button">
							<!-- Job Listing Details -->
							<div class="job-listing-details">
								<!-- Logo -->
								<div class="job-listing-company-logo">
									<img src="<?= SITE_URL.'uploads/profile/'.$cLogo?>" alt="Company Logo">
								</div>
								<!-- Details -->
								<div class="job-listing-description">
									<h3 class="job-listing-title"><?= $value['title']?></h3>
									<!-- Job Listing Footer -->
									<div class="job-listing-footer">
										<ul>
										<!-- 	<li><i class="icon-material-outline-business"></i> Hexagon <div class="verified-badge" title="Verified Employer" data-tippy-placement="top"></div></li> -->
											<li><i class="icon-material-outline-location-on"></i> <?= $value['country']?></li>
											<li><i class="icon-material-outline-business-center"></i> <?= $value['job_type']?></li>
											<li><i class="icon-material-outline-access-time"></i> <?= $value['time_ago']['result']?></li>
										</ul>
									</div>
								</div>
								<!-- Apply Button -->
								<span class="list-apply-button ripple-effect">Apply Now</span>
							</div>
						</a>	
					<?php } } ?>
				</div>
				<!-- Jobs Container / End -->
			</div>
		</div>
	</div>
</div>
<!-- How it work -->
<div class="section padding-top-65 padding-bottom-65">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<!-- Section Headline -->
				<div class="section-headline centered margin-top-0 margin-bottom-5">
					<h3>How It Works?</h3>
				</div>
			</div>
			
			<div class="col-xl-4 col-md-4">
				<!-- Icon Box -->
				<div class="icon-box with-line">
					<!-- Icon -->
					<div class="icon-box-circle">
						<div class="icon-box-circle-inner">
							<i class="icon-line-awesome-lock"></i>
							<div class="icon-box-check"><i class="icon-material-outline-check"></i></div>
						</div>
					</div>
					<h3 class="padt_30"> Create an Account</h3>
					<p>Its as simple as selecting your service category, the subcategory, and price you wish to pay. Once your information is submitted, you will be able to browse the local service providers that best fit your needs.</p>
				</div>
			</div>
			<div class="col-xl-4 col-md-4">
				<!-- Icon Box -->
				<div class="icon-box with-line">
					<!-- Icon -->
					<div class="icon-box-circle">
						<div class="icon-box-circle-inner">
							<i class="icon-line-awesome-legal"></i>
							<div class="icon-box-check"><i class="icon-material-outline-check"></i></div>
						</div>
					</div>
					<h3 class="padt_30">Post a Job</h3>
					<p>Once you create your profile, you can start creating job postings.</p>
				</div>
			</div>
			<div class="col-xl-4 col-md-4">
				<!-- Icon Box -->
				<div class="icon-box">
					<!-- Icon -->
					<div class="icon-box-circle">
						<div class="icon-box-circle-inner">
							<i class=" icon-line-awesome-trophy"></i>
							<div class="icon-box-check"><i class="icon-material-outline-check"></i></div>
						</div>
					</div>
					<h3 class="padt_30">Choose an Expert</h3>
					<p>Once you select a service provider, you can message them directly to determine if they are able to meet your project expectations.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="section gray padding-top-65 padding-bottom-55">
	
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<!-- Section Headline -->
				<div class="section-headline centered margin-top-0 margin-bottom-5">
					<h3>Testimonials</h3>
				</div>
			</div>
		</div>
	</div>
	<!-- Categories Carousel -->
	<div class="fullwidth-carousel-container margin-top-20">
		<div class="testimonial-carousel testimonials">
			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial-avatar">
						<img src="<?= SITE_URL ?>assets/images/user-avatar-small-02.jpg" alt="">
					</div>
					<div class="testimonial-author">
						<h4>Sindy Forest</h4>
						 <span>Freelancer</span>
					</div>
					<div class="testimonial">Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</div>
				</div>
			</div>
			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial-avatar">
						<img src="<?= SITE_URL ?>assets/images/user-avatar-small-01.jpg" alt="">
					</div>
					<div class="testimonial-author">
						<h4>Tom Smith</h4>
						 <span>Freelancer</span>
					</div>
					<div class="testimonial">Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. Dynamically innovate resource-leveling customer service for state of the art.</div>
				</div>
			</div>
			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial-avatar">
						<img src="<?= SITE_URL ?>assets/images/user-avatar-placeholder.png" alt="">
					</div>
					<div class="testimonial-author">
						<h4>Sebastiano Piccio</h4>
						 <span>Employer</span>
					</div>
					<div class="testimonial">Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate one-to-one customer service with robust ideas. Dynamically innovate resource-leveling customer service for state of the art.</div>
				</div>
			</div>
			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial-avatar">
						<img src="<?= SITE_URL ?>assets/images/user-avatar-small-03.jpg" alt="">
					</div>
					<div class="testimonial-author">
						<h4>David Peterson</h4>
						 <span>Freelancer</span>
					</div>
					<div class="testimonial">Collaboratively administrate turnkey channels whereas virtual e-tailers. Objectively seize scalable metrics whereas proactive e-services. Seamlessly empower fully researched growth strategies and interoperable sources.</div>
				</div>
			</div>
			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial-avatar">
						<img src="<?= SITE_URL ?>assets/images/user-avatar-placeholder.png" alt="">
					</div>
					<div class="testimonial-author">
						<h4>Marcin Kowalski</h4>
						 <span>Freelancer</span>
					</div>
					<div class="testimonial">Efficiently unleash cross-media information without cross-media value. Quickly maximize timely deliverables for real-time schemas. Dramatically maintain clicks-and-mortar solutions without functional solutions.</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Categories Carousel / End -->
</div>
<!-- Footer
================================================== -->
<div id="footer">
	<!-- Footer Top Section -->
	<?php include_once('elements/footer.php'); ?>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</div>
<!-- Footer / End -->
</div>
<!-- Wrapper / End -->
<script>
function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else { 
    console.log("Geolocation is not supported by this browser.");
  }
}
function showPosition(position) {
	console.log(position);
	    $.ajax({
          url: "helpers/functions.php?type=c2hhcmVfYWRkcmVzcw==", 
          type:'POST',
          dataType: "json",
          data: {position},
          success: function(data) { 
          	console.log(data.result);
          	$('#postal_code').val(data.result);
          }
        });  
 // x.innerHTML = "Latitude: " + position.coords.latitude + 
  //"<br>Longitude: " + position.coords.longitude;
}
getLocation();
</script>
<script type="text/javascript">
	/************** Auto Suggestion Category *****************************/   
  
      
      $(function() {  
         $("#cat-suggestion").autocomplete({           
              source: function( request, response ) {
                $.ajax({
                  url: "helpers/functions.php?type=Y2F0ZWdvcmllc19zdWdnZXN0aW9u", 
                  dataType: "json",
                  data: {
                   q: request.term
                  },
                  success: function(data) { 
                  	 if(!data.result.length){
				        var result = [
				            {
				                label: 'No category found', 
				                value: response.term
				            }
				        ];
				        response(result);
				    } else {                
                    	response(data.result); 
                    } 
                  }
                });         
              },        
              minLength: 1,
              select: function( event, ui ){ 
                $("#cat-suggestion").val(ui.item.lable);
                $('#category-id').val(ui.item.id);
              }, 
          })
      }); 
</script>
</body>
</html>