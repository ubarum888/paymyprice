<?php  
include('helpers/classes/config.php'); 

 
?>
<!doctype html>
<html lang="en">
<head>

<!--  Essential META Tags -->
<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1">
<link rel="canonical" href="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">

<meta property="og:locale" content="en_US">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
<meta property="og:site_name" content="PayMyPrice">

<title>How It Works - PayMyPrice</title>
<meta property="og:title" content="How It Works - PayMyPrice">
<meta name="twitter:title" content="How It Works - PayMyPrice">

<meta name="description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">
<meta property="og:description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">
<meta name="twitter:description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">

<meta property="og:image" content="<?= SITE_URL ?>assets/images/how-it-works-header-image.jpg">
<meta property="og:image:secure_url" content="<?= SITE_URL ?>assets/images/how-it-works-header-image.jpg">
<meta name="twitter:image" content="<?= SITE_URL ?>assets/images/how-it-works-header-image.jpg">
<meta name="twitter:card" content="summary_large_image">
<meta property="og:image:width" content="400">
<meta property="og:image:height" content="50">

 	<?php include_once('elements/head.php');?>

</head>
<body>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->
<header id="header-container" class="fullwidth">
	<?php include_once('elements/header.php');?>	 

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->



<!-- Titlebar
================================================== -->
<div class="page-banner-2" style="background-image:url('<?= SITE_URL ?>assets/images/how-it-works-header-image.jpg')">

  <div id="titlebar-2" class="no-background">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>How It Works</h1>				
        </div>
      </div>
    </div>
  </div>


</div>
	 



<!-- Page Content
================================================== -->
<div class="container">
	<div class="row">
		
		<!-- Content -->
		<div class="col-xl-12 col-lg-12 content-right-offset margin-top-50 margin-bottom-50">

		<p><strong>Connect with Service Providers</strong></p>
		<p>The PayMyPrice.com community of service providers offers you, as the consumer, the opportunity to select a contractor or freelancer that provides the service you need, at the price point that best fits your budget.</p>
		<p>Its as simple as selecting your service category, the subcategory, and price you wish to pay. Once your information is submitted, you will be able to browse the local service providers that best fit your needs.</p>
		<p>Once you select a service provider, you can message them directly to determine if they are able to meet your project expectations. Secured payment can be made directly through the service providers’ job posting via our partnership with Stripe.</p>
		<p>Gone are the days of haggling price and scouring the internet for various service providers to perform a job. No more getting quotes and waiting weeks. Get the price upfront and schedule your project to be completed quickly and easily.</p>
		<p>Save money, get connected.</p>
		<br>
		<p><strong>Connect with Buyers as a Contractor</strong></p>
		<p>As a service provider on the PayMyPrice.com platform, you are able to create a profile by submitting the information you provide. Once you create your profile, you can create up to 5 job postings. Your profile will be viewable by buyers in your area instantly. Buyers are able to contact you directly via our embedded messaging platform, as well as pay for your service once a job is complete. Encrypted payments are made through our partnership with Stripe</p>
		<p>It’s a fantastic way to meet new customers in your local service area, that are browsing the internet looking for what you offer. The benefit of using PayMyPrice.com over other platforms is that you know the price the customer wants to pay, from day 1. No beating around the bush trying to determine that magical number of where you need to be. Save time, get connected.</p>
		</div>

	</div>
</div>


<!-- Footer
================================================== -->
<div id="footer">	 
 <?php include_once('elements/footer.php');?>	
</div>
<!-- Footer / End -->

</div>
<!-- Wrapper / End -->



</body>
</html>