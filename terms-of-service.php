<?php  
include('helpers/classes/config.php'); 

 
?>
<!doctype html>
<html lang="en">
<head>

<!--  Essential META Tags -->
<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1">
<link rel="canonical" href="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">

<meta property="og:locale" content="en_US">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
<meta property="og:site_name" content="PayMyPrice">

<title>Terms of Service - PayMyPrice</title>
<meta property="og:title" content="Terms of Service - PayMyPrice">
<meta name="twitter:title" content="Terms of Service - PayMyPrice">

<meta name="description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">
<meta property="og:description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">
<meta name="twitter:description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">

<meta property="og:image" content="<?= SITE_URL ?>assets/images/terms-of-service-header-image-2.jpg">
<meta property="og:image:secure_url" content="<?= SITE_URL ?>assets/images/terms-of-service-header-image-2.jpg">
<meta name="twitter:image" content="<?= SITE_URL ?>assets/images/terms-of-service-header-image-2.jpg">
<meta name="twitter:card" content="summary_large_image">
<meta property="og:image:width" content="400">
<meta property="og:image:height" content="50">

 	<?php include_once('elements/head.php');?>

</head>
<body>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->
<header id="header-container" class="fullwidth">
	<?php include_once('elements/header.php');?>	 

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->



<!-- Titlebar
================================================== -->
<div class="page-banner-2" style="background-image:url('<?= SITE_URL ?>assets/images/terms-of-service-header-image-2.jpg')">

  <div id="titlebar-2" class="no-background">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>Terms of Service</h1>				
        </div>
      </div>
    </div>
  </div>


</div>
	 



<!-- Page Content
================================================== -->
<div class="container">
	<div class="row">
		
		<!-- Content -->
		<div class="col-xl-12 col-lg-12 content-right-offset margin-bottom-50 margin-top-50 terms_of_service_main_content">

      <p class="text-center">PayMyPrice.com</p> 
      <p class="text-center">Terms of Service</p>
      
      <p class="text-justify">These Terms of Service and the policies referred to herein contain the agreement (“Agreement”) that governs your use of the Internet Website and software located at PayMyPrice.com (the “Website”) owned, operated, licensed, or controlled by Nationwide Automation, LLC, d/b/a “PayMyPrice” (“PayMyPrice”). Throughout the Website, the terms “we”, “us” and “our” refer to PayMyPrice. This Agreement applies to all users of the Website, including without limitation users who are browsers, vendors, customers, merchants, and/or contributors of content.</p> 
      
      <p class="text-justify">PLEASE READ THIS AGREEMENT CAREFULLY. THIS AGREEMENT GOVERNS YOUR USE OF THIS WEBSITE. IF YOU DO NOT AGREE TO THE TERMS OF THIS AGREEMENT OR ANY REVISED VERSION OF THIS AGREEMENT, PLEASE DO NOT USE THIS WEBSITE.</p>
      
      <p class="text-justify">BEFORE ACCEPTING THIS AGREEMENT, PLEASE BE ADVISED:</p>
       
        <ul clas="no-style">
          <li class="text-justify">A.	This Agreement contains a binding Arbitration provision (Section 5);</li>
          <li class="text-justify">B.	This Agreement contains a Class Action Waiver (Section 5.3); and</li>
          <li class="text-justify">C.	This Agreement limits our liability and your remedies (Section 16).</li>
        </ul>
      
      <p class="text-justify">1.	<u>Acceptance.</u> By accessing or using the Website, or registering an account, you agree to be bound by this Agreement.  PayMyPrice reserves the right, at its sole discretion, to change, modify, add, or remove portions of this Agreement at any time effective immediately upon notice. If you are a registered user, PayMyPrice will notify you of any changes at the email address you provided during registration. Notice shall also appear on the Website.  Please check this Agreement periodically for changes. Continued use of the Website following the posting of changes to this Agreement will mean you have read and accept such changes. You separately acknowledge, agree, represent and warrant that any information provided by you to PayMyPrice is true, accurate, correct, complete and that this representation is an essential and material provision of this Agreement. IF YOU DO NOT AGREE TO THESE TERMS, DO NOT REGISTER AND DO NOT USE THE WEBSITE.</p>
      
      <p class="text-justify">2.	<u>Age Requirement.</u> To register for or use the Website, the software, or services provided by PayMyPrice, or on your own behalf, you must be at least 18 years of age. If you are under 18 years of age, you may not register for the service, nor may parents or legal guardians register on your behalf. If you are a parent or legal guardian entering this Agreement for the benefit of your child or a child in your legal care, be aware that you are fully responsible for the child’s use of the Service, including all financial charges and legal liability that he or she may incur.</p>
      
      <p class="text-justify">3.	<u>PayMyPrice Services.</u> PayMyPrice provides a web-based platform that enables buyers to set a price for requested services (“Budget”) to facilitate an instant connection to a local service provider that meets the Budget, and proprietary software tools and resources (“Software”) and Content (defined below). PayMyPrice provides software and information only. PayMyPrice does not manufacture any products, and does not fulfill any orders for physical goods or requests for services of any kind.  </p>
      
      <p class="text-justify text-indent-1">3.1	<u>Member Accounts, Passwords & Security.</u> Visitors may browse the contents of the Website. Registered users will receive a password and account designation upon completing the registration process.  If you register to create an account (“Account”), you are responsible for maintaining the confidentiality of the password and Account, and are fully responsible for all activities that occur using such password or Account. You agree to (a) immediately notify PayMyPrice of any unauthorized use of your password or account or any other breach of security of which you become aware, and (b) ensure that you exit from your account at the end of each session. PayMyPrice reserves the right to terminate your Account, access to the Web Site, and access to the Content if any registration information provided is false, inaccurate or incomplete. Sellers are subject to the additional Seller Policies in Appendix 1. Buyers are subject to the additional Buyer Policies in Appendix 2.</p>
      
        <p class="text-justify text-indent-1">3.2	<u>Payment.</u> PayMyPrice accepts Visa, Mastercard, American Express, Discover, JCB, Diners Club, Shop Pay, Google Pay and Apple Pay.  All purchases shall be paid in full at the time of ordering.  You warrant that you have the authority to use the payment information you are providing to PayMyPrice and you acknowledge and agree that you authorize PayMyPrice to charge you for your order. You acknowledge and agree that you shall reimburse PayMyPrice for all costs and attorney’s fees incurred by PayMyPrice in any action or proceeding to collect any payment from you under any theory of liability.</p>
      
     <p class="text-justify"> 4. 	<u>Informal Dispute Resolution.</u>  If you have a question, concern or grievance regarding your Account or the use of the service, we ask that you provide PayMyPrice with an opportunity to address your concerns prior to starting formal legal action. Before filing a claim against PayMyPrice, you agree to try to resolve the dispute informally by submitting your request to PayMyPrice in writing. If a dispute is not addressed within one hundred twenty (120) days of the date of your notice of dispute, you or PayMyPrice may commence a formal proceeding.</p>
      
      <p class="text-justify">5. 	<u>BINDING ARBITRATION; CLASS ACTION WAIVER</u></p>
      
      <p class="text-justify text-indent-1">5.1 	You and PayMyPrice agree to arbitrate any and all disputes, claims, or controversies arising out of, in connection with, or relating to this Agreement, use of the Website, and our relationship with you, including any claims that may arise after the termination of this Agreement. This agreement to arbitrate includes any claims against our employees, agents, or any subsidiaries of PayMyPrice. Arbitration is a method of claim resolution that is less formal than a traditional court proceeding in state or federal court. It uses a neutral arbitrator instead of a judge or jury and the arbitrator’s decision is subject to limited review by courts.</p>
      
      <p class="text-justify text-indent-1">5.2 	All disputes concerning the arbitrability of a claim (including disputes about the scope, interpretation, breach, applicability, enforceability, revocability or validity of this Agreement) shall be decided by the arbitrator. The arbitrator shall also decide whether any claim is subject to arbitration. You further agree that the Commercial Arbitration Rules and Mediation Procedures of the American Arbitration Association (AAA) shall govern the interpretation and enforcement of this agreement to arbitrate. The AAA Commercial Arbitration Rules are available online at:</p>
       
      <a class="text-indent-1 text-break" href="https://www.adr.org/sites/default/files/CommercialRules_Web_FINAL_1.pdf" target="_blank">https://www.adr.org/sites/default/files/CommercialRules_Web_FINAL_1.pdf. </a>
      <br>
      
      <p class="text-indent-1 text-justify">This Agreement shall be governed by and construed in accordance with the laws of the State of Illinois. The arbitrator is bound by the terms of this Agreement. The exclusive venue for any dispute or issue arising out of this Agreement shall be held in Chicago, Illinois.</p>
      
      <p class="text-indent-1 text-justify">5.3 	<strong>CLASS ACTION WAIVER: YOU AND PAYMYPRICE ALSO AGREE THAT EACH IS GIVING UP THE RIGHT TO A JURY TRIAL AND THAT EACH MAY BRING CLAIMS RELATED TO THIS AGREEMENT AGAINST THE OTHER ONLY IN YOUR OR ITS INDIVIDUAL CAPACITIES, AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY PURPORTED CLASS ACTION LAWSUIT OR REPRESENTATIVE PROCEEDING, CONSOLIDATED ACTION, OR PRIVATE ATTORNEY GENERAL ACTION.</strong> This means that neither you nor PayMyPrice can seek to assert class or representative claims against each other either in court or in arbitration and no relief can be awarded on a class or representative basis for claims related to this Agreement. The arbitrator also may not consolidate or join another person’s claim with your claim or issue an order that would achieve the same result. You and the PayMyPrice further agree that if the provisions of this paragraph, known as the “Class Action Waiver,” are found to be unenforceable, it cannot be severed from this arbitration agreement and the entire provision compelling arbitration shall be null and void.</p>
      
      <p class="text-justify text-indent-1">5.4	You agree that if PayMyPrice makes any future, material change to this arbitration provision, you may reject any change by sending written notice within thirty (30) calendar days of the date of the change to PayMyPrice LLC, 275 Payson Street, Hoffman Estates IL 60169 Attn: Legal. Your decision to reject changes in a new arbitration provision, however, does not affect any prior arbitration provisions to which you have already agreed, which remain in effect.  Your decision to reject changes shall result in the termination of your Account effective as of the date of such notice.</p>
      
      <p class="text-justift ind-1">5.5	Judgment upon the arbitration award may be entered in a court having jurisdiction, or application may be made to such court for judicial acceptance of any award and an order of enforcement, as the case may be.</p>
      
      <p class="text-justify">6.	<strong><u>License & Access.</u></strong> Subject to the terms and conditions of this Agreement, PayMyPrice grants you a revocable, limited, non-exclusive, non-assignable, non-sublicensable right and license during the Term to access the Website, Software and Content (collectively, the “Service”).  </p>
      
      <p class="text-justify ind-1">6.1	<u>License Restrictions.</u> Your right to access the Service is subject to compliance with this Agreement. This license does not include: (a) any resale or third-party commercial use of the Service or its contents; (b) any derivative use of the Service or its contents; (c) any downloading or copying of account information for the benefit of yourself, or any other individual or business; or (d) any use of data mining, robots, or similar automated data gathering and extraction tools. For avoidance of doubt, you may not share with any third-party any information accessed or retrieved form the Service. You may not frame or utilize framing techniques to enclose any trademark, logo, or other proprietary information (including images, text, page layout, or form) of the Content, the Website, PayMyPrice or our affiliates without express written consent. You may not use any meta tags or any other “hidden text” utilizing Content or the PayMyPrice name or trademarks without the express written consent of PayMyPrice. You may not use any PayMyPrice logo or other proprietary graphic or trademark without express written permission. Any unauthorized use terminates the permission or license granted by PayMyPrice. </p>
      
      <p class="text-justify ind-1">6.2 	<u>Content.</u> In this Agreement, all content, music, audio, video, audio-visual, text, graphics, artwork, images, photographs, animations, data, information, software, designs and other materials which You may encounter while using the Service shall individually and collectively be referred to as “Content.” PayMyPrice shall own all legally-protectable elements of the Content and Website including, without limitation, the selection, sequence, “look and feel” and arrangement of items, as well as all copyrights, trademarks, service marks, brand and trade names and our services, tangible or intangible, created or used by PayMyPrice.</p>
      
        <p class="text-justify ind-1">6.3 	<u>Access.</u> The words “use” or “using” or “consumer” or “consuming” in this Agreement, means any time any individual, including you, directly or indirectly, with or without the aid of a machine, automated or other device, does or attempts to access, interact with, use, display, view, print or copy or use any portion, feature, function or aspect of the Content, transmit, receive or exchange data or communicate with the Content, or in any way utilizes, benefits, takes advantage of or interacts with any function, service or feature of the Content, for any purpose whatsoever. This Agreement does not cover your rights or responsibilities with respect to third party content or web sites or any links that may direct your browser or connection to third party web sites or pages.</p>
      
        <p class="text-justify ind-1">6.4 	<u>System Requirements.</u> Use of the Website, requires Internet access (fees may apply), software (fees may apply), and may require obtaining updates or upgrades from time to time. Therefore, your ability to use the Website may be affected by these factors. You agree that such system requirements are exclusively your responsibility, that use of the Website and Content may require the use of other hardware and software products, and that such hardware and software is your responsibility. </p>
      
        <p class="text-justify ind-1">6.5 	<u>User-Provided Content (“UGC”).</u> UGC shall include anything within the definition of Content above that is uploaded to the Website or submitted to PayMyPrice. By sharing UGC, You automatically grant (or warrants that the owner of such rights has expressly granted) PayMyPrice a perpetual, royalty-free, irrevocable, non-exclusive right and license to use, reproduce, license, sub-license, modify, adapt, publish, translate, create derivative works from and distribute such materials or incorporate such materials into any form, medium, or technology now known or later developed throughout the universe, provided however, that such license shall not grant to PayMyPrice any proprietary rights in your original works of authorship. You hereby waive any so-called “moral rights” in such UGC.</p>
      
     <p class="text-justify ind-1"> 6.6	<u>Representations.</u> You hereby promise that: (a) no part of this Agreement is or will be inconsistent with any obligation you may have to others; (b) you have the full right and authority to provide the assignments and rights granted to PayMyPrice herein; (c) you have the full legal authority to share UGC, and (d) that any UGC you share does not and shall not infringe on any copyright, trademark, service mark, trade name, patent, trade secret or other intellectual property or proprietary right or right of publicity or privacy of, or libel, slander, defame or disparage, any third party (“Third Party Rights”).</p>
      
      <p class="text-justify">7.	<strong><u>Restrictions on Use.</u></strong> Unless otherwise noted, all Content and other materials that are part of any PayMyPrice Website are protected by copyright, trademark, trade dress, and industrial design rights, and/or other intellectual property rights owned, controlled or licensed by PayMyPrice. No Content from the Website or any other PayMyPrice property may be copied, reproduced, republished, uploaded, posted, transmitted, or distributed in any way, except with express written consent of PayMyPrice. In addition, You agree that you will not, and will not permit others to:</p>
      
      
        <p class="ind-1 text-justify">7.1	damage, interfere with, or unreasonably overload, the Website;</p>
        <p class="ind-1 text-justify">7.2	introduce into the Website any code intended to disrupt operations;</p>
        <p class="ind-1 text-justify">7.3	alter or delete any information, data, text, links, images, software, chat, communications and other content available through the Website; </p>
        <p class="ind-1 text-justify">7.4	access the Website by expert system, electronic agent, “bot” or other automated means;</p>
        <p class="ind-1 text-justify">7.5	use scripts or disguised redirects to derive financial benefit from PayMyPrice; </p>
        <p class="ind-1 text-justify">7.6	modify, reverse engineer, reverse assemble, decompile, copy or otherwise derive the source code of any Website for any reason; </p>
        <p class="ind-1 text-justify">7.7	rent, sell or sublicense any portion of the Website; </p>
        <p class="ind-1 text-justify">7.8	provide any unauthorized third party with access to the Website; </p>
        <p class="ind-1 text-justify">7.9	access or attempt to access confidential Content through the Website; </p>
        <p class="ind-1 text-justify">7.10	interfere with the operation of the Website, including, but not limited to, distribution of unsolicited advertising or mail messages and propagation of computer worms and viruses; </p>
        <p class="ind-1 text-justify">7.11	post advertising messages or solicitations, URLs containing a referral code or referral address, or links to businesses or pages with advertising, including “blind” or “hidden” referral links; </p>
        <p class="ind-1 text-justify">7.12	post any material in any form whatsoever on the Website that is defamatory, obscene or otherwise unlawful or violates any third party’s right of privacy or publicity; </p>
        <p class="ind-1 text-justify">7.13	infringe any third party’s patent, copyright, service mark, trademark or other intellectual property right of any kind or misappropriate the trade secrets of any third party in connection with your use of the Website; </p>
        <p class="ind-1 text-justify">7.14	engage in any activity that does not comply with applicable law and regulations or otherwise engage in any illegal, manipulative or misleading activity through the use of the Website; </p>
        <p class="ind-1 text-justify">7.15	use the manual or automated software, devices or other processes to “scrape,” “crawl,” “spider” or index any page of Content from the Website; or</p>
        <p class="ind-1 text-justify">7.16	collect, store, use or disseminate personal data or information about other users in any manner whatsoever, including but not limited to, solicitation of products or services by mail, telephone or e-mail (e.g. e-mail addresses).</p>
      
      
      <p class="text-justify">8.	<strong><u>Violations.</u></strong> In addition to any and all remedies at law or in equity, any intentional violation of this Agreement shall give PayMyPrice the right to immediately suspend or cancel your access to the Website, and any order in process, without further liability.  	</p>
      
      <p class="text-justify">9. 	<strong><u>Online Store Terms.</u></strong>	PayMyPrice reserves the right to refuse service or any order to anyone for any reason at any time. </p>
      
      <p class="text-justify ind-1">9.1	PayMyPrice is not responsible for information made available on this Website and makes no warranty that it is accurate, complete or current. The information on this Website is provided for general information only and should not be relied upon or used as the sole basis for making decisions without consulting primary, more accurate, more complete or more timely sources of information. </p>
      
      <p class="text-justify ind-1">9.2	PayMyPrice shall not be liable to you or to any third-party for any modification, price change, suspension or discontinuance of the Service.</p>
      
      <p class="text-justify ind-1">9.3	 PayMyPrice does not warrant that the quality of any products, services, information, or other material purchased or obtained by you will meet your expectations, or that any errors in the Website will be corrected.</p>
      
      <p class="text-justify">10.	<strong><u>User Comments, Feedback and Submissions.</u></strong> If you send certain specific submissions (for example contest entries), or with or without a request from PayMyPrice, if you send creative ideas, suggestions, proposals, plans, or other materials, whether online, by email, by postal mail, or otherwise (collectively, 'comments'), you agree that PayMyPrice may, at any time, without restriction, edit, copy, publish, distribute, translate and otherwise use in any medium any comments that you forward to PayMyPrice. PayMyPrice shall have no obligation to: (a) maintain any comments in confidence; (b) pay compensation for any comments; or (c) respond to any comments. PayMyPrice may, but has no obligation to, monitor, edit or remove content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party’s intellectual property or this Agreement. You may not use a false e mail address, pretend to be someone other than yourself, or otherwise mislead us or third-parties as to the origin of any comments. You are solely responsible for any comments you make and their accuracy. PayMyPrice shall have no responsibility and no liability for any comments posted by you or any third-party.</p>
      
     <p class="text-justify"> 11.	<strong><u>Privacy.</u></strong> It is our policy to respect the privacy of PayMyPrice users. Therefore, in addition to the privacy of registration data, PayMyPrice shall not monitor, edit, or disclose the contents of a guest's e-mail unless required in the course of normal maintenance of PayMyPrice and its systems or unless required to do so by law or in the good-faith belief that such action is necessary to: (1) comply with the law or comply with legal process served on PayMyPrice; (2) protect and defend the rights or property of PayMyPrice; or (3) act in an emergency to protect the personal safety of our users or the public. You shall remain solely responsible for the content of your messages.  Our Privacy Policy can be found here.</p>
      
      <p class="text-justify">12.	<strong><u>Indemnity.</u></strong> You shall indemnify, defend and hold harmless PayMyPrice, its directors, officers, employees, agents and contractors from and against any and all liabilities, claims, costs, assessments, fees or expenses of any kind, including without limitation defense costs and reasonable attorneys’ fees arising from or related in any way to (a) any violation of this Agreement, or (b) any action or claim by third parties alleging a violation of Third Party Rights, provided PayMyPrice promptly notifies you in writing of any claim, and allows you to control the conduct of any related defense or settlement negotiations.  Notwithstanding the foregoing, you shall not settle a third party claim without the prior written consent of PayMyPrice if such settlement shall require action or payment by PayMyPrice.</p>
      
       <p clas="text-justify">13.	<strong><u>Termination. </u></strong>This Agreement is effective on the earlier of the date you click “I Accept” or the first date you access Content from the Website (the “Effective Date”) and shall continue in effect until terminated by either party. You may terminate this Agreement at any time by destroying all materials obtained from PayMyPrice and all related documentation and all copies and installations thereof, whether made under this Agreement or otherwise. Any rights to access and use the Services under this Agreement shall terminate immediately without notice from PayMyPrice if in PayMyPrice's sole discretion You fail to comply with any term or provision of this Agreement. </p>
      
     <p class="text-justify"> PayMyPrice may, at its sole discretion and at any time, discontinue providing access to, alter or replace the Content in the Website, or any part thereof, with or without notice. You agree that any termination of your access to the Website under any provision of this Agreement may occur without prior notice. Further, you agree that PayMyPrice shall not be liable to you or to any third party for any termination of your access to the Website.</p>
      
      <p class="text-justify">14.	<strong><u>Jurisdiction.</u></strong> Unless otherwise specified, the Website and Content are available solely for the purpose of promoting products and/or services available in the United States, its territories, possessions, and protectorates. The Website is controlled and operated by PayMyPrice from its offices within the State of Illinois, United States of America. PayMyPrice makes no representation that materials in the Website or products purchased through the Website are appropriate or available for use in other locations. Those who choose to access this Website from other locations do so on their own initiative and are responsible for compliance with local laws, if and to the extent local laws are applicable.</p>
      
      <p class="text-justify"><strong>15.	<u>DISCLAIMER.</u> THE CONTENT AND THE WEBSITE ARE PROVIDED “AS IS” AND WITHOUT WARRANTIES OF ANY KIND EITHER EXPRESS OR IMPLIED. TO THE FULLEST EXTENT PERMISSIBLE PURSUANT TO APPLICABLE LAW, PAYMYPRICE DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.</strong></p>  
      
      <p class="text-justify"><strong>PAYMYPRICE DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THE WEBSITE OR CONTENT SHALL BE UNINTERRUPTED OR ERROR-FREE, THAT DEFECTS WILL BE CORRECTED, OR THAT THE WEBSITE OR ANY OTHER PAYMYPRICE WEBSITE OR THE SERVER(S) THAT MAKES THE WEBSITE OR CONTENT AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. PAYMYPRICE DOES NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE USE OF THE CONTENT OR THE WEBSITE IN TERMS OF THEIR CORRECTNESS, ACCURACY, RELIABILITY, OR OTHERWISE.</strong></p>
      
      <p class="text-justify"><strong>PAYMYPRICE IS NOT RESPONSIBLE FOR THE CONDUCT, WHETHER ONLINE OR OFFLINE, OF ANY MERCHANT OR USER OF THE WEBSITE. PAYMYPRICE ASSUMES NO RESPONSIBILITY AND SHALL NOT BE LIABLE FOR ANY ERROR, OMISSION, INTERRUPTION, DELETION, DEFECT, DELAY IN OPERATION OR TRANSMISSION, COMMUNICATIONS LINE FAILURE, THEFT OR DESTRUCTION OR UNAUTHORIZED ACCESS TO, OR ALTERATION OF, THE WEBSITE OR THE CONTENT. PAYMYPRICE IS NOT RESPONSIBLE FOR ANY PROBLEMS OR TECHNICAL MALFUNCTION OF ANY TELEPHONE NETWORK OR LINES, COMPUTER ONLINE SYSTEMS, SERVERS OR PROVIDERS, COMPUTER EQUIPMENT OR SOFTWARE, OR THE FAILURE OF EMAIL ON ACCOUNT OF TECHNICAL PROBLEMS OR TRAFFIC CONGESTION ON THE INTERNET OR AT ANY WEBSITE, INCLUDING INJURY OR DAMAGE TO ANY PERSON'S COMPUTER RELATED TO OR RESULTING FROM PARTICIPATING OR DOWNLOADING CONTENT IN CONNECTION WITH THIS WEBSITE.</strong></p>
      
      <p class="text-justify"><strong>16.	<u>LIMITATION OF LIABILITY.</u> IN NO EVENT SHALL PAYMYPRICE, ITS DIRECTORS, OFFICERS, EMPLOYEES, AFFILIATES, AGENTS, CONTRACTORS, INTERNS, SUPPLIERS, SERVICE PROVIDERS OR LICENSORS BE LIABLE FOR ANY INJURY, LOSS, CLAIM, DAMAGE OR ANY INDIRECT, SPECIAL, EXEMPLARY, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, WHETHER BASED IN CONTRACT, TORT OR OTHERWISE, WHICH ARISES OUT OF OR IS ANY WAY CONNECTED WITH (I) ANY USE OF THE SERVICES, THE PAYMYPRICE WEBSITE OR CONTENT, OR ANY GOODS OR SERIVCES PROCURED IN CONNECITON WITH THE WEBSITE, OR (II) ANY FAILURE OR DELAY (INCLUDING, BUT NOT LIMITED TO, THE USE OR INABILITY TO USE ANY COMPONENT OF THE SERVICES OR THE PAYMYPRICE WEBSITE), EVEN IF ADVISED OF THEIR POSSIBILITY. BECAUSE SOME STATES OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR THE LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, IN SUCH STATES OR JURISDICTIONS, OUR LIABILITY SHALL BE LIMITED TO THE MAXIMUM EXTENT PERMITTED BY LAW.</strong></p>
      
      <p class="text-justify"><strong>IN ADDITION, YOU SPECIFICALLY UNDERSTAND AND AGREE THAT ANY THIRD PARTY DIRECTING USERS TO THE PAYMYPRICE WEBSITE BY REFERRAL, LINK OR ANY OTHER MEANS IS NOT LIABLE TO USER FOR ANY REASON WHATSOEVER, INCLUDING BUT NOT LIMITED TO DAMAGES OR LOSS ASSOCIATED WITH THE USE OF THE SERVICES OR THE PAYMYPRICE WEBSITE.</strong></p>
      
      <p class="text-justify">17.	<strong><u>Links to Outside Websites and Services.</u></strong> To the extent that the Website contains links to outside services and resources, PayMyPrice does not control the availability and content of those outside services and resources. Any concerns regarding any such service or resource, or any link thereto, should be directed to the particular service or resource.</p>
      
      <p class="text-justify">18.	<strong><u>Miscellaneous.</u></strong>	This Agreement shall be governed by and construed in accordance with the laws of the State of Illinois and you agree that this Agreement shall be deemed to have been entered into and performed exclusively in the State of Illinois. You agree that any dispute shall be conducted in the State of Illinois in connection with any action at law or in equity arising out of or relating to this Agreement.  You hereby consent and submit to the personal jurisdiction of such courts for the purposes of adjudicating any such action. If any provision of this Agreement shall be unlawful, void, or for any reason unenforceable, then that provision shall be deemed severable from this Agreement and shall not affect the validity and enforceability of any remaining provisions.</p>
      
      <p class="text-center"><strong><u>APPENDIX 1.</u></strong></p>
      
      <p class="text-center"><strong><u>Seller Policies</u></strong></p>
      
      <p class="text-justify">This Policy is part of the Terms of Service.</p>
      
      <p class="ind-1 text-justify">1.	Accuracy. Seller is solely responsible for information made available on the Website including any listing that is not accurate, complete or current.</p>
      
      <p class="ind-1 text-justify">2.	Prohibited Items. Seller shall not sell or list for sale prohibited items, services, and items that violate our intellectual property policies.</p>
      
      <p class="ind-1 text-justify">3.	<u>Violations.</u> We may remove any listings that violate our policies. Listing fees are non-refundable. We may also suspend or terminate your account for any violations. You remain liable for paying any outstanding fees on your account. If PayMyPrice has reason to believe you, Your Content, or your use of the Services violate our Terms, including this Seller Policy, we may deactivate Your Content or suspend or terminate your account (and any accounts PayMyPrice determines is related to your account) and your access to the Services. Generally, PayMyPrice will notify you that Your Content or account has been suspended or terminated, unless you’ve repeatedly violated our Terms or we have legal or regulatory reasons preventing us from notifying you.</p>
      
      <p class="ind-1 text-justify">4.	<u>Communications.</u> Communications may not be used for the following activities:</p>
      
      <ul class="ind-1 ind-2 text-justify">
        <li>Sending unsolicited advertising or promotions, requests for donations or spam.</li>
        <li>Harassing or abusing another member or violating our Anti-Discrimination Policy.</li>
        <li>Contacting someone after they have explicitly asked you to stop or otherwise revoked previous consent.</li>
        <li>Interfering with a transaction or the business of another user.</li>
        <li>Contacting another user to warn them away from a particular member, shop, or item.</li>
        <li>Posting grievances or dispute in public areas of the Website.</li>
        <li>Purchasing from another seller for the sole purpose of leaving a negative review.</li>
        <li>Maliciously clicking on a competitor’s ads.</li>
      </ul>
      
      <p class="text-justify ind-1">5.	<u>Cancellations, Returns, and Exchanges.</u> Please be aware that in addition to this policy, other jurisdictions may have their own laws surrounding delivery, cancellations, returns and exchanges. You are responsible for complying with the laws of your country and those of your buyers.</p>
      
      <p class="ind-1 text-justify">Cancellations</p>
      
      <p class="text-justify ind-1">If you are unable to complete a transaction, you must notify the buyer and cancel the transaction. If the buyer already submitted payment, you must issue a full refund. You are encouraged to keep proof of any refunds in the event a dispute arises. All cancellations are subject to our Cancellation Policy.</p>
      
      <p class="text-justify ind-1">Disputes	</p>
      
      <p class="text-justify ind-1">If PayMyPrice is contact by a buyer claiming “non-delivery” or “item-not-as-described,” you must respond to any inquiry form PayMyPrice within three (3) days of the date PayMyPrice notifies you of a claim.</p>
      
      <p class="text-justify ind-1">Non-Delivery</p>
      
      <p class="text-justify ind-1">A non-delivery occurs when a buyer places an order but does not receive the item. The following are examples of non-delivery cases:</p>
      
      <p class="text-justify ind-1">There is no proof that the item was dispatched to the buyer.<br>
      An item was not sent to the address provided on PayMyPrice.</p>
      
      <p class="text-justify ind-1">Not as Described</p>
      
      <p class="text-justify ind-1">An item is not as described if the buyer can demonstrate that it is significantly different from your listing description or your photos. The following are examples of not as described cases:</p>
      
      <p class="text-justify ind-1">The item received is a different color, model, version, or size.<br>
      The item has a different design or material.<br>
      The seller failed to disclose that an item is damaged or is missing parts.<br>
      The buyer received the incorrect quantity of items.<br>
      The item was advertised as authentic but is not authentic.<br>
      The condition of the item is misrepresented (new v. used).</p>
      
      <p class="text-justify ind-1">6.	We may provide you with access to third-party tools over which we neither monitor nor have any control nor input. You acknowledge and agree that we provide access to such tools ”as is” and “as available” without any warranties, representations or conditions of any kind and without any endorsement. We shall have no liability whatsoever arising from or relating to your use of optional third-party tools. 
      Any use by you of optional tools offered through the Website is entirely at your own risk and discretion and you should ensure that you are familiar with and approve of the terms on which tools are provided by the relevant third-party provider(s). We may also, in the future, offer new services and/or features through the Website (including, the release of new tools and resources). Such new features and/or services shall also be subject to this Agreement.</p>
      
      <p class="text-center"><strong><u>APPENDIX 2</u></strong></p>
      <p class="text-center"><strong><u>Buyer Policies</u></strong></p>
      
      
      <p class="text-justify">This Policy is part of the Terms of Service.</p>
    
      
      <p class="text-justify ind-1">1.	Seller is solely responsible for information made available on the Website. Any reliance on the information on this Website is at your own risk.</p> 
      
      <p class="text-justify ind-1">2.	PayMyPrice shall not be liable to you or to any third-party for any modification, price change, suspension or discontinuance of the Service.</p>
      
      <p class="text-justify ind-1">3.	<u>Violations.</u> We may remove accounts that violate our policies. You remain liable for paying any outstanding fees on your account. If PayMyPrice has reason to believe you, Your Content, or your use of the Services violate our Terms, including this Buyer Policy, we may deactivate, suspend or terminate your account (and any accounts PayMyPrice determines is related to your account) and your access to the Services. Generally, PayMyPrice will notify you that Your Content or account has been suspended or terminated, unless you’ve repeatedly violated our Terms or we have legal or regulatory reasons preventing us from notifying you.</p>
      
      <p class="text-justify ind-1">4.	<u>Communications.</u> Communications may not be used for the following activities:</p>
      
      <ul class="ind-2">
        <li class="text-justify">Sending unsolicited advertising or promotions, requests for donations or spam.</li>
        <li class="text-justify">Harassing or abusing another member or violating our Anti-Discrimination Policy.</li>
        <li class="text-justify">Contacting someone after they have explicitly asked you to stop or otherwise revoked previous consent.</li>
        <li class="text-justify">Interfering with a transaction or the business of another user.</li>
        <li class="text-justify">Contacting another user to warn them away from a particular member, shop, or item.</li>
        <li class="text-justify">Posting grievances or dispute in public areas of the Website.</li>
        <li class="text-justify">Purchasing from another seller for the sole purpose of leaving a negative review.</li>
        <li class="text-justify">Maliciously clicking on a competitor’s ads.</li>
      </ul>
      
      
      
		</div>

	</div>
</div>


<!-- Footer
================================================== -->
<div id="footer">	 
 <?php include_once('elements/footer.php');?>	
</div>
<!-- Footer / End -->

</div>
<!-- Wrapper / End -->



</body>
</html>