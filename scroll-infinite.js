
var scrollInfinite = {};

scrollInfinite['limit'] = 8;
scrollInfinite['start'] = 0;
scrollInfinite['scroll'] = $("#load_data").data('name');
scrollInfinite['category_id'] = $('#categoryId_list').val();
scrollInfinite['user_id'] = $('#user_id').val();
scrollInfinite['post_id'] = $('#post_id').val();  

  /*--------------------------------------------------*/
  /*  Star Rating
  /*--------------------------------------------------*/
  function starRating(ratingElem) {

    $(ratingElem).each(function() {

      var dataRating = $(this).attr('data-rating');
      //console.log(dataRating);

      // Rating Stars Output
      function starsOutput(firstStar, secondStar, thirdStar, fourthStar, fifthStar) {
        return(''+
          '<span class="'+firstStar+'"></span>'+
          '<span class="'+secondStar+'"></span>'+
          '<span class="'+thirdStar+'"></span>'+
          '<span class="'+fourthStar+'"></span>'+
          '<span class="'+fifthStar+'"></span>');
      }

      var fiveStars = starsOutput('star','star','star','star','star');

      var fourHalfStars = starsOutput('star','star','star','star','star half');
      var fourStars = starsOutput('star','star','star','star','star empty');

      var threeHalfStars = starsOutput('star','star','star','star half','star empty');
      var threeStars = starsOutput('star','star','star','star empty','star empty');

      var twoHalfStars = starsOutput('star','star','star half','star empty','star empty');
      var twoStars = starsOutput('star','star','star empty','star empty','star empty');

      var oneHalfStar = starsOutput('star','star half','star empty','star empty','star empty');
      var oneStar = starsOutput('star','star empty','star empty','star empty','star empty');

      var halfStar = starsOutput('star half','star empty','star empty','star empty','star empty');
      var noStar = starsOutput('star empty','star empty','star empty','star empty','star empty');

      // Rules
          if (dataRating >= 4.75) {
              $(this).append(fiveStars);
          } else if (dataRating >= 4.25) {
              $(this).append(fourHalfStars);
          } else if (dataRating >= 3.75) {
              $(this).append(fourStars);
          } else if (dataRating >= 3.25) {
              $(this).append(threeHalfStars);
          } else if (dataRating >= 2.75) {
              $(this).append(threeStars);
          } else if (dataRating >= 2.25) {
              $(this).append(twoHalfStars);
          } else if (dataRating >= 1.75) {
              $(this).append(twoStars);
          } else if (dataRating >= 1.25) {
              $(this).append(oneHalfStar);
          } else if (dataRating >= 0.75) {
              $(this).append(oneStar);
          }else if (dataRating >= 0.25) {
              $(this).append(halfStar);
          }else{
              $(this).append(noStar);
          }

    });

  } 


 function load_infinite_data(scrollWith, filters = null, sort = null) 
 {   
  //console.log(filters);
  // console.log(scrollWith);
  
  //var jsonString = filters.serializeArray();
    //console.log(jsonString);
  $.ajax({
   url:SITE_URL+'helpers/functions.php?type=aW5maW5pdGVfc2Nyb2xs',  
   method:"POST",
   data:{'scroll_with':scrollWith, 'filter':filters, 'sort_by':sort},

   cache:false,
   success:function(data)
   {

    var obj = $.parseJSON(data);
        $('#show_query').text(obj.query);
   // console.log(obj.data);
    if(obj.data.length > 0){      

        if(obj.content == 'job-listing') {
          $.each(obj.data, function( index, value ) {
            myJobListingContent(value);  
          }); 
        }else if(obj.content == 'my-posts') {
            $.each(obj.data, function( index, value ) {
                myPostsContent(value);  
            }); 

        }else if(obj.content == 'my-jobs') {
            $.each(obj.data, function( index, value ) {
                myJobsContent(value);      
            }); 

        }else if(obj.content == 'freelancer-listing') {
            $.each(obj.data, function( index, value ) {
                freelancerListing(value); 
            }); 
        }else if(obj.content == 'freelancer-activity') {
            $.each(obj.data, function( index, value ) {
                freelancerActivityContent(value);         

            }); 
        }else if(obj.content == 'my-notes') {
            $.each(obj.data, function( index, value ) {
                myNotes(value);         

            }); 
            popupManage();
        }else if(obj.content == 'rate-from-user') {
            $.each(obj.data, function( index, value ) {
                rateFromUser(value); 
            }); 
        } else if(obj.content == 'freelancer-bid') {
            $.each(obj.data, function( index, value ) {
                freelancerBidContent(value); 
            }); 
        }   

        hideExtraContent();  
        starRating('#load_data .star-rating'); 
        tippy('[data-tippy-placement]'); 
        
        $('.popup-with-zoom-anim-ajax').magnificPopup({
            type: 'ajax',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeOnBgClick:false,
            closeBtnInside: true,
            showCloseBtn:true,
            preloader: true,
            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in',
            ajax: {
              settings: null, // Ajax settings object that will extend default one - http://api.jquery.com/jQuery.ajax/#jQuery-ajax-settings
              // For example:
              // settings: {cache:false, async:false}

              cursor: 'mfp-ajax-cur', // CSS class that will be added to body during the loading (adds "progress" cursor)
              tError: '<a href="%url%">The content</a> could not be loaded.' //  Error message, can contain %curr% and %total% tags if gallery is enabled
            },          
        });  
      }  
      if(obj.data == '' || obj.data == null) {
        $('#load_data_message').html("<button type='button' class='btn head-btn1'>No Data Found</button>");
      } else{
        //console.log(obj.count);
        if(obj.count > 7){
            $('#load_data_message').html("<button type='button' class='button dark ripple-effect load_more mb-20'><i class='icon-feather-rotate-ccw'></i> Load More</button>"); 
            $('#load_data_message .load_more').click(function(){
                scrollInfinite['start'] = scrollInfinite['start'] + scrollInfinite['limit'];  
                load_infinite_data(scrollInfinite);  
            }); 
        } 
      }
   }
  });
 }

load_infinite_data(scrollInfinite);





