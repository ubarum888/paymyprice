<?php  
include('helpers/classes/config.php'); 

 
?>
<!doctype html>
<html lang="en">
<head>

  <!--  Essential META Tags -->
<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1">
<link rel="canonical" href="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">

<meta property="og:locale" content="en_US">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
<meta property="og:site_name" content="PayMyPrice">

<title>Who We Are - PayMyPrice</title>
<meta property="og:title" content="Who We Are - PayMyPrice">
<meta name="twitter:title" content="Who We Are - PayMyPrice">

<meta name="description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">
<meta property="og:description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">
<meta name="twitter:description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">

<meta property="og:image" content="<?= SITE_URL ?>assets/images/how-we-are.jpg">
<meta property="og:image:secure_url" content="<?= SITE_URL ?>assets/images/how-we-are.jpg">
<meta name="twitter:image" content="<?= SITE_URL ?>assets/images/how-we-are.jpg">
<meta name="twitter:card" content="summary_large_image">
<meta property="og:image:width" content="400">
<meta property="og:image:height" content="50">
 	<?php include_once('elements/head.php');?>

</head>
<body>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->
<header id="header-container" class="fullwidth">
	<?php include_once('elements/header.php');?>	 

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->



<!-- Titlebar
================================================== -->
<div class="page-banner-2" style="background-image:url('<?= SITE_URL ?>assets/images/how-we-are.jpg')">

  <div id="titlebar-2" class="no-background">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>Who We Are</h1>				
        </div>
      </div>
    </div>
  </div>

</div>
	 
<!-- Page Content
================================================== -->
<div class="container">
	<div class="row">
		
		<!-- Content -->
		<div class="col-xl-12 col-lg-12 content-right-offset margin-top-50 margin-bottom-50">

			 
				 <p>In the not so distant past, people would search in their local yellow pages for a service provider. After that got phased out, people started searching online for those same service providers. Not much has changed, am I right? The problem with that model is that its clunky. You look online, find a few service providers, call them, ask for quotes, then decide on the contractor that has the best price. Process maybe takes a few weeks depending on the type of service you require. </p>
			
				<p>While you are getting quotes, you often have a price at the back of your head as your max price you want to pay. That price may be unreasonable based on your experience with that trade or could be spot-on. Nevertheless, the price exists and most often, nobody ever meets that magical number so you end up overpaying or settling on somebody just to avoid haggling or calling more companies.</p>
		
				<p>Well we had the idea to put that model on its head by you designating that number up front and matching you with a contractor in your local area who will complete the project based on your pricing requirements. No haggling or “calling around”, with our seamless turn-key PayMyPrice portal.</p>
		
				<p>Patrick Brouillette, the founder of PayMyPrice.com, has worked in business to business sales his entire career. He came up with the idea because it was always a market gap that existed when working with customers. Customers would contact him, ask for a quote, and one of 2 situations would occur. A quote would be provided and it would be too expensive and result in no sale. Or it would meet the magical number and the customer would purchase on the spot. Getting that magical number released into the wild was the goal at every sales meeting, but customers are always very guarded about their budget number., understandably. There are some ill-willed contractors out there that will over charge you if the price you give is high. But with our system, the pricing is all out in the open from all service providers, so the curtains have been drawn. No surprises here. </p>
		
				<p>Get matched with a service provider in your area today!</p>
		</div>

	</div>
</div>


<!-- Footer
================================================== -->
<div id="footer">	 
 <?php include_once('elements/footer.php');?>	
</div>
<!-- Footer / End -->

</div>
<!-- Wrapper / End -->



</body>
</html>