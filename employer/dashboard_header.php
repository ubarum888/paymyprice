<?php 
$message_user = $objUser->getMessageActivateFreelancer($_SESSION['user']['result']['id']);
//echo"<pre>";print_r($message_user);die;
$friendsList = json_encode($message_user['result']);

$userAlerts = $objUser->getUserAlerts($_SESSION['user']['result']['id']);  
?>

<div id="header">
		<div class="container">
			<!-- Left Side Content -->

			<div class="left-side">			
				<!-- Logo -->


				<div id="logo">
					<a href="<?= CLIENT_URL?>"><img src="<?= SITE_URL ?>assets/images/logo.svg" alt=""></a>
				</div>
				<nav id="navigation">
					<ul id="responsive">
						<li><a href="<?= CLIENT_URL ?>" class="current">Dashboard</a></li>
						<li><a href="<?= CLIENT_URL ?>find-freelancer.php">Find Freelancer</a></li>
					</ul>
				</nav>

				<div class="clearfix"></div>
				<!-- Main Navigation / End -->

			</div>

			<!-- Left Side Content / End -->

			<!-- Right Side Content / End -->	

			<div class="right-side">
				<!--  User Notifications -->

				<div class="header-widget hide-on-mobile">	
					<!-- Notifications -->
					<div class="header-notifications notify-alert">
						<!-- Trigger -->
						<div class="header-notifications-trigger">
							<a href="#"><i class="icon-feather-bell"></i><span class="notify-unread <?php if($userAlerts['notification'] == 0){echo 'hide';}?>"><?= $userAlerts['notification']?></span></a> 
						</div>

						<!-- Dropdown -->
						<div class="header-notifications-dropdown">
							<div class="header-notifications-headline">
								<h4>Notifications</h4>
								<!-- <button class="mark-as-read ripple-effect-dark" title="Mark all as read" data-tippy-placement="left">
									<i class="icon-feather-check-square"></i>
								</button> -->
							</div>

							<div class="header-notifications-content">
								<div class="header-notifications-scroll" data-simplebar>
									<ul>
										<span class='content-loader'></span>  
										<div id="load_notify"></div> 
               							<div id="load_data_notify" class="text-center"></div> 							
									</ul>
								</div>
							</div>
						</div>
					</div>				



					<!-- Messages -->
					<div class="header-notifications messages-alert">
						<div class="header-notifications-trigger">
							<a href="#"><i class="icon-feather-mail"></i><span class="message-unread <?php if($userAlerts['message'] == 0){echo 'hide';}?>"><?= $userAlerts['message']?></span></a> 
						</div>


						<!-- Dropdown -->

						<div class="header-notifications-dropdown">
							<div class="header-notifications-headline">
								<h4>Messages</h4>
								<button class="mark-as-read ripple-effect-dark" title="Mark all as read" data-tippy-placement="left">
									<i class="icon-feather-check-square"></i>
								</button>
							</div>

						<div class="header-notifications-content">
								<div class="header-notifications-scroll" data-simplebar>								
									<ul>
										<span class='content-loader'></span> 
										<div id="load_message"></div> 
               							<div id="load_data_messages" class="text-center"></div> 
									</ul>
								</div>
							</div>

						</div>

					</div>

				</div>



				<!--  User Notifications / End -->

				<!-- User Menu -->


				<?php if(isset($_SESSION['user']['result']['p_img']) && !empty($_SESSION['user']['result']['p_img'])){
					$pImg = $_SESSION['user']['result']['p_img'];
				}else{
					$pImg = 'user.png';
				}?>
				<div class="header-widget">					
					<!-- Messages -->
  
					<div class="header-notifications user-menu">
						<div class="header-notifications-trigger ">
							<a href="javascript:void(0)"><div class="user-avatar status-online"><img src="<?= SITE_URL ?>uploads/profile/<?= $pImg ?>" alt=""></div></a>
						</div>

						<!-- Dropdown -->

						<div class="header-notifications-dropdown">
							<!-- User Status -->
							<div class="user-status">							
								<!-- User Name / Avatar -->

								<div class="user-details">
									<div class="user-avatar status-online"><img src="<?= SITE_URL ?>uploads/profile/<?= $pImg ?>" alt=""></div>
									<div class="user-name">
										<?= ucwords($_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname'])?><span>Employer</span>
									</div>
								</div>		
								<!-- User Status Switcher -->
								<div class="status-switch" id="snackbar-user-status">

									<label class="user-online current-status">Online</label>
									<label class="user-invisible">Invisible</label>
									<!-- Status Indicator -->
									<span class="status-indicator" aria-hidden="true"></span>
								</div>	
						</div>					



						<ul class="user-menu-small-nav">
							<li><a href="<?= CLIENT_URL?>"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>

							<li><a href="<?= CLIENT_URL?>settings.php"><i class="icon-material-outline-settings"></i>Profile</a></li>

							<li><a href="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('logout')?>"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
						</ul>

						</div>
					</div>
				</div>

				<!-- User Menu / End -->

				<!-- Mobile Navigation Button -->

				<span class="mmenu-trigger">

					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</span>
			</div>

			<!-- Right Side Content / End -->
		</div>
	</div>

	<input type="hidden" id="user_id" data-id="<?= $_SESSION['user']['result']['id']?>" data-name="<?= $_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname']?>" value="<?= base64_encode($_SESSION['user']['result']['id'])?>"/>