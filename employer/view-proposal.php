<?php include('../helpers/classes/users.php');
require_once('restricted.php');
$objUser= new USER();
$proposalDetail = $objUser->getProposalDetailById($_GET['pId']); 
//echo "<pre>";print_r($proposalDetail);die;  
?>     
<!doctype html>

<html lang="en">

<head>

	<?php include_once('../elements/head.php');?>

</head>

<body class="gray">



<!-- Wrapper -->

<div id="wrapper">



<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth dashboard-header not-sticky">

  
  <?php include_once('dashboard_header.php');?>

</header>

<div class="clearfix"></div>

<!-- Header Container / End -->





<!-- Dashboard Container -->

<div class="dashboard-container">



	<!-- Dashboard Sidebar

	================================================== -->

	<div class="dashboard-sidebar">

		<?php include_once('dashboard_sidebar.php');?>

	</div>

	<!-- Dashboard Sidebar / End -->





	<!-- Dashboard Content

	================================================== -->

	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			

			<!-- Dashboard Headline -->

			<div class="dashboard-headline">
				<h3>Proposal details</h3>
					<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="">
					<ul>
						<li>
							<a href="<?= SITE_URL ?>helpers/magnific-popup.php?popup=accept-proposal&uId=<?= base64_encode($_SESSION['user']['result']['id'])?>&fId=<?= $_GET['fId'] ?>&pId=<?= $_GET['pId'] ?>&jId=<?= $_GET['jId'] ?>&b=<?= base64_decode($_GET['b'])?>" class="popup-with-zoom-anim-ajax button ripple-effect"><i class="icon-material-outline-check"></i> Accept Proposal</a>
						</li>
						<li>
							<a href="<?= SITE_URL ?>helpers/magnific-popup.php?popup=delete-proposal&uId=<?= base64_encode($_SESSION['user']['result']['id'])?>&fId=<?= $_GET['fId'] ?>&pId=<?= $_GET['pId'] ?>&jId=<?= $_GET['jId'] ?>" class="button popup-with-zoom-anim-ajax red button ripple-effect"><i class="icon-feather-x"></i> Reject Proposal</a>
						</li>									
					</ul>
				</nav>
			</div>

	

			<!-- Row -->

			<div class="row">

				<!-- Dashboard Box -->

				<div class="col-xl-12">

					<div class="dashboard-box margin-top-0">
						<!-- Headline -->

						<div class="headline">
							<h3><i class="icon-material-outline-business-center"></i><?= $proposalDetail['result']['title']?></h3>
						</div>



						<div class="content">
							<ul class="dashboard-box-list">
								<li>

									<!-- Job Listing -->

									<div class="job-listing freelancer">


										<!-- Job Listing Details -->

										<div class="job-listing-details">

											<!-- Logo -->

<!-- 											<a href="#" class="job-listing-company-logo">

												<img src="images/company-logo-05.png" alt="">

											</a> -->



											<!-- Details -->

											<div class="job-listing-description">									 

												<p><?= $proposalDetail['result']['description']?></p>		

												<!-- Job Listing Footer -->

												<div class="job-listing-footer padt_30">
													<ul>
														<li><i class="icon-material-outline-date-range"></i> Posted on <?= $proposalDetail['result']['posted_on']?></li>
														<li><i class="icon-material-outline-date-range"></i> Applied on <?= $proposalDetail['result']['applied_on']?></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>	
			</div>

			<div class="row padt_30">


				<!-- Dashboard Box -->

				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">
						<!-- Headline -->

						<div class="headline">
							<h3><i class="icon-material-outline-check-circle
							"></i>Your Cover Letter</h3>
						</div>
						<div class="content">
							<ul class="dashboard-box-list">
								<li>								
									<!-- Job Listing -->

									<div class="job-listing freelancer">

										<!-- Job Listing Details -->

										<div class="job-listing-details">

											<!-- Logo -->

<!-- 											<a href="#" class="job-listing-company-logo">

												<img src="images/company-logo-05.png" alt="">

											</a> -->



											<!-- Details -->

											<div class="job-listing-description">
												<p><?= $proposalDetail['result']['proposal']?></p>				

												<!-- Job Listing Footer -->

											</div>
										</div>
									</div>	
								</li>
							</ul>
						</div>
					</div>
				</div>	
			</div>

			<!-- Row / End -->



			<!-- Footer -->

			<div class="dashboard-footer-spacer"></div>

			<div class="small-footer margin-top-15">

				 <?php include_once('dashboard_footer.php');?>

			</div>

			<!-- Footer / End -->



		</div>

	</div>

	<!-- Dashboard Content / End -->



</div>

<!-- Dashboard Container / End -->



</div>

<!-- Wrapper / End -->





<!-- Scripts

================================================== -->
<?php include_once('../elements/foot-script.php');?>



</body>

</html>