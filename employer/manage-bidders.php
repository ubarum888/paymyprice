<?php include('../helpers/classes/users.php');  
require_once('restricted.php');
if(!isset($_GET['post']) || ($_GET['post'] == '')){   
	header('location:'.CLIENT_URL.'page-not-found.php');
	exit;
}
$objUser= new USER(); 
$proposals = $objUser->getAllProposalByPost($_GET['post']);
$user_detail = $objUser->getUserDetail($_SESSION['user']['result']['id']);  

//echo "<pre>";print_r($proposals );
//echo "<pre>";print_r($card_detail );die; 
  
  ?>  
<!doctype html>

<html lang="en">

<head>
	<?php include_once('../elements/head.php');?>
</head>

<body class="gray">

<!-- Wrapper -->

<div id="wrapper">
<!-- Header Container
================================================== -->

<header id="header-container" class="fullwidth dashboard-header not-sticky">
      <?php include_once('dashboard_header.php');?>
</header>

<div class="clearfix"></div>

<!-- Header Container / End -->

<!-- Dashboard Container -->
<div class="dashboard-container">	<!-- Dashboard Sidebar

	================================================== -->

	<div class="dashboard-sidebar">
		<?php include_once('dashboard_sidebar.php');?>
	</div>

	<!-- Dashboard Content

	================================================== -->

	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >		

			<!-- Dashboard Headline -->

			<div class="dashboard-headline">
				<h3>Manage Bidders</h3>
				<span class="margin-top-7 color">Bid for <a href="#"><?= $proposals['result'][0]['title']?></a></span>
				<!-- Breadcrumbs -->		 

			</div>

			<!-- Row -->

			<div class="row">
				<!-- Dashboard Box -->

				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">
						<!-- Headline -->

						<div class="headline">
							<h3><i class="icon-material-outline-supervisor-account"></i><?= $proposals['count']?> Bidders</h3>
							<div class="sort-by">
								<select class="selectpicker hide-tick">
									<option>Highest First</option>
									<option>Lowest First</option>
									<option>Fastest First</option>
								</select>
							</div>
						</div>


						<div class="content">
							<ul class="dashboard-box-list">
								<?php  if(!empty($proposals['result'])){
									foreach ($proposals['result'] as $key => $value) {										
										if(isset($value['p_img']) && !empty($value['p_img'])){  
										   $profile_img = SITE_URL.'uploads/profile/'.$value['p_img'];
										}else{
										   $profile_img = SITE_URL.'assets/images/user-avatar-placeholder.png';
										}
										?>
										<li>
										<!-- Overview -->
										<div class="freelancer-overview manage-candidates">
											<div class="freelancer-overview-inner">
												<!-- Avatar -->

												<div class="freelancer-avatar">
													<div class="verified-badge"></div>
													<a href="<?= CLIENT_URL?>freelancer-profile.php?fId=<?= base64_encode($value['user_id'])?>"><img src="<?= $profile_img ?>" alt=""></a>
												</div>

												<!-- Name -->
												<div class="freelancer-name">												 
														<h4>
															<a href="<?= CLIENT_URL?>freelancer-profile.php?fId=<?= base64_encode($value['user_id'])?>"><?= $value['fname'].' '.$value['lname'] ?>
																<img class="flag" src="https://www.countryflags.io/<?= $value['country']?>/shiny/64.png" alt="" title="<?= $value['country']?>" data-tippy-placement="top">
															</a>
															<?php if($value['status'] == 0) { ?>
																<span class="dashboard-status-button green"><i class="icon-material-outline-check"></i>Offer Rejected</span>
															<?php }else if($value['status'] == 2) { ?>
																<span class="dashboard-status-button green"><i class="icon-material-outline-check"></i>Offer Accepted</span>
															<?php } else if($value['status'] == 3) { ?>  
																<span class="dashboard-status-button green"><i class="icon-material-outline-check"></i>Hired</span>
															<?php } else if($value['status'] == 4) { ?>
																<span class="dashboard-status-button green"><i class="icon-material-outline-check"></i>Job Start</span>
															<?php } else if($value['status'] == 5) { ?>
																<span class="dashboard-status-button green"><i class="icon-material-outline-check"></i>Job End</span>
															<?php } ?>
														</h4>
													 
													<!-- Details -->

													<span class="freelancer-detail-item"><a href="<?= CLIENT_URL?>freelancer-profile.php?fId=<?= base64_encode($value['user_id'])?>"> <?= $value['tagline'] ?></a></span>
													

													<!-- Rating -->

													<div class="freelancer-rating">
														<div class="star-rating" data-rating="<?= $value['rate']?>.0"></div>
													</div>

													<!-- Bid Details -->

													<ul class="dashboard-task-info bid-info">
														<li><strong>$<?= number_format($value['minimal_rate'])?></strong><span>Fixed Price</span></li>
														<li><strong><?= $value['delivery_time']?></strong><span>Delivery Time</span></li>
													</ul> 

													<!-- Buttons -->

													<div class="buttons-to-right always-visible margin-top-25 margin-bottom-0">
														<a href="<?= CLIENT_URL?>messages.php?fId=<?= base64_encode($value['user_id'])?>" class="button dark ripple-effect"><i class="icon-feather-message-square"></i> Send Message</a>
														<?php if($value['status'] == 2) { ?>
															
															<a href="<?= SITE_URL ?>helpers/magnific-popup.php?popup=hire&uId=<?= base64_encode($_SESSION['user']['result']['id'])?>&fId=<?= base64_encode($value['user_id']) ?>&pId=<?= base64_encode($value['id']) ?>&jId=<?= $_GET['post'] ?>&fname=<?= base64_encode($value['fname'].' '.$value['lname'] )?>&title=<?= $proposals['result'][0]['title']?>" class="popup-with-zoom-anim-ajax button ripple-effect"><i class="icon-material-outline-check"></i> Hire</a>
															
															
																  
														<?php }else if($value['status'] == 4){?>	

															<a href="<?= SITE_URL?>helpers/magnific-popup.php?popup=end-contract&uId=<?= base64_encode($_SESSION['user']['result']['id'])?>&fId=<?= base64_encode($value['user_id']) ?>&pId=<?= base64_encode($value['id']) ?>&jId=<?= $_GET['post'] ?>&t=<?= base64_encode($proposals['result'][0]['title'])?>&fname=<?= base64_encode($value['fname'].' '.$value['lname'] )?>" class="button popup-with-zoom-anim-ajax red ripple-effect"><i class="icon-feather-x"></i> End Job</a>

														<?php }else if($value['status'] == 1){ ?>
															<a href="<?= SITE_URL ?>helpers/magnific-popup.php?popup=accept-proposal&uId=<?= base64_encode($_SESSION['user']['result']['id'])?>&fId=<?= base64_encode($value['user_id']) ?>&pId=<?= base64_encode($value['id']) ?>&jId=<?= $_GET['post'] ?>&fname=<?= base64_encode($value['fname'].' '.$value['lname'] )?>&b=<?= $value['minimal_rate']?>" class="popup-with-zoom-anim-ajax button ripple-effect"><i class="icon-material-outline-check"></i> Accept Offer</a>

															<a href="<?= CLIENT_URL?>view-proposal.php?pId=<?= base64_encode($value['id'])?>&fId=<?= base64_encode($value['user_id']) ?>&jId=<?= $_GET['post'] ?>&b=<?= base64_encode($value['minimal_rate'])?>" class="button dark ripple-effect"><i class="icon-feather-mail"></i> View Proposal</a>

															<a href="<?= SITE_URL ?>helpers/magnific-popup.php?popup=delete-proposal&uId=<?= base64_encode($_SESSION['user']['result']['id'])?>&fId=<?= base64_encode($value['user_id']) ?>&pId=<?= base64_encode($value['id']) ?>&jId=<?= $_GET['post'] ?>" class="button popup-with-zoom-anim-ajax red ripple-effect"><i class="icon-feather-x"></i> Reject Offer</a>

														<?php }else if($value['status'] == 5){  ?>	
															<?php if($value['payment_id'] == ''){?>
																<a href="<?= SITE_URL?>helpers/magnific-popup.php?popup=make-payment&a=<?= base64_encode($value['minimal_rate'])?>&aId=<?= base64_encode($value['stripe_account_id'])?>&cId=<?= base64_encode($user_detail['result']['stripe_customer_id'])?>&fId=<?= base64_encode($value['user_id']) ?>&eId=<?= base64_encode($_SESSION['user']['result']['id'])?>&jId=<?= $_GET['post'] ?>&t=<?= base64_encode($proposals['result'][0]['title'])?>&n=<?= base64_encode($value['fname'].' '.$value['lname'] )?>" class="button popup-with-zoom-anim-ajax dark ripple-effect"><i class="icon-feather-credit-card"></i> Make Payment</a>
															<?php }else { ?>
																<a href="javascript:void(0)" class="button dark ripple-effect"><i class="icon-feather-credit-card"></i> Payed</a>
															<?php } ?>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									</li>									
								<?php } } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- Row / End -->
			<!-- Footer -->

			<div class="dashboard-footer-spacer"></div>
			<div class="small-footer margin-top-15">
				 <?php include_once('dashboard_footer.php');?>

			</div>
			<!-- Footer / End -->
		</div>
	</div>
	<!-- Dashboard Content / End -->
</div>
<!-- Dashboard Container / End -->
</div>

<!-- Wrapper / End -->

<!-- Scripts

================================================== -->

<?php include_once('../elements/foot-script.php');?>
</body>
</html>