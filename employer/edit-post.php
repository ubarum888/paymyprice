<?php include('../helpers/classes/users.php');  
require_once('restricted.php');
$objUser= new USER();
if(!isset($_GET['post']) || ($_GET['post'] == '')){   
	echo '404 error';
	exit;
}
$editPost = $objUser->getPostDetailsForEdit($_GET['post']); 
$subcat = $objUser->getSubcategoryListByCategoryId($editPost['result']['category_id']);
$category_list = $objUser->getSelectedCategories($_SESSION['user']['result']['id']); 
//echo "<pre>";print_r($editPost);die; 
?>  
<!doctype html>

<html lang="en">

<head>

	<?php include_once('../elements/head.php');?>

</head>

<body class="gray">



<!-- Wrapper -->

<div id="wrapper">



<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth dashboard-header not-sticky">

        <?php include_once('dashboard_header.php');?>

</header>

<div class="clearfix"></div>

<!-- Header Container / End -->





<!-- Dashboard Container -->

<div class="dashboard-container">



	<!-- Dashboard Sidebar

	================================================== -->

	<div class="dashboard-sidebar">

		<?php include_once('dashboard_sidebar.php');?>

	</div>





	<!-- Dashboard Content

	================================================== -->

	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			

			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Edit a Job</h3>	 	
			</div>	

			<!-- Row -->

			<div class="row">

				<form id="job_update_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('job_update_form')?>" data-name="<?= base64_encode('job_update_form')?>" method="POST" onsubmit="return validateForm(this.id, event)">

				<!-- Dashboard Box -->
				<input type="hidden" name="post_id" value="<?= $_GET['post']?>"/>
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">
						<!-- Headline -->

						<div class="headline">
							<h3><i class="icon-feather-folder-plus"></i> Job Edit Form</h3>
						</div>

						<div class="content with-padding padding-bottom-10">
							<div class="row"> 
								<div class="col-xl-12 pad_all_12">
									<div class="submit-field">
										<h5>Job Title</h5>
										<input type="text" name="title" maxlength="40" title="Enter Job Title" class="with-border" value="<?= $editPost['result']['title']?>">
									</div>
								</div>

								<div class="col-xl-12 pad_all_12">
									<div class="submit-field">
										<h5>Job Description</h5>
										<textarea cols="30" rows="5" name="description" title="Enter Job Description" class="with-border"><?= $editPost['result']['description']?></textarea>	
									</div>
								</div>

								<div class="col-xl-4 pad_all_12">
									<div class="submit-field">
										<h5>Job Type</h5>
										<select class="selectpicker with-border" data-size="7" title="Select Job Type" placeholder="Job Type"name="job_type">
											<option value="">Select Job Type</option> 
											<option value="full time" <?php if($editPost['result']['job_type'] == 'full time'){echo 'selected';}?>>Full Time</option> 
											<option value="part time"  <?php if($editPost['result']['job_type'] == 'part time'){echo 'selected';}?>>Part Time</option> 
										</select>
									</div>
								</div>

								<div class="col-xl-4 pad_all_12">
									<div class="submit-field">
										<h5>Job Category</h5>
										<select class="selectpicker with-border" data-size="7" onchange="showSubcategoryList(this.value)" title="Select Category" placeholder="Select Job Category"  name="category_id">
											<option value="">Select Job Category</option> 
											<?php foreach ($category_list['result'] as $key => $value) {?>
												<option value="<?= $value['id']?>" <?php if($editPost['result']['category_id'] ==  $value['id']){echo 'selected';}?>><?= $value['category']?></option>
											<?php } ?>											
										</select>
									</div>
								</div>

								<div class="col-xl-4 pad_all_12">
									<div class="submit-field">
										<h5>Job sub Category</h5>
										<select class="selectpicker with-border" data-size="7" title="Select Sub Category" id="subcat-list" placeholder="Select Job Sub Category" name="sub_category_id">
											<option value="">Select Job Sub Category</option>
											<?php foreach ($subcat['result'] as $key => $value) {?>
												<option value="<?= $value['id']?>" <?php if($editPost['result']['sub_category_id'] ==  $value['id']){echo 'selected';}?>><?= $value['subcategory']?></option>
											<?php } ?>	 
										</select>  
									</div>
								</div>

									<div class="col-xl-5 pad_all_12">
									<div class="submit-field payment-type">
										<h5>Payment Type</h5>
										<div class="checkbox pad_all_12">
											<input type="checkbox" id="chekcbox1" class="one_checked" value="Fixed" <?php if($editPost['result']['payment_type'] == 'Fixed'){echo 'checked';}?>>
											<label for="chekcbox1"><span class="checkbox-icon pt"></span> Fixed Price</label>
										</div>

										<div class="checkbox pad_all_12">
											<input type="checkbox" id="chekcbox2" class="one_checked" value="Hourly" <?php if($editPost['result']['payment_type'] == 'Hourly'){echo 'checked';}?>>
											<label for="chekcbox2"><span class="checkbox-icon pt"></span> Hourly</label>
										</div>
									</div>
								</div>

							  <div class="col-xl-5 pad_all_12">
									<div class="submit-field payment-process">
										<h5>Payment Processing</h5>

										<div class="checkbox pad_all_12">
											<input type="checkbox" id="chekcbox3" class="one_checked" value="daily" <?php if($editPost['result']['payment_process'] == 'daily'){echo 'checked';}?>>
											<label for="chekcbox3"><span class="checkbox-icon pp"></span>Daily</label>
										</div>

										<div class="checkbox pad_all_12">
											<input type="checkbox" id="chekcbox4" class="one_checked" value="weekly" <?php if($editPost['result']['payment_process'] == 'weekly'){echo 'checked';}?>>
											<label for="chekcbox4"><span class="checkbox-icon pp"></span> Weekly</label>
										</div>

										<div class="checkbox pad_all_12">
											<input type="checkbox" id="chekcbox5" class="one_checked" value="monthly" <?php if($editPost['result']['payment_process'] == 'monthly'){echo 'checked';}?>>
											<label for="chekcbox5"><span class="checkbox-icon pp"></span> Monthly</label>
										</div>
									</div>
								</div>
							
								<div class="col-xl-4">
									<div class="submit-field">
										<h5 id="payment_type">Project Budget <i class="help-icon" data-tippy-placement="right" title="Set Min Project Price"></i></h5>
										<div class="row">
											<div class="col-xl-6">
												<div class="input-with-icon">
													<input class="with-border" name="min_budget" onkeypress="return isNumberKey(event)"type="text" placeholder="Min" title="Min Price" value="<?= $editPost['result']['min_budget']?>">
													<i class="currency">USD</i>
												</div>
											</div>
											<div class="col-xl-6">
												<div class="input-with-icon">
													<input class="with-border" name="max_budget" onkeypress="return isNumberKey(event)" type="text" placeholder="Max" value="<?= $editPost['result']['max_budget']?>">
													<i class="currency">USD</i>
												</div>
											</div>
										</div>
									</div>
								</div>  

								<div class="col-xl-6">  
									<div class="submit-field">
										<h5>Tags <span>(optional)</span>  <i class="help-icon" data-tippy-placement="right" title="Maximum of 10 tags"></i></h5>
										<div class="keywords-container">
											<div class="keyword-input-container tag-wrapper">
												<input type="text" class="keyword-input with-border" maxlength="30" placeholder="Tags"/>
												<button type="button" class="keyword-input-button ripple-effect add_button"><i class="icon-material-outline-add"></i></button>
											</div>
											<div class="keywords-list">
												<?php if(!empty($editPost['result']['tags'])){
													foreach($editPost['result']['tags'] as $tag){?>
													<span class="keyword"><span class="keyword-remove"></span><span class="keyword-text"><?= $tag?></span></span>
												<?php } } ?>								
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-12">
					  <button type="submit" name="submit" class="button ripple-effect big margin-top-30 has-spinner"><i class="icon-feather-plus"></i> Post a Job <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
				</div>
			<form>
			</div>

			<!-- Row / End -->

			<!-- Footer -->

			<div class="dashboard-footer-spacer"></div>
			<div class="small-footer margin-top-15">
				 <?php include_once('dashboard_footer.php');?>
			</div>			 

			<!-- Footer / End -->
		</div>
	</div>

	<!-- Dashboard Content / End -->

</div>
<!-- Dashboard Container / End -->
</div>

<!-- Wrapper / End -->

<!-- Scripts

================================================== -->

<?php include_once('../elements/foot-script.php');?>
<!-- Google Autocomplete -->

</body>
</html>