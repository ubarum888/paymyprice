<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include('../helpers/classes/users.php');
require_once('restricted.php');
if(!isset($_GET['fId']) || ($_GET['fId'] == '')){   
	header('location:'.CLIENT_URL.'page-not-found.php');
	exit;
}

$objUser= new USER();
$fDetail = $objUser->getFreelancerDetailsById($_GET['fId'], $_SESSION['user']['result']['id']);  
$jobDone = $objUser->getFreelancerJobsDoneCount($_GET['fId']);
//echo '<pre>';print_r($fDetail);die;
$overall_rate = $objUser->getOverallRatingOfFreelancer($_GET['fId']);
if(isset($fDetail['result']['p_img']) && !empty($fDetail['result']['p_img'])){  
   $profile_img = SITE_URL.'uploads/profile/'.$fDetail['result']['p_img'];
}else{
   $profile_img = SITE_URL.'assets/images/user-avatar-big-02.jpg';
}
// echo "<pre>";print_r($fDetail);die; 
?>

<!doctype html>
<html lang="en">
<head>
	<?php include_once('../elements/head.php');?>
</head>  
<body>
<!-- Wrapper -->

<div id="wrapper">
<!-- Header Container

================================================== -->
<header id="header-container" class="fullwidth">
  <?php include_once('dashboard_header.php');?>
</header>
<input type="hidden" id="f_id" value="<?= $_GET['fId']?>"/>	
<div class="clearfix"></div>
<!-- Header Container / End -->
<!-- Titlebar

================================================== -->

<div class="single-page-header freelancer-header" data-background-image="<?= SITE_URL ?>assets/images/single-freelancer.jpg">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-page-header-inner">
					<div class="left-side">
						<div class="header-image freelancer-avatar"><img src="<?= $profile_img ?>" alt=""></div>
						<div class="header-details">
							<h3><?= $fDetail['result']['fname'].' '.$fDetail['result']['lname']?> <span><?= $fDetail['result']['tagline']?></span></h3>
							<ul>
								<li><div class="star-rating" data-rating="<?= $overall_rate['result']['rate']?>.0"></div></li>	
								<li><img class="flag" src="https://www.countryflags.io/<?= $fDetail['result']['country']?>/shiny/64.png" alt=""><?= $fDetail['result']['country']?> </li>								
								<li><div class="verified-badge-with-title">Verified</div></li>
							</ul>
						</div>
					</div>  
				</div>  
			</div>
		</div>  
	</div>
</div>


<!-- Page Content

================================================== -->

<div class="container">
	<div class="row">	

		<!-- Content -->

		<div class="col-xl-8 col-lg-8 content-right-offset">		

			<!-- Page Content -->

			<div class="single-page-section">
				<h3 class="margin-bottom-25">About Me</h3>
				<?php if(empty($fDetail['result']['your_self'])){?>
					<p>N/A</p>
				<?php }else{?>
					<p><?= $fDetail['result']['your_self']?></p>
				<?php } ?>
			</div>



			<!-- Boxed List -->

			<div class="boxed-list margin-bottom-60">
				<div class="boxed-list-headline">
					<h3><i class="icon-material-outline-thumb-up"></i> Work History and Feedback</h3>
				</div>

				<ul class="boxed-list-ul" id="load_data" data-name="freelancer-rating"></ul>
				 <div id="load_data_message" class="text-center"></div> 	
				<!-- Pagination -->

				<div class="clearfix"></div>
			</div>

			<!-- Boxed List / End -->

			

			<!-- Boxed List -->

			<div class="boxed-list margin-bottom-60">
				<div class="boxed-list-headline">
					<h3><i class="icon-material-outline-business"></i> Employment History</h3>
				</div>

				<ul class="boxed-list-ul">
					<li>
						<div class="boxed-list-item">
							<!-- Avatar -->

							<div class="item-image">
								<img src="<?= SITE_URL ?>assets/images/browse-companies-03.png" alt="">
							</div>

							

							<!-- Content -->

							<div class="item-content">

								<h4>Development Team Leader</h4>

								<div class="item-details margin-top-7">

									<div class="detail-item"><a href="#"><i class="icon-material-outline-business"></i> Acodia</a></div>

									<div class="detail-item"><i class="icon-material-outline-date-range"></i> May 2018 - Present</div>

								</div>

								<div class="item-description">

									<p>Focus the team on the tasks at hand or the internal and external customer requirements.</p>

								</div>

							</div>

						</div>

					</li>

					<li>
						<div class="boxed-list-item">
							<!-- Avatar -->

							<div class="item-image">
								<img src="<?= SITE_URL ?>assets/images/browse-companies-04.png" alt="">
							</div>							

							<!-- Content -->

							<div class="item-content">
								<h4><a href="#">Lead UX/UI Designer</a></h4>
								<div class="item-details margin-top-7">
									<div class="detail-item"><a href="#"><i class="icon-material-outline-business"></i> Acorta</a></div>
									<div class="detail-item"><i class="icon-material-outline-date-range"></i> April 2014 - May 2018</div>
								</div>

								<div class="item-description">
									<p>I designed and implemented 10+ custom web-based CRMs, workflow systems, payment solutions and mobile apps.</p>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>

			<!-- Boxed List / End -->

		</div>

		<!-- Sidebar -->

		<div class="col-xl-4 col-lg-4">  
			<div class="sidebar-container">			

				<!-- Profile Overview -->

				<div class="profile-overview">
					<div class="overview-item"><strong>$<?= $fDetail['result']['hourly_rate']?></strong><span>Hourly Rate</span></div>
					<!--<div class="overview-item"><strong><?= $jobDone['result']['done']; ?></strong><span>Jobs Done</span></div>-->
			          <?php if(!empty($jobDone['result'])){ ?>
			            <div class="overview-item"><strong><?= $jobDone['result']['done']; ?></strong><span>Jobs Done</span></div>
			          <?php } ?>
				</div>


				<div id="make_offer">
					<!-- Button -->
					<?php if($fDetail['result']['bid_status'] == ""){
						if($fDetail['result']['o_id'] == ""){?>							
							<a href="<?= SITE_URL ?>helpers/magnific-popup.php?popup=make-offer&uId=<?= base64_encode($_SESSION['user']['result']['id'])?>&fId=<?= $_GET['fId'] ?>&uname=<?= base64_encode($_SESSION['user']['result']['fname'].' '.$_SESSION['user']['result']['lname'])?>&femail=<?= base64_encode($fDetail['result']['email'])?>&fname=<?= base64_encode($fDetail['result']['fname'].' '.$fDetail['result']['lname'])?>" class="apply-now-button popup-with-zoom-anim-ajax margin-bottom-50">Make an Offer <i class="icon-material-outline-arrow-right-alt"></i></a>	
						<?php  }else{ ?>
							<button type="button" class="button gray ripple-effect-dark margin-bottom-50 full-width">Offer Made</button>	
						<?php } ?>
						<button type="button" class="button gray ripple-effect-dark margin-bottom-50 full-width hide">Offer Made</button>
					<?php } else{
						 	if($fDetail['result']['bid_status'] == '1'){?>
								<span class="dashboard-status-button green">Bid Applied</span>
							<?php } else if($fDetail['result']['bid_status'] == '2'){?>
								<span class="dashboard-status-button green">Bid Accepted</span>
							<?php }  else if($fDetail['result']['bid_status'] == '3'){?>
								<span class="dashboard-status-button green">Hired</span>
							<?php }  else if($fDetail['result']['bid_status'] == '4'){?>
								<span class="dashboard-status-button green">Job Start</span>
							<?php }  else if($fDetail['result']['bid_status'] == '5'){?>
								<span class="dashboard-status-button green">Job End</span>
							<?php } 
					 }	?>

					 <a href="messages.php?fId=<?= $_GET['fId']?>" class="button dark ripple-effect full-width"><i class="icon-feather-message-square"></i> Send Message</a><br>
				</div>


				<!-- Widget -->
			<?php if(!empty($fDetail['result']['tags'])){?>
				<div class="sidebar-widget">
					<h3>Tags</h3>
					<div class="task-tags">
						<?php foreach ($fDetail['result']['tags'] as $key => $value) {?>
							<span><?=  $value?></span>
						<?php } ?>
					</div>
				</div>
			<?php } ?>	

			</div>

		</div>



	</div>

</div>
<!-- Spacer -->

<div class="margin-top-15"></div>

<!-- Spacer / End-->



<!-- Footer

================================================== -->

<div id="footer">	

	 <?php include_once('../elements/footer.php');?>

</div>

<!-- Footer / End -->



</div>

<!-- Wrapper / End -->





<!-- Scripts

================================================== -->  

<?php include_once('../elements/foot-script.php');?>

<script type="text/javascript">
	  
var scrollRating = {};

scrollRating['limit'] = 8;
scrollRating['start'] = 0;
scrollRating['scroll'] = $("#load_data").data('name');
scrollRating['user_id'] = $('#f_id').val();

  /*--------------------------------------------------*/
  /*  Star Rating
  /*--------------------------------------------------*/
  function starRating(ratingElem) {

    $(ratingElem).each(function() {

      var dataRating = $(this).attr('data-rating');

      // Rating Stars Output
      function starsOutput(firstStar, secondStar, thirdStar, fourthStar, fifthStar) {
        return(''+
          '<span class="'+firstStar+'"></span>'+
          '<span class="'+secondStar+'"></span>'+
          '<span class="'+thirdStar+'"></span>'+
          '<span class="'+fourthStar+'"></span>'+
          '<span class="'+fifthStar+'"></span>');
      }

      var fiveStars = starsOutput('star','star','star','star','star');

      var fourHalfStars = starsOutput('star','star','star','star','star half');
      var fourStars = starsOutput('star','star','star','star','star empty');

      var threeHalfStars = starsOutput('star','star','star','star half','star empty');
      var threeStars = starsOutput('star','star','star','star empty','star empty');

      var twoHalfStars = starsOutput('star','star','star half','star empty','star empty');
      var twoStars = starsOutput('star','star','star empty','star empty','star empty');

      var oneHalfStar = starsOutput('star','star half','star empty','star empty','star empty');
      var oneStar = starsOutput('star','star empty','star empty','star empty','star empty');

      var halfStar = starsOutput('star half','star empty','star empty','star empty','star empty');
      var noStar = starsOutput('star empty','star empty','star empty','star empty','star empty');

      // Rules
          if (dataRating >= 4.75) {
              $(this).append(fiveStars);
          } else if (dataRating >= 4.25) {
              $(this).append(fourHalfStars);
          } else if (dataRating >= 3.75) {
              $(this).append(fourStars);
          } else if (dataRating >= 3.25) {
              $(this).append(threeHalfStars);
          } else if (dataRating >= 2.75) {
              $(this).append(threeStars);
          } else if (dataRating >= 2.25) {
              $(this).append(twoHalfStars);
          } else if (dataRating >= 1.75) {
              $(this).append(twoStars);
          } else if (dataRating >= 1.25) {
              $(this).append(oneHalfStar);
          } else if (dataRating >= 0.75) {
              $(this).append(oneStar);
          }else if (dataRating >= 0.25) {
              $(this).append(halfStar);
          }else{
              $(this).append(noStar);
          }

    });

  } 

function userRating(value){
   $('#load_data').append('<li>'+
		'<div class="boxed-list-item">'+
			'<div class="item-content">'+
				'<h4>'+value.title+'<span>Rated as Freelancer</span></h4>'+
				'<div class="item-details margin-top-10">'+
					'<div class="star-rating" data-rating="'+value.rating+'.0"></div>'+
					'<div class="detail-item"><i class="icon-material-outline-date-range"></i> '+value.review_on+'</div>'+
				'</div>'+
				'<div class="item-description">'+
					'<p>'+value.message+'</p>'+
				'</div>'+
			'</div>'+
		'</div>'+
	'</li>');
  } 

 function load_infinite_data(scrollWith, filters = null, sort = null) 
 {   
  //console.log(filters);
  // console.log(scrollWith);
  
  //var jsonString = filters.serializeArray();
    //console.log(jsonString);
  $.ajax({
   url:SITE_URL+'helpers/functions.php?type=aW5maW5pdGVfc2Nyb2xs',  
   method:"POST",
   data:{'scroll_with':scrollWith, 'filter':filters, 'sort_by':sort},

   cache:false,
   success:function(data)
   {

    var obj = $.parseJSON(data);
   // console.log(obj.data);
    if(obj.data.length > 0){      

        if(obj.content == 'freelancer-rating') {
          $.each(obj.data, function( index, value ) {
            userRating(value);  
          }); 
        }
        starRating('#load_data .star-rating');
      }  
      if(obj.data == '' || obj.data == null) {
        $('#load_data_message').html("<button type='button' class='btn head-btn1'>No Data Found</button>");
      } else{
        //console.log(obj.count);
        if(obj.count > 7){
            $('#load_data_message').html("<button type='button' class='button dark ripple-effect load_more mb-20'><i class='icon-feather-rotate-ccw'></i> Load More</button>"); 
            $('#load_data_message .load_more').click(function(){
                scrollRating['start'] = scrollRating['start'] + scrollRating['limit'];  
                load_infinite_data(scrollRating);  
            }); 
        } 
      }
   }
  });
 }

load_infinite_data(scrollRating);











</script>

</body>

</html>