<?php  
$page_file_temp = $_SERVER["PHP_SELF"];
$page_directory = dirname($page_file_temp);
if(basename($page_directory) !== 'employer') {
	FD_add_notices('You have not access for employer. Please log in again', 'error');	
	header('location:'.SITE_URL);
	exit;
}

if(!isset($_SESSION['user']['result']['id'])){	
	FD_add_notices('Your session id not set or expired. Please log in again', 'error');	
	header('location:'.SITE_URL);
	exit;
}

if(isset($_SESSION['user']['result']['id']) && ($_SESSION['user']['result']['id'] == '')){	
	FD_add_notices('Your session id expired', 'error');	
	header('location:'.SITE_URL);
	exit;
}

if(isset($_SESSION['user']['result']['account_type']) && ($_SESSION['user']['result']['account_type'] == 2)) {
	FD_add_notices('Account Type not matched', 'error');	
    header('location:'.SITE_URL);
    exit;
}
?>