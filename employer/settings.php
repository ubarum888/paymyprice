<?php include('../helpers/classes/users.php');
require_once('restricted.php');
$objUser= new USER();
$profile = $objUser->getMyProfile($_SESSION['user']['result']['id']);  
$userCard = $objUser->getAllUserCards($_SESSION['user']['result']['id']);  
//echo "<pre>";print_r($profile);die;  
if(isset($profile['result']['p_img']) && !empty($profile['result']['p_img'])){  
   $profile_img = SITE_URL.'uploads/profile/'.$_SESSION['user']['result']['p_img'];
}else{
   $profile_img = '';
}
?>  
<!doctype html>

<html lang="en">

<head>
	<?php include_once('../elements/head.php');?>
	 <script src="https://js.stripe.com/v3/"></script>

</head>
<body class="gray">
<!-- Wrapper -->

<div id="wrapper">
<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth dashboard-header not-sticky">  
  <?php include_once('dashboard_header.php');?>

</header>

<div class="clearfix"></div>
<!-- Header Container / End -->

<!-- Dashboard Container -->

<div class="dashboard-container">

	 <div class="dashboard-sidebar">
		<?php include_once('dashboard_sidebar.php');?>
	</div>
	<!-- Dashboard Content

	================================================== -->

	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >			

			<!-- Dashboard Headline -->

			<div class="dashboard-headline">
				<h3>Profile</h3> 
			</div>

			<!-- Row -->

			<div class="row">
				<!-- Dashboard Box -->
				<form id="update_account_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('update_account_form')?>" data-name="<?= base64_encode('update_account_form')?>" method="POST" enctype="multipart/form-data" onsubmit="return validateForm(this.id, event)" autocomplete="off">
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-account-circle"></i> My Account</h3>
						</div>

						<div class="content with-padding padding-bottom-0">
							<div class="row">
									<div class="col-auto">
										<div class="avatar-wrapper" data-tippy-placement="top" title="Change Avatar">
											<span class='content-loader'></span>  
						                    <div id="crop_preview" class="hide"> </div>                
						                    <div id="image_preview">
						                      	<img class="profile-pic" src="<?=$profile_img?>" alt="profile image" />
						                    </div>												
											<div class="upload-button"></div>
											<input class="file-upload" type="file" accept="image/*"/>
										</div>	
										<div class="change-photo-btn save" style="display: none">  
					                        <div class="photoUpload">
					                            <span><i class="fa fa-upload"></i> Crop & Save</span>                               
					                        </div>
					                    </div>
					                    <div class="change-photo-btn reset" style="display: none">    
					                        <div class="photoUpload">
					                            <span><i class="fa fa-close"></i> Reset</span>                            
					                        </div>
					                    </div> 									
									</div> 						
								<div class="col">
									<div class="row">
										<div class="col-xl-6">
											<div class="submit-field">
												<h5>First Name</h5>
												<input type="text" class="with-border" name="fname" maxlength="20" title="Please enter first name" value="<?= $profile['result']['fname']?>">
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Last Name</h5>
												<input type="text" class="with-border" name="lname" maxlength="20" title="Please enter last name" value="<?= $profile['result']['lname']?>">
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Email</h5>
												<input type="text" class="with-border" value="<?= $profile['result']['email']?>" readonly>
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Phone </h5>
												<input type="text" class="with-border" name="phone" maxlength="15" onkeypress="return isNumberKey(event)" title="Please enter Phone Number" value="<?= $profile['result']['phone']?>">
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Address </h5>
												<input type="text" class="with-border" id="autocomplete" name="address" title="Please enter address" value="<?= $profile['result']['address']?>">
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Country </h5>
												<input type="text" class="with-border" name="country" maxlength="20" id="country" title="Please enter country" value="<?= $profile['result']['country']?>">
											</div>
										</div>  

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>State </h5>
												<input type="text" class="with-border" name="state" maxlength="20" id="administrative_area_level_1" title="Please enter state" value="<?= $profile['result']['state']?>">
											</div>
										</div>

										<div class="col-xl-6">
											<div class="submit-field">
												<h5>Zip </h5>
												<input type="text" class="with-border" name="zip" onkeypress="return isNumberKey(event)" id="postal_code" title="Please enter Postal code" maxlength="10" value="<?= $profile['result']['zip']?>">
											</div>
										</div>

										<div class="col-xl-12">
											<div class="uploadButton margin-top-30">
												<input class="uploadButton-input" type="file" accept="image/*" name="logo" id="upload">
												<label class="uploadButton-button ripple-effect" for="upload">Upload Logo</label>
												<span class="uploadButton-file-name"><?php if(!empty($profile['result']['c_logo'])){echo $profile['result']['c_logo'];}else{echo 'Company Logo that might be helpful in describing your Company';}?></span>
											</div>
										</div>	 

										 <div class="col-xl-6">	
											<div class="submit-field">
												<button type="submit" name="submit" class="button ripple-effect big margin-top-30 has-spinner">Save Changes <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
											</div>
										</div>															
									</div>
								</div>
							
							</div>
						</div>
					</div>
				</div>
			</form> 
		
			<div class="col-xl-12">
				<div id="test1" class="dashboard-box">
					<!-- Headline -->
					<div class="headline">
						<h3><i class="icon-material-outline-credit-card"></i> Add Payment Methods<img src="<?= SITE_URL?>uploads/brand/stripe-logo.png" width='100'></h3>
						 
						<?php if(!empty($userCard['result'])){ ?>
							<button class="button full-width button-sliding-icon ripple-effect margin-top-30" id="link_new_card">Link New Card</button>
						<?php } ?>
						
					</div>

					
					<div class="content with-padding">
						<div class="row <?php if(!empty($userCard['result'])){echo 'hide';}?>" id="addCard">			
							<form method="post" id="payment-form">
							  <div class="form-row">
							    <!--<label for="card-element">
							      Credit or debit card
							    </label>-->
							    <div id="card-element" style="width: 100%;">  
							      <!-- A Stripe Element will be inserted here. -->
							    </div>

							    <!-- Used to display form errors. -->
							    <div id="card-errors" role="alert"></div>
							  </div>
							  	<div class="col-xl-3">
									<div class="submit-field">
										<button class="button full-width button-sliding-icon ripple-effect margin-top-30 has-spinner" type="submit" name="submit" style="width: 475px;" id="card-button">Add Card <i class="icon-material-outline-arrow-right-alt"></i> <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>   
									</div>
								</div>
							</form>
						</div>	
					
						<div class="row <?php if(empty($userCard['result'])){echo 'hide';}?>" id="card_list">
								<span class='content-loader'></span> 
							<div class="col-xl-12">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th scope="col">Activate</th>
											<th scope="col">Card Type</th>
											<th scope="col">Card No</th>
											<th scope="col">Expiry Date</th>
											 <th scope="col">Action</th> 
										</tr>
									</thead>
									<tbody>	
										<?php if(!empty($userCard['result'])){ 
											 foreach ($userCard['result'] as $key => $value) {?>
											<tr>
												<td>	
													<div class="radio">
														<input id="card-radio-<?= $value['c_id'] ?>" class="set-default-card" name="card_active" type="radio" value="<?= $value['c_id'] ?>" <?php if($value['status'] == 1){echo 'checked';}?>>
														<label for="card-radio-<?= $value['c_id'] ?>">
															<span class="radio-label"></span>
														</label>
													</div>
												</td>	
												<td><img src="<?= SITE_URL?>uploads/brand/<?= $value['brand'] ?>.png" width="50"></td>
												<td>************<?= $value['account_no'] ?></td>
												<td><?= $value['expire_card'] ?></td>
												<?php if($value['status'] == 0){?>
													<td><a href="<?= SITE_URL?>helpers/magnific-popup.php?popup=delete-card&id=<?= base64_encode($value['c_id']) ?>" class="button ico popup-with-zoom-anim-ajax gray ripple-effect" data-tippy-placement="top" data-tippy="" data-original-title="Remove"><i class="icon-feather-trash-2"></i></a></td>
												<?php }else{ ?>
													<td>Default</td>
												<?php } ?>	
											</tr>
										<?php } } ?>
									</tbody>
								</table>
							</div>	
						</div>						
					</div>
				</div>
			</div>				
	
				<!-- Dashboard Box -->
		<form id="add_password_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('add_password_form')?>" data-name="<?= base64_encode('add_password_form')?>" method="POST" onsubmit="return validateForm(this.id, event)" autocomplete="off">		
			<div class="col-xl-12">
				<div id="test" class="dashboard-box">
					<!-- Headline -->
					<div class="headline">
						<h3><i class="icon-material-outline-lock"></i> Password & Security</h3>
					</div>
					<div class="content with-padding">
						<div class="row">								
							<div class="col-xl-3">
								<div class="submit-field">
									<h5>Current Password</h5>
									<input type="password" name="current_pass" id="current_pass" title="Please enter current password" maxlength="15" class="with-border"/>
									<span toggle="#current_pass" class="fa fa-fw fa-eye field-icon toggle-password"></span>
								</div>
							</div>

							<div class="col-xl-3">
								<div class="submit-field">
									<h5>New Password  <i class="help-icon" data-tippy-placement="right" title="Password should be at least 8 characters in length and should include at least one upper case letter, one number."></i></h5>
									<input type="password" id="psw" name="new_pass" title="Please enter new password" class="with-border" maxlength="15"/>
									<span toggle="#psw" class="fa fa-fw fa-eye field-icon toggle-password"></span>
								</div>
							</div>

							<div class="col-xl-3">
								<div class="submit-field">  
									<h5>Repeat New Password</h5>
									<input type="password" class="with-border" name="confirm_pass" title="Please confirm new password" id="confirm_pass" maxlength="15" />
									<span toggle="#confirm_pass" class="fa fa-fw fa-eye field-icon toggle-password"></span>
								</div>
							</div>

							<div class="col-xl-2">
								<div class="submit-field">
									<button class="button full-width button-sliding-icon ripple-effect margin-top-30 has-spinner" type="submit" name="submit" style="width: 475px;">Save<i class="icon-material-outline-arrow-right-alt"></i> <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		</div>
			<!-- Row / End -->

			<!-- Footer -->

			<div class="dashboard-footer-spacer"></div>

			<div class="small-footer margin-top-15">

				 <?php include_once('dashboard_footer.php');?>

			</div>
			<!-- Footer / End -->
		</div>

	</div>

	<!-- Dashboard Content / End -->

</div>

<!-- Dashboard Container / End -->


</div>
<!-- Wrapper / End -->

<!-- Scripts

================================================== -->

<?php include_once('../elements/foot-script.php');?>

<!-- Google Autocomplete -->
<?php include_once('../elements/google-provider.php'); ?>
 <script src="<?= SITE_URL ?>assets/js/stripe.js"></script> 

</body>

</html>