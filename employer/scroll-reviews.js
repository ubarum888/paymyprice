
var rate_to_user = {};

rate_to_user['limit'] = 8;
rate_to_user['start'] = 0;
rate_to_user['scroll'] = $("#rate_to_user").data('name');
rate_to_user['user_id'] = $('#user_id').val();

  /*--------------------------------------------------*/
  /*  Star Rating
  /*--------------------------------------------------*/
  function starRating(ratingElem) {

    $(ratingElem).each(function() {

      var dataRating = $(this).attr('data-rating');

      // Rating Stars Output
      function starsOutput(firstStar, secondStar, thirdStar, fourthStar, fifthStar) {
        return(''+
          '<span class="'+firstStar+'"></span>'+
          '<span class="'+secondStar+'"></span>'+
          '<span class="'+thirdStar+'"></span>'+
          '<span class="'+fourthStar+'"></span>'+
          '<span class="'+fifthStar+'"></span>');
      }

      var fiveStars = starsOutput('star','star','star','star','star');

      var fourHalfStars = starsOutput('star','star','star','star','star half');
      var fourStars = starsOutput('star','star','star','star','star empty');

      var threeHalfStars = starsOutput('star','star','star','star half','star empty');
      var threeStars = starsOutput('star','star','star','star empty','star empty');

      var twoHalfStars = starsOutput('star','star','star half','star empty','star empty');
      var twoStars = starsOutput('star','star','star empty','star empty','star empty');

      var oneHalfStar = starsOutput('star','star half','star empty','star empty','star empty');
      var oneStar = starsOutput('star','star empty','star empty','star empty','star empty');

      var halfStar = starsOutput('star half','star empty','star empty','star empty','star empty');
      var noStar = starsOutput('star empty','star empty','star empty','star empty','star empty');

      // Rules
          if (dataRating >= 4.75) {
              $(this).append(fiveStars);
          } else if (dataRating >= 4.25) {
              $(this).append(fourHalfStars);
          } else if (dataRating >= 3.75) {
              $(this).append(fourStars);
          } else if (dataRating >= 3.25) {
              $(this).append(threeHalfStars);
          } else if (dataRating >= 2.75) {
              $(this).append(threeStars);
          } else if (dataRating >= 2.25) {
              $(this).append(twoHalfStars);
          } else if (dataRating >= 1.75) {
              $(this).append(twoStars);
          } else if (dataRating >= 1.25) {
              $(this).append(oneHalfStar);
          } else if (dataRating >= 0.75) {
              $(this).append(oneStar);
          }else if (dataRating >= 0.25) {
              $(this).append(halfStar);
          }else{
              $(this).append(noStar);
          }

    });

  } 

  function rateToFreelancer(value){

  if(value.r_id == null){
    var msg ='<span class="company-not-rated margin-bottom-5">Not Rated</span>';
    var btn = '<a href="'+SITE_URL+'helpers/magnific-popup.php?popup=rate-freelancer&fId='+value.encrypt_fId+'&eId='+value.encrypt_eId+'&pId='+value.encrypt_id+'" class="popup-with-zoom-anim button ripple-effect margin-top-5 margin-bottom-10"><i class="icon-material-outline-thumb-up"></i> Leave a Review</a>';
  }else{
    var msg = '<div class="item-details margin-top-10">'+
          '<div class="star-rating" data-rating="'+value.rating+'.0"></div>'+
          '<div class="detail-item"><i class="icon-material-outline-date-range"></i> '+value.review_on+'</div>'+
        '</div>'+
        '<div class="item-description">'+value.message+'</div>';
     var btn = '<a href="'+SITE_URL+'helpers/magnific-popup.php?popup=edit-rate-freelancer&fId='+value.encrypt_fId+'&eId='+value.encrypt_eId+'&pId='+value.encrypt_id+'" class="popup-with-zoom-anim button gray ripple-effect margin-top-5 margin-bottom-10"><i class="icon-feather-edit"></i> Edit Review</a>';   
  }

   $('#rate_to_user').append('<li id="rate_to_freelancer'+value.freelancer_id+'">'+
          '<div class="boxed-list-item">'+
            '<div class="item-content">'+
              '<h4 class="post_title" data-name="'+value.fname+' '+value.lname+'">'+value.title+'</h4>'+
              msg
            +'</div>'+
          '</div>'+
          btn
        +'</li>');
  } 

 function loadRateToUser(scrollWith, filters = null, sort = null) 
 {   
  //console.log(filters);
  // console.log(scrollWith);
  
  //var jsonString = filters.serializeArray();
    //console.log(jsonString);
  $.ajax({
   url:SITE_URL+'helpers/functions.php?type=aW5maW5pdGVfc2Nyb2xs',  
   method:"POST",
   data:{'scroll_with':scrollWith, 'filter':filters, 'sort_by':sort},

   cache:false,
   success:function(data)
   {
    
    var obj = $.parseJSON(data);
   // console.log(obj.data);
    if(obj.data.length > 0){ 
        if(obj.content == 'rate-freelancer') {
          $.each(obj.data, function( index, value ) {
            rateToFreelancer(value);  
          });   
        }  
        starRating('#rate_to_user .star-rating');
        var r_title;
        var r_name;
        var r_postId;
        $('.popup-with-zoom-anim').magnificPopup({

            type: 'ajax',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeOnBgClick:false,
            closeBtnInside: true,
            preloader: true,

            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in',
            ajax: {
              settings: null, // Ajax settings object that will extend default one - http://api.jquery.com/jQuery.ajax/#jQuery-ajax-settings
              // For example:
              // settings: {cache:false, async:false}

              cursor: 'mfp-ajax-cur', // CSS class that will be added to body during the loading (adds "progress" cursor)
              tError: '<a href="%url%">The content</a> could not be loaded.' //  Error message, can contain %curr% and %total% tags if gallery is enabled
            },

            callbacks: {
              parseAjax: function(mfpResponse) {
                  var magnificPopup = $.magnificPopup.instance;                  
                  var parentId = magnificPopup.currItem.el[0].parentElement.id;
                  r_postId = magnificPopup.currItem.el[0].getAttribute('data-id');
                  r_name = $('#'+parentId+' .post_title').data('name');
                  r_title = $('#'+parentId+' .post_title').text();
                 
                  // mfpResponse.data is a "data" object from ajax "success" callback
                  // for simple HTML file, it will be just String
                  // You may modify it to change contents of the popup
                  // For example, to show just #some-element:
                   //mfpResponse.data = $(mfpResponse.data).find('#some-element');
                   //console.log(mfpResponse.data);

                  // mfpResponse.data must be a String or a DOM (jQuery) element

                  //console.log('Ajax content loaded:', mfpResponse);
                },
                ajaxContentAdded: function() {
                  // Ajax content is loaded and appended to DOM
                
                  var openPopup = this.content;
                  openPopup.find('#review-popup-header')[0].firstElementChild.innerText = r_name;
                  openPopup.find('#review-popup-header')[0].lastElementChild.innerText = r_title;
                  $('#r_postId').val(r_postId);
                }
            }
          
        });
      }  
      if(obj.data == '' || obj.data == null) {
        $('#load_rate_to_freelancer').html("<button type='button' class='btn head-btn1'>No Data Found</button>");
      } else{
        //console.log(obj.count);
         if(obj.count == 8){
            $('#load_rate_to_freelancer').html("<button type='button' class='button dark ripple-effect load_more mb-20'><i class='icon-feather-rotate-ccw'></i> Load More</button>"); 
            $('#load_rate_to_freelancer .load_more').click(function(){
                rate_to_user['start'] = rate_to_user['start'] + rate_to_user['limit'];  
                loadRateToUser(rate_to_user);  
            }); 
         } 
      }
   }
  });
 }

loadRateToUser(rate_to_user);





