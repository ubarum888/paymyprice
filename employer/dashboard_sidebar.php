<div class="dashboard-sidebar-inner" data-simplebar>
			<div class="dashboard-nav-container">

				<!-- Responsive Navigation Trigger -->
				<a href="#" class="dashboard-responsive-nav-trigger">
					<span class="hamburger hamburger--collapse" >
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</span>
					<span class="trigger-title">Dashboard Navigation</span>
				</a>
				
				<!-- Navigation -->
				<div class="dashboard-nav">
					<div class="dashboard-nav-inner">

						<ul data-submenu-title="Start">
							<li class="active"><a href="<?= CLIENT_URL?>"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>
							<li><a href="<?= CLIENT_URL?>messages.php"><i class="icon-material-outline-question-answer"></i> Messages </a></li>
							 
							<li><a href="<?= CLIENT_URL?>reviews.php"><i class="icon-material-outline-rate-review"></i> Reviews</a></li>
						</ul>
						
						<ul data-submenu-title="Organize and Manage">
							<li><a href="#"><i class="icon-material-outline-business-center"></i> Posts</a>
								<ul>
									<li><a href="<?= CLIENT_URL?>manage-posts.php">Manage Posts </a></li> 
									<li><a href="<?= CLIENT_URL?>post-job.php">Post a Job</a></li>
								</ul>	
							</li>
							<!--<li><a href="#"><i class="icon-material-outline-assignment"></i> Tasks</a>
								<ul>
								 
									<li><a href="employer-manage-bidders.php">Manage Bidders</a></li>
									<li><a href="employer-my-active-bids.php">My Active Bids <span class="nav-tag">4</span></a></li>
									 
								</ul>	
							</li>-->
						</ul>

						<ul data-submenu-title="Account">
							<li><a href="<?= CLIENT_URL?>settings.php"><i class="icon-material-outline-settings"></i> Settings</a></li>
							<li><a href="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('logout')?>"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
						</ul>
						
					</div>
				</div>
				<!-- Navigation / End -->

			</div>
		</div>