<?php include('../helpers/classes/admin.php');  
require_once('restricted.php');
$objAdmin = new ADMIN();
?>  
<!doctype html>
<html lang="en">
<head>
	<?php include_once('../elements/head.php');?>
</head>

<body class="gray">
<!-- Wrapper -->

<div id="wrapper">

<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth dashboard-header not-sticky">
        <?php include_once('dashboard_header.php');?>
</header>

<div class="clearfix"></div>
<!-- Header Container / End -->

<!-- Dashboard Container -->

<div class="dashboard-container">

	<!-- Dashboard Sidebar

	================================================== -->

	<div class="dashboard-sidebar">

		<?php include_once('dashboard_sidebar.php');?>

	</div>





	<!-- Dashboard Content

	================================================== -->

	<div class="dashboard-content-container" data-simplebar>

		<div class="dashboard-content-inner" >

			

			<!-- Dashboard Headline -->

			<div class="dashboard-headline">
				<h3>Add Main Categories</h3>	
			</div>

	

			<!-- Row -->

			<div class="row">

				<form id="add_main_categories" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('add_main_categories')?>" data-name="<?= base64_encode('add_main_categories')?>" method="POST"  enctype="multipart/form-data" onsubmit="return validateForm(this.id, event)">

				<!-- Dashboard Box -->			
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">						<!-- Headline -->

						<div class="headline">
							<h3><i class="icon-feather-folder-plus"></i> Main Category Submission Form</h3>
						</div>

						<div class="content with-padding padding-bottom-10">
							<div class="row">
								<div class="col-xl-12 pad_all_12">
									<div class="submit-field">
										<h5>Category Title</h5>
										<input type="text" name="title" maxlength="40" title="Enter Category Title" class="with-border">
									</div>
								</div>

								<div class="col-xl-12">
									<div class="submit-field">
										<h5>Describe Your Category</h5>
										<textarea cols="30" rows="5" name="description" class="with-border" title="Describe Your Category"></textarea>
										<div class="uploadButton margin-top-30">
											<input class="uploadButton-input" type="file" title="Select Banner Image" name="image" accept="image/*" id="upload" />
											<label class="uploadButton-button ripple-effect" for="upload">Upload Banner</label>
											<span class="uploadButton-file-name">Banner image that might be helpful in describing your category</span>
										</div>
										<div class="uploadButton margin-top-30">
											<input class="uploadButton-input1" type="file" title="Select Icon Image" name="icon" accept="image/*" id="upload1" />
											<label class="uploadButton-button ripple-effect" for="upload1">Upload Icon</label>
											<span class="uploadButton-file-name1">Icon image that might be helpful in describing your category</span>
										</div>
									</div>
								</div>						

								<div class="col-xl-12">  
									<div class="submit-field">
										<h5>Subcategories <i class="help-icon" data-tippy-placement="right" title="Add Subcategories For a Category"></i></h5>
										<div class="keywords-container">
											<div class="keyword-input-container tag-wrapper">
												<input type="text" class="keyword-input with-border subcat-list" maxlength="30" placeholder="Sub Category" />
												<button type="button" class="keyword-input-button ripple-effect add_button"><i class="icon-material-outline-add"></i></button>
											</div>
											<div class="keywords-list"><!-- keywords go here --></div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-12">
					  <button type="submit" name="submit" class="button ripple-effect big margin-top-30 has-spinner"><i class="icon-feather-plus"></i> Save <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>
				</div>
			<form>
			</div>

			<!-- Row / End -->



			<!-- Footer -->

			<div class="dashboard-footer-spacer"></div>
			<div class="small-footer margin-top-15">
				 <?php include_once('dashboard_footer.php');?>
			</div>			 

			<!-- Footer / End -->
		</div>
	</div>

	<!-- Dashboard Content / End -->

</div>
<!-- Dashboard Container / End -->
</div>

<!-- Wrapper / End -->


<!-- Scripts

================================================== -->

<?php include_once('../elements/foot-script.php');?>
<!-- Google Autocomplete -->

</body>
</html>