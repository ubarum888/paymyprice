<?php include('../helpers/classes/users.php');  
require_once('restricted.php');
$objUser= new USER(); ?>  
<!doctype html>

<html lang="en">

<head>

	<?php include_once('../elements/head.php');?>

</head>

<body class="gray">



<!-- Wrapper -->

<div id="wrapper">



<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth dashboard-header not-sticky">

      <?php include_once('dashboard_header.php');?>

</header>
<input type="hidden" id="user_id" value="<?= base64_encode($_SESSION['user']['result']['id'])?>"/>
<div class="clearfix"></div>

<!-- Header Container / End -->





<!-- Dashboard Container -->

<div class="dashboard-container">



	<!-- Dashboard Sidebar

	================================================== -->

	<div class="dashboard-sidebar">

		<?php include_once('dashboard_sidebar.php');?>

	</div>

	<!-- Dashboard Sidebar / End -->





	<!-- Dashboard Content

	================================================== -->

	<div class="dashboard-content-container" data-simplebar>

		<div class="dashboard-content-inner" >

			

			<!-- Dashboard Headline -->

			<div class="dashboard-headline">
				<h3>Manage Posts</h3>
			</div>

			<!-- Row -->

			<div class="row">

				<!-- Dashboard Box -->

				<div class="col-xl-12">

					<div class="dashboard-box margin-top-0">
						<!-- Headline -->

						<div class="headline">
							<h3><i class="icon-material-outline-business-center"></i> My Job Listings</h3>
						</div>

						<div class="content">
							<ul id="load_data" data-name="my-posts" class="dashboard-box-list">
							</ul>
							 <div id="load_data_message" class="text-center"></div>  
						</div>
					</div>
				</div>
			</div>
			<!-- Row / End -->

			<!-- Footer -->

			<div class="dashboard-footer-spacer"></div>
			<div class="small-footer margin-top-15">
				 <?php include_once('dashboard_footer.php');?>

			</div>
			<!-- Footer / End -->
		</div>
	</div>
	<!-- Dashboard Content / End -->
</div>

<!-- Dashboard Container / End -->
</div>
<!-- Wrapper / End -->

<!-- Scripts

================================================== -->

<?php include_once('../elements/foot-script.php');?>
<script src="<?= SITE_URL?>scroll-infinite.js"></script>
</body>
</html>