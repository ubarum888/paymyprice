<?php include('../helpers/classes/users.php');  
require_once('restricted.php');
$objUser= new USER(); 
//$categories = $objUser->getSelectedCategories($_SESSION['user']['result']['id']); 
//$feedSet = $objUser->getFeedSetByEmployerId($_SESSION['user']['result']['id']);   

$categories = $objUser->getSelectedCategories($_SESSION['user']['result']['id']); 


//echo "<pre>";print_r($feedSet);die;  
?>     
<!doctype html>

<html lang="en">

<head>

	<?php include_once('../elements/head.php');?>
</head>

<body class="gray">



<!-- Wrapper -->

<div id="wrapper">

<!-- Header Container

================================================== -->
  
<header id="header-container" class="fullwidth">
  <?php include_once('dashboard_header.php');?>
</header>

<div class="clearfix"></div>

<!-- Header Container / End -->

<!-- Spacer -->

<div class="margin-top-90"></div>
<!-- Spacer / End-->


<!-- Page Content
================================================== -->

<div class="container">
	<div class="row">
			<!-- <p id="show_query"></p>  -->
		<div class="col-xl-3 col-lg-4">
			<div class="sidebar-container">	
			

				<!-- Category -->

				<div class="sidebar-widget" id="e_category_id">
					<h3>Category</h3>
					<select class="selectpicker default" multiple data-selected-text-format="count" data-size="7" title="Your Selected Categories">
						<?php 
						foreach($categories['result'] as $key=>$cat){
							$your_categories[] = $cat['id'];?>	
							<option value="<?= $cat['id']?>"><?= $cat['category']?></option>
						<?php } ?>
					</select>
				</div>
				<input type="hidden" id="categoryId_list" value="<?= implode(',',$your_categories)?>"/>    
				<!-- Keywords -->

				<div class="sidebar-widget" id="e_set_tags">
					<h3>Keywords</h3>
					<div class="keywords-container">
						<div class="keyword-input-container">
							<input type="text" class="keyword-input" placeholder="e.g. task title"/>
							<button class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button>
						</div>
						<div class="keywords-list"><!-- keywords go here --></div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- Hourly Rate -->

				<div class="sidebar-widget" id="set_hourly_rate">
					<h3>Hourly Rate</h3>
					<div class="margin-top-55"></div>
					<!-- Range Slider -->
					<input class="range-slider" type="text" value="" data-slider-currency="$" data-slider-min="10" data-slider-max="250" data-slider-step="5" data-slider-value="[10,250]"/>
				</div>			
				<div class="clearfix"></div>
			</div>
		</div>

		<div class="col-xl-9 col-lg-8 content-left-offset">

			<h3 class="page-title">Search Results</h3>

			<div class="notify-box margin-top-15">
				<div class="switch-container">
					<label class="switch"><input type="checkbox"><span class="switch-button"></span><span class="switch-text">Turn on email alerts for this search</span></label>
				</div>


				<div class="sort-by">
					<span>Sort by:</span>
					<select class="selectpicker hide-tick" id="sort_by" onchange="e_sortBy()">
						<option value="">Relevance</option>
						<option value="DESC">Newest</option>
						<option value="ASC">Oldest</option>
						<option value="rand">Random</option>  
					</select>
				</div>
			</div>
			

			<!-- Freelancers List Container -->

			<div class="freelancers-container freelancers-grid-layout margin-top-35" id="load_data" data-name="freelancer-listing"></div>
			<div id="load_data_message" class="text-center"></div> 

			<div class="clearfix"></div>
			<!-- Pagination / End -->

		</div>
	</div>
</div>





<!-- Footer

================================================== -->

<div id="footer">
	 <?php include_once('../elements/footer.php');?>
</div>

<!-- Footer / End -->



</div>

<!-- Wrapper / End -->



<!-- Scripts

================================================== -->
<?php include_once('../elements/foot-script.php');?>
<script src="<?= SITE_URL?>scroll-infinite.js"></script>


<!-- Google Autocomplete -->
<script>
	$('#set_hourly_rate .range-slider').slider().on('slideStop', function(ev){  

    	var scrollInfinite = {};
	      scrollInfinite['limit'] = 8;
	      scrollInfinite['start'] = 0;
	      scrollInfinite['scroll'] = $("#load_data").data('name');
	      scrollInfinite['user_id'] = $('#user_id').val();

	      var set_filter = checkActivateFilterForEmployer();  
	      var sort = $('#sort_by').val();           
	         
	      $('#load_data').html('');      
	      load_infinite_data(scrollInfinite,set_filter, sort);     
	});
</script>
</body>
</html>