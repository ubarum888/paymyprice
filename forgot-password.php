<?php include('helpers/classes/config.php');   ?>
<!doctype html>

<html lang="en">

<head>

<!--  Essential META Tags -->
<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1">
<link rel="canonical" href="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">

<meta property="og:locale" content="en_US">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
<meta property="og:site_name" content="PayMyPrice">

<title>Forgot Password - PayMyPrice</title>
<meta property="og:title" content="Forgot Password - PayMyPrice">
<meta name="twitter:title" content="Forgot Password - PayMyPrice">

<meta name="description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">
<meta property="og:description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">
<meta name="twitter:description" content="Save Money. Get Connected. Thousands of homeowners and small businesses use PayMyPrice to turn their ideas into reality. Connect with experts today.">

<meta property="og:image" content="https://paymyprice.com/assets/images/logo.svg">
<meta property="og:image:secure_url" content="https://paymyprice.com/assets/images/logo.svg">
<meta name="twitter:image" content="https://paymyprice.com/assets/images/logo.svg">
<meta name="twitter:card" content="summary_large_image">
<meta property="og:image:width" content="400">
<meta property="og:image:height" content="50">

<?php include_once('elements/head.php');?>



</head>

<body>



<!-- Wrapper -->

<div id="wrapper">



<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth">

 <?php include_once('elements/header.php');?>

</header>

<div class="clearfix"></div>

<!-- Header Container / End -->



<!-- Titlebar

================================================== -->

 





<!-- Page Content

================================================== -->

<div class="container padt_70">

	<div class="row">

		<div class="col-xl-5  center">





			<div class="login-register-page">

				<!-- Welcome Text -->

				<div class="welcome-text">

					<h3>Recover your password?</h3>

					<span>Don't have an account? <a href="#sign-in-dialog" class="popup-with-zoom-anim-inline log-in-button">Sign Up!</a></span>

				</div>

					

				<!-- Form -->

				<form id="forgot_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('forgot_form')?>" data-name="<?= base64_encode('forgot_form')?>" method="POST" onsubmit="return validateForm(this.id, event)">

					<div class="input-with-icon-left">
						<i class="icon-material-baseline-mail-outline"></i>
						<input type="text" class="input-text with-border" name="email" id="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'" maxlength="50" placeholder="Email Address" title="Enter Email Address"/>

					</div>
					
					<button type="submit" name="submit" class="button full-width button-sliding-icon ripple-effect margin-top-10 has-spinner" >Recover Password <i class="icon-material-outline-arrow-right-alt"></i> <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>  
				</form>

				

				<!-- Button -->

				

				

				<!-- Social Login 

				<div class="social-login-separator"><span>or</span></div>

				<div class="social-login-buttons">

					<button class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> Log In via Facebook</button>

					<button class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i> Log In via Google+</button>

				</div>-->

			</div>



		</div>

	</div>

</div>





<!-- Spacer -->

<div class="margin-top-70"></div>

<!-- Spacer / End-->



<!-- Footer

================================================== -->

<div id="footer">

 <?php include_once('elements/footer.php');?>

</div>

<!-- Footer / End -->

</div>

<!-- Wrapper / End -->



</body>

</html>