<?php @ob_start();session_start();
$NAVBAR_VIEW = "show";  
function siteurl(){
    if(isset($_SERVER['HTTPS'])){
       $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
    $protocol . "://" . $_SERVER['HTTP_HOST'];
}
/*$configtimezone = (isset($_SESSION['timezone'])) ? $_SESSION['timezone'] : "America/New_York";
date_default_timezone_set($configtimezone);*/
date_default_timezone_set("America/New_York");
define('SITE_NAME', 'Qasrep');
define('SITE_URL_NAME', '');
define('CURRENT_DATE', date('Y-m-d H:i:s'));
define('DATE', date('Y-m-d'));
define('TIME', time());
define('IP_ADDRESS', $_SERVER['REMOTE_ADDR']);
define('BROWSER', $_SERVER['HTTP_USER_AGENT']);
define('BASE_URL', siteurl());
define('BASE_FOLDER',''); //leave empty if you using root folder
define('BASE_DIR',$_SERVER['DOCUMENT_ROOT'].BASE_FOLDER);
define('GOOGLE_API_KEY','AIzaSyBUSrDQ58L1BJa7kt3o1YU-hBwkxubmHhI');
define('SITE_URL','http://localhost/paymyprice/');
define('CLIENT_URL','http://localhost/paymyprice/employer/'); 
define('VENDOR_URL','http://localhost/paymyprice/freelancer/');

$data['DateFormat'] = 'm/d/Y h:i:s A';
$data['OnlyDate']   = 'm/d/Y';
$data['NoRecord']   = '<i class="fa fa-exclamation-triangle"></i> No record found';
$data['SuccessIcon']   = '<i class="fa fa-check"></i>';
$data['ErrorIcon']   = '<i class="fa fa-exclamation-triangle"></i>';
$data['ExclamationIcon'] = '<i class="fa fa-exclamation-circle"></i>';

class dbconfig {
  // database hostname
  protected static $host = "localhost";
  // database username
  protected static $username = "root";
  // database password
  protected static $password = "";
  //database name
  protected static $dbname = "paymyprice";

  static $con;

  function __construct() {
    self::$con = self::connect();
  }

  // open connection
  protected static function connect() {
     try {
       $link = mysqli_connect(self::$host, self::$username, self::$password, self::$dbname);
        if(!$link) {
          throw new exception(mysqli_error($link));
        }
        return $link;
     } catch (Exception $e) {
       echo "Error: ".$e->getMessage();
     }
  }

 // close connection
  public static function close() {
     mysqli_close(self::$con);
  }

// run query
  public static function run($query) {
    try {
      if(empty($query) && !isset($query)) {
        throw new exception("Query string is not set.");
      }

      $result = mysqli_query(self::$con, $query);
     
      //$insert_id = mysqli_insert_id(self::$con);

      //self::close();
     return $result;
    } catch (Exception $e) {
      echo "Error: ".$e->getMessage();
    }

  }
// insert_run
public static function insertrun($query) {
    try {
      if(empty($query) && !isset($query)) {
        throw new exception("Query string is not set.");
      }

      $result = mysqli_query(self::$con, $query);
      $insert_id = mysqli_insert_id(self::$con);
      
     return $insert_id;
    } catch (Exception $e) {
      echo "Error: ".$e->getMessage();
    }

  }
}

$config = new DBCONFIG();

function FD_add_notices($message, $notice_type="error", $key=""){
  
  $notices = array(); 
  $notices = get_session( 'ef_notices' ); 
  if(!empty($key)){
    $notices[$notice_type][$key] = $message;
  }
  else{
    $notices[$notice_type][] = $message;  
  }
  set_session('FD_notices',  $notices);
  
}

function FD_print_notices(){
    global $data;
  
  $all_notices  = array();
  $all_notices  = get_session( 'FD_notices' );
  
  if(empty($all_notices)) return;
  
  foreach ($all_notices as $key=>$notice_type ) {   
    if($key=='error' && count($notice_type) > 0) {     
      echo '<div class="alert alert-danger wow fadeInDown delay-03s" id="success-alert" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        foreach ( $notice_type as $message ) {
          echo $data['ErrorIcon'].' '.$message;
        }
        echo '</div>';
    }
    if($key=='success'  && count($notice_type) > 0) {
         echo '<div class="alert alert-success wow fadeInDown delay-03s" id="success-alert" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        foreach ( $notice_type as $message ) {
          echo $data['SuccessIcon'].' '.$message;
        }
        echo '</div>';
    }
    if($key=='warning'  && count($notice_type) > 0) {
      echo '<div class="alert alert-warning wow fadeInDown delay-03s" id="success-alert" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
      foreach ( $notice_type as $message ) {
        echo $data['ExclamationIcon'].' '.$message;
      }
      echo '</div>';
    }
    
  }   
  FD_clear_notices(); 
}
function get_session($key){
  if(isset($_SESSION[$key])){
    return $_SESSION[$key];   
    } 
}
function FD_clear_notices(){
  clear_session('FD_notices');  
}
function clear_session($key){
    unset($_SESSION[$key]); 
}
function set_session($key, $val){
    $_SESSION[$key] = $val;
}
function pr($array) {
  echo "<pre>";
  print_r($array);
  echo "</pre>";
}
function prndate($date){
  global $data;
  if(empty($date)) return '';
  if($date=='0000-00-00 00:00:00'  || $date=='0000-00-00') return '';
  return date($data['DateFormat'], strtotime($date));
}
function onlydate($date){
  global $data;
  if(empty($date)) return '';
  if($date=='0000-00-00 00:00:00' || $date=='0000-00-00') return '';
  return date($data['OnlyDate'], strtotime($date));
}
function namedate($date){
  if(empty($date)) return '';
  if($date=='0000-00-00 00:00:00' || $date=='0000-00-00') return '';
  return date('F j, Y',strtotime($date));
  
}
function random_gen($length)
{
  $random= "";
  srand((double)microtime()*1000000);
  $char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  $char_list .= "abcdefghijklmnopqrstuvwxyz";
  $char_list .= "1234567890";
    $char_list .= "*_()#";  
  // Add the special characters to $char_list if needed
  for($i = 0; $i < $length; $i++)
  {
    $random .= substr($char_list,(rand()%(strlen($char_list))), 1);  
  }
  return $random;
}

function condbformat($date){
    if(empty($date)) return '';
  $date = str_replace(' AM','', $date);
  $date = str_replace(' PM','', $date);
    $mysql_date = date('Y-m-d H:i:s', strtotime($date));
  return $mysql_date;
  }
function contimedbformat($date){
    if(empty($date)) return '';
    $mysql_date = date('Y-m-d H:i:s', strtotime($date));
  return $mysql_date;
  }
function condatedbformat($date){
    if(empty($date)) return '';
    $mysql_date = date('Y-m-d', strtotime($date));
  return $mysql_date;
  }

?>