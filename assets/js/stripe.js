// Create a Stripe client.
var stripe = Stripe('pk_live_51H3QUdL3tvxBEokV1oVPBQglpCXFdDCkdDKr6bO4eQuPOTU6z4U2PdsFLZym5AbuARRY4K6KCmNKpz0GnEH7fR7e00Tsa2jwqN');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.on('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();
  $('#card-button').prop('disabled', true);
   $('#card-button').addClass('active');
  stripe.createToken(card,  { currency: 'usd' }).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
      $('#card-button').prop('disabled', false);
    } else {
      // Send the token to your server.
          $.ajax({
            url:SITE_URL+'helpers/functions.php?type=YWRkX2NhcmRfZm9ybQ==',
            type:"POST",
            data:{'token': result.token}, 
            success:function(response) {
              var obj = $.parseJSON(response);
              if(obj.msg == 'success'){
                location.reload();
                /*$('#addCard').hide();
               // $('#card_list table tbody').html('');

                $('#card_list table tbody').prepend('<tr>'+
                      '<td>'+  
                          '<div class="radio">'+
                            '<input id="card-radio-'+obj.result.encryptId+'" name="card_active" type="radio" value="'+obj.result.encryptId+'">'+
                            '<label for="card-radio-'+obj.result.encryptId+'">'+
                              '<span class="radio-label"></span>'+
                            '</label>'+
                          '</div>'+
                        '</td>'+
                        '<td><img src="'+SITE_URL+'uploads/brand/'+obj.result.brand+'.png" width="50"></td>'+
                      '<td>'+obj.result.account_no+'</td>'+
                      '<td>'+obj.result.expire_card+'</td>'+
                      '</tr>');
                $('#card_list').removeClass('hide');*/
              }   
               $('#card-button').prop('disabled', false);
                $('#card-button').removeClass('active');
               alertNotificationMsg(obj.notice);
            }
        }); 
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
 // form.submit();
}