/*
 * Pusher chat
 * facebook like chat jQuery plugin using Pusher API 
 * version: 1.0   
 * Author & support : zied.hosni.mail@gmail.com 
 * © 2012 html5-ninja.com
 * for more info please visit http://html5-ninja.com
 *
 *
 *          $.fn.pusherChat({
                'pusherKey': // required : open an account on http://pusher.com/ to get one
                'authPath':'server/pusher_auth.php', // required : path to authentication scripts more info at http://pusher.com/docs/authenticating_users
            'friendsList' : null, // required : path to friends list json 
            // json ex : 
            /*
                {
                    "133": ["Elvis","assets/elvis.jpg","http://html5-ninja.com"],
                    "244": ["Kurt Cobain","assets/cobain.jpg","http://html5-ninja.com"],
                }               
            'serverPath' : null, // required : path to server
            'profilePage' : false, // link to friend profile page setup fom json  ex : ["Kurt Cobain","assets/cobain.jpg","path/to/profile"]
            'debug' : false // enable the pusher debug mode  - don't use this in production
            });

*/
(function( $ ){

    $.fn.pusherChat = function(options ) {
        //options
        var settings = $.extend( {
            'pusherKey': null,   // required : open an account on http://pusher.com/ to get one
            'authPath' : null , // required : path to authentication scripts more info at http://pusher.com/docs/authenticating_users
            'friendsList' : null, // required : path to friends list json 
            // json ex : 
            /*
                {
                    "133": ["Elvis","assets/elvis.jpg","http://html5-ninja.com"],
                    "244": ["Kurt Cobain","assets/cobain.jpg","http://html5-ninja.com"],
                }               
         */
            'serverPath' : null, // required : path to server
            'profilePage' : false, // link to friend profile page setup fom json  ex : ["Kurt Cobain","assets/cobain.jpg","path/to/profile"]
            'onFriendConnect' : undefined, // event : trigger whene friend connect & return his ID
            'onFriendLogOut' : undefined, // event : trigger whene friend log out & return his ID
            'onSubscription' : undefined, // return  members object
            'debug' : false // enable the pusher debug mode  - don't use this in production
        }, options);
                
        if (settings.debug){
            Pusher.log = function(message) {
                if (window.console && window.console.log) window.console.log(message);
            };
            WEB_SOCKET_DEBUG = true;
        }        
        
        // int var 
        var pageTitle = $('title').html(); // just to update page title whene message is triggered
        
        // Authenticating users
        Pusher.channel_auth_endpoint = settings.authPath;
        // create pusher object
        var pusher = new Pusher(settings.pusherKey);
        // Accessing channels
        var presenceChannel = pusher.subscribe('presence-mychanel');
  
        // subscription succeeded
        presenceChannel.bind('pusher:subscription_succeeded', function(){ 
            memberUpdate(); 
        })
        // trigger friend connection
        presenceChannel.bind('pusher:member_added', function() {
            memberUpdate();
        });
        // trigger friend logout
        presenceChannel.bind('pusher:member_removed', function() {
            memberUpdate();
        });
    
    
        if (settings.onSubscription !== undefined) {
            presenceChannel.bind('pusher:subscription_succeeded', function(members){ 
                settings.onSubscription(members);
            })
        }
        
        if (settings.onFriendConnect !== undefined) {
            presenceChannel.bind('pusher:member_added', function(member) {                
                settings.onFriendConnect(member);
            });
        }
        
        if (settings.onFriendLogOut !== undefined) {
            presenceChannel.bind('pusher:member_removed', function(member) {                
                settings.onFriendLogOut(member);
            });
        }
        /*-----------------------------------------------------------*
         * Bind the 'send-event' & update the chat box message log
         *-----------------------------------------------------------*/ 
        presenceChannel.bind('send-event', function(data) {       
           console.log(data);
              
            if(presenceChannel.members.me.id == data.to && data.from != presenceChannel.members.me.id){
              
                var to_img = $('#message-user-'+data.from+' .message-avatar img').attr('src');
                displayReceiverMessage(data.message, data.from, to_img);
               // scrollBottom('message-box-'+data.from);

                  // Check if user not on message page then send message notification along with a message to him.
                var pageURL = $(location).attr("pathname").split('/');               
                if(pageURL[2] !== 'messages.php'){ 
                    var prev_count = $('.messages-alert .message-unread').text();                  
                    var unread_msg = +prev_count +  1; 
                    $('.messages-alert .message-unread').removeClass('hide').text(unread_msg); 
                }                      
           
                $('#message-box-'+data.from+' .typing').addClass('hide');         
                     //$('#id_'+data.from+' .logMsg').scrollTop($('#id_'+data.from+' .logMsg')[0].scrollHeight);      
                     //if ($('title').text().search('New message - ')==-1)
                   // $('title').prepend('New message - ');     
            }
            if (presenceChannel.members.me.id == data.from){
                   var own_img = $('.user-menu .user-avatar img').attr('src');
                    displayOwnMessage(data.message, data.to, own_img);
                    // $('#id_'+data.to+' .logMsg').scrollTop($('#id_'+data.to+' .logMsg')[0].scrollHeight);
            }    
        });   
        

           /*-----------------------------------------------------------*
         * Bind the 'send-notification' & update the notification box log
         *-----------------------------------------------------------*/ 
        presenceChannel.bind('send-notification', function(data) {       
                        
            if(presenceChannel.members.me.id == data.to && data.from != presenceChannel.members.me.id){               
                var prev_count = $('.notify-alert .notify-unread').text();
                var unread_msg = +prev_count +  1;  
                $('.notify-alert .notify-unread').removeClass('hide').text(unread_msg);
            }               
        }); 
        /*-----------------------------------------------------------*
         * detect when a friend is typing a message
         *-----------------------------------------------------------*/         
        presenceChannel.bind('typing-event', function(data) {
            if(presenceChannel.members.me.id == data.to && data.from != presenceChannel.members.me.id && data.message=='true' ){
                $('#message-box-'+data.from+' .typing').removeClass('hide');
            }else if(presenceChannel.members.me.id == data.to && data.from != presenceChannel.members.me.id && data.message=='null' ){               
                $('#message-box-'+data.from+' .typing').addClass('hide');           
            }
        });

        
        // some action whene click on chat box
        $('.pusherChatBox').on('click',function(){
            var newMessage = false;
            $(this).removeClass('recive');
            $('.pusherChatBox').each(function(){
                if ($(this).hasClass('recive')){
                    newMessage = true;
                    return false; 
                }       
            });  
            if (newMessage == false)
                $('title').text(pageTitle);
        });


        function displayOwnMessage(msg, id, img){
            $('#message-box-'+id+' .message-content-inner .message-inner').append('<div class="message-bubble me">'+
                '<div class="message-bubble-inner">'+
                    '<div class="message-avatar"><img src="'+img+'" alt="" /></div>'+
                    '<div class="message-text"><p>'+msg+'</p></div>'+
                '</div>'+
                '<div class="clearfix"></div>'+
            '</div>');
        }

        function displayReceiverMessage(msg, id, img){
            $('#message-box-'+id+' .message-content-inner .message-inner').append('<div class="message-bubble">'+
                    '<div class="message-bubble-inner">'+
                    '<div class="message-avatar"><img src="'+img+'" alt="" /></div>'+
                    '<div class="message-text"><p>'+msg+'</p></div>'+
                '</div>'+
                '<div class="clearfix"></div>'+
            '</div>');
        } 

        function scrollBottom(divName){            
            var objDiv = document.getElementById(divName);
            objDiv.scrollTop = objDiv.scrollHeight;
        }  

        /*-----------------------------------------------------------*
         * memberUpdate() place & update friends list on client page 
         *-----------------------------------------------------------*/     
        function memberUpdate(){   
             
                var chatBoxOnline ; 
                             
                $.each(settings.friendsList, function(user_id, val) {
                   
                    if (user_id!=presenceChannel.members.me.id) {          
                        user = presenceChannel.members.get(user_id);

                        if (user ){                         
                            chatBoxOnline ='status-online';
                        }
                        else {
                            chatBoxOnline ='status-offline';
                        }              
                    }          
                    console.log(user_id) ;        
                    //$('#message-user-'+user_id+' i').removeClass('status-offline').addClass(chatBoxOnline);
                    $('.user-active-status-'+user_id).removeClass('status-offline').addClass(chatBoxOnline);
                });
                
        }
        

     
        /*-----------------------------------------------------------*
         * send message & typing event to server
         *-----------------------------------------------------------*/ 
        $(".message-reply textarea").on('keypress',function(event) {    
            var from = $(this).parents('form');  
           
                if ( event.which == 13 ) {  
                     event.preventDefault();
                    
                }else if (!$(this).val() || ($(this).next().next().next().val()=='null' && $(this).val())){ 
                    console.log(from);
                    $(this).next().next().next().val('true');
                    $.post(settings.serverPath, from.serialize());        
                }
            
        });   


        $(".message-reply #send_message").on('click',function(event) {  
           $this = $('.message-reply textarea');
            var from = $('#send_form');          
            if($this.val() !== ""){              
                $this.next().next().next().val('false');
                $.post(settings.serverPath, from.serialize());
                event.preventDefault();
                $this.val('');
                $this.focus();     
            } else{
                alert('Please eneter Message');
            }
        });    


        // trigger whene user stop typing 
        $('.message-reply textarea').on('focusout',function(){
            if($(this).next().next().next().val()=='true'){
                var from = $(this).parents('form');
                $(this).next().next().next().val('null');
                $.post(settings.serverPath, from.serialize());
            }
        });     
           
        
    };
    
    
})( jQuery );