<?php include('../helpers/classes/users.php');

//require_once('restricted.php');

$objUser= new USER();

$categories = $objUser->getAllCategories();  

?>

<!doctype html>



<html lang="en">



<head>



	<?php include_once('../elements/head.php');?>

<style type="text/css">



</style>

</head>

<body>



<!-- Wrapper -->

<div id="wrapper">



<!-- Header Container

================================================== -->

<header id="header-container" class="fullwidth">

 <?php include_once('dashboard_header.php');?>

</header>

<div class="clearfix"></div>

<!-- Header Container / End -->



 





<!-- Page Content

================================================== -->

<div class="container padt_70">

	<div class="row">

		<div class="col-xl-8 center">	

			<form id="employer_signup_form" action="<?= SITE_URL?>helpers/functions.php?type=<?= base64_encode('employer_signup_form')?>" data-name="<?= base64_encode('employer_signup_form')?>" method="POST" onsubmit="return validateForm(this.id, event)">

				<section class="wow " data-wow-duration="1.5s" id="step1">

  

						<div class="login-register-page" >

							<!-- Welcome Text -->

							<div class="welcome-text">

								<h3 style="font-size: 26px;">What service do you provide?</h3>

								<span>Select up to 10 categories.</span>

							</div>





							<div class="notification error closeable hide" id="validate_step1">

								<p>Please Select up to 10 categories</p>

								<a class="close"></a>

							</div>		

									

							<div class="row">						

								<?php foreach($categories['result'] as $key=>$cat){?>

									<div class="checkbox col-sm-4 categories-chk pad_all_12">

										<input type="checkbox" name="category[]" value="<?= $cat['id']?>" id="chekcbox<?= $key?>">

										<label for="chekcbox<?= $key?>"><span class="checkbox-icon"></span><?= $cat['category']?></label>

									</div>

								<?php } ?>											 

							</div>								

							<div class="text-center margin-top-10">

								<button type="button" class="button button-sliding-icon ripple-effect next1">Next <i class="icon-feather-arrow-right"></i></button>	

							</div>				

						</div>

					</section>



					<section class="wow slideInRight" data-wow-duration="1.5s" id="step2" style="display:none">

						<div class="login-register-page" >

						<!-- Welcome Text -->

								<div class="welcome-text">

									<h3 style="font-size: 26px;">Choose a price range</h3>							

								</div>	

							

								<div class="section-headline">

									<h5>Price</h5>  

								</div>



								<!-- Range Slider -->

								<input class="range-slider" type="text" value="" data-slider-currency="$" name="price" data-slider-min="10" data-slider-max="250" data-slider-step="5" data-slider-value="[10,250]"/>

							

								<div class="text-center margin-top-10">

									<button type="button" class="button  button-sliding-icon ripple-effect back2"><i class="icon-feather-arrow-left"></i> Prev</button>

									<button type="button" class="button button-sliding-icon ripple-effect next2">Next <i class="icon-feather-arrow-right"></i></button>	

								</div>	

						</div>

					</section>	



					<section class="wow slideInRight" data-wow-duration="1.5s" id="step3" style="display:none">  

						<div class="login-register-page" >

							<!-- Welcome Text -->

							<div class="welcome-text">

								<h3 style="font-size: 26px;">Choose a subcategory listed below ?</h3>								

							</div>





							<div class="notification error closeable hide" id="validate_step1">

								<p>Please Select sub-category</p>

								<a class="close"></a>

							</div>		

									

							<div class="category-list"></div>



							<div class="text-center margin-top-10">

								<button type="button" class="button button-sliding-icon ripple-effect back3"><i class="icon-feather-arrow-left"></i> Prev</button>

								<button type="button" class="button  button-sliding-icon ripple-effect next3">Next <i class="icon-feather-arrow-right"></i></button>		

							</div>				

						</div>

					</section>



						<section class=" wow slideInRight" data-wow-duration="1.5s" id="step4" style="display:none">

							<div class="login-register-page">

								<!-- Welcome Text -->



							<div class="welcome-text">

								<h3 style="font-size: 26px;">Let's create your account!</h3>

								<span>Already have an account? <a href="<?= SITE_URL ?>login.php">Log In!</a></span>

							</div>



							<div class="notification error closeable hide" id="validate_msg">

								<p>Please fill in all the fields required</p>

								<a class="close"></a>

							</div>	

				

								<div class="row">

									<div class="col">

										<div class="input-with-icon-left">

											<input type="text" class="input-text with-border" name="fname" maxlength="20" onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'" id="fname" placeholder="First Name" title="Please Enter First Name"/>

										</div>

									</div>



									<div class="col">

										<div class="input-with-icon-left">

											<input type="text" class="input-text with-border" name="lname" maxlength="20" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'" id="lname" placeholder="Last Name" title="Please Enter Last Name">



										</div>

									</div>



								</div>



								<div class="row">

									<div class="col">



										<div class="input-with-icon-left">

											<input type="text" class="input-text with-border" name="email" maxlength="30" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'" id="email" placeholder="Email Address" title="Please Enter Eamil Address"/>



										</div>



									</div>



									<div class="col">

										<div class="input-with-icon-left">

											<input type="text" class="input-text with-border" name="phone" id="phone" maxlength="15" onkeypress="return isNumberKey(event)" placeholder="Phone Number" title="Please Enter Phone Number"/>

										</div>

									</div>

								</div>



								<div class="row">

									<div class="col">

										<div class="input-with-icon-left">

											<input type="text" class="input-text with-border" name="address" id="autocomplete" placeholder="Address" title="Please Type Address"/>

										</div>

									</div>	

								</div>



								<div class="row">

									<div class="col">

										<div class="input-with-icon-left">

											<input type="text" class="input-text with-border" name="state" maxlength="20" id="administrative_area_level_1" placeholder="State" title="Please Enter State"/>

										</div>

									</div>



									<div class="col">

										<div class="input-with-icon-left">

											<input type="text" class="input-text with-border" name="country" maxlength="20" id="country" placeholder="Country" title="Please Enter Country"/>

										</div>

									</div>



								</div>



								<div class="row">



									<div class="col-sm-6">

										<div class="input-with-icon-left">

											<input type="text" class="input-text with-border" name="zip" maxlength="10" onkeypress="return isNumberKey(event)" id="postal_code" placeholder="Zip" title="Please Enter Zip"/>

										</div>

									</div>

								</div>			

								

			   

								<div class="row">

									<div class="col mt-20">

										<div class="input-with-icon-left">

											<div class="checkbox">

												<input type="checkbox" name="checkbox_agree" id="chekcbox-agree">

												<label class="chkbox-agree" for="chekcbox-agree"><span class="checkbox-icon"></span>  Yes, I understand and agree to the <a href="javascript:void(0)">paymyprice Terms of Service</a>, including the <a href="javascript:void(0)">User Agreement</a> and <a href="javascript:void(0)">Privacy Policy.</a></label>

											</div> 

										</div>

									</div>	

								</div>

							  

									<!-- Button -->
  
								 
								<div class="text-center margin-top-10">

								<button type="button" class="gray button button-sliding-icon ripple-effect back4"><i class="icon-feather-arrow-left"></i> Back</button>

								<button type="submit" class="button  button-sliding-icon ripple-effect  has-spinner" name="submit">Register <i class="icon-material-outline-arrow-right-alt"></i> <span class="spinner"><i class="fa fa fa-spinner fa-spin"></i></span></button>	

							</div>

						</div>

				</section>	

			</form>

		</div>

	</div>

</div>





<!-- Spacer -->

<div class="margin-top-70"></div>

<!-- Spacer / End-->



<!-- Footer

================================================== -->

<div id="footer">

 <?php include_once('../elements/footer.php');?>	

</div>

<!-- Footer / End -->



</div>

<!-- Wrapper / End -->



<!-- Scripts

================================================== -->



<?php include_once('../elements/foot-script.php');?>  

<?php include_once('../elements/google-provider.php'); ?>





<script type="text/javascript">



		$(document).ready(function(){





			 /******************* SignUp sliding infoo ********************************/

					

		wow = new WOW(

			{

			boxClass:     'wow', 	

			animateClass: 'animated',

			offset:0				

			}

		);

		wow.init();

		

		$('.next1').click(function(){

			$('#validate_step1').hide();

			if($('.categories-chk input:checkbox:checked').length > 0 && $('.categories-chk input:checkbox:checked').length < 11){

				$("section").hide();

				$(window).scrollTop(0);

				$("#step2").css({"animation-name": "slideInRight" });

				$("#step2").show();

				var category = [];

				$(".categories-chk input:checkbox:checked").each(function(){

				    category.push($(this).val());

				});

				console.log(category);

				$.ajax({

                      url: '../helpers/functions.php?type=c3ViY2F0ZWdvcnlfbGlzdA==',  

                      type:"POST",  

                       data:{'category':category},   

                       success: function(response) {

                        var obj = $.parseJSON(response); 

                        	if(obj.msg == 'success'){   

                        	console.log(obj.result);    
                        	var i = 1;
                        	var j = 1                    

                           	$.each(obj.result, function( category, subcategory ) {



                           		$('.category-list').append('<div class="row padt_30 subcategory-list'+i+'">'+

												'<div class="col-sm-12 ">'+

													'<h3 class="green">'+category+'</h3>'+

												'</div></div>');

							 	$.each(subcategory, function(index, value) {

									  $('.subcategory-list'+i).append('<div class="checkbox pad_all_12">'+
											'<input type="checkbox" name="subcategory[]" id="chkbox'+j+'" value="'+value.id+'">'+
											'<label for="chkbox'+j+'"><span class="checkbox-icon"></span>'+value.subcategory+'</label>'+
										'</div>');
									  j++;
							  	});	
							 	i++;
							});						

                           }else{

                           	 console.log('sub categories not listed..');

                           }

                       }                      

                });

			}else{

				$('#validate_step1').fadeTo(100,1);

			}   

		});

		$('.next2').click(function(){			

			//var price = $('.range-slider').val();

			//console.log(price);			

				$(window).scrollTop(0);		

				$("section").hide();

				$("#step3").css({"animation-name": "slideInRight" });

				$("#step3").show();				

			

		});

		$('.next3').click(function(){

			$("section").hide();

			$(window).scrollTop(0);

				$("#step4").css({"animation-name": "slideInRight" });

				$("#step4").show();		

		});



			$('.back2').click(function(e){ 							

				e.preventDefault();				 

					$("section").hide();	

					$(window).scrollTop(0);		

				$("#step1").css({"animation-name": "slideInLeft" });				

				$("#step1").show();

				//new WOW().init(); 

			});



			$('.back3').click(function(e){ 							

				e.preventDefault();				 

					$("section").hide();	

					$(window).scrollTop(0);		

					$("#step2").css({"animation-name": "slideInLeft" });				

					$("#step2").show();

				//new WOW().init(); 

			});

			$('.back4').click(function(e){ 							

				e.preventDefault();				 

					$("section").hide();	

					$(window).scrollTop(0);		

					$("#step3").css({"animation-name": "slideInLeft" });				

					$("#step3").show();

				//new WOW().init(); 

			});

		

		

	});



</script>

</body>

</html>